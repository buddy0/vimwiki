# PERL
*[website](https://www.perl.org/)*  
*[tutorialspoint](https://www.tutorialspoint.com/perl/index.htm)*  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INTRO](#intro)  
    a. [SYNTAX](#syntax)  
    b. [FIRST PERL PROGRAM](#first_perl_program)
2. [DATA TYPES](#data_types)
3. [VARIABLES](#variables)
4. [ARRAYS](#arrays)
5. [REFERENCES](#references)
6. [SUBROUTINES](#subroutines)
</details>

## INTRO <a name="intro"></a>
Perl is a general-purpose, stable, cross platform programming language.  
Is an interpreted language meaning that code can be run as is.  
***compilation process:*** 1. compiled into byte code 2. as the program runs its converted into machine instructions.  
***running script from command line:*** Invoke the interpreter before the script. Ex: `C:>perl script.pl`.  
***file extension:*** .pl or .PL (file names cannot contain spaces).  

### SYNTAX <a name="syntax"></a>
* case sensitive
* free-form language (does not care about whitespaces / format & indent however you like)
* every statement must end w/ semicolon
* ***single line comment:*** `# single line comment`  
* ***multi-line comment:*** `=begin words =cut`
* ***quotes in literal strings:*** Single or double quotes. However, only double quotes interpolate variables & special characters.
* ***perl identifier:*** Name for a variable, function, class, module, or other object.  
* ***perl variable:*** Starts with with $, @, or %.

### FIRST PERL PROGRAM <a name="first_perl_program"></a>
1. Interactive Mode Programming  
```
$ perl -e 'print "Hello World\n"'
```
2. Script Mode Programming
```perl
# statement below is perl interpreter binary
#!/usr/bin/perl

print "Hello World\n";`
```

## DATA TYPES <a name="data_types"></a>
Perl interpreter chooses type based on context of data itself.  
1. ***scalars:*** Simple variables. Preceded by *$*. Ethier a number, string, or reference
2. ***arrays of scalars:*** Ordered lists of scalars accessed through numeric index. Preceded by *@*.
3. ***hashes of scalars:*** Unordered sets of key/value pairs that are accessed using keys as subscripts. Preceded by *%*.

***numeric literals:*** Stored as signed integers or double-precision floating-point values.  
***string literals:*** Sequence of characters delimited by single or double quotes.  
Certain characters w/ special meaning are proceded by a back slash.

## VARIABLES <a name="variables"></a>
**varaibles:** Reserved memory locations that store values.  
Perl maintains every varaible type in separate namespaces. Thus, same name can be used for the 3 different types of variables.  
***declaration:*** Not needed, happens automatically when assigned a value.  
However, declaration is needed when statement *use strict* is used.  
***private variable:*** Prepend var_name with *my* keyword.   

```perl
# scalar variables
$age = 23;
$name = "Oatmeal";
$pi = 3.14;l

# array variables
@ages = (23, 23);
@names = ("Oatmeal", "Blue Sr");

print "\$age[0] = $age[0]\n";
# output: $age[0] = 23

# hash variables
%data = ('Oatmeal', 23, 'Blue Sr', 23);

print "\$data{'Oatmeal'} = $data{'Oatmeal'}\n";
# output: $data{'Oatmeal'} = 23
```

| context       | description                                  |
| ------------- | -------------------------------------------- |
| scalar        | Assignment to scalar var evualates RHS.      |
| list          | Assignment to array or hash evualates RHS.   |
| boolean       | Any place where expression evaulated T or F. |
| void          | Does not care & want return value.           |
| interpolative | Only happens inside quotes (or look like).   |

***special array @_ARGV:*** Contains command-line arguments intended for the script.  

## ARRAYS <a name="arrays"></a>
**array:** Ordered list of scalar values. Preceded by *@*.  
List & array reperesent data & variable, respectively.    
***access single element:*** *$* + var_name + [index].
***sequential number array:*** Ex: `@var_10 = (1..10);` where .. is the range operator.  
***array size:*** Determined using scalar context. Returned value is # of elements in array.  
```perl
# EXAMPLE 1
@array = (1, 2, 3);
print "size: "scalar @array"\n";

# EXAMPLE 2
@array = (1, 2, 3);
$size = @array;
print "size: $size\n";
```

## REFERENCES <a name="references"></a>
**reference:** Scalar data type holding location of another scalar variable. 
***define:*** Prefix variable, subroutine w/ backslash. Ex: `$scalarref = \$foo`.  
***deference:*** Returns value from a reference point to the location. Prefix ref var w/ $, @, or %.  
```perl
$var = 10;
$ref = \$var;
print "value of $var = ", $$var, "\n"; 
```

## SUBROUTINES <a name="subroutines"></a>
**subroutine:** Group of statements performing a task.  
***define:*** `sub subroutine_name{ body }`  
***call:*** `subroutine_name(args);`  
***NOTE:*** Perl compiles program before executing, thus does not matter where subroutine is declared.  
***passing arguments:*** Accessed through special character *@_*. Thus, arg_1 is *$_[0]*, arg_2 is *$_[1], etc.




