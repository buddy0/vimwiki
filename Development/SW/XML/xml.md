# eXtensible Markup Language (XML)

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpose)
2. [TREE](#tree)
3. [ELEMENTS_TAGS_ATTRIBUTES](#elements_tags_attributes)
4. [OTHER](#other)
5. [EXAMPLE](#example)
</details>

## PURPSE <a name="purpose"></a>
* Stores & transports data; in tags & in plain text format.
* Does not: do anything, use predefined tags, display information. 

## TREE <a name="tree"></a>
* XML documents form a tree structure: starting at the root element & branching to child elements. All elements can have sub elements.
* ***relationship between elements:*** parent, child, sibling

## ELEMENTS_TAGS_ATTRIBUTES <a name="elements_tags_attributes"></a>
* ***element:*** Everything from start to end tag. Can contain text, atrributes, and/or other elemenlts.   
Must contain 1 root element that is parent of all other elements.  
Are case-sensitive, must start w/ letter or unerscore, can contain letters, digits, hyphens, underscores, & periods.  
Cannot start w/ the letters xml & cannot contain spaces.
* ***tags/closing tag:*** All elements must have one. Prolog does not b/c not part of the document. Case sensitive.  
Ex: `<element></element>` or `<element />`.  
* ***atrributes:*** Contain data related to specific element. Must always be quoted (single or double). Cannot contain multiple values.  
Ex: `<person gender="male">` 

## OTHER <a name="other"></a>
* ***prolog:*** Optional but must come 1st.
* ***entity references:*** Use to avoid syntax errors by inserting character in double quotes or use the 5 pre-defined entity references.
* ***white-space:*** Preserved in XML.
* ***new line:*** Stores as line feed (LF).

## EXAMPLE <a name="example"></a>
```xml
<!-- EXAMPLE -->
<?xml version="1.0" encoding="UTF-8"?>
<teddystore>
    <teddybear category="bear">
        <species>Golden</species>
        <height>Medium</meduem>
    </teddybear>
</teddystore>
```
