# SDL CODE SNIPPETS

## DRAWING A RECT MIDDLE OF WINDOW
```c
// SDL_Rect defining a structure & SDL_QueryTexture() giving dimensions of the texture
SDL_Rect dest;
SDL_QueryTexture(texture, NULL, NULL, &dest.w, &dest.h);
dest.w /= 4;
dest.h /= 4;

// Position the sprite at middle of the screen
dest.x = (WINDOW_WIDTH - dest.w) / 2;
dest.y = (WINDOW_HEIGH - dest.h) / 2;

// Annimation loop
while(!close_requested){
    // Code
    
    SDL_RenderCopy(renderer, texture, NULL, &dest);
}

```
