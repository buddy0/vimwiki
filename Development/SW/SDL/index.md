# SDL TOPICS

[Questions](helpsdl.md)
[CodeSnippets](codesnippets.md)

## WINDOW
Physical pixels

## RENDERER
Goes along with the window.  
Place to store settings/context.  
"Abstract object in charge of doing drawing operations to the window".  
Corresponds to a program loaded into the graphics HW in the computer.  

**Functions**:  
`SDL_RenderCopy(renderer, texture, NULL, NULL)` ([link](http://wiki.libsdl.org/SDL_RenderCopy)): Take texture data and copy onto window.  
`SDL_RenderPresent(renderer)`([link](http://wiki.libsdl.org/SDL_RenderPresent)): **Double buffering:** back buffer in memory and front buffer for whats on the screen at the moment.  
Draw operations such as SDL_RenderCopy and SDL_RenderClear to the back buffer. When were done doing render operations which could take a long time,  
just switch the buffers by calling SDL_RenderPresent. This prevents screen refreshing when only half of our objects are on the screen.   

## SURFACE
Data in main memory.  
Example, an image needs to be loaded into memory using *SDL_image* library function.  
Loads into memory and stores it as a thing called SDL surface.
`SDL_Surface` is a struct that represents image data in memory.

## TEXTURE
Data in graphics memory.   
Corresponds to image data loaded into the graphics HW VRAM memory.   
Example, an image needs to be a texture before it is drawn onto a window.  

## ANIMATION
Human eye can only see 12 distinct images per second.  
Average computer screens are 60fps.  
Anamation loops: 
1. Clear the window: `SDL_RenderClear(rend)`  
2. Drawing operations  
3. Draw the image to the window: `SDL_RenderCopy(args)` & `SDL_RenderPresent(rend)`  
4. Wait 1/60th of a second, target is 60fps: `SDL_Delay(1000/60)`. Assuming drawing operatios take zero time.   
   In real game, subtract drawing operations length time from the rest time

## ORIGIN CORDINATE SYSTEM
![origin cordinate system picture](origin.png)  

## EVENTS
* `SDL_Event` ([link](http://wiki.libsdl.org/SDL_Event)) *union*: Containing structures of all the different events.   
Exactly like structs but only 1 member can have a value at a time.  
The same memory space is used for all members. The actual variable can only have one of the members types at a time.   
SDL queues up events in a literal queue behind the scenes. And then each frame we can dequeue events from that queue  
using this call `SDL_PollEvent`.  
* `SDL_PollEvent` ([link](http://wiki.libsdl.org/SDL_PollEvent)): *function declaration:* `int SDL_PollEvent(SDL_Event * event);` Returns 1 if pending event or 0 if none available.  
* `SDL_KeyboardEvent` ([link](http://wiki.libsdl.org/SDL_KeyboardEvent)): *structure:* Containing keyboard button event info.    
* `SDL_GetKeyboardState` ([link](http://wiki.libsdl.org/SDL_GetKeyboardState)): *function declaration:* `const Uint8* SDL_GetKeyboardState(int *numkeys);` Returns a pointer to an array of key states.  

## USEFUL FUNCTIONS
* `SDL_Rect` ([link](http://wiki.libsdl.org/SDL_Rect)): *structure:* defines a rectangular area of the screen.  
* `SDL_QueryTexture` ([link](http://wiki.libsdl.org/SDL_QueryTexture)): *function declaration:* `int SDL_QueryTexture(SDL_Texture texture, Uint32 * format, int *access, int *w, int *h);` Query the attributes of a texture.  

