# QUESTIONS & ANWSERS

## ASSIGNING A POINTER TO A FUNCTION CALL?
**Why:** Never seen this before. Pointers store memory addresses.  

```c
// In SDL/include/SDL_video.h
typedef struct SDL_Window SDL_Window;

// In SDL/src/video/SDL_video.c
SDL_Window *SDL_CreateWindow(const char *title, int x, int y, int w, int h, Uint32 flags);
```
**Answer:** Right Left rule. SDL_CreateWindow is a function returning a pointer of type SDL_Window.  

## HOW TO READ MULTIPLE INPUT EVENTS AT THE SAME TIME?
**Why:** Trying to quit the window using LALT, LSHIFT, & C instead of SDL_QUIT.  
**Answer:** [link1](http://forums.libsdl.org/viewtopic.php?t=9738) [link2](http://lazyfoo.net/tutorials/SDL/18_key_states/index.php) [link3](https://stackoverflow.com/questions/1252976/how-to-handle-multiple-keypresses-at-once-with-sdl)  
Multiple ways to get the state of input devices (mouse, keyboard, etc.): events, key states. In the event loop  
`while(SDL_PollEvent(&event) != 0)` no key states are being checked. However, SDL still needs an event loop running b/c  
SDL's internal keystates are updated SDL_PollEvent is called.  
**Example:**  
```c
// set to 1 when LALT & ESC are pressed
char close_requested = 0;
SDL_Event event;

// while application was running
while(!close_requested){
    while(SDL_PollEvent(&event) != 0){}

    const Uint8 *keyboard_state_array = SDL_GetKeyboardState(NULL);
    if(event.type = SDL_KEYDOWN){
        if(keyboard_state_array[SDL_SCANCODE_LALT] && keyboard_state_array[SDL_SCANCODE_ESCAPE])
            close_requested = 1;
        }
    // Clear window
    SDL_RenderClear(renderer);    // makes screen go black

    // Draw image to window 
    SDL_RenderCopy(renderer, texture, NULL, NULL);
    SDL_RenderPresent(renderer);
    SDL_Delay(1000/60);
}
```



