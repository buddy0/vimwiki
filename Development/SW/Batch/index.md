# BATCH 
*[tutorialspoint](https://www.tutorialspoint.com/batch_script/index.htm)*  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INTRO](#intro)
2. [VARIABLES](#variables)
</details>

## INTRO <a name="intro"></a>
**bash scripting:** Automates command sequences. Stored in simple text files w/ a bat or cmd file extension.  
File is executed through a shell by a command interpreter such as cmd.exe.  

## VARIABLES <a name="variables"></a>
***command line arguments:*** stored in variables 1, 2, etc.  
***initialize:*** Through set command. Ex: `set var_name=value`  
***display value:*** Variable name enclosed in % signs. Ex: `echo %var_name&`  
***properties:*** By default global scope. SETLOCAL & ENDLOCAL,EXIT, or EOF commands for local scope.

