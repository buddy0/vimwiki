# EXTERN KEYWORD - [link](https://www.youtube.com/watch?v=ySY_FlA7EvA)  

**translation unit:** Compiled object file.
```bash
# header files are not compiled 
gcc -c main.c -o main.o # translatioin unit
gcc -c display.c -o display.o # translation unit
gcc -o main main.o display.o # linking phase
```

**extern:** Declaring a variable in one translation unit, but use it in another. Tells the compiler this variable exists, I am going to use it, but it's declared somewhere else in another translation unit.  
When object files are linked together this results in a single variable usable throughout the whole program.  

