## INLINE KEYWORD - [link1](https://www.youtube.com/watch?v=t6Jfrmdg5rk),  [link2](https://www.geeksforgeeks.org/inline-function-in-c/), [link3](https://www.greenend.org.uk/rjk/tech/inline.html)  

***inline functions***: Functions whose definitions are small & substituted at the place where its function call is happened.   
*inline* is a hint (not forcing) to the compiler saying to inline this; meaning to optimize the code & don't have to use stack for simple functions.  
***NOTE:*** Used in embedded programming due to optimizations changing timing. A function call takes longer than inline code.  

## LINKER ERROR
```c
/* Inline function in C */
inline int foo(void)
{
    return 2;
}
  
/* Driver code */
int main()
{
    int ret;
    ret = foo();    /* inline function call */
    printf("Output is: %d\n", ret);
    return 0;
}
```
Errors can occur with different compilers. For example, *GCC* performs inline substitution as part of optimisation, so no function call present in main. GCC's file scope ***not extern linkage***, meaining inline function is never provided to the linker.  
***solution:*** Use *static* keyword before *inline* keyword forcing compiler to consider inline function in the linker.  
