# VOLATILE KEYWORD 

* Tells compiler that a volatile variable can change in ways that not be apparent to the compiler which might be another thread, signal handlerer, interrupt service routine.
* Tells the compiler to not make any assumptions about when this variable can change & not to optimize it out. 
* Volatile can be declared by a:
**variable:*** `volatile int done;`.  
***pointer to volatile int:*** `volatile int* done;`. Pointer is not volatile, but the *int* it points to is volatile.  
***volatile pointer to an int:*** `int *volatile done;`. The pointer name is the volatile one, the address can change, but the integer it points to is *not* considered volatile.   
***volatile pointer to a volatile int:*** `volatile int *volatile done;`.  

## Why is volatile needed in C?. - [link](https://stackoverflow.com/questions/246127/why-is-volatile-needed-in-c)  
**A:** *volatile* tells compiler not to optimize anything that has to do w/ the *volatile* variable. Compiler is forced to do what you wrote. It can't remove memory assignments, can't cache variables in registers, and can't change order of assignments either. *volatile* helps us access the value fresh every time.  
3 reasons to use it:  
1. Interfacing w/ HW that changes the value itself.  
2. Another thread running that also uses the variable.  
3. Signal handler that might change the value of the variable.  
