# CONST KEYWORD VS DEFINE PREPROCESSOR DIRECTIVE - [link](https://www.youtube.com/watch?v=ZIhy4uy5uJM&t=28s) 

```c
#define ARRAY_LENGTH 10
const int ARRAY_LENGTH = 10;
```

## CONST KEYWORD
* Value in program that is never going to change. Declares a variable that is not going to vary. 
* Compiler knows that this is an *int* so it can do type checking, thus safer option.
* Scope control, thus can limit its visibility. 

## DEFINE DIRECTIVE
* Handled by the preprocessor, so before the program is compiled. 
* Globally defined. 
* Can be defined outside of source code; defining when compiling program using *gcc -d* option. 
