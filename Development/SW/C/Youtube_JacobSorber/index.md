# JACOB SORBER

## KEYWORDS
* [static](static.md)  
* [extern](extern.md)  
* [volatile](volatile.md) 
* [inline](inline.md)  

## TOPICS
* [semaphore](semaphore.md)  
* [bitfield](bitfield.md)  
* [constvsdefine](constvsdefine.md)

## OS
[CPU Sceduling Basics](cpu_scheduling.md)  
