# SEMAPHORE - [link1](https://www.youtube.com/watch?v=ukM_zzrIeXs), [link2](https://www.geeksforgeeks.org/semaphores-in-process-synchronization/)

Fall into the category of ***synchronization primitives*** which helps coorinate activities between multiple concurrently running threads or processes. Interested in threads or processes that share info.: memory, data.
**concurrent:** Working at the same time.  
**semaphore:** Variable that is an unsigned integer & shared between threads. Some quirks include that changes are atomic & interactions done w/ semaphores are only done through 2 operations:  
1. `wait()`: Decrements the value of the semaphore & it returns. If the value equal to 0, it waits until the semaphore's value becomes positive. Once the value is positive, then its able to decrement & return.  
```c
/* pseudo-code */
wait() {
    while (1) {
        atomic {
            if (v > 0) {
                 v--;
                 return;
            }     
        }
    }
}
```
2. `post()` or `signal()`: Increments the value of the semaphore & returns
```c
/* pseudo-code */
post() {
    atomic {
        v++;
        return;
    }
}
```
Therefore, a semaphore's value isn't accessed directly. 
**atomic:** If one thread or process increments the integer & another wants to decrement the integer, those increment & decrement operations cannot interrupt each other. 


## HOW IT WORKS
Lets say I create a semaphore & give it a value of 1. I also have 3 threads or process sharing this semaphore.  
If thread A calls `wait()` then the value drops from 1 to 0.  
Thread B calls `wait()`, but now the value is 0 so thread B cannot decrement the value so it just waits.  Thread C calls `post()`, the value increases & now thread B can decrement the value and return from *wait*.  

Lets say the value is 1 again, but now threads A & B call `wait()` at the same time. 1 of them will go first (decrement the value and return from *wait*) since they are ***atomic*** & the other thread will have to wait untill there is a `post()`.   

## GOOD FOR?
