# BITFIELD - [link1](https://www.youtube.com/watch?v=aMAM5vL7wTs&t=230s), [link2](https://www.youtube.com/watch?v=Ew2QnDeTCCE) 
  
**bitfield:** Series of bits where individual bits have meaning. Mainly used to reserve space. 3 options to uses:
1. Status flags
2. Want numerical values like integers but don't want to use all the space.   
```c
struct counters {
    int little : 2;
    int medium : 4;
    int large  : 6;
};
```
3. Acting like an array of bits.  

## STANDARD INTEGER TYPES
The type *int* may be 2 or 4 bytes on different machines. For better portability, standard ints were created in the C99 standard w/ the `#include <stdint.h>`. Now, integer variables are the same size on any machine.  
**Exs.**  
`uint8_t`: unsigned 8-bit integer  
`int64_t`: signed 64-bit integer  

## BITMASK
**bit mask:** Binary pattern used to modify another binary pattern using bitwise operations: *&*, *|*, *~*, etc.  



