# STATIC KEYWORD

* Affects the scoping.  
* Can make a local variable, global variable, & a function static.  
* ***static (global variable/function):*** Only visisble w/in its own translation unit.  
Ex. Declaring same function names in 2 differnt files that do different actions. Identifiers don't have to be unique.  
* ***static (local variable):*** Persistent across function calls & only visible w/in its funciton.  
Ex. 
```c
void main (void)
{
  printf ("value of x 1st time.\n");
  func ();
  printf ("value of x 2nd time.\n");
  func ();
}

void func (void)
{
  static int x = 0;
  printf ("%d\n", x);
  x += 1;
  printf ("%d\n", x);
}

/*
 *    OUTPUT
 *    value of x 1st time.
 *    0
 *    1
 *    value of x 2nd time.
 *    1
 *    2
 */
```
