## 6.5.3.2 ADRESS & INDIRECTION OPERATORS (pg. 90)

### CONSTRAINTS
**`&`** Operand shall be:      
* function designator  
* result of a [] or unary `*` operator  
* lvalue that designates an object, not bit-field & *register* storage-class specifier

**`*`** Operand be:   
* pointer type: null pointer, void pointer, wild pointer, dangling pointer, complex pointer, near pointer, far pointer, huge pointer.  

### SEMANTICS
**`&`** operator yields the address of the operand.  
If operand has type "*type*", result has type "pointer to type".  
If operand is result of a unary `*` operator, result is omitted.  
**`*`** operator denotes indireciton.  
If operand points to a function, result is a function designator.  
If operand points to an object, result is an lvalue designating the object.  
If operand has type "pointer to *type*", result has type *type*.  
If invalid value has been assigned to the pointer, the beahvior of `*` operator is undefined.  

## 6.5.3.4 SIZEOF OPERATOR (pg. 92) 

### CONSTRAINTS
`sizeof` shall *NOT* be applied to an expression of function type or incomplete type, to the parenthesized name of such a type, or to bit-field member expression.  

### SEMANTICS
`sizeof` yields size (in bytes) of its type operand.  
When applied to an operand that has array type, result is total number of bytes in the array. *(88)*  
The value of the result is implementation-defined, and its type (an unsigned integer type) is *size_t*.  
If the operand is the result of a [] operator, neither the & operator nor the unary * that is implied by the [] is evaluated and the result is as if the & operator
were removed and the [] operator were changed to a + operator.

*[88]:* When applied to a parrameter declared to have an array or function type, `sizeof` operator yields size of the adjusted (pointer) type 


