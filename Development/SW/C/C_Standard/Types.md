# TYPES (pg. 45)

**pointer type:** May be derived from a function type, an object type, or an incomplete type, called the referenced type.  
Describes an object whose value provides a reference to an entity of the referenced type.  
A pointer type derived from the referenced type T is sometimes called ‘‘pointer to T’’.   
