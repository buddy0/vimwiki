# 6.3.2 OTHER OPERANDS

## 6.3.2.1 LVALUES, ARRAYS, & FUNCTION DESIGNATORS (pg.58)
**lvalue:** Expression with an object type or an incomplete type other than void; *(53)* if an lvalue does not designate an object when it is evaluated, the behavior is undefined.  
**modifiable lvalue:** lvalue that does *not* have an array type, incomplete type, const-qualified type, & if structure or union w/ a member of const-qualified type.    

**lvalue conversion:** Except when it is the operand of the sizeof operator, the unary & operator, the ++operator, the -- operator, or the ***left operand*** of the . operator or an assignment operator, an lvalue that does not have array type is converted to the value stored in the designated object (and is no longer an lvalue). If the lvalue has qualified type, the value has the unqualified version of the type of the lvalue; otherwise, the value has the type of the lvalue. If the lvalue has an incomplete type and does not have array type, the behavior is
undefined.  

Except when it is the operand of the sizeof operator or the unary & operator, or is a string literal used to initialize an array, an expression that has type ‘‘array of type’’ is converted to an expression with type ‘‘pointer to type’’ that points to the initial element of the array object and is not an lvalue. If the array object has register storage class, the behavior is undefined.  

**function designator:** Expression that has a function type.

*(53)*: The name *lvalue* comes originally from the assignment expression `E1 = E2`, in which the left operand *E1* is required to be a (modifiable) lvalue.  
It is perhaps better considered as representing an object **locator value**. What is sometimes called **rvalue** is in this International Standard described
as the value of an expression. 

