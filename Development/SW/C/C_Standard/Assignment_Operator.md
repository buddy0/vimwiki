## 6.5.16 ASSIGNMENT OPERATOR (pg.103)
## SYNTAX
assignment-expression: *conditional-expression* *unary-expression* *assignment-operator* *assignement-expression*  

### CONTRAINTS
AO shall have a modifiable lvalue as its left operand. 

### SEMANTICS
AO stores a value in the object designated by the left operand.  
AE has the value of the left operand after the assignment, but is not an lvalue.  

## 6.5.16.1 SIMPLE ASSIGNMENT (pg.104)
## CONSTRAINTS
* The left operand has qualified or unqualified arithmetic type and the right has
arithmetic type.  
see pdf for more  

## SEMANTICS
1. Value of right operand converted to type of *assignment expression* 
2. Replaces value stored in object designated by the left operand. 
Ex. `int var = 10 + 1;` Result is 11. 
