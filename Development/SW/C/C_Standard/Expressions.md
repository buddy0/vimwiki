# 6.5 EXPRESSIONS (pg.79)  

**expression:** Sequence of operators and operands that specifies computation of a value,  
or that designates an object or a function,  
or that generates side effects,  
or that performs a combination thereof.
