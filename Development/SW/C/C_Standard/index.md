# THE STANDARD 
* [wiki](https://www.iso-9899.info/wiki/The_Standard#C89_.2F_C90_.2F_C95) 
* [c committee website](http://www.open-std.org/jtc1/sc22/wg14/)  
* documents: 
    * ISO/IEC 9899:1999 (C99) | draft: N1256  
    * ISO/IEC 9899:2011 (C11) | draft: N1570 

**semantics:** What syntactically valid programs mean, what they do. Describes the processes a computer follows when executing a program in that specific language.  
**object:** Region of data storage in the execution environment, the contents of which can represent values.  
**value:** Contents of an object when interpreted as having a specific type.  

## SECTIONS
* [6.2.2 Linkages](Declarations.md)
* [6.2.5 Types](Types.md)
* [6.3.2 Other Operands](Other_Operands.md)
* [6.5 Expressions]Expressions.md)  
    * [6.5.16 Assignment Operator](Assignment_Operator.md)
    * [6.5.3 Unary Operators](Unary_Operators.md) 
* [6.7.7 Type definitions](Type_Definitions.md)

