# DECLARATIONS

## SECTION 6.2.2 - Linkages of identifiers
**Q:** What is a global variable's storage class, scope, linkage when there is no specified storage.  
**A:** 5. "If the declaration of an identifier for a function has no storage-class specifier, its linkage is determined exactly as if it were declared with the storage-class specifier extern. If the declaration of an identifier for an object has file scope and no storage-class specifier, its linkage is external."
