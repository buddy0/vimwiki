# 6.7.7 TYPE DEFINITIONS
*Pg. 135*  

***Syntax:*** `typedef-name: identifier`  
***Constraints:*** If typedef name specifies a variably modified type then it shall have block scope.  
***Semantics:*** typedef declaration does not introduce a new type, only a synonym for the type specified.  
```c
typedef T type_ident;
type_ident D;  /* type_ident is a typedef name w/ type specified by declaration specifiers in T*/
```
<details>
<summary><b>EXAMPLE 1</b></summary>

```c
typedef int MILES, KLICKSP();
typedef struct { double hi, lo; } range;

MILES distance;
extern KLICKSP *metricp;
range x;
range z, *zp;
```
</details>

<details>
<summary><b>EXAMPLE 4</b></summary>

typedef improves code readability. All 3 of the following declarations of the signal function specify same type, the first w/o making use of any typedef names.
```c
typedef void fv(int), (*pfb)(int);

void (*signal(int, void (*)(int)))(int); /* signal is a pointer variable pointing to a function w/ ? */
fv *signal(int, fv *);
pfv signal(int, pfv);
```
</details>
