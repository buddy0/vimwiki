# A TUTORIAL ON POINTERS AND ARRAYS

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [CHAPTER 1 - WHAT IS A POINTER](#what_is_a_pointer)
2. [CHAPTER 2 - POINTER TYPES & ARRAYS](#pointer_types_and_arrays)
3. [CHAPTER 3 - POINTERS & STRINGS](#pointers_and_strings)
4. [CHAPTER 4 - MORE ON STRINGS](#more_on_strings)
5. [CHAPTER 5 - POINTERS and STRUCTURES](#pointers_and_structures)
6. [CHAPTER 6 - SOME MORE ON STRINGS and ARRAYS OF STRINGS](#some_more_on_strings_and_arrays_of_strings)
7. [CHAPTER 7 - MORE ON MULTI-DIMENSIONAL ARRAYS](#more_on_mult-dimensional_arrays)
8. [CHAPTER 8 - POINTERS TO ARRAYS](#pointers_to_arrays)
9. [CHAPTER 9 - POINTERS & DYNAMIC ALLOCATION OF MEMORY](#pointers_and_dynamic_allocation_of_memory)

</details>

## CHAPTER 1 - WHAT IS A POINTER <a name="what_is_a_pointer"></a>
**variable**:    
- Storage location to hold a value.  
- Compiler reads the variable and assigns a specific block of memory to hold the value.  
- Declaring a variable `int k;` informs the compiler the *name* and *type* of the vairable.   
  The *type* determines how many bytes of memory will be reserved.  
  A symbol table is created: symbol **k** and the relative address of where the memory was assigned for the variable.  
  Asigning the variable `k = 2;`, at runtime 2 is placed at the reserved memory location.  
- **object**: "named region of storage; an lvalue is an expression referring to an object"  
  `k` is an object with 2 values/expressions:  
  1. **rvalue**: value stored  at `k` (only on right side of assignment).  
  2. **lvalue**: memory location, address of `k` (left or right side of assignment).  
  `k = 2;` The compiler interprets k as the address of k and creates code to copy the value 2 to that address.  

![object](Images/object.png)

**pointer variable:**
- Variable holding an lvalue.  
- Declaring a pointer vairable `int *ptr;`  
  *Type* of data refers to the type of data stored at the address. 
  The `*` informs the compiler to reserve the bytes of a memory address.  
  An unassigned pointer variable may or not be equaled to zero, therefore set it equal to the marco NULL for compatiblity with different compilers.  
- `&` **operator**: retrieves the lvalue, address.  
- `*` **dereferencing operator**: copies the assigned value to the address pointed to by the pointer variable.   
    Refering to the value pointed by the pointer vairable, *not* the value of the pointer itself.  
    Value stored at the address pointed to by the pointer vairable.  

## CHAPTER 2 - POINTER TYPES & ARRAYS <a name="pointer_types_and_arrays"></a>

**pointer arithmetic**: `int *ptr;` Compiler knows that `ptr + 1` means were incrementing by sizeof(type).  

**array**:
`int myarry[];` In C, whenever we might use `&myarry[0]` we can replace with `myarry`.   
- The name of the array is the address of the first element in the array. Arrays are *not* pointers.  
  Correct: `ptr = myarry;` `ptr` is a variable that can hold a value.  
  Incorrect: `myarray = ptr;` `myarry` is a constant and the lcoation of the first element cannot be changed once an array is declared.   

## CHAPTER 3 - POINTERS & STRINGS <a name="pointers_and_strings"></a>

In C, there is no data type for a string.  
**string**: array of characters terminated with a *nul* character, binary zero `'\0'`  
        *nul* is NOT the same as *null*  
**nul**: escape sequence `'\0'` occupying 1 byte of memory  
**NULL**: macro used to initialize null pointers  

```c
// Examples of writing strings
// 1
char my_string[4];
my_string[0] = 'B';
my_string[1] = 'u';
my_string[2] = 'd';
my_string[3] = '\0';

// 2
char my_string[4] = {'B', 'u', 'd', '\0'};

// 3
char my_string[4] = "Bud";
```
- Single quotes need the *nul* character, however double quotes auto appends to the end of the string.  

* Program 3.1: ~/development/cprograms/strings  

## CHAPTER 4 - MORE ON STRINGS <a name="more_on_strings"></a>

* 2 syntaxes to identify a given element in an array: pointer arithmetic and array indexing

Example  
```c
char my_strcpy(char *destination, char *source){
    char *p = destination;
    while(*source != '\0'){
        *p++ = *source++;
    }
    *p = '\0';
    return desination;
}
```

```c
char my_strcpy2(char dest[], char source[]){
    int i = 0;
    while(source[i] != '\0'){
        dest[i] = source[i];
        i++;
    }
    dest[i] = '\0';
    return dest;
}
```
* Parameters are passed by value whether passing a character pointer or name of array.  
  The value passed in this case is an address of the first element of each array.  
  Therefore, `source[i] == *(p+i)`
* speed up pointer version: `while(*source != '\0')` = `while(*source)`  

## CHAPTER 5 - POINTERS and STRUCTURES <a name="pointers_and_structures"></a>

**struct**:
* Block of data containing different data type.  
* Access structure members with the dot operator.  

```c
struct tag{   // structure type
    int age;
};
struct tag my_struct; // declared structure my_struct

struct tag *st_ptr = &my_struct;   // initalizing pointer to a structure and pointing to my_struct

(*st_ptr).age = 22;  // deferencing the pointer: (*str_ptr) means get the contents that is being pointed to which is my_struct
str_ptr->age = 22;   // deferencing the pointer: thus it really means my_struct.age
```
  
## CHAPTER 6 - SOME MORE ON STRINGS and ARRAYS OF STRINGS <a name="some_more_on_strings_and_arrays_of_strings"></a>
An alternate approach to `char my_name[] = "Buddy";` is `char *my_name = "Buddy";`  
There is a difference between these 2 approaches:  
**array notation:**  
6 bytes of storage in static memory block are used up.  
`my_name` = `&my_name[0]`  
Location of the array is fixed during run time meaning its a **constant** not a variable.  
                
**pointer notation:**  
Same 6 bytes are required plus ***N*** bytes to store the pointer variable *my_name*.  
`my_name` is a **variable** not a constant.  

```c
void my_function_A(char *ptr){
    char a[] = "ABCDE";
}

void my_function_B(char *ptr){
    char *cp = "FGHIJ";
}
```

Both functions have local variables, with string `ABCDE` and the value of the pointer `cp` on the stack.  
However, the string `FGHIJ` can be stored anywhere (think data segment).  

**multi-dimensional arrays:**  
`char multi[2][3];`  
Think of `multi[2]` as the name of the array. Therefore, we have an array of 3 characters.   
But, `multi[2]` itself is an array indicating that there are 2 elements each bing an array of 3 charaters.  
Thus, array of 2 arrays with 3 character each. Rows X Columns.  

* C interprets a 2D array as an array of 1D arrays. Array of arrays. In memory this looks like one giant array.  
  `char multi[2][3]` consist of 2 arrays with 3 blocks each  
  Example: The first element of a 2D array of chars is a 1D array of chars.  
* `*(*(multi + row) + col) == mult[row][col] == mult[row * width + col]`   
  `&muti[0][0] == multi`  

**Example**  
![2D array picture](Images/2Darray.png)

```
int a[2][2] = {1, 2, 3, 4};
      ^  ^
      |  |
      |  2 elements in each 1D array
  2 1D arrays

  +-----------+-----------+
  |           |           |
  | +----+----+----+----+ |
a | | 1  | 2  | 3  | 4  | |
  | +----+----+----+----+ |
  |  1000 1004|1008 1012  |
  +-----------+-----------+
   1000        1008 
```
| code              | transform | value (meaning)                            |
| ----------------- | --------- | ------------------------------------------ |
| `a`               | &a[0]     | 1000 (pointer to 1st 1D array)             |
| `a + 1`           | &a[1]     | 1008 (pointer to 2nd 1D array)             |
| `*a`              | &a[0][0]  | 1000 (pointer to 1st 1D array 1st element) |
| `*a + 1`          | &a[0][1]  | 1004 (pointer to 1st 1D array 2nd element) |
| `*(a + 1)`        | &a[1][0]  | 1008 (pointer to 2nd 1D array 1st element) |
| `(*(a + 1) + 1)`  | &a[1][1]  | 1012 (pointer to 2nd 1D array 2nd element) |
| `*(*(a + 1) + 1)` | a[1][1]   | 4                                          |

## CHAPTER 7 - MORE ON MULTI-DIMENSIONAL ARRAYS <a name="more_on_mult-dimensional_arrays"></a>
`int multi[row][column];`  
Previous section, we can access individual elements of a 2D array by:  
`multi[row][col]` or `*(*(multi + row) + col)`  
`*(X + col)`: X is a pointer to the 1st element of the 1st 1D array. col is an integer.  
`mulit + row + #` means increase by `COLS * sizeof(int)`

To evaluate a 2D array, 5 values must be known:  
1. address of the first elemen of array => mulit (name of array)  
2. size of the type of elements of the array => sizeof(int)  
3. 2nd dimension of array  
4. specific index value for 1st dimension, **row**  
5. specific index value for 2nd dimension, **column**   

```c
// Don't need 1D b/c only one parameter value can be passed to this function 
// which is the name of the 2D array acting as a pointer to the array.
// Only way of informing the compiler of the 2D is by including it in the parameter definiton.
void set_value(int m_array[][COLS]);
void set_value2(int m_array[][2D][3D]);
```

## CHAPTER 8 - POINTERS TO ARRAYS <a name="pointers_to_arrays"></a>
Pointers can also be parameters of a function, designed to manipulate an array.  
`void a_func(int *p);` or `void a_func(int p[]);`  
What gets passed is the value of a pointer (address) to 1st element of the array.  
Thus, array notaton doesn't need to pass dimension since we're only passing the address to the 1st element, *not* the whole array.  

keyword **typedef**: the new name for the data type is the name used in the definition of the data type   
```c
// syntax
typedef unsigned char byte;  // the name byte now means unsigned char
```

**Example**
```c
typedef int Array[10];  // Array becomes a data type for an array of 10 integers

Array my_arr;   // my_arr is now an array of 10 integers
Array arr2d[5]; // arr2d[5] is now an array of 5 arrays with 10 integers each
Array *p1d;     // p1d points to an array of 10 integers

// Since the data type Array is an array of 10 integers
p1d + 1;        // the value would change by 10*sizeof(int)
```
typedef makes things clear for the reader an and programmer, *but* it's not really necessary.  
Delcaring a pointer without a typedef keyword looks like this: 
```c
int (*p1d)[10]; // p1d is a pointer to an array of 10 integers
int *p1d[10];   // p1d is the name of an array of 10 pointers to type int
```

## CHAPTER 9 - POINTERS & DYNAMIC ALLOCATION OF MEMORY <a name="pointers_and_dynamic_allocation_of_memory"></a>
