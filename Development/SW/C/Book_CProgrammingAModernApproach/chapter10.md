# PROGRAM ORGANIZATION
**scope:** Portion of program text in which variable can be referenced.  
**storage duration:** Portion of a program execution during which storage for the variable exists. Where memory is allocated & deallocated.    

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [LOCAL VARIABLES](#local_variables)
2. [EXTERNAL VARIABLES](#external_variables)
3. [BLOCK](#block)
4. [SCOPE](#scope1)
5. [ORGANIZING A C PROGRAM](#organizing_a_c_program)
</details>

## LOCAL VARIABLES <a name="local_variables"></a>
**local:**  Declared in body of a function. Referenced at its point of declaration to function enclosing curl brace.  
PROPERTIES (linkage later on)
* **DEFAULT: automatic storage duration:** Storage is "automatically" allocated & deallocated when object is declared & at end of block scope, respectively.   
A local variable doesn't retain its value when its enclosing function returns. Thus, when function is called again, variable might have a different value.  
* **DEFAULT: block scope:**  Visible from its point of declaration to end of enclosing function body. Thus, other functions *CAN* use the same variable names.  
```c
void main(void){
    int var; /* block scope, automatic storage duration, & no linkage */
}
```
* **C99: local variable static storage duration:** Permanent storage location. Retains its value throughout execution of program even when leaving and going back to the function. Storage is "automatically" allocated & deallocated when object is declared & at end of file scope, respectively. Still has a scope block.  
```c
void main(void){
    static int var; /* block scope, static storage duration, & no linkage */
} 
```
Parameters have both properties, only difference is that they automatically get initialiazed when function is called.  

## EXTERNAL VARIABLES <a name="external_variables"></a>
**external, global:** Declared outside the body of a function.  
Make sure they have meaningful names, since the whole program will be using them.  
PROPERTIES (linkage later on)
* **DEFAULT: static storage duration:** Similar to declared local variables with *static*. Value stored will stay there indefinitely.  
* **DEFAULT: file scope:** Visible from its point of declaration to end of enclosing file.  
```c
[extern] int var; /* file scope, static storage duration, & external linkage, confusing b/c static storage duration, but `extern` storage class */ 
static int var_2; /* file scope, static storage duration, & internal linkage */
void main(void)
{
} 
```
Convenient when many functions must share a variable. However, in most cases its *better* to communicate thorugh parameters rather than by sharing variables. B/C:  
* Changing an external variable type may effect functions that use the variable.  
* If external variable is assigned incorrect value, then its hard to identify guilty function.  
* Functions that use external variables are not self-contained, hard to reuse in other programs, since the external variables need to be dragged along with them.  

## BLOCK <a name="block"></a>
Bascially a compound statement. Anything that has the form `{ declarations statements }`. Therefore `functions(){}` and `if(){}` have blocks.  

## SCOPE <a name="scope1"></a>
**"** When a declaration inside a block names an identifier that's already visible, new declaration temporarily "hides" the old one, and identifier takes on a new meaning. At end of block identifier regains old meaning. **"**    

## ORGANIZING A C PROGRAM <a name="organizing_a_c_program"></a>
*#include* directives  
*#define* directives  
Type definitions  
Declarations of external variables  
Prototypes for functions other than *main*  
Definition of *main*  
Definition of other functions  


