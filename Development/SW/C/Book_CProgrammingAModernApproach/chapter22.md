# INPUT/OUTPUT

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [STREAMS](#streams)  
    a. [FILE POINTERS](#file_pointers)  
    b. [STANDARD STREAMS & REDIRECTION](#standard_streams_and_redirection)  
    c. [TEXT FILES VS BINARY FILES](#text_files_vs_binary_files)  
2. [FILE OPERATIONS](#file_operations)  
    a. [OPENING FILE](#opening_file)  
    b. [CLOSING FILE](#closing_file)  
    c. [ATTACHING FILE TO OPEN STREAM](#attaching_file_to_open_stream)  
    d. [OBTAINING FILE NAMES FROM COMMAND LINE](#obtaining_file_names_from_command_line)  
    e. [TEMPORARY FILES](#temporary_files)  
    f. [FILE BUFFERING](#file_buffering)  
    g. [MISCELLANEOUS](#miscellaneous)  
3. [FORMATTED IO](#formatted_io)  
    a. [PRINTF FUNCTIONS](#printf_functions)  
    &emsp;I. [PRINTF CONVERSION SPECIFICATIONS](#printf_conversion_specifications)  
    b. [SCANF FUNCTIONS](#scanf_functions)  
    &emsp;I. [SCANF CONVERSION SPECIFICATIONS](#scanf_conversion_specifications)  
4. [DETECTING END-OF-FILE & ERROR CONDITIONS](#detecting_end_of_file_and_error_conditions) 
5. [CHARACTER IO](#character_io)  
    a. [OUTPUT FUNCTIONS](#char_output_functions)  
    b. [INPUT FUNCTIONS](#char_input_functions)  
6. [LINE IO](#line_io)  
    a. [OUTPUT FUNCTIONS](#line_output_functions)  
    b. [INPUT FUNCTIONS](#line_input_functions)  
7. [BLOCK IO](#block_io)  
8. [FILE POSITIONING](#file_positioning)
9. [STRING IO](#string_io)  
    a. [OUTPUT FUNCTIONS](#string_output_functions)  
    b. [INPUT FUNCTIONS](#string_input_functions)  
10. [QUESTIONS](#questions)
</details>

***functions in <stdio.h>:*** byte input/output functions  
***functions in <wchar.h>:*** wide_character input/output functions  

## STREAMS <a name="streams"></a>
**stream:** Any source of input or destination for output.  
***ex:*** screen, keyboard, hard drives, CDs, DVDs, flash memory, network ports, printers.  

### FILE POINTERS <a name="file_pointers"></a>
***accessing stream in C program done through:*** file pointer.  
***syntax:*** `FILE *<ptr_name>;`  

### STANDARD STREAMS & REDIRECTION <a name="standard_stream_and_redirection"></a>
***stdio.h provides 3 standard streams:*** Ready to use, do not have to declare, open, or close them.
| file pointer | stream          | default meaning |
| ------------ | --------------- | --------------- |
| stdin        | standard input  | keyboard        |
| stdout       | standard output | screen          |
| stderr       | standard error  | screen          |

***redirection:*** Mechanism that allows the OS to change the default meanings.  
***input redirection file ex:*** `demo <in.dat`.  Data now reads from file in.dat instead of the keyboard  
***output redirection file ex:*** `demo >out.dat`. Data now goes to file out.dat instead of the screen  
***problem w/ output redirection:*** Everything written to stdout is put into a file.  
If program goes off rails, won't see the error messages unless the file is looked at.  
&emsp;***solution:*** Writing error messages to stderr instead of stdout guarantees that the messages will appear on the screen even when stdout has been redirected. 

### TEXT FILES VS BINARY FILES
***stdio.h supports 2 kinds of files:*** text and binary.  
***text file bytes:*** Represent characters. 
***binary file bytes:*** Do not normally represent characters. Group of bytes might represent other types of data: ints, floats. Ex. C executable program.  
***characteristic of text files:***  
1. ***Divided into lines***. Each line normally ends w/ one or two special characters.  
Windows: carriage return character `'\x0d'` followed by line-feed character `'\x0a'`.  
Unix: single line-feed character  
2. ***May contain special 'end-of-file' marker***.  
Windows: Ctrl-Z `'\x1a'`  
Unix: n/a  

```
How number 32767 stored in:
text file
+----------+----------+----------+----------+---------+
| 00110011 | 00110010 | 00110111 | 00110110 | 0110111 |
+----------+----------+----------+----------+---------+

binary file
+----------+----------+
| 01111111 | 11111111 |
+----------+----------+
```

## FILE OPERATIONS <a name="file_operations"></a>
* Redirection is simple: no need to open, close, or perform explicit file operations.  
However, it is limited b/c program has no control over its file.  
Solution is to use stdio.h file operations.  

### OPENING FILE <a name="opening_file"></a>
***fopen declaration:*** `FILE *fopen(const char * restrict filename, const char * restrict mode);`   
***define fopen:*** Returns either file pointer that program saves in a variable & use to perform operations on that file. Or  
Returns null pointer if file doesn't exist, can't open, in wrong place, no permission to open it, etc.  
&emsp;***arguement filename:*** string of file name/path to be opened.  
&emsp;***arguement mode:*** operations intended for the file.  
| string                  | meaning                                             |
| ----------------------- | --------------------------------------------------- |
| "r" or "rb"             | open for reading                                    |
| "w" or "wb"             | open for writing                                    |
| "a" or "ab"             | open for appending (file does not exist)            |
| "r+" or "r+b" or "rb+"  | open for reading & writing, starting at beginning   |
| "w+" or "w+b" or "wb+"  | open for reading & writing (truncate if file exist) |
| "a+" or "a+b" or "a+b"  | open for reading & writing (append if file exist)   |

```c
FILE* fp; /* fp is a pointer variable of type FILE* */
fp = fopen("input.txt", "r");
```
***NOTE:*** If binary file is being opened, then 'b' must be inlcuded in the mode string.  
***NOTE:*** C99 keyword `restrict` indicates that *file name* & *mode* should point to strings that don't share memory locations.  
***NOTE:*** Be carefule when using file path for the *file name*. Compiler treats *\t* as a character escape.  
Solutions: Use `\\` or `/`.     
  
### CLOSING FILE <a name="closing_file"></a>
***fclose declaration:*** `int fclose(FILE* stream);`  
***define fclose:*** Closes a file that it is no longer using. Arguement must be obtained from a call of *fopen* or *freopen*.  
Returns zero if file close successfully, other error code *EOR* (macro defined in stdio.h).  

<details>
<summary><b>EXAMPLE PROGRAM</b></summary>

```c
#include <stdio.h>
#include <stdlib.h>

void main(void)
{
    FILE* fp;
    
    fp = fopen("input.txt", "r");

    if (fp == NULL)
    {
        printf("File cannot open");
        exit(EXIT_FAILURE);
    }

    fclose(fp);
}
```
</details>

### ATTACHING FILE TO OPEN STREAM <a name="attaching_file_to_open_stream"></a>
***freopen declaration:*** `FILE *freopen(const char* restrict filename, const* char restrict mode, FILE* restrict stream);`  
***define freopen:*** Attaches different file to a stream that is already open.  
Normal use is to associate a file w/ one of the standard streams: stdin, stdout, stderr.  
Normal return value is third file pointer, otherwise a null pointer.  
***C99:*** if *filename* is a null pointer, then *freopnen* attempts to change stream's mode to that specified by mode parameter.  

### OBTAINING FILE NAMES FROM COMMAND LINE <a name="obtaining_file_names_from_command_line"></a>
***main function declaration:*** `int main(int argc, char* argv[]);`  
***arguemnets:*** *argc* is number of command-line arguements, *argv* array of pointers to the arguement strings.  
`argv[0]` points to program name.  
`argv[1]` through `argv[argc - 1]` point to remaining arguements.  
`argv[argc]` is a null pointer.  

<details>
<summary><b>EXAMPLE</b></summary>

`$ ./output input_1.txt input_2.txt`  
| argc | argv         |
| ---- | ------------ |
| 0    | program name | 
| 1    | input_1.txt  |
| 2    | input_2.txt  |
| 3    | null         |
</details>

### TEMPORARY FILES <a name="temporary_files"></a>
***temporary files:*** Files that exist only when program is running. C compilers often use temp files.  
****stdio.h* functions:***  
`tempfile`: Creates temp file w/ mode "wb+" that exists until closed or program ends. Returns a file pointer, otherwise null pointer.  
Drawbacks: **1.** Don't know name of file. **2.** Can not make file permanent later.  
Alternative is to create temp file using fopen.  
`tmpnam`: Generates name for a temporary file. If arguement is null pointer, then *tmpnam* stores file name in static variable & returns pointer to it.  

<details>
<summary><b>EXAMPLE</b></summary>

/* tmpname w/ parameter null pointer */
char* filename;
filename = tmpname(NULL); /* creates temp file name */

/* array */
char* filename[L_tmpnam]; /* macro in stdio.h specifies how long to make char array to hold temp file name */
filename(filename);       /* creates temp file name */
</details>

### FILE BUFFERING <a name="file_buffering"></a>
***buffering:*** Achieves acceptable performance for transferring data. Makes one large "block move" rather than tiny byte moves.  
Functions in *stdio.h* perfrom buffering automatically when it seems advantageous.  
***output buffer ex:*** Data written to stream stored in buffer area of memory. When full or stream is closed, it is flushed meaning written to actual output device.  
***input buffer ex:*** Buffer contains data from input device, read from buffer instead of input device.  
***function declarations:***  
1. `int fflush(FILE* stream);`  
Returns zero if successful, otherwise an EOR.  
***Example:*** `fflush(NULL);` flushes all buffers.  
2. `int setvbuf(FILE* restrict stream, char* restrict buf, int mode, size_t size);`  
Returns zero if successful, otherwise nonzero.   
Allows to change way stream is buffered and control size & location of buffer.  
***3rd arguement:***    
`_IOFBF`: Full buffering, data read from stream when buffer is empty or written to stream when full.  
`_IOLBF`: Line buffering, data read from stream or written to stream on line at a time.  
`_IONBF`: No buffering, data read from stream or written to stream directly, w/o a buffer.  
Function's last arguement is number of bytes in buffer.  
***Example:*** `char buffer[N]; setvbuf(stream, buffer, _IOFBF, N);`     

### MISCELLANEOUS <a name="miscellaneous"></a>
Below file management operations work w/ file names instead of file pointers. Succeed return zero. Fail return nonzero.   
***remove declaration:*** `int remove(const char *filename);`    
***rename declaration:*** `int rename(const char *old, const *new);`  
***NOTE:*** When using function *rename* be sure to close the file berfore renaming.

## FORMATTED IO <a name="formatted_ion"></a>
Following functions have ability to convert data from:  
character form to numeric form during input & numeric form to character form during output.  

### PRINTF FUNCTIONS <a name="printf_functions"></a>
Below functions write to an output stream. Both return number of characters written. Negative return indicates an error occurred.  
***fprintf declaration:*** `fprintf(FILE * restrict stream, const char * restrict format, ...);`  
Always writes to output stream stdout.  
***printf declaration:*** `printf(const char * restrict format, ...);`  
Writes to stream indicated by its first arguement. Useful to writting error messages to error stream stderr.

#### PRINTF CONVERSION SPECIFICATIONS <a name="printf_conversion_specifications"></a>
***conversion specification:*** consists of % character followed by maybe 5 items: 
```
+---+-------+-----------------+-----------+-----------------+----------------------+
| % | flags | min field width | precision | length modifier | converison specifier |
+---+-------+-----------------+-----------+-----------------+----------------------+
```
<details>
<summary><b>FLAGS</b></summary>

| flag   | meaning                                                                                                                                           |
| ------ | ------------------------------------------------------------------------------------------------------------------------------------------------- |
| -      | Left-justify w/in field (default if right-justify).                                                                                                    |
| +      | Numbers produced by signed conversion always begin w/ + or - (normally, only negative #'s are preceded by a sign).                                     |
| space  | Nonnegative #'s produced by signed conversions are preceded by a space (+ flag overwrites the space flag).                                             |
| #      | Octal numbers begin w/ a 0, nonzero hex #'s begin w/ 0x or 0X. Floating point #'s always have decimal point. Trailing #'s aren't removed from numbers printed w/ the g or G conversions.                                                                                                                                |
| (zero) | Numbers are padded w/ leading zeros up to the field width. )" lfoag is ignored if conversion is d, i, o, u ,x, or X & a precision is specified (- flag overrides 0 flag).                                                                                                                                                 |
</details>

<details>
<summary><b>MIN FIELD WIDTH (OPTIONAL)</b></summary>

Item too small will be padded (default, spaces are added to the left, thus right justifying).  
Field width is either an integer or character *.
</details>

<details>
<summary><b>PRECISION (OPTIONAL)</b></summary>

Is a period followed by an integer or character *.  
Depends on the conversion:  
d, i, o, u, x, X: min # of digits (leading 0s are added if # has fewer digits)  
a, A, e, E, f, F: # of digits after decimal point  
g, G; # of significant digits  
s: max # of bytes
</details>

<details>
<summary><b>LENGTH MODIFIER (OPTIONAL)</b></summary>

Indicates that item to be displayed has a type that's longer or shorter.
| lengh modifier | conversion specifiers | meaning                              |
| -------------- | ---------------------- | ----------------------------------- |
| hh              | d, i, o, u, x, X       | signed char, unsignedchar             |
|                 | n                      | signed char *                         |
| h               | d, i, o, u, x, X       | short int, unsigned short int         |
|                 | n                      | short int *                           |
| l               | d, i, o, u, x, X       | long int, unsigned long int           |
|                 | n                      | long int *                            |
|                 | c                      | wint_t                                |
|                 | s                      | wchar_t *                             |
|                 | a, A, e, E, f, F, g, G | no effect                             |
| ll (C99 only)   | d, i, o, u, x, X       | long long int, unsigned long long int |
|                 | n                      | long long int *                       |
| j (C99 only)    | d, i, o, u, x, X       | intmax_t, uintmax_t                   |
| n (C99 only)    | n                      | intmax_t *                            |
| z (C99 only)    | d, i, o, u, x, X       | size_t                                |
|                 | n                      | size_t *                              |
| t (C99 only)    | d, i, o, u, x, X       | ptrdiff_t                             |
|                 | n                      | ptrdiff_t *                           |
| L               | a, A, e, E, f, F, g, G | long double                           |
</details>

<details>
<summary><b>COVERSION SPECIFIER</b></summary>

| conversion specifier | meaning                                |
| -------------------- | -------------------------------------- |
| d, i                 | Converts an int value to decimal form. |
| o, u, x, X           | Converts an unsigned int value to base 8 (o), base 10(u), or base 16 (x, X). x displays hexadecimal digits a-f in lower case; X displays them in upper case. 
| f, F                 | Converts a double value to decimal form, putting decimal point in correct position. If no precision is specified, displays 6 digits after decimal point. |
| e, E                 | Converts a double value to scientific notation. If no precision is specified, displays 6 digits after decimal point. If e is chosen, exponent is preceded by the lettere; if E is chosen, the exponent is preceded by E. |
| g, G                 | g converts a double value to either f form or e form. e form is selected if the number's exponent is less than -4 or greater than or equal to the precision. Trailing zeros are not displayed (unless # flag is used); a decimal point appears only when followed by a digit. G chooses between F and E forms. |
| a, A                 | Converts a double value to hexadecimal scientific notation using the form [-]0xh. hhhhp+-d, wher [-] is an optional minus sign, the h's represent hex digits, +- is either a plus or minus sign, and d is the exponent. d is a decimal number that represents a power of 2. If no precision is specified, enough digits are displayed after the decimal point to represent the exact value of the number (if possible). a displays the hex digits a-f in lower case; A displays them in upper case. The choice of a or A also affects the case of the letters x and p. |
| c                    | Displays an int value as an unsigned character. |
| s                    | Writes the characters pointed to by the arguement. Stops writing when the number of bytes specified by the precision (if present) is reached or a null character is encountered. |
| p                    | Converts a void * value to printable form. |
| n                    | The corresponding argument must point to an object of type int. Stores in this object the number of characters written so far by this call of printf; produces no output. |
| %                    | Writes the character %. | 
</details>

<details>
<summary><b>EXAMPLES PRINTF CONVERSION SPECIFICATIONS</b></summary>

<details>
<summary><b>EFFECTS OF FLAGS ON %d CONVERSION</b></summary>

| converison specification | result of applying conversion 123 | result of applying conversion to -123 |
| ------------------------ | --------------------------------- | ------------------------------------- |
| %8d                      | *****123                          | ****-123                              |
| %-8d                     | 123*****                          | -123****                              |
| %+8d                     | ****+123                          | ****-123                              |
| % 8d                     | *****123                          | ****-123                              |
| %08d                     | 00000123                          | -0000123                              |
| %-+8d                    | +123****                          | -123****                              |
| %- 8d                    | *123****                          | -123****                              |
| %+08d                    | +0000123                          | -0000123                              |
| % 08d                    | *0000123                          | -0000123      
</details>

<details>
<summary><b>EFFECTS OF # FLAG</b></summary>

| conversion specification | result of applying conversion to 123 | result of applying conversion to 123.0 |
| ------------------------ | ------------------------------------ | -------------------------------------- |
| %8o                      | ******173                            |                                        |
| %#8o                     | *****0173                            |                                        |
| %8x                      | *******7b                            |                                        |
| %#8x                     | *****0x7b                            |                                        |
| %8X                      | *******7B                            |                                        |
| %#8X                     | *****0X7B                            |                                        |
| %8g                      |                                      | *****123 ?                             |
| %#8g                     |                                      | *123.000 ?                             |
| %8G                      |                                      | *****123 ?                             |
| %#8G                     |                                      | *123.000 ?                             |
</details>

<details>
<summary><b>EFFECTS OF MIN FIELD WIDTH & PRECISION ON %s CONVERSION</b></summary>

| conversion specifier | result of applying conversion to "bogus" | result of applying conversion to "buzzword" |
| -------------------- | ---------------------------------------- | ------------------------------------------- |
| %6s                  | *bogus                                   | buzzword                                    |
| %-6s                 | bogus*                                   | buzzword                                    |
| %.4s                 | bogu                                     | buzz                                        |
| %6.4s                | **bogu                                   | **buzz                                      |
| %-6.4s               | bogu**                                   | buzz**
</details>

<details>
<summary><b>EFFECTS OF %g CONVERION</b></summary>

| number             | result  of applying %.4g conversion to number |
| ------------------ | --------------------------------------------- |
| 123456.            | 1.235e+05                                     |
|  12345.6           | 1.235e+04                                     |
|   1234.56          | 1235                                          |
|    123.456         | 123.5                                         |
|     12.3456        | 12.35                                         |
|      1.23456       | 1.235                                         |
|       .123456      | 0.1235                                        |
|       .0123456     | 0.01235                                       |
|       .00123456    | 0.001235                                      |
|       .000123456   | 0.0001235                                     |
|       .0000123456  | 1.1235e-05                                    |
|       .00000123456 | 1.1235e-06                                    |
</details>
</details>

### SCANF FUNCTIONS <a name="scanf_functions"></a>
Below functions read data items from input stream using a format string to indicate layout of input. Return prematurely if input/matching/encoding failure occurs.  
If successful, both return # of data items that were read & assigned to objects, otherwise EOF.  
They are *pattern-matching* functions. Format string represents a pattern that scanf attempts to match as it reads input.    
***fscanf declaration:*** `fscanf(FILE * restrict stream, const char * restrict format, ...);`  
Always reads from stream indicated by its first arguement.  
***scanf declaration:*** `scanf(const char * restrict format, ...);`  
Always reads from stdin.  
***format string may contain 3 things:***  
1. ***conversion specifications:*** Most CS skip white-space characters at beginning of input item (exceptions: %c & %n). However, they never skip *trailing* white-space characters.   
Ex. `*123^` %d consumes *, 1, 2, 3 but leaves ^ unread (* = space, ^ = new line).  
2. ***white-space characters:*** White-space characters in format string match white-space characters in input stream.  
3. ***non-white space characters:*** Non-white space character other than % matches same character in input stream. 

#### SCANF CONVERSION SPECIFICATIONS <a name="scanf_conversion_specifications"></a>
Consist of % followed by items listed below as shown:  
```
+-----------------------------+-----------------+-----------------+
| assignment suppresssion (*) | max field width | length modifier |
+-----------------------------+-----------------+-----------------+
```

<details>
<summary><b>LENGTH MODIFIERS</b></summary>

| length modifier | conversion specifier   | meaning                                   |
| --------------- | ---------------------- | ----------------------------------------- |
| hh              | d, i, o, u, x, X, n    | signed char *, unsigned char *            |
| h               | d, i, o, u, x, X, n    | short int *, unsigned short int *         |
| l               | d, i, o, u, x, X, n    | long int *, unsigned long int *           |
                  | a, A, e, E, f, F, g, G | double *                                  |
                  | c, s, or [             | wchar_t *                                 |
| ll              | d, i, o, u, x, X, n    | long long int *, unsigned long long int * |
| j               | d, i, o, u, x, X, n    | intmax_t *, uintmax_t *                   |
| z               | d, i, o, u, x, X, n    | size_t *                                  |
| t               | d, i, o, u, x, X, n    | ptrdiff_t *                               |
| L               | a, A, e, E, f, F, g, G | long double *                             |
</details>

<details>
<summary><b>CONVERSION SPECIFIER</b></summary>

| conversion specifier   | meaning                                                                       |
| ---------------------- | ----------------------------------------------------------------------------- |
| d                      | Matches a decimal integer; corresponding arguement assumed to have type int * |
| i                      | Matches an integer; corresponding arguement assumed to have type int *. Integer assumed to be in base 10 unless it begines w/ a 0 (octal) or 0x or 0X (hexadecimal). |
| o                      | Matches w/ octal integer; corresponding arguement assumed to have type unsigned int * |
| u                      | Matches a decimal integer; corresponding arguement assumed to have type unsigned int * |
| x, X                   | Matches a hexadecimal integer; corresponding arguement assumed to have type unsigned int * |
| a, A, e, E, f, F, g, G | Matches a floating point number; corresponding arguement assumed to have type flaot *. C99 number can be infinity or NaN. | 
| c                      | Matches n characters, where n is max field width, or one character if no field width is specified. Corresponding arguement assumed to be a pointer to a character array (or a character object, if no field width is specified). Doesn't add null character at the end. |
| s                      | Matches sequence of non-white-characters, then adds null character at the end. Corresponding arguement assumed to be a pointer to character array.
| [                      | Matches nonempty sequence of characters from a scanset, then add null character at the end. Corresponding arguement assumed to be pointer to character array. |
| p                      | Matchesa pointer value in form that ...printf would have written it. Corresponding arguement assumed to be pointer to void * object. |
| n                      | Corresponding argument must point to an object of type int. Stores in this object number of characters read so far by this call. No input is consumed & return value of ...scanf isn't affected. |
| %                      | Matches the character %. |
</details>

<details>
<summary><b>EXAMPLES</b></summary>

<details>
<summary><b>GROUP 1</b></summary>

Strikeout means characters are consumed by the call.  
| scanf call                     | input           | variables    |
| ------------------------------ | --------------- | ------------ |
| `n = scanf("%d%d", &i, &j);`   | <s>12*</s>,*34^ | n: 1         |
|                                |                 | i: 12        |
|                                |                 | j: unchanged |
| `n = scanf("%d,%d", &i, &j);`  | <s>12</s>*,*34^ | n: 1         |
|                                |                 | i: 12        |
|                                |                 | j: unchanged |
| `n = scanf("%d ,%d", &i, &j);` | <s>12*,*34^</s> | n: 2         |
|                                |                 | i: 12        |
|                                |                 | j: 34        |
| `n = scanf("%d, %d", &i, &j);` | <s>12</s>*,*34^ | n:  1        |
|                                |                 | i: 12        |
|                                |                 | j: unchanged |
</details>

<details>
<summary><b>GROUP 2</b></summary>

| scanf call                             | input                | variables    |
| -------------------------------------- | -------------------- | ------------ |
| `n = scanf("%*d%d", &i();`             | <s>12*34</s>^        | n:
|                                        |                      | i:
| `n = scanf("%*s%s", str);`             | <s>My*Fair</s>*Lady^ | n:
|                                        |                      | str:
| `n = scanf("%1d%2d%3d", &i, &j, &k);`  | <s>12345</s>^        | n:
|                                        |                      | i:
|                                        |                      | j:
|                                        |                      | k:
| `n = scanf("%2d%2s%2d", &i, str, &j);` | <s>123456</s>^       | n:
|                                        |                      | i:
|                                        |                      | str:
|                                        |                      | j:
</details>

<details>
<summary><b>GROUP 3</b></summary>

| scanf call                          | input                | variables    |
| ----------------------------------- | -------------------- | ------------ |
| `n = scanf("%i%i%i", &i, &j. &k");` | <s>12*012*0x12</s>^  | n:
|                                     |                      | i:
|                                     |                      | j:
|                                     |                      | k:
| `n = scanf("%[0123456789]", str);`  | <s>123</s>abc^       | n:
|                                     |                      | str:
| `n = scanf("%[0123456789]", str);`  | abc123^              | n:
|                                     |                      | str:
| `n = scanf("%[^0123456789]", str);` | <s>abc</s>123^       | n:
|                                     |                      | str:
| `n = scanf("%*d%d%n", &i, &j);`     | <s>10*20</s>*30^     | n:
|                                     |                      | i:
|                                     |                      | j:
</details>
</details>

## DETECTING END-OF-FILE & ERROR CONDITIONS <a name="detecting_end_of_file_and_error_conditions"></a>
Every stream has 2 failure indicators: ***error indicator*** & ***end-of-file indicator***. They are cleared when stream is opened. Remains set unless explicitely cleared.  
3 possibilities where scanf returns less than n data items:  
1. **end-of-file:** function encountered end-of-file before matching format string completely. Sets end-of-file indicator.
2. **read error:** function unable to read characters from the stream. Sets error indicator.
3. **matching failure:** data item in wrong format.  

***clearerr declaraton:*** `void clearerr(FILE *stream);`. Clears both failure indicators.  
***feof declaration:*** `int feof(FILE *stream);`.  Returns nonzero value if end-of-file indicator is set, otherwise zero.  
***ferror declaration:*** `int ferror(FILE *stream);`. Returns nonzero value if error indicator is set, otherwise zero.  

## CHARACTER IO <a name="character_io"></a>
Below functions read/write single characters.  
Work equally w/ text streams & binary streams.  
Functions treat characters as values of type int, not char.  
Because, input functions indicate an end-of-file condition by returning EOF, which is a negtive int constant.  

### OUTPUT FUNCTIONS <a name="char_output_functions"></a>
***fputc declaration:*** `int fputc(int c, FILE *stream);`. Writes character to arbitrary stream. Usually implemented as a function.  
***putc declartion:*** `int putc(int c, FILE *stream);`. Writes character to arbitrary stream. Usualy implemented as macro (as well as a function).   
***putchar declaration:*** `int putchar(int c);`. Writes one character to stdout stream.  Usually a macro (#define putchar(c) putc((c), stdout)).  
***NOTE:*** If error occurs, all 3 functions set error indicator for the stream & return EOF, otherwise return character that was written.  
***NOTE:*** Programmers usually prefer putc over fputc.  

### INPUT FUNCTIONS <a name="char_input_functions"></a>
***fgetc declaration:*** `int fgetc(FILE *stream);`. Read character from arbitrary stream. Usually implmented as a function.  
***getc declaration:*** `int getc(FILE *stream);`. Read character from arbitrary stream. Usually implemented as macro (as well as a function).  
***getchar declaration:*** `int getchar(void);`. Reads character from stdin stream. Usually implemented as macro (#define getchar() getc(stdin)).  
***NOTE:*** All 3 functions treat character as unsigned char (then converted to int type before it's returned).  
IMPORTANT - Always store return value in an int variable.  
***NOTE:*** If error occurs, all 3 functions set error indicator for the stream & return EOF, otherwise return character that was written.  
***NOTE:*** Programmers usually prefer getc over fgetc since getc tends to be in macro form which is a bit faster.  
Thus, never return negative value other than EOF.  
***ungetc declaration:*** `int ungetc(int c, FILE *stream);`. Pushes back character read from a stream & clears stream's EOF indicator. Handy for a "lookahead" character.  
Returns pushed back character, otherwise returns EOF if attempt is made to push back EOF or push back more characters than implementation allows.  
Only first call is guaranteed to succeed. Calling file-positioning function (fseek, fsetpos, rewind) causes pushed-back characters to be lost.

## LINE IO <a name="line_io"></a>
Below functions read/write lines.
Used mostly w/ text streams (also binary too).

### OUTPUT FUNCTIONS <a name="line_output_functions"></a>
***fputs declaraton:*** `int fputs(const char * restrict s, FILE * restrict stream);`. Indicates output stream to be written. Does not add new-line character at end.   
***puts declaration:*** `int puts(const char *s);`. Writes string of characters to stdout. Always adds new-line character at end.  
***NOTE:*** Both functions return EOF if write error occurs, otherwise returns nonnegative number.

### INPUT FUNCTIONS <a name="line_input_functions"></a>
***fgets declaration:*** `char *fgets(char * restrict s, int n, FILE * restrict stream);`. Read from any stream. Safer than gets. Ex: `fgets(str, sizeof(str), <fp> or <stdin>);`.  
Reads characters until reaches 1st new-line character or sizeof(str) - 1 characters. If reads new-line character, it is stored along w/ other characters.  
***gets declaration:*** `char *gets(char *s);` Reads line from stdin.  
Reads characters one by one, storing them in the array pointed to by s, until it reads new-line character (which it discards).  
***NOTE:*** Both functions return null pointer if read error occurs or they reach end of input stream before storing any chacacters.  
Otherwise, both return 1st argument, which points to array in whihc input was stored.  
***NOTE:*** Both functions store null character at end of the string.  
***NOTE:*** Use fgets instead of gets. gets has the possibility of stepping outside bounds of receiving array.  

## BLOCK IO <a name="block_io"></a>
Below functions read/write large blocks of data in single step.  
Used primarily w/ binary streams (text streams too).  
***fwrite declaration:*** `size_t fwrite(void * restrict ptr, size_t size, size_t nmemb, FILE * restrict stream);`. Copies array from memory to a stream.  
>***1st argument:*** array's address  
>***2nd argument:*** size of array element in bytes  
>***3rd argument:*** number of elements to write  
>***4th argument:*** file pointer indicating where data should be written  
>***Ex:*** `fwrite(a, sizeof(a[0]), sizeof(a) / sizeof(a[0]), fp);`  
>***Return:*** Number of elements written.

***fread declaration:***  `size_t fread(void * restrict ptr, size_t size, size_t nmemb, FILE * restrict stream);`. Reads elements of an array from a stream. 
>Same aruments in fwrite.  
>***Ex:*** `n = fread(a, sizeof(a[0]), sizeof(a) / sizeof(a[0]), fp);`  
>***Return:*** value should equal the 3rd argument unless end of input file was reached or read error occurred. 

***NOTE:*** Both functions work with arrays, variables, structs

## FILE POSITIONONG <a name="file_positioning"></a>
**file position:** Every stream has one. When file is opened, file position set at beginning of file (append mode may be at beginning or end).  
When a read/write operation performs, the file position moves auto. in a sequential manner.
The Below functions to move file position anywhere on the stream. Best used though on binary streams.

***fgetpos declaration:*** `int fgetpos(FILE * restrict stream, fpos_t * restrict pos);` Stores file position associated w/ stream in pos variable.   
***fsetpos declaration:*** `int fsetpos(FILE *stream, const fpos_t *pos);` Sets file position for stream to be value stored in pos.  
***NOTE:*** Both functions are used when working w/ large files b/c they use values of type fpos_t.  
***NOTE:*** Both functions return 0, otherwise nonzero when failed.  

***fseek declaration:*** `int fseek(FILE *stream, long int offset, int whence);`  
2nd argument is byte count, 3rd argument specifies file position at beginning file (SEEK_SET), current file position (SEEK_CUR), or end of file (SEEK_END).  
Normally returns zero, otherwise nonzero value.  
***ftell declaration:*** `long int ftell(FILE *stream);` Returns current file position, otherwise -1L & stores error code in errno.  
***rewind declaration:*** `void rewind(FILE *stream);` Sets file position at beginning. Similar to `fseek(fp, 0L, SEEK_SET)`.  

## STRING IO <a name="string_io"></a>
Functions below have nothing to do w/ streams or files. They allow reading/writing of data using a string as though it was a stream.  

### OUTPUT FUNCTIONS <a name="string_output_functions"></a>
***sprintf declaration:*** `int sprintf(char * restrict s, const char * restrict format, ...);`   
Similar to printf & fprintf, except it writes output into character array instead of a stream.  
Ex: `sprintf(date, "%d/%d/%d", 11, 24, 1998);` sprintf adds null character at thed end.  
Returns # of characters stored (not counting null character), otherwise negative value.  
***snprintf declaration:*** `int snprintf(char * restrict s, size_t n, const char * restrict format, ...);` Writes at most n characters counting the null character.  
Returns # of chracters written w/ no length restriction, otherwise negative number.

### INPUT FUNCTIONS <a name="string_input_functions"></a>
***sscanf declaration:*** `int sscanf(const char * restrict s, const char * restrict format, ...);`  
Similar to scanf & fscanf, exept it reads from a string instead of a stream.  
Ex. might use fgets to obtain line of input, then use sscanf for futher processing.  
***NOTE:*** Advantage, can examing input line as many times as needed unlike scanf & fscanf
