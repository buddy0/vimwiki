# LIBRARY SUPPORT FOR NUMBERS & CHARACTER DATA

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [THE STRING HEADER](#the_string_header)  
    a. [COPYING FUNCTIIONS](#copying_functions)  
    b. [CONCATENATION FUNCTIONS](#concatenation_functions)  
    c. [COMPARISON FUNCTIONS](#comparison_functions)  
    d. [SEARCH FUNCTIONS](#search_functions)  
    e. [MISCELLANEOUS FUNCTIONS](#miscellaneous_functions)  
</details>

## THE <string.h> HEADER: STRING HANDLING <a name="the_string_header"></a>
This header provides 5 kinds of functions:  
1. Copying functions
2. Concatenation functions
3. Comparison functions
4. Search functions
5. Miscellaneous functions

***mem functions:*** Deal w/ blocks of memory instead of strings. Thus, are not necessarily null terminated.  
These memory blocks can contain data of any type, hence the type *void instead of *char.  

### COPYING FUNCTIIONS <a name="copying_functions"></a>
Below functions: 
* copy characters (bytes) from one place in memory (source) to another (destination).  
* 1st argument is always points to destination & 2nd to source.  
* return 1st argument.

***memcpy declaration:*** `void *memcpy(void * restrict s1, const void * restrict s2, size_t n);`.  
Copies n characters from s to d. If s & d overlap, returns underfined.  
***memmove declaraton:*** `void *memmove(void *s1, const void *s2, size_t n);`.  
Similar to memcpy except that it can overlap.  
***strcpy declaraton:*** `char *strcpy(char * restrict s1, const char * restrict s2);`.  
Copies null terminated string from s to d. Not guaranteed to work if s & d overlap.  
***strncpy declaraton:*** `char *strncpy(char * restrict s1, const char * restrict s2, size_t n);`.  
Similar to strcpy, but won't more than n characters. Not guaranteed to work if s & d overlap.  
If null character is encountered in the source, then adds null characters to the destination until it has written total of n characters.  

***NOTE:*** memcpy, memmove, & strncpy do not require null-terminated string.  
strcpy only works w/ null-terminated strings.

### CONCATENATION FUNCTIONS <a name="concatenation_functions"></a>
***strcat declaration:*** `char *strcat(char * restrict s1, const char * restrict s2);`.  
Appends 2nd argument to end of 1st argument.  
Both arguments must be null terminated strings & inserts null character at end of concatenated string.  
Returns 1st argument.    
***strncat declaration:*** `char *strncat(char * restrict s1, const char * restrict s2, size_t n);`.  
Similar to strcat except limits number of characters it will copy in 2nd argument.  

### COMPARISON FUNCTIONS <a name="comparison_functions"></a>
Below functions:  
* Compare contents of 2 character arrays. Pass pointers to character arrays.
* Characters in first array compared 1 by 1 w/ characters in 2nd array.
* Return once mismatch is found.  
Returns either negative, zero, or positive integer depending whether stopping character in 1st array is <, =, or > than stoppping character in 2nd array.  
* Difference deal w/ when to stop comparing characters if no mismatch is found.

***memcmp declaration:*** `int memcmp(const void *s1, const void *s2, size_t n);`.  
Limits number of comparisons performed. Pays no attention to null characters.  
***strcmp declaration:*** `int strcmp(const char *s1, const char *s2);`.  
Stops when reaches null character in either array.  
***strncmp declaration:*** `int strncmp(const char *s1, const char *s2, size_t n);`.  
Stops when n comparisons have been performed or null character is reached in either array.  
 
Below functions used if locale is needed.  
***strcoll declaration:*** `int strcoll(const char *s1, const char *s2);`.  
Similar to strcmp, but outcome depends on current locale.  
***strxfrm declaration:*** `size_t strxfrm(char * restrict s1, const char * restrict s2, size_t n);`.  
Alternative to strcoll that is faster & able to change locale w/o affecting the comparison.  
Transforms 2nd argument into result in array pointed to by 1st argument. 3rd argument limits number of characaters written to array, including null character.  
Returns length of transformed string.   

### SEARCH FUNTIONS <a name="search_functions"></a>
***strchr declaration:*** `char *strchr(const char *s, int c);`.  
Searches string for a character.  
Returns pointer to 1st occurence (2nd if s + 1)), otherwise null pointer.  
***memchar declaration:*** `void *memchr(const void *s, int c, size_t n);`.  
Similar to strchr, but stops searching after set number of characters instead of at 1st null characater.  
Limits number of characters it can examine.  
Returns pointer to 1st occurence (2nd if s + 1)), otherwise null pointer.  
***strrchr declaration:*** `char *strrchr(const char *s, int c);`.  
Similar to strchr, but searches string in reverse order.  
***strpbrk declaration:*** `char *strpbrk(const char *s1, const char *s2);`.  
More general than strchr. Returns pointer to leftmost character in 1st argument that matches any character in 2nd argument.  
***strspn declaration:*** `size_t strspn(const char *s1, const char *s2);`.  
Return integer of size_t, representing position w/in string.  
Returns 1st character not in the set.  
***strcspn declaration:*** `size_t strcspn(const char *s1, const char *s2);`.  
Return integer of size_t, representing position w/in string.  
Returns 1st character in the set.  
***strstr declaration:*** `char *strstr(const char *s1, const char *s2);`.  
Searches 1st argument for a match w/ 2nd argument.  
Returns pointer to 1st occurence of the search string, other wise null character if can not locate string.  
***strtok declaration:*** `char *strtok(char * restrict s1, const char * restrict s2);`.  
Searches a string for a token (sequence of characters that doesn't include certain delimiting characters).  
Marks end of token by storing null character in argument 1 just after last character in token. Returns pointer to 1st character in token.

### MISCELLANEOUS FUNCTIONS <a name="miscellaneous_functions"></a>
***memset declaration:*** `void *memset(void *s, int c, size_t n);`.  
Stores multiple copies of a character in specified area of memory.    
Returns 1st aguement (pointer).   
***strlen declaration:*** `size_t strlen(const char *s);`.  
Returns length of string, not counting null character.    
