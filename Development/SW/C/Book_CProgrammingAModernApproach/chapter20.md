# LOW-LEVEL PROGRAMMING

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [BITWISE OPERATORS](#bitwise_operators)  
    a. [BITWISE SHIFT OPERATORS](#bitwise_shift_operators)  
    b. [BITWISE COMPLEMENT, *AND*, EXCLUSIVE *OR*, & INCLUSIVE *OR*](#bitwise_complement)  
    c. [USING BITWISE OPERATORS TO ACCESS BITS](#using_bitwise_operators_to_access_bits)  
    d. [USING BITWISE OPERATORS TO ACCESS BIT-FIELDS](#using_bitwise_operators_to_access_bit_fields)
2. [BIT-FIELDS IN STRUCTURES](#bit_fields_in_structures)
3. [OTHER LOW-LEVEL TECHNIQUES](#other_low_level_techniques)  
    a. [USING POINTERS AS ADDRESSES](#using_pointers_as_addresses) **TODO**  
    b. [`volatile` TYPE QUALIFIER](#volatile_type_qualifier)
</details>

## BITWISE OPERATORS <a name="bitwise_operators"></a>
**bitwise operators:** Operate at the bit level on integer data. 6 total: 2 bitwise shift, bitwise complement, bitwise *and*, bitwise exclusie *or*, & bitwise inclusive *or*. 

### BITWISE SHIFT OPERATORS <a name="bitwise_shift_operators"></a>
![Table of Bitwise Shift Operators](Images/bitwise_shift_operators.png)  
***bitwise shift operators:*** Transform binary representation of an integer by shifting its bits to the left or right. 
Operands may be of any integer type. Integer promotions are performed on both operands. Result has type of left operand after promotion.  
**Ex.** `i << j` means bits in *i* are shifted ***left*** by *j* places. Each bit shifted off at the left end, a *0* enters at right.  
**Ex.** `i >> j` means bits in *i* are shifted ***right*** by *j* places. If *i* is an unsigned type or nonnegative, *0* added to left as needed. If *i* is negative, result is *implementation-defined* either adding *0* or *1*.  
***portability tip:*** Best to perform shifts only on unsigned numbers.  
***NOTE:*** Neither operator modifies its operands. Modifing a vairable by shifting can be done through compund assignment operators *<<=* & *>>=*.  
***NOTE:*** Lower precedence than arithemetic operators.  

### BITWISE COMPLEMENT, *AND*, EXCLUSIVE *OR*, & INCLUSIVE *OR*  <a name="bitwise_complement"></a>
![Table of Remaining Bitwise Operators](Images/bitwise_remaining_operators.png)  
All operators perform *boolean operations* on all bits in operands. Table shows precedence as well w/ `~` as highest & `|` as lowest.  
`~`: Unary. 0s replace by 1s and 1s replace by 0s.  
`&`: Binary. Performs boolean *and* operation in all bits in both operands.  
`^`: Binary. Perform boolean *or* operation; produces a **0** when both operands have a 1 bit.  
`|`: Binary. Perform booelan *or* operation; produces a **1** when both operands have a 1 bit.  
***NOTE:*** Don't confuse bitwise operators `&` and `|` w/ logical operators `&&` and `||`.  

### USING BITWISE OPERATORS TO ACCESS BITS <a name="using_bitwise_operators_to_access_bits"></a>
**Ex.** `unsigned short i; // 16-bits`  
***setting a bit:*** Set bit 4 by using a *mask* w/ 1 in bit 4.  
`i |= 0x0010` OR `i |= 1 << j /* sets bit j */`.  
***clearing a bit:*** Clear bit 4 by using a *mask* w/ 0 in bit 4.  
`i &= ~0x0010` OR `i &= ~(1 << j) /* clears bit j */`.  
***testing a bit:*** Test if bit 4 is set.  
`if(i & 0x0010) /* tests bit 4 */` OR `if (i & 1 << j) /* tests bit j */`  
***NOTE:*** Easier if we five a certain bits names such as `#define BLUE 1`. That way:  
`i |= BLUE; /* sets BLUE bit */`.  
`i &= ~BLUE; /* clears BLUE bit */`.    
`if (i & BLUE) /* tests BLUE bit */`.  

### USING BITWISE OPERATORS TO ACCESS BIT-FIELDS <a name="using_bitwise_operators_to_access_bit_fields"></a>
**bit-field:** Group of several consecutive bits.  
***modifying a bit-field:*** Requires a bitwise *and* to clear the bit-field, followed bit bitwise *or* to store new bits in the field.  
**Ex.** `i = i & ~0x0070 | 0x0050; /* stores 101 in bits 4-6 */`. OR `i = (i & ~0x0070) | (j << 4); /* can drop parentheses *.`.  
***retrieving a bit-field:*** 2 examples: 
If bit-field is at right end of number. **Ex.** `j = i & 0x0007; /* retrieves bits 0-2 */`.  
If bit-field isn't at right end of number. **Ex.** `j = (i >> 4) & 0x0007; /* retrieves bits 4-6 */`

## BIT-FIELDS IN STRUCTURES <a name="bit_fields_in_structures"></a>
Structures whose members represent bit-fields.  
Type of a bit-field must be integer: *int*, *unsigned int*, or *signed int*.  
Restriction of bit-fields in a structure is they don't have addresses in the usual sense.
"We could have used the bitwise operators to accomplish same effect (prolly faster). However, having a readable program more important than gaining few microseconds."
***how bit-fields are stored:*** Pg. 517-518.  
```c
struct file_date 
{
    unsigned int day: 5;
    unsigned int month: 4;
    unsigned int year: 6;
};
```
![Table representing Bit-Fields in Structures](Images/bitfields_structures.png)  

## OTHER LOW-LEVEL TECHNIQUES <a name="other_low_level_techniques"></a>
***using unions to provide mutliple views on data:*** Pg. 519-520
**big-endian:**  Most significant byte stored first (lower adddress).  
**litle-edian:** Least significant byte stored last (highest address).  
![Picture of Endianess](Images/endian_example.png)  

### USING POINTERS AS ADDRESSES <a name="using_pointers_as_addresses"></a>

### `volatile` TYPE QUALIFIER <a name="volatile_type_qualifier"></a>
**volatile:** Value stored at such a location can change as a program is running, even though the program itself isn;t storing new values there.  
Informs the compiler that data in a program is volatile.  
Typically appears in declaration of a pointer vairable that will point to a volatile memory location. **Ex.** `volatile BYTE *p; /* p will point to a volatile byte */`  
***why:*** Suppose that *p* points to memory location containg recent character typed at user's keyboard. This location is volatile: its value changes each time user enters a character.  
We might use following loop to obtain characters from a keyboard & store them in a buffer array: 
```c
while(buffer not full)
{
    wait for input;
    buffer[i] = *p;
    if buffer[i++] == '\n')
        break;
}

/* Compiler might notice that this loop changes neither p nor *p, so it could optimize program by altering *p is fetched just once. */
store *p in a register;
while (buffer not full)
{
    wait for input;
    buffer[i] = value stored in register;
    if (buffer[i++] == '\n')
        break;
}
```
* Optimized program will fill buffer many copies of same character!
* Declaring that *p* points to a volatile data avoids this by telling compiler that *p must be fetched from memory each time it's needed!  
