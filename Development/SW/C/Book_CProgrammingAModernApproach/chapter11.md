# POINTERS

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [POINTER VARIABLES](#pointer_variables)  
    a. [DECLARING POINTER](#declaring_pointer)
2. [ADDRESS & INDIRECTION OPERATORS](#address_and_indirection_operators)
3. [POINTER ASSIGNMENT](#pointer_assignment)
4. [POINTERS AS ARGUMENTS](#pointer_as_arguments)
5. [POINTERS AS RETURN VALUES](#pointer_as_return_values)
</details>

## POINTER VARIABLES <a name="pointer_variables"></a>
Main memory is normally divided into **bytes:** 8 bits.  
Each byte has a unique **address:** distinguish from other bytes in memory.  
Executable program consists of both code (machine instructions from statements) and data (variables). Each variable occupies 1 or more bytes of memory; address of the 1st byte is address of variable.  
![Table of Memory with Address & Contents](Images/memoryAddressContents.png)  
**pointer variable:** Variable that can store an address.  
***purpose:*** Indirect addressing; use pointer name as an alias to an object.  

### DECLARING POINTER <a name="declaring_pointer"></a>
`int *p; // p(name of pointer variable) is pointing to an object of type int`.  
Just like a normal variable, except the pointer variable name is preceded by an `*`.    
Informs compiler to allocate memory space to hold an address.   
Pointers point to **objects** (not always variables) b/c *p* might point to an area of memory that doesn't belong to a variable.  
**referenced type:** C requires that every pointer variable point only to objects of a particular type.  

## ADDRESS & INDIRECTION OPERATORS <a name="address_and_indirection_operators"></a>
**address of operator `&`:** Provides address of the object.  
Declaring pointer variable sets aside space for pointer, but doesn't point to an object (no assignment is done).    
Its crucial to initialize *p* before we use it.  
***assignment:*** Assign pointer variable an lvalue, address by using the *&*. Ex. `int i; p = &i;`.   
Now *p* points to to *i*.  ![Picture of p Pointing to I](Images/pPointsToI.png)  
**indirection operator `*`:** Gains access to the pointed object's stored value. Ex. `printf("%d\n", *p)`.   
Therefore, `*p` is an alias for *i*. `*p` is an lvalue so assignment is legal also. Ex. `*p = 2; //i=2`.  
`*` acts like an inverse to `&`. So `j = *&i; // j = i`.  

## POINTER ASSIGNMENT <a name="pointer_assignment"></a>
C allows use of assignment operator to copy pointers, provided they have the same type. Ex. `int *q; q = p;`.  
Any number of pointer variables may point to the same object.  

## POINTERS AS ARGUMENTS <a name="pointer_as_arguments"></a>
**"** Instead of passing a variable *x* as argument to a function, we'll supply *&x*, pointer to *x*. Corresponding parameter will be *p* to be a pointer.  
When function is called, *p* will have value *&x*, hence `*p` (object that *p* points to) will be an alias for *x*.   
EACH APPEARANCE OF `*p` IN BODY OF FUNCTION WILL BE INDIRECT REFERENCE TO *x*, ALLOWING FUNCTION TO BOTH READ *x* AND MODIFY IT. **"**  
| statement                           | legal         | illegal                  | comment                                                   |
| ----------------------------------- | ------------- | ------------------------ | --------------------------------------------------------- |
| `int func(const int *ptr){}`        | `ptr = &var;` | `*p = 0;`                | Pointed to object's value cannot be changed.              |
| `int func(int * const ptr){}`       | `*p = 2;`     | `ptr = &var2`            | The pointer variable's value (address) cannot be changed. |
| `int func(const int * const ptr){}` | na            | `ptr = &var;`, `*p = 0;` | Both objects values cannot be changed.                    |

## POINTERS AS RETURN VALUES <a name="pointer_as_return_values"></a>
We can not only write pointers to functions, but also write functions that *return* pointers.  
```c
int *max(int *ptr_1, int *ptr_2){  /* max is a function returnting a pointer to an object of type int */
    if (*ptr_1 > *ptr_2)
        return ptr_1;
    else
        return ptr_2;
}

int main(void){
    int *p, var_1, var_2;
    p = max(&var_1, &var_2)
}
```

**"** A function can also return a pointer to an external variable or to a local variable that's been declared *static*. **"**  

<details>
<summary><b>N:</b> Never return a pointer to an <i>automatic</i> local variable.</summary>

```c
/* ERROR */
int *foo(void);

void main(void)
{
    int *ptr_main = foo();
    printf("%d\n", *ptr_main);
    *ptr_main = 2;
    printf("%d\n", *ptr_main);
}

int *foo(void)
{
    int var_foo;
    return (&var_foo); /* warning: function returns address of local variable [-Wreturn-local-addr] */
}
```
```c
/* ? NO ERROR ? */
int *foo(void);

void main(void)
{
    int *ptr_main = foo();
    printf("%d\n", *ptr_main);
}

int *foo(void)
{
    int var_foo;
    int *ptr_foo = &var_foo;
    *ptr_foo = 1;
    return (ptr_foo); 
}
```
</details>
 



