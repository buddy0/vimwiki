# BASIC TYPES

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INTEGER TYPES](#integer_types)  
    a. [INTEGER CONSTANTS](#integer_constants)
2. [FLOATING TYPES](#floating_types)  
    a. [FLOATING CONSTANTS](#floating_constants)
3. [CHARACTER TYPES](#character_types)  
    a. [CHARACTER CONSTANTS](#character_constants)
4. [ARITHMETIC TYPES](#arithmetic_types)
5. [ESCAPE SEQUENCES](#escape_sequences)
6. [TYPE CONVERSION](#type_conversion)  
    a. [USUAL ARITHMETIC CONVERSIONS](#usual_arithmetic_conversions)  
    b. [CONVERSION DURING ASSIGNMENT](#conversion_during_assignment)  
    c. [CASTING](#casting)
7. [TYPE DEFINITIONS](#type_definitions)
8. [SIZEOF OPERATOR](#sizeof_operator)
</details>

## INTEGER TYPES <a name="integer_types"></a>
**integer type:** Whole number.  
***6 combinations:*** signed & unsigned short int, signed & unsigned long int, **C99** signed & unsigned long long int.   
***notes:*** Allowed to drop *int* and *signed*. Range of values vary from machine and compiler. Range determined by header file *<limites.h>*.    
***printf/scanf conversion specificers:*** *d*, *u*, *o* and *x*, and putting *h*, *l*, and *ll* in front represents the notation. 

 | signed or unsigned | meaning            | 2<sub>7</sub> | 2<sub>6</sub> | 2<sub>5</sub> | 2<sub>4</sub> | 2<sub>3</sub> | 2<sub>2</sub> | 2<sub>1</sub> | 2<sub>0</sub> | equals               |
 | ------------------ | ------------------ | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- | ------------- | -------------------- |
 | signed             | **smallest value** | 1             | 0             | 0             | 0             | 0             | 0             | 0             | 0             |                      |
 |                    | 1s complement      | 0             | 1             | 1             | 1             | 1             | 1             | 1             | 1             |                      |
 |                    | 2s complement      | 1             | 0             | 0             | 0             | 0             | 0             | 0             | 0             | 2<sub>7</sub> = -128 |
 |                    | **largest value**  | 0             | 1             | 1             | 1             | 1             | 1             | 1             | 1             | 2<sup>6</sup> + ... + 2<sup>0</sup> *OR* 2<sup>7</sup> - 1 = 127 |
 | unsigned           | **smallest value** | 0             | 0             | 0             | 0             | 0             | 0             | 0             | 0             | 0                    |
 |                    | **largest value**  | 1             | 1             | 1             | 1             | 1             | 1             | 1             | 1             | 2<sup>7</sup> + ... + 2<sup>0</sup> or 2<sup>8</sup> - 1 = 255 |

| type                             | # of bits         | smallest value (meaning)                      | largest value (meaning)                           | force constant to be this type (case insevetive) |
| -------------------------------- | ----------------- | --------------------------------------------- | ------------------------------------------------- | ------------------------------------------------ |
| signed short int                 | 16 (digit 0 - 15) | -32,768 *(2<sup>15</sup>)*                    | 32,767 *(2<sup>15</sup> - 1)*                     | do nothing                                       |
| unsigned short int               | 16 (digit 0 - 15) | 0                                             | 65,535 *(2<sup>16</sup> - 1)*                     | u                                                |
| signed int / signed long int     | 32 (digit 0 - 31) | -2,147,483,648 *(2<sup>31</sup>)*             | 2,147,483,647 *(2<sup>31</sup> - 1)*              | l                                                |
| unsigned int / unsigned long int | 32 (digit 0 - 31) | 0                                             | 4,294,967,295 *(2<sup>32</sup> - 1)*              | ul                                               |
| signed long long int             | 64 (digit 0 - 63) | -9,223,372,036,854,775,808 *(2<sup>63</sup>)* | 9,223,372,036,854,775,807 *(2<sup>63</sup> - 1)*  | ll                                               |
| unsigned long ling int           | 64 (digit 0 - 63) | 0                                             | 18,446,744,073,709,551,615 *(2<sup>65</sup> - 1)* | ull                                              |

### INTEGER CONSTANTS <a name="integer_constants"></a>
**constants:** Numbers representing text of a program, not numbers that are read, written, or computed. Allowed to be written in:  
   * **decimal *(base 10)*:**  Contain digits 0-9, but must *NOT* begin with a 0.  
   * **octal *(base 8)*:**  Contain digits 0-7, and must begin with a 0.  
   * **hexidecimal *(base 16)*:**  Contain digits 0-9 and A-F, and always begin with 0x.  

***note:*** Different formats have no effect on how the number is stored (since integers are always stored in binary).  
***constant type determined by:*** Compiler treats as signed int or can be forced by the programmer. See chart above.    
**overflow:** Result requiring too many bits. Signed and unsigned recieve undefined behavior and 0, respectively.  

## FLOATING TYPES <a name="floating_types"></a>
**floating type:** Numbers w/ the decimal point.  
***3 combinations:*** float, double, and long double. Precision is based on the IEEE standard 754.  
***note:*** Macros found in header file *<float.h>*.  
***printf/scanf conversion specificers:*** %e*, *%f* and *%g* and putting *l* or *L*.  

| type   | smallest '+' value          | largest value              | precision | force constant to be this type (case insevetive) |
| ------ | --------------------------- | -------------------------- | --------- | ------------------------------------------------ |
| float  | 1.17549 x 10<sup>-38</sup>  | 3.40282 x 10<sup>38</sup>  | 6 digits  | f                                                |
| double | 2.22507 x 10<sup>-308</sup> | 1.79769 x 10<sup>308</sup> | 15 digits | do nothing                                       |

### FLOATING CONSTANTS <a name="floating_constants"></a>
***must contain:*** Decimal point and/or exponent.  
***constant type determined by:*** Compiler treats as double or can be forced by the programmer as shown in table above.  
***57.0 can be written in many forms:*** 57.0, 57., 57.0e0, 57E0, 5.7e1, 5.7e+1, .57e2, 570.e-1.  

## CHARACTER TYPES <a name="character_types"></a>
***character:*** In truth at its core realy an integer.  
***2 combinations:*** signed & unsigned char.   
***most popular character set:*** American Standard Code for Information Interchange (ASCII). [ASCII Conversion Chart](Images/ASCII Conversion Chart.pdf)  
***note:*** Can be *signed* or *unsigned*.  
***printf/scanf conversion specificer:*** *%c*. Also use functions *getchar()* and *putchar()*.    

| type          | # of bits     | smallest value (meaning) | largets value (meaning)    | force constant to be this type |
| ------------- | ------------- | ------------------------ | -------------------------- | ------------------------------ |
| signed char   | 8 (digit 0-7) | -128 *(2<sup>7</sup>)*   | +127 *(2<sup>7</sup> - 1)* | no nothing                     |  
| unsigned char | 8 (digit 0-7) | 0                        | +255 *(2<sup>8</sup> - 1)* | ?                              |

### CHARACTER CONSTANTS <a name="character_constants"></a>
***represented:*** Enclosement of constant in single quotes.  
***note:*** C treats characters as small integers. The connection between characters and integers in C is so strong that characters actually have *int* type rather than *char* type.
```c
   char char_var = 'a';
   int int_var_1 = 'a';
   int int_var_2 = 97;
   
   printf("%d, %c\n", char_var, char_var);    /* output: 97, a */
   printf("%d, %c\n", int_var_1, int_var_1);  /* output: 97, a */
   printf("%d, %c\n", int_var_2, int_var_2);  /* output: 97, a */
``` 

## ARITHMETIC TYPES <a name="arithmetic_types"></a>
***include:***    
1. **integral types:** Represent characters and integers: char, signed & unsigned integer types, & enumerated types
2. **floating types:** Represent float and double.  

## ESCAPE SEQUENCES <a name="escape_sequences"></a>
***escape sequences:*** Special characters that perform specific action. Can't be written in single quotes b/c there invisble (non printing), can't be entered from the keyboard.  
***2 kinds:*** character escapes & numeric escapes.    
```c
void main(void)
{
   char ch1 = '\x21';
   char ch2 = 33; /* hex: 0x21 */
   char ch3 = '!';
   
   printf("Decimal = \a %d | %d | %d\n",    ch1, ch2, ch4); /* output: Decimal = [Bell Sound] 33 | 33 | 33 */
   printf("Char    = %c %c | %c | %c\n", 7, ch1, ch2, ch4); /* output: Char    = [Bell Sound] !  | !  | ! */ 
}
```

## TYPE CONVERSION <a name="type_conversion"></a>
***needed:*** Normally computers don't allow mixing of arithmetic operand types. However, C allows mixing types. Thus, compiler does implicit converisons.  
**implicit conversions:** Compiler handles conversions automatically, w/o programmers involvement. Performed in these situations:  
* When operands don't have same type in arithmetic or logical expression.
* When type of expression on RHS of assignment operator doesn't match lvalue type on LHS.  
* When type of a function call argument doesn't match type of corresponding parameter.  
* When type of expression in a *return* statement doesn't match function's return type.

***Ex:*** If we add a 16-bit *short* and a 32-bit *int*, compiler will arrange *short* value to be converted to 32 bits.  
***Ex:*** If we add a *int* and *float*, compiler will arrange *int* to be converted to *float*.  
**explicit conversions:** Programmer manually performs conversions using the cast operator.  

### USUAL ARITHMETIC CONVERSIONS <a name="usual_arithmetic_conversions"></a>
Arithmetic conversion applied to most binary operators.  
Strategy, convert operands to "narrowest" (fewer bytes to store) type that will safely accommondate both values.  
**promotion:** Converting operand of the narrower type to the type of other operand.  
**integral promotion:** Converts character or short integer to type *int* (or *unsigned int*).  
* ***Floating type:*** *float* -> *double* -> *long double*.  
* ***Not floating type:*** 1) Perform integral promotion. 2) *signed int* -> *unsigned int* -> *signed long int* -> *unsigned long int*.   

Watch out when comparing signed and unsigned int. Signed int will convert to unsigned int. Ex. `-10 -> 4,294,967,296`  

| statement      | comment                       |
| -------------- | ----------------------------- |
| `i = i + c;`   | c converted to int            |
| `i = i + s;`   | s converted to int            |
| `u = u + i;`   | i converted to unsigned int   |
| `l = l + u;`   | u convertd to long int        |
| `ul = ul + l;` | l converted to unsigned long  |
| `f = f + ul;`  | ul converted to unsigned long |
| `d = d + f;`   | f converted to double         |
| `ld = ld + d;` | d converted to long double    |

### CONVERSION DURING ASSIGNMENT <a name="conversion_during_assignment"></a>
Arithmetic conversions don't apply to assignment. Simply, right side is coverted to left side.  
A "narrowing" assignment may return a compiler warning.  
Also, its good to always append *f* to float type variable constants because by default these constants are double.  

| statement      | comment                 |
| -------------- | ----------------------- | 
| `i = c;`       | c converted to int      |
| `f = i;`       | i converted to float    |
| `d = f;`       | f converted to double   |
| `i = 842.97;`  | 842.97 truncated to 842 |
| `c = 1000;`    | wrong                   |
| `i = 1.0e20;`  | wrong                   |
| `f = 1.0e100;` | wrong                   |

### CASTING <a name="casting"></a>
A greater degree of control over type conversion.  
Has the form: `(type-name)expression`. *type-name* specifies type to which the expression should be converted.   
Ex. `float f, frac_part;` `frac_part = f - (int) f;`.  
Cast expressions enable documentation of type conversions that would take place anyway. Ex. `i = (int) f; // f is converted to int`.  
Enable overruling of the compiler and force to do coversions that we want.  
C regards *type-name* as a unary operator with higher precedence than binary operators. 
Useful to avoid overflow.  

## TYPE DEFINITIONS <a name="type_definitions"></a>
**type definition:** `typedef old-type new-type;`. C programmers normally captilize the first letter of the *new-type*.  
Causes the compiler to add *new-type* to the list of type names that it recognizes.  
Advantages: Make a program more understandable, easier to modify, and portable.  
"C library uses *typedef* to create names for types that can vary from one C implementation to another; these types often have names end with *_t*."  
**C99:** *<stdint.h> header uses *typedef* to define names for integer types with particular # of bits. Ex. *int32_t* is signed integer with 32 bits.  
* *typedef* names are at the same scope rule as variables; a *typedef* name defined inside a function body wouldn't be recognized outside the function.
* Macro names are replaced by the preprocessor wherever they appear.  

## SIZEOF OPERATOR <a name="sizeof_operator"></a>
**sizeof operator:** Determines memory space to store a value of a particular type. Form: `sizeof(type-name)`.  
Result is an *unsigned long int* representing # of bytes to store a value belonging to *type-name*.  
Unary operator and higher precedance over binary operators.   
* The type of *sizeof* expression is an implementation-defined type named *size_t*, and guaranteed to be unsigned integer type.    
    * **C89:** Best to convert the value of expression to a known type before printing it. Ex. `printf("Size of int: %lu\n", (unsigned long) sizeof(int))`.   
    * **C99:** *size_t* can be > than *unsigned long*. *printf()* capable of displaying *size_t* directly. Ex. `printf("Size of int: %zu\n", sizeof(int))`.  
