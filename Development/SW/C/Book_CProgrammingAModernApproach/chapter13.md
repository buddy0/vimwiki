# STRINGS
**string:** Series of characters.  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [STRING LITERALS](#string_literals)  
    a. [STRING LITERALS STORED](#string_literals_stored)  
    b. [OPERATIONS STRING LITERALS](#operations_string_literals)  
    c. [STRING LITERALS VS CHARACTER CONSTANTS](#string_literals_vs_character_constants)
2. [STRING VARIABLES](#string_variables)  
    a. [CHARACTER ARRAY VS CHARACTER POINTERS](#character_array_vs_character_pointers)
3. [READING & WRITING STRINGS](#reading_and_writing_strings)  
    a. [READING STRINGS CHARACTER BY CHARACTER](#reading_strings_character_by_character)
4. [ACCESSING CHARACTERS IN A STRING](#accessing_characters_in_a_string)
5. [USING C STRING LIBRARY](#using_c_string_library)
6. [ARRAY OF STRINGS](#array_of_strings)  
    a. [COMMAND-LINE ARGUMENTS](#command_line_arguments)
</details>

## STRING LITERALS <a name="string_literals"></a>
**string literal:** Sequence of characters enclosed w/in double quotes. String constants. Escape sequences can be inside a string literal.  
***continuing string literal on next line:*** End first line with a backslash character `\` w/ no other characters after it.  
**splicing:** In general, `\` can be used to join 2 or more lines into a single line. Drawback with `\`: string *MUST* continue at begining of next line, thus wrecking indented structure.  
```c
void main(void)
{
    int var = \
              100;
    printf("%d\n", var);
    printf("Hello, my name is \
            Buddy!\n");
    printf("Hello, my name is \
Buddy!\n");
    printf("Hello, my name is "
           "Buddy!\n");

    /* OUTPUT: 100
               Hello, my name is       Buddy!
               Hello, my name is Buddy!
               Hello, my name is Buddy!
     */
}
```

### STRING LITERALS STORED <a name="string_literals_stored"></a>
***stored:*** As character arrays. C compiler encounters string literal of length *n*, it sets aside *n+1* bytes of memory for the string.  
**null character:** Marks end of string. Byte whose bits are all zeros. Represented by `\0`.  
***NOTE:*** Compiler treats string literal as a pointer of type `char *`.  

### OPERATIONS STRING LITERALS <a name="operations_string_literals"></a>
Wherever C allows `char *` pointer. Ex. `char *p; p = "abc";`. Assignment makes *p* point to first character of string.  Subscripted: 
```c
char DigitHex2Char(int digit);

void main(void)
{
    printf("%d\n", DigitHex2Char(2));
    printf("%c\n", DigitHex2Char(2));
}

char DigitHex2Char(int digit)
{
    return ("0123456789ABCDEF"[digit]);
}

/* OUTPUT: 50
           2
 */
```
***NOTE:*** Attempting modify string literal causes undefined behavior.  

### STRING LITERALS VS CHARACTER CONSTANTS <a name="string_literals_vs_character_constants"></a>
**string literal `"string"`:** Represented by a pointer to a memory location that contains characters followed by a null character.  
**character constant `'s'`:** Represented by an integer (numerical code for character).  

## STRING VARIABLES <a name="string_variables"></a>
**string variables:** Change during execution of program. Any 1D array of characters can be used to store a string followed by a null character at the end.  
Common practice among C programmers to define a macro for the character string length, then declaring the actual string length do "macro + 1".  
***NOTE:*** Length of string depends on positin of terminating null character, *NOT* on length of array in which string is stored.  
***initializing string variable:*** Omitting the length is legal and compiler will automatically set aside # of characters + null character, however once program is compiled the length is fixed.  
```c
char data[6] = "Buddy";                               /* Not a string literal. Abbreviation for array initalizer */
char data[6] = {'B', 'u', 'd', 'd', 'y', '\0'};
char data[7] = {'B', 'u', 'd', 'd', 'y', '\0', '\0'}; /* If last null character isn't manually set, then compiler will auto do it. */
```

### CHARACTER ARRAY VS CHARACTER POINTERS <a name="character_array_vs_character_pointers"></a>
Either one can be passed as an argument, however they are *not* interchangeable.  
`char name[] = "Buddy";` *name* is an array & can be modified.  
`char *name = "Buddy";` *name* is a pointer variable & points to a string literal & shouldn't be modified, but it can point to other strings.  
```c
void main(void)
{
    char string_arry[6] = "Buddy";
    char *string_ptr = "Buddy";
    
    string_arry[0] = 'D';
    printf("%s\n", string_arry);
    
    *string_ptr = "Cuddy";        /* warning: assignment to ‘char’ from ‘char *’ makes integer from pointer without a cast [-Wint-conversion] */
    printf("%s\n", string_ptr);
}

/* OUTPUT: warning message
           Duddy
 */
```
***NOTE:*** Needing a string to be modified needs to be an array of characters.  
Declaring pointer variable `char *p;` causes compiler to set aside memory for a pointer variable, *BUT* doesn't allocate space for a string.  
***SOLUTIONS:*** 
1. Make *p* point to a string variable array. Ex. `char str[STR_LEN+1], *p; p = str;`
2. Make *p* point to a dynamically allocated string.  

## READING & WRITING STRINGS (PG. 284) <a name="reading_and_writing_strings"></a>
***reading:*** `gets` skips white space while `scanf` does not.  
**Ex:** Hello buddy the elf
* ***output `gets`:*** Hello buddy the elf
* ***output `scanf`:*** Hello  

***NOTE:*** Reading characters into an array, *scanf* & *gets* are unsafe b/c no way to detect when its full. *scanf* can have *%ns* to indicate max. # of characters.  
*fgets* better than *gets*.  

***writing:*** `puts` 1 argument (string to be printed), always writes on a new line. `printf` writes characters in string 1 by 1 until encounters null character.  

### READING STRINGS CHARACTER BY CHARACTER <a name="reading_strings_character_by_character"></a>
Since both *scanf* & *gets* are risky, C programmers often write their own functions.  

## ACCESSING CHARACTERS IN A STRING <a name="accessing_characters_in_a_string"></a>
Strings stored as arrays therefore, subscripting can be used to access chacracters.  
```c
/* ARRARY SUBSCRIPTING */
int CountSpaces(const char s[])
{
    int count = 0;
    for(int i = 0; s[i] != `\0`; i++)
    {
        if(s[i] == ' ')
            count++;
    } 
    return (count)  
}

/* POINTER ARITHMETIC */
int CountSpaces(const char *s)
{
    int count = 0;
    for(;*s != `\0`;s++)
    {
        if(*s == ' ')
            count++
    }
    return (count)
}
```

## USING C STRING LIBRARY <a name="using_c_string_library"></a>
Arrays can't be copied or compared using operators.  
Using an array name as the left operand of `=` illegal. **Ex:** `char str1[10]; str1 = "aba";`  
However, *initializing* a character array `=` is legal. **Ex:** `char str1[10] = "aba";`  

C library (string.h) provides many  functions dealing with strings.  
The string parameters are declared to have type `char *`, allowing argument to be a **1)** character array, **2)** variable of type `char *`, **3)** string literal.  
Paramaters that don't have `const` means that the parameters can be modified therefore don't use a string literal argument.  

***useful functions:*** *strcpy*, *strlen*, *strcat*, *strcmp*  

## ARRAY OF STRINGS <a name="array_of_strings"></a>
Create a 2D array of characters storing strings in array, 1 per row. **Ex:** `char planets [][8] = {"Earth", ...};`.  
**ragged array:** 2D array whose rows can have different lengths. Done by creating an array whose elements are pointers to strings. **Ex:** `char *planets[] = {"Earth", ...};`.  
*planets* array consist of elements that are pointers to strings.  

### COMMAND-LINE ARGUMENTS <a name="command_line_arguments"></a>
**command-line arguments, program parameters:** `int main(int argc, char *argv[]){}`.  
`argc` (argument count): # of command-line arguments(including name of program itself).  
`argv` (argument vector): Array of pointers to command-line arguments, which are stored in string form.  
`argv[0]` points to name of program. `argv[1 to argc-1]` point to remaining command line arguments.  
Additionally, *argv* has `argv[argc]`, which is a **null pointer:** special pointer poitning to nothing.  
Ex.`ls -l remind.c`. `argc` will be 3. `argv[0]` point to string containing program. `argv[1]` point to string *-l*. `argv[2]` point to string *remind.c*. `argv[3]` will be a null pointer.  
