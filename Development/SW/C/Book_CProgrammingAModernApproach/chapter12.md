# POINTERS AND ARRAYS

<details>
<summary><b>TABLE OF CONENTS</b></summary>

1. [POINTER ARITHMETIC](#pointer_arithmetic)  
    a. [COMPARING POINTERS](#comparing_pointers)  
    b. [POINTERS TO COMPOUND LITERALS](#pointers_to_compound_literals)
2. [USING POINTERS FOR ARRAY PROCESSING](#using_pointers_for_array_processing)  
    a. [COMBINING `*` & `++` OPERATORS](#combining_*_++_operators)
3. [USING ARRAY NAME AS POINTER](#using_array_name_as_pointer)  
    a. [ARRAY ARGUMENTS](#array_arguments)  
    b. [USING POINTER AS AN ARRAY NAME](#using_pointer_as_an_array_name)
4. [POINTERS & MULIDIMENSIONAL ARRAYS](#pointers_and_multidimensional_arrays)  
    a. [PROCESSING ROWS OF MULTIDIMENSIONAL ARRAY](#processing_rows_of_multidimensional_array)  
    b. [PROCESSING COLS OF MULTIDIMENSIONAL ARRAY](#processing_cols_of_multidimensional_array)  
    c. [USING NAME OF A MULTIDIMENSIONAL ARRAY AS A POINTER](#using_name_of_a_multidimensional_array_as_a_pointer)
5. [QUESTIONS](#questions)
</details>

## POINTER ARITHMETIC <a name="pointer_arithmetic"></a>
**pointer arithmetic, address arithmetic:** Accessing array elements. C supports 3 forms of pointer arithmetic:  
1. Adding an integer to a pointer.  
If `p` points to array element `a[i]`, then `p + j` points to `a[i + j]`.

2. Subtracting an integer from a pointer  
If `p` points to array element `a[i]`, then `p - j` points to `a[i - j]`.  
 
3. Subtracting one pointer from another  
If `p` points to `a[i]` and `q` points to `a[j]`, then `p - q` is equal to `i - j`. TRUE: Not sure why I would though?  

```c
void main(void){
    int a[5] = {10, 20, 30, 40, 50};
    int *p = a;    /* arry == &arry[0] */

    printf("%d\n", *(p+1));
    printf("%d\n", *p+1);
    p += 2;
    printf("%d\n", *p);

    /* OUTPUT: 20 
               11
               30
     
       a 
       +----------------------------------------------------+
       |   a[0]       a[1]      a[2]      a[3]      a[4]    |
       |    *p       *(p+1)    *(p+2)    *(p+3)    *(p+4)   |
       | +---------+---------+---------+---------+---------+|
       | |+-------+|+-------+|+-------+|+-------+|+-------+||
       | ||  10   |||  20   |||  30   |||  40   |||  50   |||
       | |+-------+|+-------+|+-------+|+-------+|+-------+||
       | | 0x00000 | 0x00004 | 0x00008 | 0x0000C | 0x00010 ||
       | +---------+---------+---------+---------+---------+|
       +----------------------------------------------------+
         ^         ^                             ^
         |         |                             |
         p        p+1                           p+4
        &a[0]     &a[1]                         &a[4]               
    */
```

***NOTE:*** Performing arithmetic on a pointer that doesn't point to an array element causes undefined behavior. 
```c
/* NO UNDEFINED BEHAVIOR */
void main(void)
{
    int var = 0;
    int *p = &var;
    p += 2;                /* Doesn't make sense to do this! Pointing to some an address space possibly w/ no object! */
    printf("%p\n", &var);
    printf("%p\n", p);
}
```

***NOTE:*** Subtracting one pointer from another is underfined unless both point to elements of same array.  

### COMPARING POINTERS <a name="comparing_pointers"></a>
* Pointers can be compared using relational and equlaity operators. 
* Meaningful only when both pointers point to elements of the same array. 
* Outcome based on the relative position of the 2 elements in the array.  
**Ex:** `p = &a[5]; q = &a[0];`.  
`p <= q;` produces 0.  
`p >= q;` produces 1.  

### POINTERS TO COMPOUND LITERALS <a name="pointers_to_compound_literals"></a>
***RECALL:*** Compound literal creates an array with no name.  
`int *p = (int []){3, 0, 3, 4, 1}` == `int a[] = {3, 0, 3, 4, 1}; int *p = &a[0]`.  

## USING POINTERS FOR ARRAY PROCESSING <a name="using_pointers_for_array_processing"></a>
Pointer arithmetic allows us to visit elements of an array by repeatedly incrementing pointer variable, in contrast to subscripting.  
```c
int ArraySubsripting(int *a, int length);
int PointerArithmetic(int *a, int length);   /* No Change to for loop expression statements. */
int PointerArithmetic2(int *a, int length);  /* Used new pointer & array pointer in for loop expressions. */
int PointerArithmetic3(int *a, int length);  /* Used only arrray pointer in for loop expressions. */

void main(void){
    int a[] = {10, 20, 30, 40, 50};
    int size = sizeof(a) / sizeof(a[0]);
    int sum_as = ArraySubsripting(a, size);
    int sum_pa1 = PointerArithmetic1(a, size);
    int sum_pa2 = PointerArithmetic2(a, size);
    int sum_pa3 = PointerArithmetic3(a, size);
    printf("%d\n%d\n%d\n%d\n", sum_as, sum_pa1, sum_pa2, sum_pa3);
    // OUTPUT
    // 150
    // 150
    // 150
    // 150
}

int ArraySubsripting(int *a, int length){
    int sum = 0; /* To prevent unitialized garbage */
    for(int i = 0; i < length ; i++)
        sum += a[i];
    return (sum);
}

int PointerArithmetic1(int *a, int length){
    int sum = 0;
    int *ptr = a;                    
    for(int i = 0; i < length; i++)   
        sum += *(ptr + i);
    return (sum);
}

int PointerArithmetic2(int *a, int length){
    int sum = 0;                                // OR                                       
    for(int *ptr = a; ptr < &a[length]; ptr++)  // for(int *ptr = a, ptr < &a[length]; ){}      
        sum += *ptr;                            //     sum += *p++;
    return (sum);                              
}

int PointerArithmetic3(int *a, int length){
    int sum = 0;
    for(int i = 0; &a[i] < &a[length]; a++, i++)
        sum += *a;
    return (sum);
}
```

### COMBINING `*` & `++` OPERATORS <a name="combining_*_++_operators"></a>
Postfix `++` takes precedance over `*` operator.  
However, *p++* is really *p* due to postfixing (*p* won't be evaluated till after expression is evaluated).  

| Expression         | Meaning                                                             |
| ------------------ | ------------------------------------------------------------------- |
| `*p++` or `*(p++)` | Value of expression is `*p` before increment; increment `p` later.  |
| `(*p)++`           | Value of expression is `*p` before increment; increment `*p` later. |
| `*++p` or `*(++p)` | Increment `p` first; value of expression is `*p` after increment.   |
| `++*p` or `++(*p)` | Increment `*p` first; value of expression is `*p` afrer increment.  |   

```c
// Instead of writing
for(p = &a[0]; p < &a[n]; p++){
    sum += *p;
}

// We could write
p = &a[0];
while(p < &a[n]){
    sum += *p++;
}
```

## USING ARRAY NAME AS POINTER <a name="using_array_name_as_pointer"></a>
***STAR:*** **"** The name of an array can be used as a pointer to the first element in the array. **"**  
Normally, variable name itself represents the value or contents of the cell.  
However, an array name represents this value but its now an address!   

```c
// Instead of writing
int a[2];
a[0] = 7;
a[1] = 12;

// We could write. Interesting b/c assignment operator needs modifiable lvalue as left operand.
int a[2];
*a = 7;        // a == &a[0], thus *a == *&a[0] == a[0]
*(a + 1) = 12;
```

**Therefore:**  
`a[i] == *(a + i) == *&a[i] == a[i]`  
`a + i == &a[i]`. Both represent a pointer, lvalue (techincally no), address.  
`*(a + i) == a[i]`. Both represent a value of a particular element.   
**Additionally:**  
`for(p = &a[0]; p < &a[n]; p++){sum += *p;} == for(p = a; p < a + n; p++){sum += *p;}`  
**However:**  
"Although an array name can be used as a pointer, it's NOT possible to assign it a new value!" Ex. `while (*a != 0) a++; // WRONG`.  
**But:**  
We can always copy *a* into a pointer variable, then change the pointer variable. Ex. `int *p = a; while (*p != 0) p++;`.    

### ARRAY ARGUMENTS (REVISTED) <a name="array_arguments"></a>
When passed to a function, an array name is always treated as a pointer. Array itself isn't copied. 
That is why the function defintion argument array length isn't needed, since no copy of array is made.  
`int sum(int a[], int n); == int sum(int *a, int n);`.  

### USING POINTER AS AN ARRAY NAME <a name="using_pointer_as_an_array_name"></a>
C allows us to subscript a pointer as though it were an array name.  
Ex. `int *p = a; p[i] == *(p + i)`.  

## POINTERS & MULIDIMENSIONAL ARRAYS <a name="pointers_and_multidimensional_arrays"></a>
```c
int row, col;
for(row = 0; row < NUM_ROWS; row++)
    for(col = 0; col < NUM_COLS; col++)
        a[row][col] = 0;

/**************** OR *******************/

int *p;
for(p = &a[0][0]; p < &a[NUM_ROWS - 1][NUM_COLS - 1]; p++)
    *p = 0;
```
Loop begins with `a[0][0]`, `a[0][1]`, to `a[0][NUM_COLS-1]`, then to `a[1][0]`, `a[1][1]`, to `a[1][NUM_COLS-1]`, ... , `a[NUM_ROWS-1][NUM_COLS-1]`.  

### PROCESSING ROWS OF MULTIDIMENSIONAL ARRAY <a name="processing_rows_of_multidimensional_array"></a>
`p = &a[i][0];` == `p = a[i];`. `a[i]` is a pointer to first element in row *i*.    

| Array      | Pointer                                |
| ---------- | -------------------------------------- |
| `a[i]`     | `*(a+1)`                               |
| `&a[i][0]` | ? `&(*(a+i)+0)` == `&a[i]` == `a[i]` ? |

**Ex:** `int a[NUM_ROWS][NUM_COLS], *p, i; for(p = a[i]; p < a[i] + NUM_COLS; p++) *p = 0;`.  

### PROCESSING COLS OF MULTIDIMENSIONAL ARRAY <a name="processing_cols_of_multidimensional_array"></a>
**Ex:** `int a[NUM_ROWS][NUM_COLS], (*p)[NUM_COLS], i; for(p = &a[0]; p < a[NUM_ROWS]; p++) (*p)[i] = 0;`.  
*p* is a pointer to an array of length *NUM_COLS* whose elements are integers. The parantheses around **p* are required. W/o them, compiler would treat statement as array of pointers instead of pointer to an array.   
*p++* advances *p* to beginning of next row. In `(*p)[i]`, *(*p)* represents an entire row of a, while *[i]* selects the element if column *i* of that row.  

### USING NAME OF A MULTIDIMENSIONAL ARRAY AS A POINTER <a name="using_name_of_a_multidimensional_array_as_a_pointer"></a>
`int a[NUM_ROWS][NUM_COLS];`. *a* is *NOT* pointer to `a[0][0]`; instead it's a pointer to `a[0].`
*a* is a 1D array whose elements are 1D arrays.  

## POINTERS & VARIABLE-LENGTH ARRAYS (C99)

## QUESTIONS <a name="questions"></a>
<details>
<summary><b>Q:</b> If a pointer is an address, does that mean <code>p + j</code> means add <code>j</code> to the address stored in <code>p</code>?</summary>

**A:** No. Integers used in pointer arithmetic are scaled depending on type of pointer.  
**Ex.** If `p` is of type `int *`, then `p + j` adds `4 x j` to `p`.
</details>

<details>
<summary><b>Q:</b> Why is <code>a</code> same as <code>a[]</code> in parameter declaration?</summary>

**A:** Both arguments are pointers. Same operations on `a` are possible in both cases (pointer arithmetic and array subscripting). `a` itself can be assigned a new value w/in the function.  
However, C allows us to use name of an *array variable* only as a ***constant pointer***. There are no such restriction on the name of an *pointer parameter*.
```c
int foo(int a[]);

void main(void){
    int a[] = {10, 20, 30, 40, 50};
    int b[5];
    b = a;                             /* error: assignment to expression with array type */
    foo(a);
}

int foo(int a[]) 
{
    int b[] = {11, 20, 30, 40, 50};
    a = b;                             /* No error b/c parameter a[] is treated as `*a`! */
}
```
</details>

<details>
<summary><b>Q:</b> Arrays and pointers are interchangeable? </summary>

**A:** No,  
*sizeof(a)* total # of bytes in array.  
*sizeof(p)* is number of bytes to store a pointer value.  
</details>

<details>
<summary><b>Q:</b> If <code>a</code> is 2D array, why can we pass <code>a[0]</code>, but not <code>a</code> itself. Don't both <code>a</code> & <code>a[0]</code> point to same place (beginning of array)?</summary>

**A:** Both point to element `a[0][0]`. The problem is that `a` has wrong type: used as argument its a pointer to an array. But, function is expecting a pointer to an integer.  
However, `a[0]` has type `int *`, so its an acceptable argument.
```c
int foo(int a[]);

void main(void){
    int a[2][3] = {{11, 12, 13}, {21, 22, 23}};
    foo(a);      /* warning: passing argument 1 of ‘foo’ from incompatible pointer type [-Wincompatible-pointer-types] */
}

int foo(int a[]) /* note: expected ‘int *’ but argument is of type ‘int (*)[3]’ */
{}
```
</details>
