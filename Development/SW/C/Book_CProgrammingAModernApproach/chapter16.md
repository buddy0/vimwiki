# STRUCTURES, UNIONS, & ENUMERATIONS
**structure:** Aggreagate data structure that is user defined. Able to hold a collection of values (members), possibly of different types.  
Each structure represents a new scope or separate **name space**.  
Declaraing a struct by itself does not allocate storage. It is only when a struct variable is declared when memory is allocated.  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [STRUCTURE VARIABLES](#structure_variables)  
    a. [OPERATIONS ON STRUCTURES](#operations_on_structures)
2. [STRUCTURE TYPES](#structure_types)  
3. [STRUCTURES AS ARGUMENTS & RETURN VALUES](#structures_as_arguments_and_return_values)  
    a. [COMPOUND LITERALS](#compund_literals)
4. [NESTED ARRAYS & STRUCTURES](#nested_arrays_and_structures)
5. [UNIONS](#unions)
6. [ENUMERATIONS](#enumerations)  
    a. [ENUMERATION TAGS & TYPE NAMES](#enumeration_tags_and_type_names)  
    b. [ENUMERATIONS AS INTEGERS](#enumerations_as_integers)
</details>

## STRUCTURE VARIABLES <a name="structure_variables"></a>
**members:** Elements of a structure (similar to elements in an array), aren't required to have same type.  
Members have names and its how they are selected (unlike arrays whose elements are selected based on position).   
Members are stored in memory in order they appear.  
***declaring structure variable:*** `struct{members} var1_struct, var2_struct;`.  
***initializing structure variable:*** While its being declared.  
Members that the initializer fails to account for are set to zero.  
**C89:** Values in initializer *must* appear in same order as members of structure.  
**C99:** Combination of period and member name is **designator**.  
```c
/* C89 */
struct{
    int grade;
    char name[6];
} student_1 = {90, "Buddy"}, \
  student_2 = {88, "Beary"}; 
```
```c
/* C99 */
struct {
    int grade;
    char name[6];
} student_1 = {.name = "Buddy", .grade = 90}, \
  student_2 = {.name = "Beary", .grade = 88};
```
### OPERATIONS ON STRUCTURES <a name="operations_on_structures"></a>
* ***access member w/in structure:*** Write: **1.** structure variable name, **2.** period (dot), **3.** member name.  
**Ex:** `printf("%d\n", student_1.grade)`.  
Members are *lvalues* when structure variable declared, thus can be on left side of assignment operator (except cannot assign a value with an array type as an lvalue).  
**dot operator:** Same precedance as postfix *++* & *--* operators, so it takes precedanace nearly all other operators.  

* ***structure assignment:*** *Structures* can, but *arrays* can't.  
**Ex:** `student2 = student1;`.    
The assignment operator only **compatible** with structures of same type.  

## STRUCTURE TYPES <a name="structure_types"></a>
2 ways to name structures:  
1. **DECLARING STRUCTURE TAG**  
**structure tag:** Name used to identify particular kind of structure.  
***NOTE:*** Semicolon *must* be present terminate declaration. Once a tag is created it can be used to create variable. Unfortunately cannot drop word *struct*.  
```c
struct NAME_TAG{
    /* members */
};
struct NAME_TAG var1_struct;
```
2. **DEFINING STRUCTURE TYPE**   
Can use `typedef`. Name of structure type (*NOT* variable) must come at the end, *NOT* after word *struct*. Since *struct_name* is a *typedef* name, *NOT* allowed to write *struct struct_name*.  
```c
typedef struct NAME_TAG{
    /* members */
} NAME_TYPE;
NAME_TYPE var1_struct;
```

## STRUCTURES AS ARGUMENTS & RETURN VALUES <a name="structures_as_arguments_and_return_values"></a>
***function prototype:*** `void print_student(struct student s);`.  
***function call:*** `print_student(student1);`.  
***returning structure:***     
```c
#include <stdio.h>
#include <string.h>

typedef struct STUDENT_TAG{
    int grade;
    char name[6];
} STUDENT_TYPE;

STUDENT_TYPE PrintInfo(int grade, char *name)
{
    STUDENT_TYPE s;
    s.grade = grade;
    strcpy(s.name, name);
    printf("%d\n%s\n", s.grade, s.name);
    return (s);
}

void main(void){
    PrintInfo(95, "Cuddles");
}

/* OUTPUT: 95
           Cuddles
 */
```
**"** Passing structure and returning structure from a function both require making copy of all members in structure. To avoid overhead try passing *pointer* to a structure **"**.  

### COMPOUND LITERALS (C99) - PG. 386 <a name="compund_literals"></a>

## NESTED ARRAYS & STRUCTURES <a name="nested_arrays_and_structures"></a>
Structures and arrays can be combined w/o restriction. Arrays may have structures as their elements & structures may contain arrays and structures as members.  
***nested structures:*** Useful for putting a smaller subset structure into a larger structure.  
***arrays of structures:*** Array whose elements are structures.  
**Ex:** `struct student total[100];`. `total[0].grade = 90;`. `total[0].name = "Buddy";`.  
***initializing array of structures:*** Similar to a multidimensional array. 

## UNIONS <a name="unions"></a>
**union:** Similar to a structure, except that its members share same storage. As a result, a union can only store one member at a time. 
Compiler allocates only enough space for largest of the members. Members of a union are stored at the same address, while a struct's members are stored at different addresses.  
Since compiler overlays storage for the members, changing one member alters any value previously stored in the union.  
**C89:** Only the first member of a union can be given an initial value.  
**C99:** Designated initializer can be used to specify which member of a union should be initialized.  
***advantages:*** Save space in structures. Build mixed data structures.  
***NOTE:*** Normally, *not* a good idea to store value of a member of a union since values of other members are undefined.  
However, C has a special case: 2 or more of the members of the union are structures, and structures begin w/ 1 or more matching members. If 1 of structures is valid, then matching members of other strucuters are vaild, too.  
```c
/* Union name `item` is a member of `struct CATALOG_ITEM_TAG`.
   `book`, `mug`, and `shirt` are members of `item`. 
   Can print book's title using `c` as a `catalog_item` structure: `printf("%s", c.item.book.title)`.
 */

struct CATALOG_ITEM_TAG{
    int stock_number;
    double prices;
    int item_type;

    union{
        struct{
            char title[TITLE_LEN+1];
            char author[AUTHOR_LEN+1];
            int num_pages;
        } book;

        struct{
            char design[DESIGN_LEN+1];
        } mug;

        struct{
            char design[DESIGN_LEN+1];
            int colors;
            int sizes;
        } shirt;
    } item;
};
```
***ADDING TAG FIELD TO A UNION:*** PG. 400  

## ENUMERATIONS <a name="enumerations"></a>
**enumeration:** Integer type whose values are named by the programmer.  
A special kind of type designed specifically for variables that have a small # of possible values.  
**enumerated type:** Type whose values are listed *enumerated* by the programmer, who must create a name **enumerated constant** for each of the values.  
**Ex:** `enum {CLUBS, DIAMONDS, HEARTS, SPADES} suit_1, suit_2;`.  
The values *CLUBS*, *DIAMONDS*, *HEARTS*, & *SPADES* can be assigned to the variables *suit_1*, & *suit_2*.  
Enumeration constants are local to the function declared.  

### ENUMERATION TAGS & TYPE NAMES <a name="enumeration_tags_and_type_names"></a>
2 ways to name an enumerations:  
1. **DECLARING A TAG**
```c
enum SUIT_TAG{
    CLUBS,
    DIAMONDS,
    HEARTS,
    SPADES
};
enum SUIT_TAG suit_1, suit_2;
```
   
2. **TYPEDEF**
```c
typedef enum SUIT_TAG{
    CLUBS,
    DIAMONDS,
    HEARTS,
    SPADES
} SUIT_TYPE
SUIT_TYPE suit_1, suit_2;
``` 

### ENUMERATIONS AS INTEGERS <a name="enumerations_as_integers"></a>
C treats enumeration variables & constants as integers. By default, compiler assigns integers 0, 1, 2, ... to the constants in an enumerations.  
**Ex:** *CLUBS*, *DIAMONDS*, *HEARTS*, & *SPADES* represent 0, 1, 2, and 3 respectively.  
***USING ENUMERATIONS TO DECLARE TAG FIELDS:*** PG. 404.  
