# FUNCTIONS

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [DEFINING AND CALLING FUNCTIONS](#defining_and_Calling_functions)
2. [FUNCTION DEFINITIONS](#function_definitions)
3. [FUNCTION DECLARATIONS](#function_declarations)
4. [ARGUMENTS](#arguments)
5. [ARGUMENT CONVERSIONS](#argument_conversions)
6. [ARRAY ARGUMENTS](#array_arguments)
7. [C99: VARIABLE-LENGTH ARRAY PARAMETERS](#c99_variable_length_array_parameters)
8. [C99: USING STATIC IN ARRAY PARAMETER DECLARATIONS](#c99_using_static_in_array_parameter_declarations)
9. [C99: COMPOUND LITERALS](#c99_compund_literals)
10. [RETURN STATEMENT](#return_statement)
11. [RECURSION](#recursion)
</details>

## DEFINING AND CALLING FUNCTIONS <a name="defining_and_Calling_functions"></a>
**return type:** Type of data that function returns. Function that doesn't want to return anything has a return type *void*.  
**parameters:** Dummy variables whose initial value will be supplied later by a function call.  
Appear in function defintions.      
**arguments:** Expressions supplying information to a function definition. Can be variables or constant expressions. Ex. `sum(a, 1);`.  
Appear in function calls.  
**body:** Executable part, enclosed in curly braces.  
* NOTE: Value returned from a function isn't saved anywhere, unless the value is assigned to a variable. Appending *(void)* infront of function call is a polite way of saying "throwing away".  
* NOTE: A function call doesn't need a semicolon, except for a void function. Acts like an expression.    

## FUNCTION DEFINITIONS <a name="function_definitions"></a>
**definition:** `return type funciton_name(parameters){body}`.  
***RULES return type:*** 
1. Functions can not return arrays.  
2. Return type *void* means function doesn't return a value. 
3. **C89:** If return type omitted, function returns value of type int. **C99:** ILLEGAL to omit return type.  

***RULES parameters:***  
1. Preceded by its type.  
2. Parameters are separated by commas. 
3. If function has no parameters, then *void* should appear between parentheses.  

***RULES body:***  
1. Can include both declaration and statements.  
2. **local scope:** Variables declared in body belong exclusively to that function.  
**C89:** Variable declarations *MUST* come first. **C99:** Declarations & statements can be mixed.  

## FUNCTION DECLARATIONS <a name="function_declarations"></a>
When functions are defined below *main* function, compiler has no information when it encounters the function call in *main*.  
**implicit declaration:** Instead of producing error message, compiler assumes the function returns an *int*.  
Errors still can occur due to the actual return might be a double.  
To avoid CALL-BEFORE-DEFINED error, arrange program so function definitions precede its calls.  
**function declaration, prototypes:** Declaring a function before calling it. Provides compiler w/ brief glimpse at a function whose full definiton will appear later.  
Just first line of function definition with semicolon added at end.  
Ex. `return-type function_name(parameters);`.  
**C99:** Calling a function for which compiler has not yet seen a declaration or definition is an error.  

## ARGUMENTS <a name="arguments"></a>
In C, arguments are **passed by value:** During function call, arguments are evaluated & its contents, value is copied, assigned to their corresponding parameter.  
Any changes made to the parameter don't affect the argument.  
Thus, each parameter behaves like a variable thats been initialized to the value of the matching argument.  

## ARGUMENT CONVERSIONS <a name="argument_conversions"></a>
C allows function calls in which types of arguments don't match types of parameters. The rules are based on whether the compiler has seen the prototype for the function or function's full definition.   
**Compiler *has* encountered prototype prior to call:** Value of each arguement is implicitly converted to type of corresponding parameter.  
**Compiler *has not* encountered prototype prior to call:** Compiler performs **default argument promotion:** 1) *float* arguements converted *double*, 2) integral promotions, causing *char* and *short* converted *int*.  

## ARRAY ARGUMENTS <a name="array_arguments"></a>
Array arguments are **passed by reference:** Address is passed. Operations done affect variables that are related to the arguements' addresses. 
When a function parameter is 1-D array, length normally left unspecified. Ex. `int f(int a[]){}`.  
B/c the compiler ignores a specified length & only starting address of 1st element is passed.  
***Q:*** How will *f* know how long array is?  
***A:*** We'll have to supply length as an additional arguement.  
Function is allowed to change the elements of array parameter & change is reflected in corresponding argument.  
When a function parameter is 2-D array, only length of 1-D maybe omitted when parameter is declared.  

## C99: VARIABLE-LENGTH ARRAY PARAMETERS <a name="c99_variable_length_array_parameters"></a>
Allows length of array to be specified using a non-constant expression. Can also be parameters.  
Ex. `int sum_array(int a[], int n){} // no direct link between n and length of array a.`.  
Ex. `int sum_Array(int n, int a[n]){} // a's length is n`. Order is IMPORTANT.  

## C99: USING STATIC IN ARRAY PARAMETER DECLARATIONS <a name="c99_using_static_in_array_parameter_declarations"></a>
Ex. `int sum_array(int a[static 3], int n){}`. *static* has no effect on behavior of the program. Merely a "hint" allowing compiler to generate faster instructions for accessing array.  
Can only be used in the 1-D.  

## C99: COMPOUND LITERALS <a name="c99_compund_literals"></a>
**compound literal:** Unnamed array that's created "on the fly" by simply specifying which elements it contains.  Consist of a *type name* within parentheses followed by set of values enclosed in curly braces.  
Resembles a cast applied to an initializer.  
Ex. No compund literal: `int b[] = {3, 0, 3, 4, 1}; total = sum_array(b, 5)`.  
Ex. With compund literal: `total = sum_array((int []){3, 0, 3, 4, 1}, 5)`.  
May contain expressions as well as constants.  
Is an lvalue, so values of its elements can be changed. Can be "read only" by adding `(const int[]){0, 1}`.  

## RETURN STATEMENT <a name="return_statement"></a>
Form: `return expression;`.  
If type of *expression* doesn't match functions return type, *expression* will be implicitly coverted to return type.  

## RECURSION <a name="recursion"></a>
**recursive:** Function calling itself.  
