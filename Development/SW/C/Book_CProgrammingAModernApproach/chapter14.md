# THE PREPROCESSOR
**preprocessor:** Piece of SW that edits C programs prior to compilation.  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [HOW PREPROCESSOR WORKS](#how_preprocessor_works)
2. [PREPROCESSING DIRECTIVES](#preprocessing_directives)
3. [MACRO DEFINITIONS](#macro_definitions)  
    a. [SIMPLE MACROS](#simple_macros)  
    b. [PARAMETERIZED MACROS](#parameterized_macros)  
    c. [`#` & `##` OPERATORS](##_and_##_operators)  
    d. [GENERAL PROPERTIES OF MACROS](#general_properties_of_macros)  
    e. [PARANTHESES IN MACRO DEFINITIONS](#parantheses_in_macro_definitions)  
    f. [CREATING LONGER MACROS](#creating_longer_macros)  
    g. [PREDEFINED MACROS](#predefined_macros)  
    h. [EMPTY MACRO ARGUMENTS / MACROS W/ VARIABLE NUMBER OF ARGUMENTS / __func__ IDENTIFIER](#empty_macro_arguments)
4. [CONDITIONAL COMPILATION](#conditional_compilation)  
    a. [`#if` & `#endif` DIRECTIVES](#if_and_endif_directives)  
    b. [`defined` OPERATOR](#defined_operator)  
    c. [`#ifdef` & `#ifndef` DIRECTIVES](#ifdef_and_ifndef_directives)  
    d. [`#elif` & `#else` DIRECTIVES](#elif_and_else_directives)  
5. [MISCELLANEOUS DIRECTIVES](#miscellaneous_directives)  
    a. [`#error` DIRECTIVE](#error_directive)  
    b. [`#line` DIRECTIVE](#line_directive)  
    c. [`#pragma` DIRECTIVE](#pragma_directive)  
6. [QUESTIONS](#questions)
</details>

## HOW PREPROCESSOR WORKS <a name="how_preprocessor_works"></a>
**preprocessing directives:** Commands that begin with `#` character. What controls the behavior of the preprocessor.  
**`#include <file_name>`:** Opens a particular file & includes its contents as part of the file being compiled. **Ex:** `#include <string.h>`  
**`#define macro:`** Name that represents something else. **Ex:** `#define GPIO_PORTA_PIN1 LED1`.  
![Table of Preprocessor Process](Images/preprocessorProcess.png)  

## PREPROCESSING DIRECTIVES <a name="preprocessing_directives"></a>
Most preprocessing directives fall into these 3 categories:  
**1. Macro definition:** `#define` directive defines a macro. `#undef` directive removes macro definition.  
```c
#include <stdio.h>
#define PIE 3.14
void main(void){
    printf("%.2f\n", PIE);
    #undef PIE
    printf("%.2f\n", PIE); /* error: ‘PIE’ undeclared (first use in this function) */
}
```
**2. File inclusion:** `#include` directive causes contents of a specified file to be included in a program.   
**3. Conditional compilation:** `#if`, `#ifdef`, `#ifndef`, `#elif`, `#else`, and `#endif` directives allow blocks of text to either be included or excluding from a program, depending on contitions tested by preprocessor.  
Remaining directives: `#error`, `#line`, & `#pragma` more specialized & used less often.  
***RULES*** that apply to all direcives:  
* Directives always begin with `#` symbol.  
* Any # of spaces and tabs may separate tokens in a directive.  
* Directives always end at end of line, unless explicitly continued.  
* Directives can appear anywhere in a program.  
* Comments may appear at same line as a directive.  

## MACRO DEFINITIONS <a name="macro_definitions"></a>

### SIMPLE MACROS <a name="simple_macros"></a>
**simple macro, object-like macro:** Form `#define identifier replacement-list`. *replacmenet-list* is any sequence of **preprocessing tokens** which can include identifiers, keywords, numeric constants, etc.  
When preprocessor encounters macro definition, wherever *identifier* appears later in file is being substituted w/ the *replacement-list*.   
***Advantages of `#define`:*** Makes programs easier to read and modify. Helps avoid inconsistencies and typographical errors. Renaming types though *typedef* is superior. Controlling conditional compilation.  

### PARAMETERIZED MACROS <a name="parameterized_macros"></a>
**parameterized macro, function-like macro:** Form `#define identifier(x1, ... , xn) replacement-list`. *x1, ... ,zn* are macro's **parameters** & can appear as many times in the *replacement-list*.  
***NOTE:*** There must be *no space* between *identifier* and left parenthesis. If space is left, preprocessor will assume that is one of the tokens for the *replacement-list*.  
When preprocessor ecounters parameterized macro, stores definition away for later use. Where ever a **macro invocation** of form `identifier(y1, ... , yn)` appears later in program, preprocessor replaces it w/ *replacement-list*.  
***Advantages:*** Program may be slightly faster, Macros are generic meaning have no particular type.  
***Disadvantages:*** Compiled code will be longer. Arguments aren't type checked. It's not possible to have a pointer to a macro. Macro may evaluate its arguments more than once.  

### `#` & `##` OPERATORS <a name="#_and_##_operators"></a>
**`#`, stringization:** Converts macro argument into string literal. Can appear only in *replacement-list* of a parameterized macro.   
**`##`, token pasting:** Can *paste* 2 tokens together to form a single token. If 1 operands is macro parameter, pasting occurs after parameters has been replaced by corresponding argument.  

### GENERAL PROPERTIES OF MACROS <a name="general_properties_of_macros"></a>
* Macro's *replacement-list* may contain invocations of other macros.   
* Preprocessor replaces only entire tokens, not portions of tokens.  
* Macro defintion normally remains in effect until end of file in which it appears.  
* Macro may not be defined twice unless new definition is identical to old one.  
* Macros may be *undefined* by the `#undef` directive. Form: `#undef identifier` where *identifier* is macro name.  

### PARANTHESES IN MACRO DEFINITIONS <a name="parantheses_in_macro_definitions"></a>
***RULES:***  
1. If macro's *replacement-list* contains an operator, always enclose *replacement-list* in parantheses.
```c
#include <stdio.h>
#define PIE1 3.14 + 1 
#define PIE2 (3.14 + 1) 
void main(void){
    printf("%.2f\n", PIE1 * 2);
    printf("%.2f\n", PIE2 * 2);
}
/* OUTPUT: 5.14
           8.28
 */
```
2. If macro has parameters, put parentheses around each parameter every time it appears in *replacement-list*.  

### CREATING LONGER MACROS <a name="creating_longer_macros"></a>
Comma operator, compound statement, & wrapping statments in a *do* loop whose condition is false used to make *replacement-list* series of expressions.  
Ex. `#define ECHO(s) (getd(s), puts(s))`. `#define ECHO(s) { gets(s); puts(s); }`.   

### PREDEFINED MACROS <a name="predefined_macros"></a>
Each macro represents an integer constant or string literal.  
![Table of Compilation / Compiler Predefined Macros](Images/compilationPredefinedMacros.png)  

### EMPTY MACRO ARGUMENTS / MACROS W/ VARIABLE NUMBER OF ARGUMENTS / __func__ IDENTIFIER (all C99) <a name="empty_macro_arguments"></a>
Allows any or all arguments in a macro call to be empty.  
**ellipsis (token `...`):** Goes at end of macro's parameter list, preceded by ordinary parameters.  
**`__VA_ARGS__`:** Special identifier can appear only in *replacemnet-list* of a macro with a variable # of arguments.  
**`__func__`:** Identifier has nothing to do with preprocessor, but it's useful for debugging.  

## CONDITIONAL COMPILATION <a name="conditional_compilation"></a>
**conditional compilation:** Inclusion or exclusion of section of program text depending on outcome of test performed by preprocessor.  

### `#if` & `#endif` DIRECTIVES <a name="if_and_endif_directives"></a>
Form `#if constant-expression`. `#endif`. If *constant-expression* is zero, then lines between directives will be removed during the preprocessor phase.
```c
#define DEBUG 1
#if DEBUG
    printf("DEBUG ON");
#endif
/* OUTPUT: DEBUG ON */
```

### `defined` OPERATOR <a name="defined_operator"></a>
Produces value `1` if identifier is currently defined macro, otherwise `0`. Normally used in conjunction w/ `#if` directive.  
Parentheses not required below. Since `defined` tests only whether *DEBUG* is defined or not, not necessary to give *DEBUG* a value.  
```c
#if defined(DEBUG)
printf("DEBUG ON");
#endif
```
```c
#define DEBUG
#if defined(DEBUG)
printf("DEBUG ON");
#endif
/* OUTPUT: DEBUG ON */
```

### `#ifdef` & `#ifndef` DIRECTIVES <a name="ifdef_and_ifndef_directives"></a>
Form `#ifdef identifier`. `#ifndef identifier`. Directive tests whether identifier is currently or *NOT* defined as a macro, respecitvely.  
`#ifdef identifier` == `#if defined(identifier)`.  
`#ifndef identier` == `#if !defined(identifier)`.  

### `#elif` & `#else` DIRECTIVES <a name="elif_and_else_directives"></a>
The `#if`, `#ifdef`, & `#ifndef` directives can be nested just like ordinary *if* statements.  
Some programmers put a comment on each closing `#endif` indicating what condition the matching *#if* tests. Additionally:  
Form `#elif constant-expression`. `#else`. Can be used in conjunction with `#if`, `#ifdef`, & `#ifndef`.  
Any number of `#elif` directives, but at most one `#else` may appear between `#if` and `#endif`.  
```c
#if expr1
/* Lines to be included if expr1 is nonzero. */
#elif expr2
/* Lines to be included if expr1 is zero but expr2 is nonzero. */
#else
/* Lines to be included if expr1 & expr2 are zero. */
#endif
```

**Advantages:** Writing programs portable to several machines or operating systems, compiled with different compilers. Providing default definition for a macro. Temporarily disabling code that contains comments.  

## MISCELLANEOUS DIRECTIVES <a name="miscellaneous_directives"></a>
Specialized and used less often.  

### `#error` DIRECTIVE <a name="error_directive"></a>
Form `#error message`. Encountering an `#error` directive indicates serious flaw in the program. Used in conjunction w/ conditional compilation to check for situations that shouldn't arise.  
```c
#if defined(WIN32)
...
#elif defined(MAC)
...
#elif defined(LINUX)
...
#else
#error "No OS specified!"
#endif
```

### `#line` DIRECTIVE <a name="line_directive"></a>
Used to alter way program lines are numbered *OR* make compiler think that it's reading program from a file with a different name.  
Form `#line n`. Causes subsequent lines in program to be numbered *n*, *n+1*, etc.  
Form`#line n "file"`. Lines that follow this directive are assumed to come from *file*.  

### `#pragma` DIRECTIVE <a name="pragma_directive"></a>
Provides a way to request special behavior from compiler. 
Form `#pragme tokens`.  

## QUESTIONS <a name="questions"></a>
<details>
<summary><b>Q:</b> Macro expansion by aliasing variables, funcitons, etc.?</summary>

**A:** Yes, is possible.  
```c
#define var var1

void main(void)
{
    /* var = 0; */ /* don't need anymore, instead of changing this throughout entire code base, just use an alias! */
    var1 = 1;
    printf("%d\n", var);
}

/* OUTPUT: 1 */
```
</details>

