# ADVANCED USES OF POINTERS

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [DYNAMIC STORAGE ALLOCATION](#dynamic_storage_allocation)  
    a. [NULL POINTERS](#null_pointers)
2. [DYNAMICALLY ALLOCATED STRINGS](#dynamically_allocated_strings)  
    a. [USING `malloc` TO ALLOCATE MEMORY FOR A STRING](#using_malloc_to_allocate_memory_for_a_string)
3. [DYNAMICALLY ALLOCATED ARRAYS](#dynamically_allocated_arrays)    
    a. [USING `malloc` TO ALLOCATE STORAGE FOR AN ARRAY](#using_malloc_to_allocate_storage_for_an_array)  
    b. [`calloc` FUNCTION](#calloc_function)  
    c. [`realloc` FUNCTION](#realloc_function)  
4. [DEALLOCATING STORAGE](#deallocating_storage)  
    a. [`free` FUNCTION](#free_function)  
    b. ["DANGLING POINTER" PROBLEM](#dangling_pointer_problem)
5. [LINKED LISTS](#linked_lists) **TODO**
6. [POINTERS TO POINTERS](#pointers_to_functions) **TODO**
7. [POINTERS TO FUNCTIONS](#pointers_to_functions)  
    a. [FUNCTION POINTERS AS ARGUMENTS](#function_pointers_as_arguments)
8. [RESTRICTED POINTERS](#restricted_pointers) **TODO**
9. [FLEXIBLE ARRAY MEMBERS](#flexible_array_members) **TODO**
</details>

## DYNAMIC STORAGE ALLOCATION <a name="dynamic_storage_allocation"></a>
**dynamic storage allocation:** Ability to allocate storage during program execution. This allows data structures to grow and shrink as needed. Used most often for strings, arrays, and structures.  
***memory allocated functions:*** To allocate storage dynamically, `<stdlib.h>` header includes the following functions
* `malloc`: Allocates block of memory but doesn't initialize it.  
* `calloc`: Allocates block of memory and clears it.  
* `realloc`: Resizes previously allocated block of memory.  

Calling one of these memory allocated functions to request a block of memory does not know what type of data were planning to store in the block. Therefore, it can't return a pointer to an ordinary type such as *char* & *int*.  
Instead, the function returns a value of type `void *`. A `void *` is a *generic* pointer, just a memory address.  

### NULL POINTERS <a name="null_pointers"></a>
There is always a possibility that calling a memory allocated function will not be able to locate a memory large enough to satify our request.  
If that happens, function will return a **null pointer**: Pointer to nothing, special value distinguished from all valid pointers.   
After storing function's return value in a pointer variable, a test is ideal to see if it's a null pointer.  
Represented by a macro named `NULL`.  
![Program Null Pointer Test1](Images/programNullPointerTest1.png) ![Program Null Pointer Test2](Images/programNullPointerTest2.png)  
***NOTE:*** In C, pointers test *true* or *false* same way as numbers. Non-null pointers test *true*; only null pointers test *false*.  
Ex. Test **false:** `if(p == NULL)` *or* `if(!p)`.  
Ex. Test **true:** `if(p != NULL)` *or* `if(p)`.  

## DYNAMICALLY ALLOCATED STRINGS <a name="dynamically_allocated_strings"></a>
Strings are stored in character arrays and can be hard to figure out the length of the array. By allocating strings dynamically, the decision can be postponed until the program is running, *not compiled*.  

### USING `malloc` TO ALLOCATE MEMORY FOR A STRING <a name="using_malloc_to_allocate_memory_for_a_string"></a>
***`malloc` function prototype:*** `void *malloc(size_t size);`. 
`malloc` allocates a block of *size* bytes and returns a pointer to it. *size* has type *size_t*: unsigned integer type.  
Ex. `p = malloc(n + 1);`. *p* is a `char *` variable. The generic pointer that `malloc` returns will be converted to `char *` when assignment is performed; no cast necessary.  
However, programmers prefer to cast `malloc` return value.  
Ex. `p = (char *)malloc(n + 1);`.  
***NOTE:*** Memory allocated using `malloc` isn't cleared or initialized, so *p* points to an unititialized array of *n + 1* characters. Calling `strcpy(p, "abc");` initializes this array.  

## DYNAMICALLY ALLOCATED ARRAYS <a name="dynamically_allocated_arrays"></a>
C allows a program to allocate space for an array durring execution, then access the array through a pointer to its first element

### USING `malloc` TO ALLOCATE STORAGE FOR AN ARRAY <a name="using_malloc_to_allocate_storage_for_an_array"></a>
Unlike strings whose characters are 1 byte long, arrays can be any type thus `sizeof` operator is used to calculate space needed for each element.  
Ex. `int *a; a = (int *)malloc(n * sizeof(int));`.  
""Once *a* points to a dynamically allocated block of memory, we can ignore the fact that *a* is a pointer, and use *a* as an array name (due to relationship between arrays & pointers).   
Can use pointer arithmetic or subscripting to initialize the array".  

### `calloc` FUNCTION <a name="calloc_function"></a>
***`calloc` function prototype:*** `void *calloc(size_t nmemb, size_t size);`.  
`calloc` allocates space for an array w/ *nmemb* elements, each of which is *size* bytes long. After allocating memory, initialization occurs by setting all bits to 0.  
Ex. `a = calloc(n, sizeof(int));`.  
***NOTE:*** `calloc` clears memory that it allocates.  

### `realloc` FUNCTION <a name="realloc_function"></a>
***`realloc` function prototype:*** `void *realloc(void *ptr, size_t size);`.  
`realloc` calls, *ptr* must point to memory block obtained from a previous call of `malloc`, `calloc`, or `realloc`. *size* parameter represents new size of the block.  
***RULES:***
* When it expands a memory block, `realloc` doesn't initialize bytes that are added to the block. 
* If `realloc` can't enlarge memory, returns a null pointer; data in old memory block is unchanged.  
* If `realloc` called w/ null pointer as its 1st argument, behaves like `malloc`.  
* If `realloc` called with 0 as 2nd argument, frees the memory block.  

***NOTE:*** Reducing size of memory; `realloc` should shrink block *in place* w/o moving data stored in block.  
***NOTE:*** Expanding size of memory; Expand memory block w.o moving it, however if unable b/c the following bytes are used up, `realloc` will allocate a new block elsewhere, then copy contents of old block into new one.  
***NOTE:*** Once `realloc` has returned be sure to update all pointers to memory block, since block might have moved elsewhere.  

## DEALLOCATING STORAGE <a name="deallocating_storage"></a>
**heap:** Storage pool where memory allocation functions obtain memory blocks.  
**garbage:** Block of memory that's no longer accessible to a program.  
**memory leak:** Program that leaves garbage left behind.  
**garbage collector:** Some programming languages provide this that automatically locates and recycles garbage, but C doesn't.  
Instead, each C program is responisble for recycling its own garbage by calling `free` function to release unneeded memory.  

### `free` FUNCTION <a name="free_function"></a>
***`free` fuction prototype:*** `void fee(void *ptr);`  
***NOTE:*** The argument to `free` must be a pointer previously returned by a memory allocated function.  

### "DANGLING POINTER" PROBLEM <a name="dangling_pointer_problem"></a>
The call `free(p)` deallocates memory block that *p* points to, but doesn't change *p* itself. Forgetting that *p* no longer points to valid memory block, chaos may ensure.  

## LINKED LISTS <a name="linked_lists"></a>

## POINTERS TO POINTERS <a name="pointers_to_pointers"></a>

## POINTERS TO FUNCTIONS <a name="pointers_to_functions"></a>
Functions occupy memory locations, so every function has an address. 

### FUNCTION POINTERS AS ARGUMENTS <a name="function_pointers_as_arguments"></a>
Ex. `double integrate(double (f*)(double) ,double a, double b);`. Parentheses around `(*f)` indicate *f* is a pointer to a function, *NOT* function that returns a pointer.  
Ex. `double integrate(double f(double), double a, double b);`. Identical to prototype above.  

## RESTRICTED POINTERS (C99) <a name="restricted_pointers"></a>

## FLEXIBLE ARRAY MEMBERS C99) <a name="flexible_array_members"></a>

