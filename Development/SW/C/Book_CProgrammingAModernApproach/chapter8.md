# ARRAYS

**scalar:** Holding single data item.  
**aggregate:** Store collection of values. Two kinds in C: arrays & structures.  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [ONE-DIMENSIONAL ARRAYS](#one_dimensional_arrays)  
    a. [C99](#c99)
2. [SIZEOF OPERATOR WITH ARRAYS](#sizeof_operator_with_arrays)
3. [MULTIDIMENSIONAL ARRAYS](#multidimensional_arrays)
4. [CONSTANT ARRAYS](#constant_arrays)
5. [VARIABLE LENGTH ARRAYS](#variable_length_arrays)
</details>

## ONE-DIMENSIONAL ARRAYS <a name="one_dimensional_arrays"></a>
**array:** Data structure containing data values (aggregate), all of same type.  
**elements:** Values in array. Can be individually selected by position within array. Start from 0 to N-1.  
***declaration:*** `type array_name[number of elements];`.  
**subscripting, indexing:** Accessing a particular element in an array. Array name followed by the integer value or expression in square brackets.  
Ex. `a[0]` or `a[b + 1]`.    
Expressions of subscript are lvalues, therefore can be on the LHS of assignment operator. `a[0] = 1;`  
***initialization:*** `type array_name[# of elements] = {#, ... , #};`. Always done during declaration.  
**initializer:** `{#, ... ,#}`  
* LEGAL: Initializer can be *shorter* than array, thus remaining elements are 0.
* LEGAL: Initializer is present, length of array in brackets may be omitted.  
* ILLEGAL: Initializer to be empty.  
Ex. `int a[10] = {} // WRONG` `int a[10] = {0}; // CORRECT`.  
* ILLEGAL: Initializer to be *longer* than array it initializes (# of elements).  
Ex. `int a[1] = {1, 1}; // WRONG` `int a[0] = {1}; // CORRECT`.  

### C99 <a name="c99"></a>
**designated initializers:** `int a[15] = {[2] = 29, [9] = 7, [14] = 48}; // MEM: 0,0,29,0,0,0,0,0,0,7,0,0,0,0,48`.  
**designated:** Number in bracket.  
Order no longer matters.  

## SIZEOF OPERATOR WITH ARRAYS <a name="sizeof_operator_with_arrays"></a>
Determine size of an array (in bytes). 
Ex. How do we know array has N elements. `int N = sizeof(array_name) / sizeof(array_name[0])`.  

## MULTIDIMENSIONAL ARRAYS <a name="multidimensional_arrays"></a>
***declaration:*** `int array_name[row][column];`  
"Although we visualize 2-dimensional arrays as tables, C stores them in **row-major order** with row 0 first and so on."  
***initialization:*** `int multi[2][3] = {{1,1,1}, {1,1,1}};`. Inner braces are optional.  

## CONSTANT ARRAYS <a name="constant_arrays"></a>
**const:** Will produce compiler error if object is modified. Ex. `const char multi[2][3] = {'0', '1', '2', '3', '4', '5'}`

## VARIABLE LENGTH ARRAYS (C99) <a name="variable_length_arrays"></a>
In C89, length of array variable *must* be constant expression, however in C99 this expression does not have to be constant.  
Ex. VLA can be computed when the program is executed, *NOT* when the program is compiled.  
