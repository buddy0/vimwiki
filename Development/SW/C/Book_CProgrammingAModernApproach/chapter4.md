# EXPRESSIONS
***[C Operator Precedence Table](https://en.cppreference.com/w/c/language/operator_precedence):*** When in doubt use parentheses.     
A ***variable*** represents a value to be computed as the program runs; a ***constant*** represents a value that does not change.  
<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [ARITHMETIC OPERATORS](#arithmetic_operators)
3. [ASSIGNMENT OPERATORS](#assignment_operators)
4. [RVALUE & LVALUE](#rvalue_and_lvalue)
5. [INCREMENT & DECREMENT OPERATORS](#increment_and_decrement_operators)
6. [EXPRESSION STATEMENTS](#expression_statements)
7. [QUESTIONS](#questions)
</details>

## ARITHMETIC OPERATORS <a name="arithmetic_operators"></a>  
| unary (ra) | binary - multiplicative (la) | binary - additive (la) | 
| ---------- | ---------------------------- | ---------------------- |
| plus (+)   | multiplication (*)           | addition (+)           |
| minus (-)  | division (/)                 | subtraction (-)        | 
|            | remainder (%)                |                        |

***unary operator:*** Require 1 operand. Right associativity.  
***binary operator:*** Require 2 operands. Left associativity.  
***mixed types:*** Allowed by all binary operators except *%*.  
**Ex:** `9 + 2.5f = 11.5` `6.7f / 2 = 3.35`  

```c
float badscale = 5 / 9;   /* compiler treats constants as ints */
printf("%f\n", badscale); /* OUTPUT: 0.000000 */

float corscale = 5 / 9.f; /* compiler implicitly converts mixed constants, int & float as float */
printf("%f\n", corcale);  /* OUTPUT: 0.555556 */
```

***special care for / & % operators:***    
* When using / operator and both operands are ints, result truncates by dropping fractional part. **Ex:** `5 / 9 = 0` not 0.56.  
* % operator requires both operands to be ints otherwise program won't compile.  
* Using zero as right operand causes undefined behavior.  
* Negative numbers in C89 states that if either operand is negative, result of a division can be rounded up or down. Ex. `-9 / 7 = -1 or -2`. The sign of i % j depends on the implementation. Ex. `-9 % 7 = -2 or 5`.  
Negative numbers in C99 states that the result of a division always truncates toward zero. Ex. `-9 / 7 = -1`. The value of i % j has the same sign as i. Ex. `-9 % 7 = -2`.  
```                             __________
Dividend / Divisor == Divisor | Dividend = Q R
Check: Q * Divisor + R = Dividend

         -1             -2               C89
      ______          _______            i % j -> -9 % 7 = -2 or 5 
    7 | -9          7 | -9
       --7             -14               C99
      _____           _____              i % j -> -9 % 7 = -2
        -2              5
```

## ASSIGNMENT OPERATORS <a name="assignment_operators"></a>
***assignment in c:*** Not a statement, but an operator. Produces a result just like adding 2 numbers produces a result. Assignments can be chained together.  
***`v = e`:*** Evaluate expression *e* & copy its value into *v*. Therefore, the value of `v = e` is *v* after the assignment.     
****v* & *e* different types:*** Value of *e* converts to type of *v*.  
 
## RVAVUE & LVALUE <a name="rvalue_and_lvalue"></a>
**rvalue:** Expression appearing on the RHS of the assignment operator.  
**lvalue:** An object stored in computer memory, not a constant or result of computation; variables are but expressions are not.  
Assignment operator requires an lvalue as its left operand.  


## INCREMENT & DECREMENT OPERATORS <a name="increment_and_decrement_operators"></a>
Similar to the assignment operators, these operators also suffer from side effects such as modifing the value of their operands.  Ex. `++i` yields *i* + 1 and as a side effect increments *i*.  

## EXPRESSION STATEMENTS <a name="expression_statements"></a>
Any expression can be used as a statement. 
However some statements are discarded if operands are not changed. Error: statement with no effect.  
Ex. `i = 1;` `i--;` modify operands in memory but `i * j - 1;` has no effect since *i* and *j* are not changed.  
Rvalue (expression) is always (for the most part, except *++* and *--*) discarded.  

## QUESTIONS <a name="questions"></a>
<details>
<summary><b>Q:</b> Macros are by default what data type?</summary>

**A:** Macros do *not* have a data type. They are preprocessor directives that are used for text replacement.  
Once, the modified program comes out of preprocessor, a replace text being a constant/literal (lets say 10) will be treated as a signed int by the compiler.  
</details>
