# INTRODUCING C
![The C Programming Language](Images/c.png)  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [HISTORY OF C](#history_of_c)
2. [STRENGTHS and WEAKNESSES OF C](#strengths_and_weaknesses_of_c)
</details>

## HISTORY OF C <a name="history_of_c"></a>
C is a by-product of the UNIX OS, developed at Bell Laboratories by Ken Tompson, Dennis Ritchie, and others.  
Unix was first written in assembly: hard to debug and enhance. As a result, Thompson created a higher-level language named B (based on BCPL which is based on Algol 60).  
Ritchie joined the UNIX project and began programming in B. However, when Bell Labs acquired PDP-11, B was not well suited.  
Therefore, Ritchie develped an extended version of B called NB ("New B") and the name eventauly led to C.  
1. **1978 (K&R C):** *The C Programming Language by Brian Kernighan and Dennis Ritchie* was released in.  
2. **1980s:** C was changing, new features were being, and K&R failed to make distinction between which features belong to C and UNIX. An updated standard of the language needed to be done.  
3. **1983:** Development of a U.S. standard for C under the Americal National Standards Institute (ANSI).  
4. **1990 (C89/C90):** International Organization for Standardization (ISO) approved the new C standard, ISO/IEC 9899:1990, known as C89 or C90, to separate it from K&R C.   
5. **1999 (C99):** C was again changing and a new standard risen, ISO/IEC 9899:1999, known as C99.  

## STRENGTHS and WEAKNESSES OF C <a name="strengths_and_weaknesses_of_c"></a>
C is a:   
**low-level language:** provides access to machine-level concepts (bytes and addresses) and operations close to a computers built-in instructions  
**small languae:** C relies heavily on a "library" of standard functions  
**permissive language:** assumes the user knows what their doing, allows more freedom in tradeoff to detail error checking  

***STRENGTHS***  
**efficiency**, **portability**, **power**, **flexibility**, **standard library**, **integration with UNIX**

***WEAKNESSES***  
**C programs can be error-prone**, **difficult to understand & modify**  


