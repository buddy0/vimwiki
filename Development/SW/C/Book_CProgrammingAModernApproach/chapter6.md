# LOOPS

**loop:** Statement that repeatedly executes some other statement, **loop body**.  
**controlling expression:** Controls whether loop body will be executed. Every loop has one.  
**iteration:** Every time the loop body is executed.  
**interation statements:** *while*, *do*, and *for*  
**infinite loop:** Execute forever unless the loop body contains *break*, *goto*, or *return* or a function call that causes program to terminate.  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [WHILE STATEMENT](#while_statement)
2. [DO STATEMENT](#do_statement)
3. [FOR STATEMENT](#for_statement)
4. [BREAK STATEMENT](#break_statement)
5. [CONTINUE STATEMENT](#continue_statement)
6. [NULL STATEMENT](#null_statement)
</details>

## WHILE STATEMENT <a name="while_statement"></a>
***form:*** `while (control expression) statement` or `while (control expression){compound statement}`  

## DO STATEMENT <a name="do_statement"></a>
***form:*** `do statement while(control expression);`  

## FOR STATEMENT <a name="for_statement"></a>
***form:*** `for (expr1; control expr2; expr3) statement`  
***execution order:*** 
1. *expr1* 
2. *control expr2* 
3. if *control expr2* condition is true, then statement, else break out of loop 
4. *expr3*
5. goto step 2.   

***note:*** *expr#* can be omitted, but the semicolons must always be present. `for (;;)` represents an infinite loop.   
**comma expression:** Only for *expr1* and *expr3*.  
**C99:** Allows *expr1* to be replaced by a variable declaration used only for the scope of the loop.  

## BREAK STATEMENT <a name="break_statement"></a>
***break statement:*** Transfers control out of the ***innermost*** enclosing *while*, *do*, *for*, or *switch* statement. Jumps only forward.  

## CONTINUE STATEMENT <a name="continue_statement"></a>
***continue statement:*** Transfers control just *before* end of loop body, remaining inside the loop. Only limited to loops. Jumps only forward.  

## NULL STATEMENT <a name="null_statement"></a>
Just a semicolon `;`.  

