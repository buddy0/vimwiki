# DEFINITIONS

## FUNCTIONS
**definition:** `int foo(void){}`  
**declaration, prototype:** `int foo(void);`

## MACROS
**definition:** `#define identifier`

## VARIABLES - [G4G](https://www.geeksforgeeks.org/difference-between-definition-and-declaration/)
**declaration & definition:** `int i;`. Declares *i* to be a variable of type *int* and defines *i* causing compiler to set aside space for *i*.   
**declaration:** `extern int i;`. Declares *i* w/o defining it.  

## STRUCTURES
**declaration:** `struct {} varible_names;`.   
