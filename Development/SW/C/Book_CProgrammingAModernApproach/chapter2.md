# C FUNDAMENTALS

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [COMPILING AND LINKING](#compiling_and_linking)
2. [SIMPLE PROGRAM](#simple_program)
3. [TERMS](#terms)
</details>

## COMPILING AND LINKING <a name="compiling_and_linking"></a>
* **preprocessing:** Modifies the original program. Happens prior to compilation & not part of the compilation process.  
Porgram first given to a preprocessor, which obeys **directives**: editing commands that start with *#*.  
* **compiling:** Modified program now goes to a compiler translating to **object code** or machine instructions.  
* **linker:** Combines the object code produced by the compiler and any additional code.  


```text
                    +--------------+                       +----------+                   +--------+
original program -> | preprocessor | - modified program -> | compiler | - object files -> | linker | - executable file
                    +--------------+                       +----------+                   +--------+
```

## SIMPLE PROGRAM <a name="simple_program"></a>
```c
/* directives */

int main(void){ /* main() returns an integer */ 
    /* statements */
    return 0; /* terminates the function & returns a value of 0 */
}
```
* **directives:** Editing commands intended for the preprocessor. One line long, no semicolon.  
* **functions:** Blocks of executable code that peform specific tasks. *main* function is mandatory and is called automatically when the program is executed.  
* **statements:** Command to be executed when the program runs. End with a semicolon (exceptions).  

## TERMS <a name="terms"></a>
* **variable:** Storage location. Every variable has a type specifing the data it will hold. Type is critical b/c affects how data is stored and what operations can be performed. Also, every variable has 3 properties: **storage duration**, **scope**, & **linkage**.  
    * **declaration:** Informs compiler of variable characteristics/properties: *type* & *name* (identifier) w/o storage allocation. Info. put into **symbol table** (data structure). Variables must be declared before use.  
    * **definition:** Allocates storage in memory for a variable.
    * ***note:*** In C, variable *definition* & *declaration* done at the same time. **Ex.** `int a;`.  
    * **assignment:** Giving value to a variable. The value can be an **expression**: constants, variables, & operators.  
    * **initialization:** Declaring and assigning a variable. A variable with no value is uninitialized.   
* **macros:** Alias. Names for constants. Ex. `#define FIND REPLACE`. The REPLACE can be expressions. If operators, must be enclosed in parentheses.  
* **identifiers:** Names for objects (variables, functions, macros, & other entities). Must begin with a letter or underscore. C is **case-sensitive**.
* **tokens:** Groups of characters that can't be split up w/o changing their meaning such as keywords, identifiers, operators, string literals. Any amount of space can be inserted between tokens.  
* **keywords:** Special significant words to C compilers and therefore can not be used as identifiers.  

| keywords |          |        |          |            |         |          |              |
| -------- | -------- | ------ | -------- | ---------- | ------- | -------- | ------------ |
| auto     | continue | enum   | if       | restrict ! | static  | unsigned | _Complex !   |
| break    | default  | extern | inline ! | return     | struct  | void     | _Imaginary ! |
| case     | do       | float  | int      | short      | switch  | volatile |              |
| char     | double   | for    | long     | signed     | typedef | while    |              |
| const    | else     | goto   | register | sizeof     | union   | _Bool !  | ! = C99 only |
