# WRITING LARGE PROGRAMS

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [SOURCE FILES](#source_files)
2. [HEADER FILES](#header_files)  
    a. [`#include` DIRECTIVE](#include_directive)  
    b. [PROTECTING HEADER FILES](#protecting_header_files)
3. [BUILDING MULTIPLE-FILE PROGRAM](#building_multiple_file_program)  
    a. [MAKEFILES](#makefiles)
</details>

## SOURCE FILES <a name="source_files"></a>
**source files:** Contain definitions of functions and external variables. Extension is normally `.c`.

## HEADER FILES <a name="header_files"></a>
**header files, include files:** Contain information to be shared among source files. Extension is `.h`.  
**Q:** How can a function in one file call a function that's defined in another file? OR access an external variable in another file? Or 2 files sharing same macro definiton or type definition.  
**A:** The `#include` directive shares information among any # of source files.  
***NOTE:*** C standard uses term *source files* to refer to all files written by the programmer, both `.c` & `.h` files.  

### `#include` DIRECTIVE <a name="include_directive"></a>
2 primary forms:
1. `#include <filename>`. Used for header files belong to C's own library. Compiler searches directory (directories) in which system header files reside.  
2. `#include "filename"`. All other header files. Compiler searches current directory, then search directory (directories) in which system header files reside.  
3. `#include tokens`. Used less ofthen. 

File name can include infomation to help locate the file. **Windows:** `#include "c:\cprogs\utils.h"`. **UNIX:** `#include "/cprogs/utils.h"`.  

***sharing macro and type definitions:*** These definitions should go into header files.  
***sharing function prototypes:*** To avoid messing around function prototypes in many files, these declarations or prototypes need to got into a header file.  
***NOTE:*** Always include the header file declaring a function in the source file that contain's functions definiton.  
***NOTE:*** Functions intended for use only w/in source file shouldn't be declared in header file.  
***sharing variable declarations:*** External variables are done much the same way as functions; defintions in `.c` file & declarations in `.h` file.  
***declaring a variable:*** Ex. `int i;`. Declares and defines *i*. Declares *i* to be a variable of type *int* & defines *i* causing compiler to set aside space for *i*.  
**extern:** Declare a variable w/o defining it. Ex. `extern int i;`. Informs compiler that *i* is defined elsewhere in the program, so no need to allocate space for it.
***NOTE:*** To avoid messing up the type. Ex. `int i;` in one file and `extern long i` in anothe file, put variable declarations of shared variables in header files.  
***nested includes:*** `#include` directives in header files.  
[Declarations Definitions Prototypes for Functions, Macros, Variables](definitions.md)  

### PROTECTING HEADER FILES <a name="protecting_header_files"></a>
If a source file includes same header files twice, compilation errors may result. Common when header files include other header files. Good idea to protect all header files against multiple inclusion.   
***below:*** When *prog.c* is compiled *file3.h* will be compiled twice. `#ifndef` can fix this. Note: Identifiers can't contain periods.  
![Picture Header File Error](Images/pictureHeaderFileError.png) ![Program for Header File Protection](Images/programHeaderFileProtection.png)  

## BUILDING MULTIPLE-FILE PROGRAM <a name="building_multiple_file_program"></a>
***compiling:*** Each source file in the program must be compiled separately (Header files don't need to be compiled, since they are included in the source files).  
Each source file generates an object code file.  
**object files:** The result from the compiler from each source file. Extension: **UNIX** `.o`. **WINDOWS** `.obj`.   
***linking:*** Combines object files created by compiler along w/ code for library functions to produce an executable file. Also responsible for resolving external references left behind from compiler.  
Ex. `gcc main.c foo.c -o ouput`. 2 source files are compiled into object code. Object files are automatically passed to linker combining them into a single file.  

### MAKEFILES <a name="makefiles"></a>
**makefile:** File containing information necessary to build a program. Not only lists the files part of the program but also describes **dependencies** among the files. 
Ex. Suppose *foo.c* includes *bar.h*. *foo.c* depends on *bar.h* b/c a change to *bar.h* requires us to recompile *foo.c*.  
**rule:** A group of lines.  
**target:** 1st line in each rule. Followed by files in which the target depends.  
**command:** 2nd line in each rule. Executed if the target should need to be rebuilt b/c of a change to one of its dependent files.  
* Each command (2nd line) must be preceded by a tab character
* Normally stored in a file named `Makefile`.
* To invoke *make* utility use this command: `make target`. If no target is specified *make* will build target of the first rule.   
***errors during linking:*** Misspellings. Missing files. Missing libraries.   
***rebuilding a program:*** To save time, revuilding process should recompile only those files that might be affected by the lastest change.  
Maunally: `gcc main.c foo.o -o output`. Automatically: *makefile* automatically rebuilds the files that are changed, needed.  
***defining macros outside a program:*** GCC `-D` & `-U` to define and undefine a macro. Ex. `gcc -DDEBUG=1 foo.c`.  

