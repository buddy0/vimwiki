# SELECTION STATEMENTS

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [TYPES OF STATEMENTS](#types_of_statements)
2. [LOGICAL EXPRESSIONS](#logical_expressions)
3. [BOOLEAN VALUES in C89](#boolean_values_in_c89)
4. [BOOLEAN VALUES in C99](#boolean_values_in_c99)
5. [SWITCH STATEMENT](#switch_statement)
</details>

## TYPES OF STATEMENTS <a name="types_of_statements"></a>
**return:** Returns a value whose type is dependant on the return type of the function declaration.  
**expression:** Any expression regardless of its type can be turned into a statement, some can have no effect.  
**selection:** Program selects a particular execution path from a set of alternatives. Ex. *if*, *switch*  
**iteration:** Support iteration.  
**jump:** Cause unconditional jump to some other place in the program (return statement belongs here as well). Ex. *break*, *continue*, *goto*  
**compound:** Groups several statements into a single statement.  
**null:** Performs no action.  

## LOGICAL EXPRESSIONS <a name="logical_expressions"></a>
***note:*** In C, a comparison yields an *integer*: **0** (false) or **1** (true).  
***note:*** Treat any nonzero operand as **1** (true) and any zero operand as **0** (false).   
***note:*** Both *&&* and *||* *short circuit* evaulate left than the right operand.  
| operator type | symbol       | meaning                                          | associative | precedance                      |
| ------------- | ------------ | ------------------------------------------------ | ----------- | ------------------------------- |
| relational    | <, >, <=, >= | less/greater than, less/greater than or equal to | left        | lower than arithmetic operators |
| equality      | ==, !=       | equal to, not equal to                           | left        | lower than relational operators |
| logical       | !            | logical negation (unary)                         | right       | same as plus, minus operators   |
|               | &&, //       | logical AND, logical OR (binary)                 | left        | lower than equality operators   |

## BOOLEAN VALUES in C89 <a name="boolean_values_in_c89"></a>
C language doesn't have a Boolean type. Most programmers just declare an int variable with **0** or **1**. Ex. `int flag; flag = 1; flag = 0;`  
For better readability that the *flag* value is boolean, C89 programmers define macros. Ex. `#define TRUE 1` `#define FALSE 0`  
Test whether a flag is **true** or **false**: `if(flag == TRUE)` `if(flag)` and `if(flag == FALSE)` `if(!flag)`  

## BOOLEAN VALUES in C99 <a name="boolean_values_in_c99"></a>
A Boolean variable can be declared by `_Bool flag`, which can only be assigned **0** or **1**.   

## SWITCH STATEMENT <a name="switch_statement"></a>
***how it works:*** Works similar to an conditional *if else if* block. If a case is selected the following statements will execute unless a break statement is reached.  

```
switch(control expression){
    case (constant numerical expression): {statements}
    ...
    case (constant numerical expression): {statements}
    default: {statements}
}
```
**control expression:** Must have type int.  
**constant numerical experssion:** Can't contain variables or function calls. Must also evaulate to an integer.  
**statements:** No braces are required. Last statement is normally *break*.  
* Only one constant expression can follow the word *case*; however, several case labels may precede the same group of statements. 
  There's no way to write a case label to specify a range of values.  

