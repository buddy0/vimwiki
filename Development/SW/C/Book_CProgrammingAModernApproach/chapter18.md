# DECLARATIONS 
<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [DECLARATION SYNTAX](#declaration_syntax)
2. [VARIABLE STORAGE CLASS](#variable_storage_class)  
    a. [PROPERTIES OF VARIABLES](#properties_of_variables)  
    b. [`auto` STORAGE CLASS](#auto_storage_class)  
    c. [`static` STORAGE CLASS](#static_storage_class)  
    d. [`extern` STORAGE CLASS](#extern_storage_class)  
    e. [`register` STORAGE CLASS](#register_storage_class)  
3. [FUNCTION STORAGE CLASS](#function_storage_class)
4. [SUMMARY](#summary)
5. [TYPE QUALIFIERS](#type_qualifiers)
6. [DECLARATORS](#declarators)
7. [DECIPHERING COMPLEX DECLARATIONS](#deciphering_complex_declarations)
8. [INITIALIZERS](#initializers)
9. [UNINITIALIZED VARIABLES](#uninitialized_variables)
10. [INLINE FUNCTIONS](#inline_functions)  
    a. [INLINE DEFINITIONS](#inline_definitons)  
    b. [RESTRICTIONS ON INLINE FUNCTIONS](#restrictions_on_inline_functions)
11. [QUESTIONS](#questions)
</details>

## DECLARATION SYNTAX <a name="declaration_syntax"></a>
**declarations:** Provide info to compiler about meaning of identifiers.  
***declaration form:*** `declaration-specifiers declarators ;`    
**declaration-specifiers:** Describe properties of variables or functions being declared.    
**declarators:** Names & may provide additional info. about their properties.  
declaration-specifiers falls into 3ish categories:  
1. **storage class:** ***This and type of either global or local determines 3 var properites.*** 5 of them: `auto`, `static`, `extern`, `register`, & `typedef`. Only one is present at a time!  
2. **type qualifiers:** *C89:* 2 of them: `const` & `volatile`. *C99:* adds 1 more, `restrict`. Declaration may contain 0 or more. Order not important.   
3. **type specifiers:** keywords: `void`, `char`, `short`, `int`, `long`, `float`, `double`, `signed`, `unsigned`, structures, unions, enumerations, & typedefs. 
4. **function specifier:** *C99:* keyword `inline`  
* Declarators include identifiers of simple variables, followed by `[]` (array names), preceded by`*` (pointer names), follwed by `()` (function names).  
* As a matter of style I like to put declarations in this order: `function-specifier storage-class type-qualifiers type-specifiers declarators ;`.

| storage class | type qualifiers | type specifiers   | declarators  | eq | initializer |
| ------------- | --------------- | ----------------- | ------------ | -- | ----------- |
| static        |                 | float             | x, y, *p;    |    |             |
|               | const           | char              | month[]      | =  | "November"; |
| extern        | const           | unsigned long int | a[10];       |    |             |
| extern        |                 | int               | square(int); |    |             |

## VARIABLE STORAGE CLASS <a name="variable_storage_class"></a>
***2 storage durations:*** automatic & static.  
***4 storage classes:*** auto, static, extern, & register.  

### PROPERTIES OF VARIABLES <a name="properties_of_variables"></a>
Every variable has 3 properties: 
1. **storage duraion:** When memory is set aside & released.  
**automatic storage duration:** Variable is allocated when surrounding block is execued; Deallocated when block terminates, causing variable to lose its value.  
**static storage duration:** Variable stays at same storage location as long as program is running, retaining its value indefinitely. Thus, preserving state. 
2. **scope:** Portion of program text where the variable can be referenced.  
**block scope:** Variable is visible from its point of declaration to end of enclosing block.  
**file scope:** Variable is visible from its point of declaration to end of file.  
3. **linkage:** Determines extent to which a variable can be shared by different parts of program.  
**external linkage:** Variable may be shared by several (or all) files in program.  
**internal linkage:** Variable is restricted to single file, but may be shared by functions in same file (if variable w/ same name in different file, that variable treated as different variable).  
**no linkage:** Variable belongs to single function & can't be shared.  

Default storage duration, scope, & linkage of a variable depend on where it's declared.  
* Variables declared *inside* a block have *automatic* storage duration, *block* scope, & *no* linkage.  
* Variables declared *outside* a block at outermost level of program have *static* storage durcation, *file* scope, & *external* linkage.

### `auto` STORAGE CLASS <a name="auto_storage_class"></a>
Almost never specified since its default for variables declared inside a block.  
***availability:*** Only in *block* scope for variables.  
***properties:*** *automatic storage duration*, *block scope*, & *no linkage*.  
```c
void main(void)
{
    int var;  /* automatic storage duration, block scope, no linkage */
}
```

### `static` STORAGE CLASS <a name="static_storage_class"></a>
***availability:*** Either *block* or *file* scope for variables, but they have different effects.  
***properties inside block:*** Storage duration *static* (previously *automatic*), *block scope*, still *no linkage*.  
***properties outside block:*** Storage duration *static*, *file* scope, linkage *internal* (previously *external*).  
```c
static int i;     /* static storage duration, file scope, internal linkage */

void foo(void)
{
    static int j; /* static storage duration, block scope, no linkage */
}
```
***properties:***   
* implements a technique known as *information hiding*. Section 19.2, pg. 487.
* *static* variable in block only initialized once, where *auto* is initialized every time it comes to existence.  
* *static* variable is shared by all calls of function, where *auto* gets new set of variables. 
* Pointer can be returned to a *static* variable, while *auto* should not.  

### `extern` STORAGE CLASS - ALSO IN 15.2 <a name="extern_storage_class"></a>
Enables several source files to share same variable.  
***declaration `extern int i;`:*** Informs compiler that *i* is an *int* variable, but doesn't allocate memory for *i* for its declared & defined in another file.  
***properties:*** *static storage duration*, either *block* or *file* scope, & *internal* linkage if variable was declared *static* earlier in file (*extern* decalration prolly in *block* scope) and *external* linkage otherwise (normal case).  
***NOTE:*** A variable can have many declarations but should only have 1 definition (exception of course).  
```c
extern int i;     /* static storage duration, file scope, unkown linkage */

void foo(void)
{
    extern int j; /* static storage duration, block scope, unkown linkage */
}
```

### `register` STORAGE CLASS <a name="register_storage_class"></a>
Asks compiler to store variable in a register instead of keeping it in main memory (request , not a command). Only legal for variables belonging to *block* scope.  
**register:** Storage area located in computer's CPU. Data stored in a register can be accessed & updated faster than data stored in ordinary memory.  
Properties are the same as *auto* variables.  
***NOTE:*** *register* variables don't have addresses. 

## FUNCTION STORAGE CLASS <a name="function_storage_class"></a>
***property:*** Linkage: either *extern* or *static*.  
***function declared w/ *extern*:*** Function can be called by other files, external linkage.  
If no storage class specified, this is the default option. This actually serves no purpose, like declaring variables to be *auto* in *block* scope.  
***function declared w/ *static*:*** Function can't be called from other files, internal linkage (exception, indirect call via function pointer).  
Provides *easier maintenance* & *reduced "name space pollution"*.  
***function parameters properties:*** Same as *auto* variables: *automatic storage duration*, *block scope*, & *no linkage*. Only storage class is *register*, tho.  

## SUMMARY <a name="summary"></a>

***Default Variable Storage Class & Properties:***   
***note:*** Changing variable properties caused by adding storage class & location of the variable in program  

| type - storage class          | storage duration | scope | linkage              | comment                                                                                          |
| ----------------------------- | ---------------- | ----- | -------------------- | ------------------------------------------------------------------------------------------------ |
| **global** - **extern**       | static           | file  | external             | Confusing b/c *static* storage duration & not static storage class. Var declared but not define. |
| **global** - none             | static           | file  | external             | Default (C standard: 6.2.2 Linkages of identifiers). Var declared & defined. *see note below*    | 
| **global** - **static**       | static           | file  | internal or external |                                                                                                  |
| **local**  - **auto** or none | automatic        | block | none                 | Default.                                                                                         |
| **local**  - **static**       | static           | block | none                 | Remember, no linkage for local variables.                                                        |

***NOTE- from C Standard (n1256) 6.2.2 Linkages of identifiers:*** "If the declaration of an identifier for a function has no storage-class specifier, its linkage is determined exactly as if it were declared with the storage-class specifier extern. If the declaration of an identifier for an object has file scope and no storage-class specifier, its linkage is external."

***Default Function Storage Class & Property:***  

| type   | storage class | linkage  | comment  |
| ------ | ------------- | -------- | -------- |
| global | extern        | external | Default. |
| local  | static        | internal |          |

***code example:***  
```c
int a;
extern int b;
static int c;

void foo(int d, register int e)
{
    auto int g;
    int h;
    static int i;
    extern int j;
    register int k;
}
```
| Variable Name | Storage Duration | Scope | Linkage                              |
| ------------- | ---------------- | ----- | ------------------------------------ |
| a             | static           | file  | ***external***                       |
| b             | static           | file  | *unknown*                            |
| c             | static           | file  | internal                             |
| d             | automatic        | block | none                                 |
| e             | automatic        | block | none                                 |
| g             | automatic        | block | none                                 |
| h             | automatic        | block | none                                 |
| i             | static           | block | ***none***                           |
| j             | ***static***     | block | *unkown*                             |
| k             | automatic        | block | none                                 |

## TYPE QUALIFIERS <a name="type_qualifiers"></a>
**`const`:** Declare objects that are *read-only*.  
***advantages:*** Form of documentation, compiler can check to see this object doesn't change, & embedded systems can make this object to be stored in ROM.  
***differences between `#define` & `const` pg. 466:*** `#define` has no scope or address, `const` can be viewed in a debugger.  
**`volatile`:** See chapter 20 section 3.  
**`restrict`:** **C99:** Used only w/ pointers.  

## DECLARATORS <a name="declarators"></a>
In simplicity, declarators are identifiers that may also contain symbols such as `*`, `[]`, & `()`.  
**Ex.** `int *p;` represents a pointer.  
**EX.** `int a[10];` represents an array. The brackets may be empty if array is a parameter, if it has an initializer, or if storage class is *extern*.  
**Ex.** `int f (void);` represents a function.  

### DECIPHERING COMPLEX DECLARATIONS <a name="deciphering_complex_declarations"></a>
2 simple rules:
1. Always read declarators from the inside out. Locate the identifier first.  
2. When there's a choice, always favor `[]` and `()` over `*`.  

**Examples:**  
`int *ap[10];` *ap* is an array of pointers.  
`float *fp(float);` *fp* is a function that returns a pointer to a *float*.  
`void (*pf)(int);` *pf* is a pointer to a function that returns a *void* type.  
`int *(*x[10])(void);` *x* is an array of pointers that points to a function that returns a pointer of type *int*.  
`int f(int)[]; /** WRONG **/` Functions can't return ararys.  
`int g(int)(int); /** WRONG **/` Functions can't return functions.  
`int a[10](int); /** WRONG **/` Arrays of functions aren't possible.  
In each *WRONG* case, pointers can be used to get desired effect.  

## INITIALIZERS <a name="initializers"></a>
Intial values can be written when declaraing variables.  
**initializer:** Expression.   
***operation:*** After the declarator, write the `=` symbol followed w/ an initializer.  
***NOTE:*** `=` in a declaration is *different* then `=` in an assignment.  
***RULES:***  
* If an initilaizer has *static storage duration*, then it must be constant.  
***C Standard (n1246 pg. 137):*** "All the expressions in an initializer for an object that has static storage duration shall be constant expressions or string literals." 
```c
void main(void){
    int var;
    static int index = var; /* error: initializer element is not constant */
}
```
* If an initilaizer has *automatic storage duration*, then need not be constant.  
* A brace-enclosed initializer for an array, structure, or union must contain only contant expressions.  
* **C99:** Initializer for *automatic* structure or union can be another structure or union of the proper type.   

### UNINITIALIZED VARIABLES <a name="uninitialized_variables"></a>
Initial value of a variable depends on its storage duration:  
***variables w/ automatic storage duration:*** Have no default initial value. Can't be predicted & may be different each time variable comes into existence.   
***variables w/ static storage duration:*** Have the value zero be default.  
***matter of style:*** Better to just initialize all variables to avoid confusion.  

## INLINE FUNCTIONS (C99) <a name="inline_functions"></a>
**overhead:** Cumulative work required to call a funcion & later return from it.  
**inline:** Suggests an implementation strategy in which compiler replaces each call of the function by machine instructions. 
**`inline`:** Functions specifier that is a suggestion that compiler should try to make calls of function as fast as possible (tradeoff of possible increase program length). 

### INLINE DEFINITIONS <a name="inline_definitons"></a>
```c
inline double average(double a, double b){
    return (a + b / 2)
}
```
***complication:*** Normally, w/o `inline` keyword, *average* would have external linkage. However, *average* above does not have external defintion, but inline definition.  
Calling *average* from other files results in an error.  
***2 ways to avoid above complication:***  
1. Add the `static` keyword to the function definition. *average* now has internal linkage & can't be called from other files.  
2. Provide external definiton so calls are permitted from other files. Write *average* function a second time (w/o using inline) in different source file.  
Legal, but not good idea.  
***better approach:*** Put inline definition in a header file (average.h)
```c
/* file: average.h */
#ifndef _AVERAGE_H_
#define _AVERAGE_H_

inline double average(double a, double b){
    return (a + b / 2)
}

#endif

/* file: average.c */
#include "average.h"

extern double average(double a, double b); /* Causes inline definiton to be treated as an external definition */
```
***RULE:*** If all top-level declarations of a function includes `inline` but not `extern` then definiton treated as inline.  
If function is used anywhere in the program (inlucding file containss inline definition), then an external definition of the function will need to be provided by another file.  
Compiler can choose the inline one or external one, thus both need consistent definitions.  

### RESTRICTIONS ON INLINE FUNCTIONS <a name="restrictions_on_inline_functions"></a>
* Function may not define a modifiable `static` variable.  
* Function may not contain references to variables w/ internal linkage.  

## QUESTIONS <a name="questions"></a>
<details>
<summary><b>Q:</b> Why <code>extern</code> needed even tho default global variables have external linkage?</summary>  

**[A](http://www.it.uc3m.es/pbasanta/asng/course_notes/variables_en.htm):**  
1. **Compiler reads each file in one pass.** Thus, if variable declared in same file, declaration must preced its use.  
2. **Compiler only remembers info. in file that is processing.** Thus, if variable declared in another file, exact same definition must be included in file w/ prefix `extern`.  

```c
/* main.c */
int global_var = 1;

void main(void){}
---------------------------
/* foo.c */
extern int global_var;

int funciton(void)
{
    return (global_var++);
}
```
**error:** If `extern int global_var;` in *foo.c* omitted, then compiler emits error that variable `global_var` hasn't been declared.  
**error:** If `extern` in *foo.c* omitted, then compiler emits error that variable `global_var` has been multiply defined. 
</details>

<details>
<summary><b>Q:</b> What if I create a global variable w/ <code>auto</code> storage class?</summary>  

**A:** error: file-scope declaration of ‘global_var’ specifies ‘auto’.  
</details>

