/***************************************************************************************************************************
 * Leet Code - Two Sum                                                                                                         
 *
 * Given an array of integers nums and an integer target, return indices of the two numbers such that they add up to target.
 * You may assume that each input would have exactly one solution, and you may not use the same element twice.
 * You can return the answer in any order.
 ****************************************************************************************************************************/

#include <stdio.h>
#include <stdlib.h>

int* TwoSums (int* nums, int target, int nums_size);

int main (void)
{
  int nums[4] = {2, 7, 11, 15};
  int target = 9;
  int nums_size = sizeof(nums) / sizeof(nums[0]);
  int* result;
  
  result = TwoSums(nums, target, nums_size);

  printf("[%d, %d]\n", result[0], result[1]);
  
  return 0;
}

int* TwoSums (int* nums, int target, int nums_size)
{
  int index_i;
  int index_j;
  int* result;

  result = (int *)malloc(sizeof(int) * 2);

  for (index_i = 0; index_i < nums_size; index_i++)
  {
    for (index_j = index_i + 1; index_j < nums_size; index_j++)
    {
      if (target == (nums[index_i] + nums[index_j]))
      {
        result[0] = index_i;
        result[1] = index_j;
      }
    }
  }
  return (result);
}