# C LANGUAGE

## BOOKS & VIDEOS
[book: A Tutorial on Pointers and Arrays in C by Ted Jensen](Book_TutorialPointersArrays/PointersArrays.md)  
[book: C Programming A Modern Approach ny K.N.King](Book_CProgrammingAModernApproach/index.md)  
[youtube: Jacob Sorber](Youtube_JacobSorber/index.md)  
[youtube: Mitch Davis](Youtube_MitchDavis/index.md)   
[document: C Standard](C_Standard/index.md)  
[youtube: VertoStudio3D]  

## TOPICS
* [Precedance Chart](Topics/precedance.md), [Rules](Topics/rule.md)
* [Objects/Lrvalues/Operators](Topics/Objects_Lrvalues_Operators.md)
* [Variables](Topics/variables.md), [Functions](Topics/functions.md), [Pointers](Topics/pointers.md), [Function Pointers](Topics/Function_Pointers.md)
* [Arrays](Topics/arrays.md), [Structs](Topics/structs.md), [Pointer vs Arrays](Topics/pointersvsarrays.md)  
* [Enums](Topics/enums.md), [Selection](Topics/Selection.md)
* [Preprocessor](Topics/preprocessor.md), [Other](Topics/other.md), [Memory](Topics/Memory.md)
* [Compiler](Topics/Compiler.md)

## PRACTICE
* [LeetCode](LeetCode/index.md)
