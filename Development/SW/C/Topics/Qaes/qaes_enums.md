# ENUMS QAES 

## QUESTIONS
<details>
<summary><b>Q:</b> Are enum tags worthless & just for readability?</summary>

**A:** Normally an enum is created to use as a *type* rather than just using the names directly.  
However, tags aren't necessary but increases readability.  
</details>

<details>
<summary><b>Q:</b> Do I need to declare varaiable of type enum to use the enumerations?</summary?>

**A:** "These enumerated types can be used like any other type in a program.  
The type name "Boolean" can be used to define variables or the return type of functions.  
The actual enumerations can be used directly in the code."
</details>

## ERRORS
<details>
<summary><b>E:</b> redeclaration of enumerator ‘spring’</summary>

**why:** An enum enumerator has to be unique even if multiple enums are declared (even declared w/ different tags & variables).  
```c
enum{
    spring, summer, fall, winter
};

enum{
    spring, summer, fall, winter    /* ERROR */
};

/* SOLUTION */
enum{
    spring, summer, fall, winter
};

enum{
    spring2, summer2, fall2, winter2    /* ERROR */
};
```
</details>


