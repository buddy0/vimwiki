# OBJECTS LRVALUES OPERATORS QUESITONS ANWSERS ERRORS SOLUTIONS/

## QAES

<details>
<summary><b>Q:</b> So, lvalues are objects not addresses?</summary>

**A:** Yes, for example `ptr = &var;` means ptr will obtain the address of an object, not the address of an address.
</details>
<details>
<summary><b>E:</b> lvalue required as left operand of assignment</summary>

**why:** An address is not an lvalue, merely a value and no object is holding this value. Thus cannot be used as left operand.  
An lvalue is an expression that references an object.  
Thus, LHS requires name of an object or lvlaue. Code below is in *main* function.  
```c
/* ILLEGAL */
int var;
&var = 1; /* error */ /* This is like saying the address is now equal to 1. Want the object's value, not address, to equal 1; */

/* SOLUTION */
int var;
var = 1;
```
</details>

<details>
<summary><b>E:</b> invalid type argument of unary ‘*’ (have ‘int’)</summary>

**solution:** Indirection operator is a unary operator that needs an operand that is pointer type.  
```c
int var = 0;
*var = 1; /* error: * must have a pointer type */
printf()
/* ALSO */
int var = 1;
int *ptr = &var;
printf("%d\n", &*var); /* error: * is not a binary, but unary operator! */
```
</details>

<details>
<summary><b>W:</b> assiggment to 'int *' from 'int' makes pointer from integer without a cast [-Wint-conversion]</summary>

**solution:** A cast of pointer type needs to be explicitly added. 
```c
    int var = 0;
    int *ptr = &var;
    *&ptr = 1;        /* error */
    *&ptr = (int *)1; /* solution */
```
</details>
