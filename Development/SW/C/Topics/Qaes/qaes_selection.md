# SELECTION OPERATORS QUESITONS ANWSERS ERRORS SOLUTIONS

<details>
<summary><b>Q:</b> Range of values: For some reason, the below code example is executing case 0 & 1. Why?</summary>

**A:** [[TutorialsPoint]](https://www.tutorialspoint.com/cprogramming/switch_statement_in_c.htm)  
Case 0 does not have a break.  
If a case is selected, then statements following case will execut unless a break statement is reached.
If no break appears, then the flow of control will fall through to subsequent cases until a break is reached.  

```c
void main(void)
{
    int index = 0;
    
    switch (index)
    {
        case (0):
            printf("Hi0\n");
            
        case (1):
            printf("Hi1\n");
            break;
            
        case (2):
            printf("Hi2\n");
            break;
            
        default:
            break;
    }
}

/* OUTPUT
   Hi0
   Hi1
*/
```

</details>
