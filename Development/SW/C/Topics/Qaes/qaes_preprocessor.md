# PREPROCESSOR QAES

## QUESTIONS
<details>
<summary><b>Q:</b> Difference between compilation directives (<code>#if</code>), & selection statements (<code>if</code>)?</summary>

**A:** Compilaton directives happen before compilation, thus comparisons are made w/ directives such as macros.  
Selection statements happen during compilation, thus comparisons can be made w/ objects. 
</details>
