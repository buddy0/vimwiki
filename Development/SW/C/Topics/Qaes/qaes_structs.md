# STRUCTS QAES
***NOTE:*** Struct w/ no declared variable results in no memory being allocated. 

## QUESTIONS 
<details>
<summary><b>Q:</b> How to declare pointer to struct?</summary>

**A:** Below Code
```c
typedef struct CAR_TYPE_TAG
{
    int year;
    int mileage;
} CAR_TYPE;

void main(void){
    CAR_TYPE mustang;
    CAR_TYPE* ptr = &mustang;
    ptr->year = 2021;
    
    printf("%d\n", mustang.year);
    printf("%d\n", (*ptr).year);
    printf("%d\n", ptr->year);

    /* OUTPUT: 
       2021
       2021
       2021
     */           
}
```
</details>

<details>
<summary><b>Q:</b> How to assign pointer of struct type to array of struct type?</summary>

**A:** Below Code  
```c
typedef struct CAR_TYPE_TAG
{
    int year;
    int mileage;
} CAR_TYPE;

void main(void){
    CAR_TYPE mustang[2];
    CAR_TYPE* ptr = &mustang[0];
    ptr->year = 2021;
    
    printf("%d\n", mustang->year);
    printf("%d\n", (*ptr).year);
    printf("%d\n", ptr->year);

    mustang + 1;   /* Also, `mustang + 1;` */
    ptr->year = 2022;
    printf("%d\n", ptr->year);

    /* OUTPUT:
       2021
       2022
      */  
}
```
</details>

## ERRORS
<details>
<summary><b>E: lvalue array type:</b> <i>error: assignment to expression with array type</i></summary>

**why:** [SO](https://stackoverflow.com/questions/37225244/error-assignment-to-expression-with-array-type-error-when-i-assign-a-struct-f)  
LHS `emp1.lname` is an array type which is *not* assignable.  
"Assignment operator shall have a modifiable lvalue as its left operand"  
"A modifiable lvalue is an lvalue that does not have array type"  
```c
// ERROR CODE
struct{
    char fname[15];
} emp1;

void main(){
    emp1.fname = "Buddy"; // incorrect
}

// SOLUTION 1 - use pointer instead of array when declaring methods
struct{
    char *fname;
} emp1;

void main(){
    emp1.fname = "Buddy";
}

// SOLUTION 2
struct{
    char fname[15];
} emp1;

void main(){
    strcpy(emp1.fname, "Buddy");
    memcpy(emp1.fname, "Buddy", 6);
}

// SOLUTION 3 - Initialization method
struct tag{
    char name[15];
};

struct tag car1 = {"Ford"};
```
</details>

<details>
<summary><b>E: initializing array members:</b> <i>error: expected expression before '{' token</></summary>

**why:**  [SO](https://stackoverflow.com/questions/13275751/expected-expression-before-token#13275857)  
Cannot assign an array with {}, this only works when initializing.  
Sense b/c you do not assign a value to an array element like this `arry[0] = {1};`  
```c
// EEROR CODE
struct{
    char fname[15]; // same applies to char *fname;
} emp1;

void main(){
    emp1.fname = {"Buddy"}; // error b/c were assigning with an initializing method
}

// SOLUTION 1
struct emp{
    char fname[15];
};

void main(){
    struct emp emp1 = {"Buddy"}; // correct b/c were initializing
}
```
</details>

<details>
<summary><b>E: struct tags:</b> <i>error: expected identifier or ‘(’ before ‘=’ token</i><br></summary>

**why:** Without struct tags you can not create new objects!
```c
// ERROR CODE
void main(){
    struct{
        char *name;
    };
    
    struct var1.name = "Buddy"; // error
}

// SOLUTION 1 - Declare variables when declaring struct
void main(){
    struct{
        char *name;
    } var1;

    var1.name = "Buddy";
}

// SOLUTION 2 - Include a struct tag
void main(){
    struct tag{
        char *name;
    } var1;
    
    var1.name = "Buddy";
    struct tag var2.name = "Cuddles";
}
```
</details>

<details>
<summary><b>E: initialize before struct declared:</b> <i>error: expected expression before ‘.’ token /. error: expected ‘=’, ‘,’, ‘;’, ‘asm’ or ‘__attribute__’ before ‘.’ token</i></summary>

**why:** Cannot accessing a struct method when it has not yet been declared.
```c
// ERROR CODE
struct employee{
    char *fname;
};

struct employee var1.fname = "Buddy";

// SOLUTION 1 - Initalize first then access using dot operator
struct employee{
    char *fname;
};

struct employee var1 = {"Buddy"};
struct employee var2;
var2.fname = "Cuddles";
```
</details>

<details>
<summary><b>E:</b> invalid initializer</summary>

**why:** Initializer data type needs to match left operand. Compiler can't implicitly convert for example U32 into a struct type.  
```c

typedef struct STUDENT_TYPE_TAG
{
    U32 grade;
    S32 amount_due;
} STUDENT_TYPE;

static STUDENT_TYPE StudentIndex;

void main(void)
{
    const STUDENT_TYPE studentGrade = studentIndexInfo.grade; /* error */
    const U32 studentGrade = studentIndexInfo.grade;          /* solution */
```
</details>
