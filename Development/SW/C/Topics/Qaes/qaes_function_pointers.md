# FUNCTION POINTERS QAES 

## QUESTIONS
<details>
<summary><b>Q:</b> Want to get address of a function, can I use a regular pointer variable for this?</summary>

**A:** Maybe, but a compiler warning will appear. Have to use function pointer. Look at example below.

***error***
```c
int GetSum(int a, int b){ return (a + b); }

void main(void)
{
    int *add_fun_ptr = &GetSum; /* warning: assignment to ‘int *’ from incompatible pointer type ‘int (*)(void)’ [-Wincompatible-pointer-types] */
}
```

***solution***
```c
int GetSum(int a, int b){ return (a + b); }

void main(void)
{
    int (*add_fun_ptr)(int, int);
    add_fun_ptr = &GetSum;  
}
```
</details>

## COMMENTS
<details>
<summary><b>C:</b> 2 ways for function <i>return type</i> as function pointer</summary>

```c
typedef void *(*FUN_PTR_TYPE)(void);
FUN_PTR_TYPE add_fun_ptr;

/* 1ST WAY */
void* GetSum(void){ return ((*add_fun_ptr)()); }

/* 2ND WAY */
FUN_PTR_TYPE GetSum(void){ return ((*add_fun_ptr)()); }
```

***good to know error***  
```c hl_line="1"
void *(*add_fun_ptr)(void) GetSum(void){ return ((*add_fun_ptr)()); } /* return type is not a type. Is the declaration of a function pointer */

void main(void){}
```
</details>

## ERRORS
<details>
<summary><b>E:</b> Wrong function pointer return type</summary>

***error***    
```c
typedef void (*FUN_PTR_TYPE)(void);
FUN_PTR_TYPE add_fun_ptr;

void* GetSum(void)
{
    return (add_fun_ptr()); /* error: void value not ignored as it ought to be */
}
```

***solution***  
```diff
- typedef void (*FUN_PTR_TYPE)(void);
+ typedef void *(*FUN_PTR_TYPE)(void)
FUN_PTR_TYPE add_fun_ptr;

void* GetSum(void)
{
    return ((*add_fun_ptr)()); /* or return (add_fun_ptr());
}
```
</details>
