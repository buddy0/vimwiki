# QAES MEMORY

# QUESTIONS
<details>
<summary><b>Q:</b> Difference between static & dynamic memory alloaction?</summary>

**A:** [G4G](https://www.geeksforgeeks.org/difference-between-static-and-dynamic-memory-allocation-in-c/)
**memory allocation:** Programs are assigned with memory space
**static memory/compile time allocation:** Declared variables by the compiler.    
**dynamic memory/run time allocation:** Done at the time of execution.  
![Picture of Static Vs Dynamic Memory Allocation](Images/Static_Vs_Dynamic_Memory_Allocation.png)  

</details>

<details>
<summary><b>Q:</b> Below code belongs to what memory section?</summary>

**A:** [SO](https://stackoverflow.com/questions/14588767/where-in-memory-are-my-variables-stored-in-c/14588866)
</details>


