# FUNCTIONS QAES

## WARNINGS
<details>
<summary><b>W</b> implicit declaration of function ‘foo’ [-Wimplicit-function-declaration]</summary>

**why:** I'm calling a function, but compiler doesn't know any info. such as return type, how many & type of parameters.  
**solution:** Insert a declaration, prototype before function is called.  
```c
void main(void)
{
    foo(); /* WARNING */
}

/************************************************************************
 * SOLUTION 
 ***********************************************************************/
/*
 * statment: void foo(void);
 * property: external linkage. 
 * void foo(void); == extern void foo(void);. 
 * Serves no purpose saying extern b/c it's default case.
 * Informs compiler that foo can be called from other files.
 */
void foo(void); 

void main(void)
{
    foo();
}
```
</details>

## QUESTIONS
<details>
<summary><b>Q:</b> Code listed below. Will var_main & var_foo have the same addresses?</summary>

**A:** No, in this case arguments are passed by value & the values are copied to the parameters, which are new local variables.   
**why:** Parameters are objects. The objects values comes from the arguements in a function call. 
```c
void foo(int a[], int var_foo);

void main(void){
    int var_main;
    printf("%p\n", &var_main);
    foo(a, var_main);
}

void foo(int var_foo){
   printf("%p\n", &var_foo);
}
```
</details>
