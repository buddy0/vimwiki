# POINTERS QAES

## QUESTIONS
<details>
<summary><B>Q:</b> Difference between <code>const int* ptr</code> & <code>int* const ptr</code>?</summary>

**A:**  
| code             | description                                                                               |
| ---------------- | ----------------------------------------------------------------------------------------- |
| `const int* ptr` | const only applies to the *, meaning just the pointed object's value.                     |
| `int* const ptr` | const only applies to the variable, meaning just the ptr variable value itself (address). |

`const int* ptr`: Pointer to an object of type *const int*. Thus, pointed objects value cannot be changed.  
*const* can go before or after the *int*, but has to be before the asterisk character.  
```c
/* ERROR */
void main(void)
    int var; 
    const int* ptr = &var;
    *ptr = 1;               /* error: assignment of read-only location '*ptr' */
}

/* WARNING */
void main(void){
    int var; 
    const int* ptr = &var;
    foo(ptr);               /* warning: passing argument 1 of ‘foo’ discards ‘const’ qualifier from pointer target type [-Wdiscarded-qualifiers] */
    printf"%d\n", *ptr);    /* OUTPUT: 1 */
}

void foo(int* par_ptr);
    *par_ptr = 1;       /* note: expected ‘int *’ but argument is of type ‘const int *’ */
```
`int* const ptr`: Constant *ptr* to an object of type int. Thus, *ptr* value (address) cannot be changed.  
*const* has to go after the asterick character.  
```c
/* ERROR */
void main(void){
    int var1;
    int var2;
    int* const ptr = &var1;
    ptr = &var2;                         /* error: assignment of read-only variable ‘ptr’ */
}

/* WARNING */
void main(void){
    int var1, var2;
    int* const ptr = &var1;
    foo(ptr, var2);
}

void foo(int* par_ptr, int par_var2)
    *par_ptr = &var2;                /* warning: assignment to ‘int’ from ‘int *’ makes integer from pointer without a cast [-Wint-conversion] */
```
</details>

## ERRORS
<details>
<summary><b>E:</b> lvalue required as left operand of assignment?</summary>

Weird b/c `ptr + 1` results in an address, but I guess not modifiable?
Makes sense since we can't do `var + 1 = 22`. 
**lvalue:** Expression w. an object type.  
```c
/* ERROR */
void main(void){
    int arry[5] = {10, 20, 30, 40, 50};
    int *ptr = arry;
    ptr + 1 = 22;    /* error */

/* SOLUTION */
void main(void){
    int arry [5] = {10, 20, 30, 40, 50};
    int *ptr = arry;
    *(ptr + 1) = 22;
    printf("%d\n", a[1]);
    /* OUTPUT: 22 */
}
```
</details>

<details>
<summary><b>E:</b> invalid type argument of unary ‘*’ (have ‘int’)</summary>

**S:** According to the C standard, indirection operator needs an operand with a pointer type.  
```c
/* ERROR */
int var = 10;
printf("%d\n", *var); /* error. */

/* SOLUTION */
int var = 10;
printf("%d\n", var);
```
</details>

<details>
<summary><b> </b></summary>

**Solution:** Pointer variables are meant to store addresses, not data values.  
Only objects and functions have addresses.
```c
int* ptr = 1000; /* warning: initialization of `int *` make pointer from integer w/o a case [wint-conversion] */
*1000 = 1;       /* error: invalid type argurement of unary '*' (have 'int')
printf("%d\n", *ptr);
</details>

## WARNINGS
<details>
<summary><b>W:</b> Pointers returning automatic variables address?</summary>

* **"** A function can also return a pointer to an external variable or to a local variable that's been declared static. **"**
* Weird, can't return an address of a local variable (which I get), but  
I can return a pointer variable whose value is the address of the local variable.  
* Returning from *foo*, object *temp* no longer exists, but  
since *ptr* was already pointed to temp, we can still access in memory.  
```c
int *foo(int *ptr){  /* foo is a function returnting a pointer to an object of type int */
    int temp = 30;
    ptr = &temp;
    return &temp;    /* warning: function returns address of local variable [-Wreturn-local-addr] */
}

int main(void){
    int *p, var_1;
    var_1 = 1;
    p = foo(&var_1);
    printf("%d\n", *p);
}
```
```c
int *foo(int *ptr){  /* foo is a function returnting a pointer to an object of type int */
    int temp = 30;
    ptr = &temp;
    return ptr;
}

int main(void){
    int *p, var_1;
    var_1 = 1;
    p = foo(&var_1);
    printf("%d\n", *p);
    *p = 31;
    printf("%d\n", *p);
}

/* OUTPUT: 30 
           31
*/
```
</details>
