# ARRAYS QAES

## WARNINGS
<details>
<summary><b>W:</b> function returns address of local variable [-Wreturn-local-addr]</summary>

**why:** Declaring a local variable inside a function will be destroyed when exiting the function. Thus its address after exiting the function will be invalid.  
[solution](https://www.educative.io/edpresso/resolving-the-function-returns-address-of-local-variable-error)  
```c
int* foo(int a, int b)
{
    int sum = a + b;
    return (&sum); // WARNING!
}

/* Solution */
int* foo(int a, int b, int *sum)
{
    *sum = a + b;
    return (sum); 
}
```
</details>

<details>
<summary><b>W:</b> initialization of ‘int *’ from incompatible pointer type</summary>

[solution](https://stackoverflow.com/questions/44523828/initialization-from-incompatible-pointer-type-warning-when-assigning-to-a-pointe)  
***why:*** ptr and arry have different types.  
***C11 section 6.3.2.1:*** "Except when it is the operand of the sizeof operator, the _Alignof operator, or the unary & operator, or is a string literal used to initialize an array, an expression that has type ‘‘array of type’’ is converted to an expression with type ‘‘pointer to type’’ that points to the initial element of the array object and is not an lvalue."  

```c
/* ERROR */ 
void main(void)
{
    int arry[5] = {0};
    int* ptr = &arry; /* error */
}

/* SOLUTION */
void main(void)
{
    int arry[5] = {0};
    int* ptr = arry; /* arry type int [] auto converts to ptr type int* */
}
```

| code                  | RHS type   | meaning                  |
| --------------------- | ---------- | ------------------------ | 
| ptr                   | int*       |                          | 
| `int* ptr = arry;`    | int [5]    | int [5] decays into int* | 
| `int* ptr = &arry;`   | int* [5]   | type int* != int* []     |
| `int* ptr = arry_2D;` | int (*)[5] | see below warning        |
</details>

<details>
<summary><b>W:</b> initialization of ‘int *’ from incompatible pointer type ‘int (*)[5]’ [-Wincompatible-pointer-types]</summary>

***why:*** Weird b/c  
1D array works: `int* ptr_1D = arry_1D;` RHS type side is int [5], but auto converts to int*.  
2D array does not: `int* ptr_2D = arry_2D;` RHS type is int (*)[5] != int*.  
```c
/* ERROR */
void main(void)
{
    int arry_2D[5][5] = {0};
    int *ptr_2D = arry_2D; /* error */
}

/* SOLUTION */
void main(void)
{
    int arry_2D[5][5] = {0};
    int *ptr_2D = *arry_2D; /* == &arry_2D[0] */
    /* OR */
    int *ptr_2D = &arry_2D[0][0];
}
```
</details>

## ERRORS
<details>
<summary><b>E:</b> assignment to expression with array type</summary>

**why:** The lvalue is an *array* type which is *NOT* assignable. "Assignment operator shall have a modifiable lvalue as its left operand. A modifiable lvalue is an lvalue that does not have an array type".
[solution](https://stackoverflow.com/questions/37225244/error-assignment-to-expression-with-array-type-error-when-i-assign-a-struct-f) 
```c
/* ERROR */
void main(void){
    char a[] = "buddy";
    a = "beary";  /* error */
}

// SOLTION 1
void main(void){
    char *a = "buddy";
    a = "beary";
}

// SOLUTION 2
void main(void){
    char a1[] = "buddy", a2[6];
    strcpy(a2, a1);
}
```
</details>

## WIERD FINDINGS
<details>
<summary><b>Array of pointer elements:</b> Accessing them with 1D & 2D</summary>

Can be explained by the file `PointersArrays.md`, Chapter 6.  
When an array is declared with pointer elements, then accessing them through:  
* 1D obtains the address. B/c array[i] == *(array + i) == the pointer value itself.
* 2D obtains the value. B/c array[i][j] == *(*(array + i) + j) == the pointed object's value.
```c
void main(void)
{
    int var_1 = 1;
    int var_2 = 2;
    int var_3 = 3; 
    int var_4 = 4;
    int var_5 = 5;
    int* array[5] = {&var_1, &var_2, &var_3, &var_4, &var_5};
    
    for (int i = 0; i < 5; i++)
    {
        printf("array[%d] = %p\n", i, array[i]);
        printf("array[%d][0] = %d\n", i, array[i][0]);     /* Weird how using 2D array, we can get the value while 1D gets the address. */
        printf("array[0][%d] = %d\n", i, array[0][i]);
        printf("array[%d][2] = %d\n", i, i, array[i][2]);
        printf("array[%d][3] = %d\n", i, i, array[i][3]);
        printf("array[%d][%d] = %d\n", i, i, array[i][i]);
        printf("-------------------------------------\n");
    }
}

 array[0]  array[1]  array[2]  array[3]  array[4]    object
+---------+---------+---------+---------+---------+
| &var_1  | &var_2  | &var_3  | &var_4  | &var_5  |  value 
+---------+---------+---------+---------+---------+
 &array[0] &array[1] &array[2] &array[3] &array[4]   address

/* OUTPUT
type as unsigned char -> much better
array[0] = 0x7ffd4d7a83d7
array[0][0] = 1
array[0][0] = 1
array[0][2] = 3 x
array[0][3] = 4
array[0][0] = 1
-------------------------------------
array[1] = 0x7ffd4d7a83d8
array[1][0] = 2
array[0][1] = 2
array[1][2] = 4 x
array[1][3] = 5
array[1][1] = 3
-------------------------------------
array[2] = 0x7ffd4d7a83d9
array[2][0] = 3
array[0][2] = 3
array[2][2] = 5 x
array[2][3] = 2
array[2][2] = 5
-------------------------------------
array[3] = 0x7ffd4d7a83da
array[3][0] = 4
array[0][3] = 4
array[3][2] = 3 x
array[3][3] = 0
array[3][3] = 0
-------------------------------------
array[4] = 0x7ffd4d7a83db
array[4][0] = 5
array[0][4] = 5
array[4][2] = 0 x
array[4][3] = 0
array[4][4] = 0
-------------------------------------

type as int -> some numbers don't make sense
array[0] = 0x7ffe960c75d8
array[0][0] = 1
array[0][0] = 1
array[0][2] = 0
array[0][3] = 0
array[0][0] = 1
-------------------------------------
array[1] = 0x7ffe960c75dc
array[1][0] = 2
array[0][1] = 2
array[1][2] = 1
array[1][3] = 1
array[1][1] = 3
-------------------------------------
array[2] = 0x7ffe960c75e0
array[2][0] = 3
array[0][2] = 3
array[2][2] = 2
array[2][3] = 2
array[2][2] = 5
-------------------------------------
array[3] = 0x7ffe960c75e4
array[3][0] = 4
array[0][3] = 4
array[3][2] = 3
array[3][3] = 3
array[3][3] = -1777568296
-------------------------------------
array[4] = 0x7ffe960c75e8
array[4][0] = 5
array[0][4] = 5
array[4][2] = 4
array[4][3] = 4
array[4][4] = -1777568292
-------------------------------------
*/
```
</details>

