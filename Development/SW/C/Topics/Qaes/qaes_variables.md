# VARIABLES QAES
[variables shared between source files](https://stackoverflow.com/questions/1433204/how-do-i-use-extern-to-share-variables-between-source-files)  

## QUESTIONS
<details>
<summary><b>Q:</b> How does compiler know a variable is an <b>lvalue or rvalue</b>?</summary>

**A:** Variable names (idenitifiers) are objects; represent named storage place during execution. Thus, they represent lvalues; expressions respresenting objects (not just an address).
</details>

<details>
<summary><b>Q:</b> Does declaring a global variable in header file require the declaration specifier storage class of <code>extern</code> enabling its use for other files?</summary>

**A:** Yes.  
</details>

<details>
<summary><b>Q:</b> Does declaring a variable <b>allocate storage</b> location?</summary>  

**A:** [G4G](https://www.geeksforgeeks.org/difference-between-definition-and-declaration/)  
**declaration:** Informs compiler about properties of a variable: name of variable, type of value it holds, & initial value if any.  
**definition:** Allocates memory for the variable.  
In C, *declaration* & *definition* takes place at the same time.  
**Ex.** `int a;` Variable name `a` & type `int` sent to compiler to be stored into a data structure known as **symbol table**. Additionaly, a size of 2 or 4 bytes will allocated in memory.  
**Ex.** `extern int a;` The above comments can be carried down except for the memory allocation.
</details>

<details>
<summary><b>Q:</b> What happens to a <b>declared, unintialization variable</b> in C?</summary>

**A:** [SO](https://stackoverflow.com/questions/1597405/what-happens-to-a-declared-uninitialized-variable-in-c-does-it-have-a-value) - Static or global initialized to 0, indeterminate if storage class is *auto*.  
</details>

<details>
<summary><b>Q:</b> How to modify a <code>const</code> variable in C?</summary>

**A:** [SO](https://stackoverflow.com/questions/1597405/what-happens-to-a-declared-uninitialized-variable-in-c-does-it-have-a-value) - Through a pointer.  
**"** The variables declared using const keyword, get stored in .rodata segment, but we can still access the variable through the pointer and change the value of that variable **"**.   
```c
/* ILLEGAL */
void main(void){
    const int var = 1;
    var = 2;    /* error: assignment of read-only variable ‘var’ */
}
/* SOLUTION OKAY */
void main(void){
    const int var = 1;
    int *ptr = &var;
    *ptr = 2;    /* warning: initialization discards ‘const’ qualifier from pointer target type [-Wdiscarded-qualifiers] */
}
/* SOLUTION BETTER */
void main(void){
    const int var = 1;
    *((int *)&var) = 2; 
    printf("%d\n", var);

    /* OUTPUT: 2 */
}
```
</details>

<details>
<summary><b>Q:</b> What is the <b>scope</b> of variables in a conditional, loop?</summary>

**A:** Scope of a local variable only falls within enclosing brackets [[SO]](https://stackoverflow.com/questions/8543167/scope-of-variables-in-if-statements).  
```c
void main(void){
    int state = 0;
    int var_fuc = 0;               /* START var_fuc local scope */
    
    if(1)
    {
        int var_cond = 0;          /* START var_cond local scope */
        printf("%d\n", var_cond);
        
        while(state == 0){
            int var_loop = 0;      /* START var_loop local scope */
            state = 1;    
        }                          /* END var_loop local scope */
    }                              /* END var_cond local scope */
    
    printf("%d\n", var_cond);      /* error: ‘var_cond’ undeclared (first use in this function) */
    printf("%d\n", var_loop);      /* error: ‘var_loop’ undeclared (first use in this function) */
}                                  /* END var_fuc local scope */
```
</details>

<details>
<summary><b>Q:</b> How do to not use global variables?</summary>

1. Use static variables if variable (preserves state) needs to be reused in multiple function calls.
1. Create a local variable that is a paramter to a function.
2. Use a struct.
</details>

# ERRORS
<details>
<summary><b>E:</b> 'global_var' undeclared (first use in this function)</summary>

**why:** Compiler processes one file at a time & only remembers info. in file that is processing. Therefore it doesn't know that the variable declaration, definition is in another file.  
**solution:**  
1. If variable declared in same file, declaration must precede its use.
2. If variable declared in another file, exact same definition must be included in file but w/ prefix `extern`.
```c
/********************************ERROR*******************************/

/**********/
/* main.c */
/**********/
extern void foo(void); 
int global_var = 1;

void main(void){
    foo();
}

/*********/
/* foo.c */
/*********/
void foo(void){
    global_var++; /* ERROR */
}

/*******************************SOLUTION*********************************/

/**********/
/* main.c */
/**********/
int global_var = 1;

void main(void)
{
    foo();
}
/***********************************************************************/
/* foo.c                                                               */
/*                                                                     */
/* statment: extern int global_var;                                    */
/* properties: static storage duration, file scope, & external linkage.*/
/* extern informs compiler that global_var is being used in this file, */
/* BUT it's declared in another file so don't declare space in memory. */
/***********************************************************************/
extern int global_var;

void foo(void)
{
    global_varr++;
}
```
</details>

<details>
<summary><b>E:</b> multiple definition of 'global_var';</summary>

**why:** Compiler knows that `global_var` is already declared? I thought compiler only remembers info. that file is processing tho?  
**solution:** Add extern before global variable.  
```c
/********************************ERROR*******************************/

/**********/
/* main.c */
/**********/
extern void foo(void); 
int global_var = 1;

void main(void)
{
    foo();
}

/*********/
/* foo.c */
/*********/
int global_var; /* ERROR */

void foo(void)
{
    global_var++;
}

/*******************************SOLUTION*********************************/

/**********/
/* main.c */
/**********/
int global_var = 1;

void main(void)
{
    foo();
}

/*********/
/* foo.c */
/*********/
extern int global_var;

void foo(void)
{
    global_varr++;
}
```
</details>
