# FUNCTION POINTERS
*[Questions Answers Errors Solutions](Qaes/qaes_function_pointers.md)*  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [EXAMPLE CODE](#example_code)
</details>

**function pointers:** Pointer variable that stores address of a function.  
Points to code, not data. Typically stores start of executable code.  
***declaration example w/o `typedef`:*** `int (*int)(foo_ptr)(int, int);`  *foo_ptr* is a function pointer.  
***declaration example w/ `typedef`:*** `typedef (*int)(foo_ptr_t)(int, int);` *foo_ptr_t* is a function pointer type.  
***invoking function w/ function pointer:*** `foo_ptr(5, 5)`. Function pointer acts like a function call.  
***hard to understand, syntax is to blame:*** Unlike variable syntax where identifier is at right, function pointer identifier is in middle. To read use the RL rule.  
***why use function pointers:*** when functions have same signature,  
***notes:***  
* Usually defined by `typedef`, and used as param & return values.
* A function's name can also be used to get functions' address.
* [**SO**](https://stackoverflow.com/questions/1591361/understanding-typedefs-for-function-pointers-in-c): Like any other pointer, function pointer needs to be *type*d correctly. Functions are defined by their return value and type(s) of parameter(s). Thus in order to fully describe a function, return value & type of each parameter it accepts. When using typedef, a friendly name is used to create and reference pointers using that definition. This friendly name can point to **ANY** function that returns same value & takes in same type parameters. 

## EXAMPLE CODE <a name="example_code"></a>
<details>
<summary><b>EX CODE 1 - [YT:TK](https://www.youtube.com/watch?v=KrjrCgttZuM)</b></summary>

```c
/* var is a variable w/ type int */
int var;

/* var_t is a type, alias for int */
typedef int var_t;

/* function type that returns an int and takes 2 ints */
int(int, int)

/* foo is a function that returns an int and takes 2 ints */
int foo(int, int);

/* foo is a function that returns a pointer to an int and takes 2 ints */
int *foo(int, int);

/* foo_ptr is a function pointer that returns an int and takes 2 ints */
int (*foo_ptr)(int, int);

/* foo_ptr_t is a function pointer type, alias for a function pointer that returns int and takes 2 ints */
typedef int (*foo_ptr_t)(int, int);
```
</details>

<details>
<summary><b>EX CODE 2 - [YT:NA](https://www.youtube.com/watch?v=BRsv3ZXoHto)</b></summary>

***declaration of pointer to an array:***  
```c
/* WRONG */
int *ptr[10]; /* ptr is an arary of 10 pointers pointing to integers */

/* CORRECT */ 
int (*ptr)[10]; /* ptr is a pointer variable pointing to an array of 10 integers */  
```

***declaration, assignment, & usage of pointer to a function:***  
```c
int add(int a, int b){ return (a+b); }

/* DECLARATION */
int (*ptr)(int, int); /* ptr is a pointer variable pointing to a function containing 2 int arguments & it returns an int. */

/* INITIALIZATION & USAGE */
int (*ptr)(int, int) = &add;  
int result = (*ptr)(10, 20);   /* (*ptr) == add */  
/* or */  
int (*ptr)(int, int) = add;  
int result = ptr(10, 20);
```
</details>

<details>
<summary><b>BUDDY: FUNCTION POINTER AS VARIABLE</b></summary>

***w/o `typedef`***  
```c
int add(int a, int b){ return (a + b);}
int mult(int a, int b){ return (a * b);}

void main(void)
{
    int (*add_fun_ptr)(int, int) = &add;  /* or ... = add; */
    int (*mult_fun_ptr)(int, int) = &mult;
    
    printf("sum = %d\n", (*add_fun_ptr)(5, 5)); /* or ... = add_fun_ptr(5, 5); */
    printf("multipy = %d\n", (*mult_fun_ptr)(5, 5));
}
```

***w/ `typedef`***   
```c
int add(int a, int b){ return (a + b);}
int mult(int a, int b){ return (a * b);}

typedef int (*MATH_PTR_TYPE)(int, int);

void main(void)
{
    MATH_PTR_TYPE add_fun_ptr = &add;
    MATH_PTR_TYPE mult_fun_ptr = &mult;
    
    printf("sum = %d\n", add_fun_ptr(5, 5));
    printf("multipy = %d\n", mult_fun_ptr(5, 5));
}
```
</details>

<details>
<summary><b>BUDDY: FUNCTION POINTER AS PARAMETER</b></summary>

***w/o `typedef`***  
```c
int add(int a, int b){ return (a + b);}
int mult(int a, int b){ return (a * b);}

int MathCall(int (*math_call_ptr)(int, int), int a, int b){ return (*math_call_ptr)(a, b); }

void main(void)
{
    int (*add_fun_ptr)(int, int) = &add;
    int (*mult_fun_ptr)(int, int) = &mult;
    
    printf("sum = %d\n", MathCall(add_fun_ptr, 5, 5));
    printf("multipy = %d\n", MathCall(mult_fun_ptr, 5, 5));
}
```
OR  
```c
int add(int a, int b){ return (a + b);}
int mult(int a, int b){ return (a * b);}

int MathCall(int (*math_call_ptr)(int, int), int a, int b){ return (*math_call_ptr)(a, b); }

void main(void)
{
/* - */   int (*add_fun_ptr)(int, int) = &add;
/* - */   int (*mult_fun_ptr)(int, int) = &mult;

/* + */  printf("sum = %d\n", MathCall(&add, 5, 5));
/* + */  printf("multipy = %d\n", MathCall(&mult, 5, 5));
}
```

***w/ `typedef`***  
```c
int add(int a, int b){ return (a + b);}
int mult(int a, int b){ return (a * b);}

typedef int (*MATH_FUN_PTR_TYPE)(int, int);

int MathCall(MATH_FUN_PTR_TYPE math_call_ptr, int a, int b){ return (*math_call_ptr)(a, b); }

void main(void)
{
    printf("sum = %d\n", MathCall(&add, 5, 5));
    printf("multipy = %d\n", MathCall(&mult, 5, 5));
}
```
</details>

<details>
<summary><b>BUDDY: FUNCTION PARAMAETER AS RETURN</code></b></summary>

***w/o `typedef` -*** <span style="color:red">**ERROR**</span> 
```c
int add(int a, int b){ return (a + b);}

int (*add_fun_ptr)(int, int) Get_Add_Fun_Ptr(void){ return(&add); }

void main(void)
{
    int (*add_fun_ptr)(int, int);
    printf("sum = %d\n", (*add_fun_ptr)(5, 5));
}
```

***w/ `typedef`***  
```c
int add(int a, int b){ return (a + b);}

typedef int (*ADD_FUN_PTR_TYPE)(int, int);

ADD_FUN_PTR_TYPE Get_Add_Fun_Ptr(void){ return(&add); }

void main(void)
{
    ADD_FUN_PTR_TYPE add_fun_ptr = Get_Add_Fun_Ptr();   /* Q: Why can't use & or need ()? */
    printf("sum = %d\n", (*add_fun_ptr)(5, 5));         /* A: B/c we want the return value! */
}
```
</details>


### EXAMPLE

```c
typedef unsigned char U8;

U8 GetOne(void){ return (1); }

typedef U8 (*FOO_PTR_TYPE)(void);

void main(void)
{
    FOO_PTR_TYPE get_one_fun_ptr = &GetOne;
    
    printf("%d\n", get_one_fun_ptr());  /* OUTPUT: 1 */
}
```

```c
typedef unsigned char U8;

typedef U8 (*FOO_PTR_TYPE)(void);

U8 Foo_One(void){ return(1); }

void main(void)
{
    FOO_PTR_TYPE fun_ptr_1 = Foo_One;
    
    FOO_PTR_TYPE fun_ptr_2 = fun_ptr_1;
    
    printf("%d\n", fun_ptr_2()); /* OUTPUT: 1 */
}
```

```c
typedef unsigned char U8;

typedef U8* (*FUN_PTR_TYPE)(void);

U8 Global_Var = 1;

U8* GetOneFun(void){ return (&Global_Var); } 

void main(void)
{
typedef unsigned char U8;

typedef U8* (*FUN_PTR_TYPE)(void);

U8 Global_Var = 1;

U8* GetOneFun(void){ return (&Global_Var); } 

void main(void)
{
    FUN_PTR_TYPE fun_ptr_one;
    fun_ptr_one = &GetOneFun;
    
    printf("%p\n", &Global_Var);    /* OUTPUT: same address */
    printf("%p\n", fun_ptr_one());
    printf("%p\n", GetOneFun());
}
```

```c
typedef unsigned char U8;

typedef U8* (*FUN_PTR_TYPE)(void);

U8 Global_Var = 1;

U8* GetOneFun(void){ return (&Global_Var); } 

void main(void)
{
    FUN_PTR_TYPE fun_ptr_one;
    fun_ptr_one = GetOneFun();
    
    printf("%p\n", &Global_Var);
    printf("%p\n", fun_ptr_one);
    printf("%p\n", GetOneFun());
}
```
