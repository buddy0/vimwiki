# STRUCTS - Neso Academy
*[Questions Answers Errors Solutions (QAES)](Qaes/qaes_structs.md)*

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [STRUCTURE](#structure)
2. [STRUCTURE TAG](#structure_tag)
3. [TYPEDEF](#typedef)
4. [POINTER TO STRUCTURE VARIABLE](#ptr_to_structure_variable)
</details>
 
## STRUCTURE <a name="structure"></a>
**structure:** User defined data type used to group elements of different types into a single type. When a data type is created, no memory is allocated. Memory is only allocated when variables are created. Can be initalized with `{}` or by the dot operator.  
**declaration:** 
```c
struct [tag]{
    // methods
} [variables];   
```
**initialization:**  
Initlization/assigning of elements within a struct's {} are *not* allowed.  
```c
/* METHOD 1 - when declaring a struct */
struct{
    char *food;
} buddy = {"mac and cheese"}; /* variable buddy of type struct */

/* METHOD 2 - outside of struct */
struct{
    char *food;
} buddy;

void main(){
    buddy = {"mac and cheese"};
}
```
**designated initialization:**  
Allows structures to be initialized in any order.  
Dot operator is needed to access the members of the structure.  
```c
struct abc{
    int x, y, z;
};

void main(){
    struct abc a = {.y = 2, .x = 1, .z = 3};
}
```
**accessing members of structure:** Achieved through the **dot operator (`.`).   
```c
struct{
    age;
}student;

student.age = 22;
```

## STRUCTURE TAG <a name="structure_tag"></a>
Identify a particular kind of structure.  
Without struct tags you can not create new objects!!  
```c
/* If we want a variable to be declared in the local scope,
   then we have to redeclare the struct within the function. */
struct{
    /*  elements */
} emp1, emp2;

int manager(){
    struct {
        /* elements */
    } manager;
}
```
```c
/* Avoid redundacy, a type for a structure can be created.
   employee is a structure tag which helps create a type of the structure. */
struct employee{
    /* elements */
};

int manager(){
    struct employee manager;
}

int main(){
    struct employee emp1;
    struct employee emp2;
}
```

## TYPEDEF <a name="typedef"></a>
**typedef:** User can create their own data types.  
**syntax:** `typedef existing_data_type new_data_type;`    
![typedef struct](Images/typedefstruct.png)

## ARRAY OF STRUCTURES <a name=""></a> 
"Instead of declaring multiple variables, we can also declare an array of structure in which each element of the array will represent a structure variable."   
```c
struct car{
    // members
};

void main(){
    struct car c[2];
}
```

## POINTER TO STRUCTURE VARIABLE - [link](https://overiq.com/c-programming-101/pointer-to-a-structure-in-c/) <a name="ptr_to_structure_variable"></a>
***pointer to a structure:*** Pointer variable points to the address of a structure variable
***declaratoin:*** `struct [tag] *<name_ptr>;`.  
***initalization:*** `<name_ptr> = &<desired_struct>;`  
***access members:*** 2 ways - Both give the contents of the desired structure variable & access a particular member of the structure.
1. **indirection, dereferencing (`*`) operator and dot (`.`) operator:** `(*ptr_var).<member_name>`. Parentheses needed b/c the dot operator has higher precedance than indirection operator ([link](https://en.cppreference.com/w/c/language/operator_precedence)).  
2. **arrow (`->`) or membership operator:**
```c
struct car{
    int model;
};

struct car ford = {1, 2};
struct car *ptr_car = &ford;  /* ptr_car is a pointer to some variable of type struct ford*/

(*ptr_car).model = 4;   /* (*ptr).x == (*&ford).x == ford.x */
ptr_car->model = 3;
```
