# VARIABLES
[Questions Answers Errors Solutions (QAES)](Qaes/qaes_variables.md)

## OVERVIEW
**variable:** Storage location. 
* [BZ](https://boredzo.org/pointers/)
```c
int var = 1;

/*
      var
    +-----+
    |     | 
    +-----+
      &var

    var is a box/cell in memory.
    Each box can be located by its address.
    Accessing the address actually accesses the contents of the box.
    Whenever var is used in the program. We are going to the address & retrieving its contents/value.
 */
```


