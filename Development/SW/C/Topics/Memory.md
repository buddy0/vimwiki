# MEMORY
[Questions Answers Errors Solutions](Qaes/qaes_memory.md)

## MEMORY LAYOUT - [G4G](https://www.geeksforgeeks.org/memory-layout-of-c-program/)  
```c
/*
+------------------------------+
| env/cmd line arguements      |
+------------------------------+
| stack segment                | --> initialized auto vars         
+------------------------------+
| ...                          |
+------------------------------+
| heap segment                 | --> dynamic allocation
+------------------------------+
| bss segment                  | --> unitialized global & static vars (0 or unspecifed)
+------------------------------+
| data segment (RW & RO areas) | --> initialized global & static vars
+------------------------------+
| text segment (RO)            | --> initialized auto vars, funciton info., & executable instructions
+------------------------------+
*/
```
## EXAMPLE 
1. `$ gcc -nostartfiles -nostdlib -nodefaultlibs main.c - o output`
2. `$ size output`

***note:*** adding `const` doesn't change anyting  

### LOCAL SCOPE
| code                  | text    | data    | bss | dec     | comment                             |
| --------------------- | ------- | ------- | --- | ------- | ----------------------------------- |  
| empty main function   | 248     | 208     | 0   | 456     |                                     |
| `int var;`            | 248     | 208     | 0   | 456     | compiler smart, doesn't do anything | 
| `int var = 0;`        | **255** | 208     | 0   | **463** | add to text & stack                 |
| `int var = 1;`        | **255** | 208     | 0   | **463** | add to text & stack                 |
| `static int var;`     | 248     | 208     | 0   | **464** | ?add to stack?                      |
| `static int var = 0;` | 248     | 208     | 0   | **464** | ?add to stack?                      |
| `static int var = 1;` | 248     | **212** | 0   | **460** | add 4 to data                       |

### GLOBAL SCOPE  
| code                  | text | data    | bss   | dec     |comment      |
| --------------------- | ---- | ------- | ----- | ------- |------------ |  
| empty main function   | 248  | 208     | 0     | 456     |             |
| `int var;`            | 248  | 208     | **8** | **464** | add to bss  | 
| `int var = 0;`        | 248  | 208     | **8** | **464** | add to bss  |
| `int var = 1;`        | 248  | **212** | 0     | **460** | add to data |
| `static int var;`     | 248  | 208     | **8** | **464** | add to bss  |
| `static int var = 0;` | 248  | 208     | **8** | **464** | add to bss  |
| `static int var = 1;` | 248  | **212** | 0     | **460** | add to data |  
