# ARRAYS
[Questions Answers Errors Solutions (QAES)](Qaes/qaes_arrays.md)

**[CS]:** `E1[E2] == (*((E1) + (E2)))`    
**[CS]:** `int x[3][5];` x is an array of 3 element objects, each of which is an array of 5 ints.  

```c
void main(void)
{
    int a[5] = {10, 20, 30, 40, 50};
    printf("a[0]   = %d\n", a[0]);
    printf("*(a+0) = %d\n", *(a+0));
    printf("a      = %p\n", a);      /* a == &a[0] */
    printf("a+1    = %p\n", a+1);

    /* OUTPUT: 10
               10 
               0x7ffde76aa3f0
               0x7ffde76aa3f4
     */
}
```

## ARRAY_NAME VS `&`ARRAY_NAME 
**[[SO]](https://stackoverflow.com/questions/19344162/how-array-name-has-its-address-as-well-as-its-first-element-in-it):** Array name decays into a pointer to the first element when used in an expression. 3 exceptions:  
1. Array appears as operand of `sizeof`
2. Array's address is taken w/ `&` operator ([CS `&`](../C_Standard/Other_Operands.md))
3. Array is a string or wide-string literal identializer
```c
int a[5] = {0};
printf("%p", a);     /* OUTPUT: 0x12345abcdef. */
printf("%p", &a[0]); /* OUTPUT: 0x12345abcdef. */

/* If this wasn't an expression, then would be printing an address of a pointer (address of an address).
   However, we're taking address of array, not the pointer b/c this is an exception. */
printf("%p", &a);    /* OUTPUT: 0x12345abcdef. */

```

**[[G4G]](https://www.geeksforgeeks.org/whats-difference-between-array-and-array-for-int-array5/):** `a` and `&a` value (address) are the same, however they are different types of addresses. **Ex.**  
```c
   int a[5] = {0};
   printf("a      = %p\n&a     = %p\n", a, &a);          /* array is a pointer to first element of the array */
   printf("a[0]   = %d\n&a[0]  = %p\n", a[0], &a[0]);
   printf("a + 1  = %p\n&a + 1 = %p\n", a + 1, &a + 1);  /* &array is a pointer to whole array of 5 ints */

   /* OUTPUT 
      a      = 0x11242ef345b0
      &a     = 0x11242ef345b0
      a + 1  = 0x11242ef345b4
      &a + 1 = 0x11242ef345c8

      DIAGRAM             
      a   
      +------------------------------------------------------------------------------------+
      |   a[0], *(a+0)    a[1], *(a+1)    a[2], *(a+2)   a[3], *(a+3)     a[4], *(a+4)     | 
      | +---------------+---------------+---------------+---------------+----------------+ | 
      | |+-------------+|+-------------+|+-------------+|--------------+|+--------------+| |
      | || 0           ||| 0           ||| 0           || 0            ||| 0            || | 
      | |+-------------+|+-------------+|+-------------+|--------------+|+--------------+| | 
      | |0x11242ef345b0 |0x11242ef345b4 |0x11242ef345b8 |0x11242ef345c0 |0x11242ef345c4  | |0x11242ef345c8
      | +---------------+---------------+---------------+---------------+--------------- + |
      +------------------------------------------------------------------------------------+ 
      ▲ ▲               ▲                                                                  ▲
      | |               |                                                                  |
      | a              a+1                                                                 |
      &a=0x11242ef345b0                                                                 &a+1=0x11242ef345c8
    */
```

## INDIRECTION OPERATOR - OPERAND BEING ARRAY_NAME
```c
int a[5] = {10, 20, 30, 40, 50};
printf("%d\n", *a);       // *a       == *&a[0]       == a[0]
printf("%d\n", *a + 1);   // *a + i   == *&a[0] + i   == a[0] + i
printf("%d\n", *(a + 1)); // *(a + i) == *(&a[0] + i) == a[i]
                          //               ^ by sizeof(a[0])
/* OUTPUT
   10
   11
   20
 */
```

## SUMMARY
`int a[], b[][];`  
| LHS - PA      | eq op | RHS - AS  | comment                                          |
| ------------- | ----- | --------- | ------------------------------------------------ |
| `a`           | ==    | `&a[0]`   | Name of aray is a pointer.                       |
| `a + i`       | ==    | `&a[i]`   | Pointer arithmetic to get address.               |
| `*a`          | ==    | `a[0]`    | Dereferencing an array (mostly pointer) results. |
| `*(a+i)`      | ==    | `a[i]`    | Pointer arithmetic to get value for 1D array.    |
| `*(*(a+i)+j)` | ==    | `b[i][j]` | Pointer arithmetic to get value for 2D array.    |
| `a`           | !=    | `&a`      | Same but different addresses.                    |
