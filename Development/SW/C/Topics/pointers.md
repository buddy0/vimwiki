# POINTERS
[Questions Anwsers Errors Solutions](Qaes/qaes_pointers)  
[video: everything you need to know about pointers](https://www.youtube.com/watch?v=Rxvv9krECNw&t=2s)    

* **pointer:** Variable that stores a memory location.  
* **indirect addressing:** Accessing a variable w/o using its name. Syntax `*address`. Goto address and retrieve its value.
* Every memory cell has an address and possibly a value.
* Only objects and functions can have addresses.

![memory](Images/pointers.png)

## EXAMPLES
```c
void main(void)
{
    int x, y:
    Task(&x, &y);
}

void Task(int *p1, int *p2){
    /* Function pointer parameters are expecting addresses. Pass Task(&x, &y) or Task (ptr1, ptr2). */
    /* The parameters are expected addresses of ints. */
    /* *p1 is an int and the contents of *p1 = *&x = x is int as well. */
}
```

```c
int var = 1;
int *ptr = &var;

printf("%d\n", var);   /* OUTPUT: 1 */
printf("%d\n", *var);  /* error: invalid type argument of unary '*' (have int) */ /* *var == *value_of_var and value_of_var treated like an address. */
printf("%d\n", *&var); /* OUTPUT: 1 */

printf("%d\n, ptr);    /* OUTPUT: &var */
printf("%d\n, *ptr);   /* OUTPUT: 1 */ /* *ptr == *&var == var */
printf("%d\n, *&ptr);  /* OUTPUT: &var */
```



[Neso Academy](nesoacademy.md)
