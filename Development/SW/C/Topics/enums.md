# ENUMS
[Questions Answers Errors Solutions](Qaes/qaes_enums.md)  
***useful links:*** [Enumerated Types](https://www.cs.utah.edu/~germain/PPS/Topics/C_Language/enumerated_types.html), [C enumeration declarations](https://docs.microsoft.com/en-us/cpp/c-language/c-enumeration-declarations?view=msvc-160)  

## DEFINE
**enumeration type (enum):** User define data type used to assign names to inegral constants.  
***enumeration type declaration:*** Gives name of optional enumeration tag & defines a set of named integer identifiers (enumeration set, enumeration constatns, enumerators, or members).  
***why use enums:*** A *enum* variable only takes 1 value. 
**Ex.** `enum boolean {false, true}`. A new type *enum boolean* is created.  
***enumeration type variable declaration:***    
1. w/ enum tag
2. w/ enum tag & variable inside
3. w/ enum tag & variable outside
4. w/ enum typedef
```c
#include <stdio.h>

// #define ENUM_TAG
// #define ENUM_TAG_VAR_IN
// #define ENUM_TAG_VAR_OUT
// #define ENUM_TYPEDEF

#ifdef ENUM_TAG
enum SEASON_TYPE_TAG
{
    spring, 
    summer, 
    fall, 
    winter
};
#endif /* ENUM_TAG */

#ifdef ENUM_TAG_VAR_IN
enum SEASON_TYPE_TAG
{
    spring, 
    summer, 
    fall, 
    winter
} season;
#endif /* ENUM_TAG_VAR_IN */

#ifdef ENUM_TAG_VAR_OUT
enum SEASON_TYPE_TAG
{
    spring, 
    summer, 
    fall, 
    winter
};
#endif /* ENUM_TAG_VAR_OUT */

#ifdef ENUM_TYPEDEF
typedef enum SEASON_TAG
{
    spring, 
    summer, 
    fall, 
    winter
} SEASON_TYPE;
#endif /* ENUM_TYPEDEF */

void main(void)
{
    #ifdef ENUM_TAG
    printf("%d\n", spring);
    #endif /* ENUM_TAG */

    #ifdef ENUM_TAG_VAR_IN
    season = summer;
    printf("%d\n", season);
    #endif /* ENUM_TAG_VAR_IN */

    #ifdef ENUM_TAG_VAR_OUT
    enum SEASON_TYPE_TAG season = fall;
    printf("%d\n", season);
    #endif /* ENUM_TAG_VAR_OUT */

    #ifdef ENUM_TYPEDEF
    SEASON_TYPE season = winter;
    printf("%d\n", season);
    #endif /* ENUM_TYPEDEF */
}
```


**Ex.**   
1. `enum boolean {false, true} check;`
2. `enum boolean {false, true};` `enum boolean check`





