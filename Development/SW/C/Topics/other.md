# OTHER

## INTRINSIC - [link1](https://en.wikipedia.org/wiki/Intrinsic_function), [link2](https://stackoverflow.com/questions/2268562/what-are-intrinsics)  
* **intrinsic function, built-in function:** Implementation is handled specially by the compiler. 
* Function which compiler implements directly when possible, rather than linking to a library-provided implementation of the function.  
* Instrinsic function is implemented & provided by the compiler in lieu of a function call. The definition in the C program is not written nor does it need to be linked. 
* Instrinsic & inline functions are *similar*: compiler produces compiled transalation of the function directly into object code, omitting overhead of function call.  
* Instrinsic functions are *different*; "present", no header file containing function declaration. Definition located inside compiler. 
* Inline functions are *different*; the compiler reads the source code. Headed file needed if function not declared. Definition located inside program. 

## FILE INCLUSION - COMPILE SEARCH - [link](https://commandlinefanatic.com/cgi-bin/showarticle.cgi?article=art026)  
1. `#include "file_name"` 
2. `#include <file_path>`  
* Using `""`, compiler searches the current directory first. If header file isn't found, then `<>` search path is used, where the include path is hopefully in the standard list of system directories.  

## DEFINE VS DECLARE - [link](https://stackoverflow.com/questions/1433204/how-do-i-use-extern-to-share-variables-between-source-files)
**define:** Compiler is informed that variable exists (& its type) & doesn't allocate storage.  
**declare:** Compiler allocates storage for the variable.  

## PRAGMAS - [SO](https://stackoverflow.com/questions/232785/use-of-pragma-in-c), [USOFT](https://docs.microsoft.com/en-us/cpp/preprocessor/pragma-directives-and-the-pragma-keyword?redirectedfrom=MSDN&view=msvc-160) , [TASKING](http://www.tasking.com/support/tricore/tc_reference_guide_v2.2.pdf)
Compiler directive informing compiler to do something. Specifies machine or OS-specific compiler features.  
Keywords in C source that control behavior of the compiler.  
Overrule compiler options & keywords.  
**syntax:** `#pragma <token_string>`. *token_string* represents a specific compiler instruction & arguements







