# Object, LRVALUES, & OPERATORS - [link](https://www.geeksforgeeks.org/lvalue-and-rvalue-in-c-language/)
[Questions Answers Errors Solutions (QAES)](Qaes/qaes_obj_lrvalues_ops.md)  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [OBJECT & LRVALUES](#objects_and_lrvalues)
2. [OPERATORS](#operators)  
&emsp;a. [ASSIGNMENT OPERATOR](#assignment_operator)  
&emsp;b. [ADDRESS OF OPERATOR](#address_of_operator)  
&emsp;c. [INDIRECTION OPERATOR](#indirection_operator)  
&emsp;d. [SUMMARY](#summary)  
</details>

## OBJECT & LRVALUES <a name="objects_and_lrvalues"></a>
* **object:** Named region of storage. Thus, only during program execution.
* **expression:** Sequence of operators and operands that specifies *computation of a value*, or that designates an object or a function, or that generates side effects, or that performs a combination thereof.  
* **lvalue:** Expression referring to an object.  
Represents an object which has a memory location.  
May appear on the LHS or RHS of the **assignement operator**.  
Not an address, simply the object.  
* **modifiable lvalue:** Not an array type, const type, or the members of structs, unions don't have const type.
*  ***lvalue conversion (from C Standard):*** "Except when it is the operand of the sizeof operator, the unary & operator, the ++ operator, the -- operator, or the left operand of the . operator or an assignment operator, an lvalue that does not have array type is converted to the value stored in the designated object (and is no longer an lvalue); this is called lvalue conversion".  
* **rvalue:** Expression whose value is stored at some address in memory.  
Can't have a value assigned to it since it is a value to begine with.    
Can only appear on the RHS of the **assignment operator**.  

***NOTE:*** Name (identifier) of a variable represents a storage location.  
***NOTE:*** Value of a variable represents the value stored at that location. 

```
int var = 1;
/*
 * var (lvalue)
 * +-------------+
 * |       +---+ |
 * |  &var | 1 | |
 * |       +---+ |
 * +-------------+
 */
```

## OPERATORS <a name="operators"></a>

### ASSIGNMENT OPERATOR `=` <a name="assignment_operator"></a>
* ***define:*** Stores a value in the object designated by the left operand.
* ***constraint:*** Left operand must be a modifiable lvalue.
* ***output/result:*** Not an lvalue. 
* ***Ex:*** `var = 1;` -> Result equaled to 1.  

### ADDRESS OF OPERATOR `&` <a name="address_of_operator"></a>
* ***define***: Obtains the address of an operand.  
  * Operand is a function designator, result of a `[]` or `*` operator, or lvalues that designates an object.  
  * If its just a random address, `error: lvalue required as unary ‘&’ operand`.  
* ***output/result of expression [(SO)](https://stackoverflow.com/questions/57653553/how-does-c-compiler-determine-a-valid-lvalue):*** Not an lvalue.  Result is a value of an expression of pointer type, an address. Even though an address points to an objects, it is *NOT* the object.  
  * ***NOTE:*** Thus, operand is an lvalue, but result of expression is just an address & not an lvalue.
```c
house my_house;
&my_house;  /* Address of the house, but not the house, i.e. not an lvalue.        */
*&my_house; /* House located at address of my_house is a house, i.e. is an lvalue. */
```

### INDIRECTION OPERATOR `*` <a name="indirection_operator"></a>
* ***define:*** Obtains the object that is being pointed to, if the operand is an object.  
Used to obtain object value w/o using the object name
* ***syntax (from example code below):*** `*address` == `*ptr` == `*&var` == `var` == `1`
* ***constraint operand:*** Requires an lvalue of pointer type. 
* ***output/result of expression:*** lvalue, not an adress.
```c
int var;
int *ptr = &var;  
*ptr = 1;    /* Statement equivalent to var = 1; Thus, *ptr is an alias to var. */
             /* *ptr == *&var == var == 1. */

/*    
    a      v         o
   ---    ---       ---
 +---------------+ 
 |     +-------+ |         
 |1000 |   1   | |  var
 |     +-------+ |
 +---------------+ 
 +---------------+ 
 |     +-------+ |         
 |2000 | 1000  | |  ptr
 |     +-------+ |
 +---------------+ 
*/
```

### SUMMARY <a name="summary"></a>
| operator | operand      | result    | comment                                                                       |
| -------- | -------------| --------- | ----------------------------------------------------------------------------- |
| &        | lvalue       | no lvalue | B/c an lvalue is the object & an address does not.                            |
| *        | pointer type | lvalue    | Useful for indirect referencing. Expression before, aliases the objects name. | 
| =        |              | no lvalue |                                                                               |
