# POINTERS VS ARRAYS

**ARRAYS:**  
* Once program is compiled the starting location, address cannot be changed. `int a[5] = {0}, b[5] = {0}; a = b; // error`.  
Elements, values can be modified, but the array itself cannot be set to another array. 

**POINTERS:**  
* The value of a pointer can be changed, able to point to other variables, strings during program execution.    
* STRINGS: Pointer points to a string literal which once declared cannot be changed.  

[[G4G]:](https://www.geeksforgeeks.org/difference-pointer-array-c/) Arrays & Pointers differ from their memory size.  
```c
int a[5] = {0};
int *ptr = a;

printf("%ld\n", sizeof(a)); 
printf("%ld\n", sizeof(ptr));

/* OUTPUT
   20
   4
```
## PROPERTIES THAT MAKE ARRAYS LOOK SIMILAR TO POINTERS
1. Array names gives address of first array element.
`a == &a[0]` 
2. Array elements are accessed through pointer arithmetic. 
Compiler inteprets `a[2]` as `*(a + 2)` ([CS:`*`](../C_Standard/Unary_Operators.md)).   
```c
int a[5] = {10, 20, 30 ,40 , 50};
printf("%d\n", a[2]);
printf("%d\n", *(a + 1))
/* OUTPUT
   30
   30

   DIAGRAM             
   a   
   +-----------------------------------------------------------------------------------+
   |   a[0], *(a+0)    a[1], *(a+1)    a[2], *(a+2)   a[3], *(a+3)     a[4], *(a+4)    | 
   | +---------------+---------------+---------------+---------------+---------------+ | 
   | |+-------------+|+-------------+|+-------------+|--------------+|+-------------+| |
   | || 10          ||| 20          ||| 30          || 40           ||| 50          || | 
   | |+-------------+|+-------------+|+-------------+|--------------+|+-------------+| | 
   | |0x11242ef345b0 |0x11242ef345b4 |0x11242ef345b8 |0x11242ef345c0 |0x11242ef345c4 | |0x11242ef345c8
   | +---------------+---------------+---------------+---------------+---------------+ |
   +-----------------------------------------------------------------------------------+ 
   ▲ ▲               ▲                                                                  ▲
   | |               |                                                                  |
   | a              a+1                                                                 |
   &a=0x11242ef345b0                                                                 &a+1=0x11242ef345c8
 */
```
3. Array parameters are always passed as pointers even when declared as [].  
As a result, the pointer can be equaled to a new address!  
Also, assigning addresses to array variables are not allowed!  
```c
// ILLEGAL
void main(void){
    int a[5] = {10, 20, 30, 40, 50};
    int var = 0;
    a = &var; // error: assignment to expression with array type
}

// LEGAL
void foo(int a[]);

void main(void){
    int a[5] = {10, 20, 30, 40, 50};
    foo(a);
}

void foo(int a[]){
    int var = 0;
    a = &var;
}
```


```c
// WIERD
void foo(int a[], int var_foo);

void main(void){
    int a[5] = {10, 20, 30, 40, 50};
    printf("%p\n", a); 
    int var_main = 2;
    printf("%p\n", &var_main);
    foo(a, var_main);
    
    printf("%d\n", a[0]); 
    printf("%p\n", a);    
}

void foo(int a[], int var_main){
    printf("%p\n", a);
    a = &var_main;
    printf("%d\n", a[0]); 
    printf("%p\n", a);
}

/* OUTPUT
   &a[0]
   &var_main
   &a[0] in foo
   a[0] in foo
   &a[0] in foo
   a[0] in main
   &a[0] in main
 */  
 ```
 ```c
 // WIERD 
void foo(int b[]);

void main(void){
    int a[5] = {10, 20, 30, 40, 50};
    foo(a);
    printf("a[0] = %d\n", a[0]); // OUTPUT: a[0] = 10 
}

void foo(int b[]){
    int var = 0;
    b = &var;
    b[0] = 11;
}
//////////////////////////////////////////////
void foo(int b[]);

void main(void){
    int a[5] = {10, 20, 30, 40, 50};
    foo(a);
    printf("a[0] = %d\n", a[0]); // OUTPUT: a[0] = 11 
}

void foo(int b[])
    b[0] = 11;
 ```
