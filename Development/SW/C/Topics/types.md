# TYPES

## IMPORTANT
* A declared variable cannot change its type (storage space) after.
Thus `short` cannot hold a `long`, but a `long` can hold `int`.  
