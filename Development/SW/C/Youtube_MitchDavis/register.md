# REGISTERS

## SET REGISTERS BY MEMORY ADDRESS
```c
volatile byte *directionRegisterB = 0x25;
*direcitonRegisterB = 32; 

/* OR */

*( (volatile byte*) 0x25) = 32;

/* OR */

#define PORTB *( (volatile byte*) 0x25)
PORTB = 32;
```  
***NOTE:***  
* *volatile* needed b/c the compiler tries to read our code, tries to figure out what does nothing, and if it thinks it does nothing it completely ignores it.  
* Statement `*directionRegisterB = 32;` essentialy is setting a variable. Compiler sees this but looks through code and finds were never using this variable anywhere. Compiler then says this is a useless variable & doesn't set it b/c we don't even use it later on.  
* *volatile* informs the compiler to listen to what were saying, overriding compiling optimization.  

