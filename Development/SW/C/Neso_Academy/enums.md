# ENUMS - [link](https://www.youtube.com/watch?v=9QdJExC2AVg) 

**enumeration type:** User defined type used to asign names to *integral constants* b/c names are easer to handle in program.  
Declared like a structure or union. 
```c
enum Bool {false, true};    /* false & true are names to integral constatns */

void main(void){
    enum Bool var = true; 
    printf("%d\n", var);    /* OUTPUT: 1 */
}
```

### Q: WHY USE ENUM WHEN `#define` CAN ASSIGN NAMES TO INTEGRAL CONSTANTS?
1. Enums can be declared in local scope. 
2. Enum names are automatically initialized by the compiler. If we do not assign values to enum names, then compiler automatically will assign values to them starting from 0. 

### FACTS
* Two or more names can have the same value. Additonally, the names can be used directly w/o even declaring a variable or any object of type *enum point*.  
***enum main job:*** Assign names to integral constants.
```c
void main(void){
enum point { x = 0, y = 0, z = 0};
printf("%d %d %d\n", x, y, z);    /* OUTPUT: 0 0 0 */
}
```
* We can assign values in any order. All unassigned names will get value as value of previous name + 1.
```c
void main(void){
enum point { y = 2, x = 34, t, z = 0};
printf("%d %d %d %d\n", x, y, z, t);    /* OUTPUT: 34 2 0 35*/
}
```
* Only integral values are allowed. 
* All enum constant must be unique in their scope.
```c
enum point1 {x = 34, y = 2, z = 0};
enum point2 {x = 4, p = 25, q = 1};    /* ERROR: Redeclaration of enumerator 'x' */
```
