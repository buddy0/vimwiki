# CONTEXT SWITCHING

* When an interrupt occurs, the system needs to save the current **context** of the process currently running on the CPU so that it can restore that context when its processing is done, essentially suspending the process & then resuming it.
* The context is represented in the Process Control Block(PCB) of the process.  
**context switch:** State save of a current process & state restore of a different process.


