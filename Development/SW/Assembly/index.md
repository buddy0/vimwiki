# ASSEMBLY

## GCC & GDB
**manuels:** [GCC](https://gcc.gnu.org/onlinedocs/) & [GDB](https://www.gnu.org/software/gdb/documentation/)  
**how I view assembly:** In terminal on project path enter: `$ gdb -tui <executable>`. `(gdb) layout split`. 

## LINKS
* [Understanding C by learning assembly](https://www.recurse.com/blog/7-understanding-c-by-learning-assembly) 
* [x86 Assembly Language Reference Manual](https://docs.oracle.com/cd/E19253-01/817-5477/817-5477.pdf)
* [AT&T assembly syntax and IA-32 instructions](https://gist.github.com/mishurov/6bcf04df329973c15044)
* [AT&T Assembly Syntax](https://csiflabs.cs.ucdavis.edu/~ssdavis/50/att-syntax.htm)
* [x86 Assembly Guide](http://flint.cs.yale.edu/cs421/papers/x86-asm/asm.html)
* [x86 Assembly Guide](http://www.cs.virginia.edu/~evans/cs216/guides/x86.html)
* [A Readers Guide to x86 Assembly](https://cseweb.ucsd.edu/classes/sp11/cse141/pdf/02/S01_x86_64.key.pdf)
* [x86 Assembly Tutorial](https://www.cs.princeton.edu/courses/archive/fall17/cos318/precepts/0-x86-tutorial.pdf)  

## X86 ASSEMBLY W/ AT&T SYNTAX
**syntax:** `mnemonic source, destination`  
***mnemonic:*** Human readable name for the instruction. 
***source, destination:*** Operands that can be immediate values, registers, memory addresses, or labels.  
***immediate values:*** Constants, prefix w/ `$`.  
***register names:*** Prefix w/ `%`.  

### REGISTERS
**register:** Physical data location on the CPU. The size & width of a register defines the CPU architecture. Very fast to acccess.  
***general purpose register:*** Used for any operation & value has no particular meaning to CPU. **Ex.**  
`%eax`: Extended AX register.  
`%ecx`: Extended CX register.  
***special purpose register:*** CPU relies on these for their own operation & value do have meaning. **Ex.**  
`%rbp`: Base pointer, points to *base* of current stack frame. Always has higher value tha *%rsp* b/c stack starts at high memory address & goes downwards.  
`%rsp`: Stack pointer, points to *top* of current stack frame.  
***instruction pointer/ program counter register:*** `%rip` which stores memory address of next intruction.  

### MEMORY
Parentheses in operands dictates a memory address.  
**Ex.** `-0x4(%rbp)`, equivalent to `%rbp + -0x4`. Subtracting b/c stack starts high and goes downward. 
***base address:*** `%rbp`  
***displacement:*** `-0x4`
***suffix mnemonic:*** Signifies operands are  
`l`: long, 32 bits for integer  
`b`: byte, 8 bits  
`s`: short, 16-bits  
`w`: word, 64 bits  
***NOTE:*** No suffix signifies size of operands are determined by *source* or *destination* registers.  

## C -> ASSEMBLY PROGRAMS 
| C                | Assembly                 |   | C                | Assembly                 |
| ---------------- | ------------------------ | - | ---------------- | ------------------------ |
| `int var1;`      |                          |   | `int var1 = 11;` | `movl $0xb, -0x8(%rbp)`  |
| `int var2 = 22;` | `movl $0x16, -0x8(%rbp)` |   | `int var2 = 22;` | `movl $0x16, -0x4(%rbp)` |
| `var1 = var2;`   | `mov -0x8(%rbp), %eax`   |   | `var1 = var2;`   | `mov -0x4(%rbp), %eax`   |
|                  | `mov %eax, -0x4(%rbp)`   |   |                  | `mov %eax, -0x8(%rbp)`   |

| C                | Assembly                 |
| ---------------- | ------------------------ |
| `int var1;`      |                          |
| `int *ptr;`      |                          |
| `*ptr = &var1;`  | `lea -0x14(%rbp), %rax`  |
|                  | `mov %rax, -0x10(%rbp)`  |
| `*ptr = 11`      | `mov -0x10(%rbp), %rax`  |
|                  | `movl $0xb, (%rax)`      |

| C                  | Assembly                 |
| ------------------ | ------------------------ |
| `int var1 = 11;`   | `$0xb, -0x14(%rbp)`      |
| `int *ptr = &var;` | `lea -0x14(%rbp), %rax`  |
|                    | `mov %rax, -0x10(%rbp)`  |
| `*ptr = 12;`       | `mov -0x10(%rbp), %rax`  |
|                    | `movl $0xc,(%rax)`       | 

***TODO:*** Try this again to see memory addresses!!!
Add array & struct examples
