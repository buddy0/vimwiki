# DEVELOPMENT

* **SW:** [C](SW/C/index.md), [Assembly](SW/Assembly/index.md), [XML](SW/XML/xml.md), [SDL](SW/SDL/index.md), [Batch](SW/Batch/index.md), [Perl](SW/Perl/index.md), [Elisp](SW/Elisp/index.md)
* **HW:** [Infineon - XMC4700](HW/Infineon/Boards/Xmc4700.md), [ADC](HW/Adc.md), [DMA](HW/Dma.md), [Duty Cycle](HW/Duty_Cycle.md), [Loop Systems](HW/Loop_Systems.md), [Memory Types](HW/Memory_Types.md), [Input Output](HW/Input_Output.md), [Equations](HW/Equations.md)
* **TOOLS:** [Git](Tools/Git.md)
* **Other:** [General Notes](Other/development.md), [Physics](Other/Physics/index.md), [TODO](Other/Todo.md), [Development](Other/development.md)
