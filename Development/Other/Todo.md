# TODO

## C PROGRAMMING A MODERN APPROACH
* SELECTION STATEMENTS
   * Logical operator table fix
* EXPRESSIONS
   * Arithemetic operators table fix. Add associativity, mabe precedance. 
* BASIC TYPES
   * Integer types table fix & wording above!
* PROGRAM ORGANIZATIONS
   * Storage duration vs storage class. 
* POINTERS
   * create better pointer example image
   ```c
   int var;    /* Declare & define variable */
   int *ptr;   /* Declare & define pointer variable */
   ptr = &var; /* Simple assignment: 1. evaluate RHS (obtain address of lvalue var) & copy to objects value in LHS 2. Result is the LHS object's value (result not lvalue). */ 
   *ptr = 1;   /* Indirection: Obtain object that being pointed to. Set this object's value equal to 1. */ 
   ```

## ERRORS
* Add array error: expected expression befire '{' token. Ex: `int array[5];` `array = {0};` 
    * SOLUTION: Assignment operator needs modifiable lvalue. 

