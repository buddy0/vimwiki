# WAVE PERIOD & FREQUENCY - [YT](https://www.youtube.com/watch?v=v3CvAW8BDHI)  

## TERMS & EQUATIONS
* **wave period:** Time between oscillations in a wave.  
* **frequency:** How many oscillations/waves per unit time.  
* `T = 1/F` & `F = 1/T`
* ![Picture of Frequency Graphs](Images/Understanding_Frequency.png)


