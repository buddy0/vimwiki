# DEVELOPMENT

## GCC + GDB
$ sudo pacman -S gdb  
$ gcc -ggdb main.c -o a.out  
$ ./a.out  

Important Commands:  
Print out # of bytes in memory at the address of a variable. $ x/#xb &var  
CTRL P or N recieves hsitory  
local variables: `info locals`  
args: `info args`   
print expression: `print [expression]`  
print stack frames: `where`  
see source code: `list [startline, endline]`  
breakpoints: `br [file:line#]`  
conditional breakpoint: `br [file:line#] if [expression]`  
continue debugger until the next breakpoint: `continue`  
watchpoints pause when the value of an expression changes, prints old and new values: `watch [expression]`  
print value of expression every time program stops: `display [expression]`  `i display` `[enable] [delete] [disable] display`  

## MAKEFILE
```bash
# target: dependencies
#   action

output: main.c
    gcc hello.o -o output
    
main.o: main.c
    gcc -ggdb main.c
        
clean:
    rm -f *.o output
```

## VIMSPECTOR
1. use vim plugin  
2. 2. $ cd ~/.vim/plugged/vimspector/  
3. 3. Install the switch (for installgadget.p)  
   This acts like a bridge from vimspector graphical debugger to the native debugger (gdb)
`$ ./install_gadget.py --enable-c or --all`   
4. 4. Adapter used for :VimspectorInstall  
`$ vscode-cpptools`  

## JSON
**J**ava **S**cript **O**bject **N**otation  
Data representation format, similar to xml and yml.  
Commonly used for APIs and Configs.  
Lightweight and easy to read/write.  
Integrates easily with most languages.  

### SYNTAX
| Types    | Example                    |
| -----    | ------                     |
| strings  | "Hello Word" "M"           |
| numbers  | 1 1.5 -20                  |
| booleans | true false                 |
| null     | null                       |
| arrays   | [1,2,3] ["Hello", "World"] |
| objects  | { "key": "value"  }        |

Type *object* example  
```bash
{ "age": 22,
  "name": "Matthew",
  "isBoy": true,
  "hobbies": ["snowboarding", "videogames", "chess"]
}
```

## SDL
**Install**  
1. goto https://libsdl.org/download-2.0.php and install the source code
2. `$ cd ~/downloads`
3. `$ unzip SDL2-2.0.14`
4. `$ mv SDL2-2.0.14 ~/programs`  
5. `$ cd ~programs/SDL2-2.0.14`  
6. `$ ./configure`  
7. `$ make`  
8. `$ sudo make clean install` 

OR    
:) `$ sudo pacman -S sdl2`  

* When creating cprograms the preproprocessor directive needs to be `# include <SDL2/SDL.h>`  
* GCC include path: `$ echo | gcc -E -Wp,-v -`  
 `stdio.h` is in */usr/include* directory  
 `SDL.h` is in */usr/include/SDL2* directory  

**Extension SDL2_image**  
1. goto https://www.libsdl.org/projects/SDL_image/ and install the source code  
2. steps 2-8 from previous  
 
OR  
`$ sudo pacman -Ss sdl2`  
:) `$ sudo pacman -S sdl2_image`  

**MakeFile**  
```c
output: main.o
    gcc -lSDL2main -lSDL2_image main.o -o output

main.o: main.c
    gcc -c -ggdb main.c

clean: 
    rm -f *.o output
```

## GIT - CREATING REPO FROM CLI

