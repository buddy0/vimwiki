# INPUT OUTPUT

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INPUT](#input)  
    a. [PULL UP/DOWN RESISTORS](#pull_up_down_resistors)  
    b. [DIGITAL BUFFER](#digital_buffer)  
    c. [TRISTATE BUFFER](#tristate_buffer)  
2. [OUTPUT](#output)  
    a. [PUSH/PULL](#push_pull)  
    b. [OPEN DRAIN](#open_drain)  
</details>

## INPUT 

###  PULL UP/DOWN RESISTORS - [electronic turtorials](https://www.electronics-tutorials.ws/logic/pull-up-resistor.html), [ee power](https://eepower.com/resistor-guide/resistor-applications/pull-up-resistor-pull-down-resistor/#) <a name="pull_up_down_resistors"></a>

**floating inputs:** Unconnected inputs.  
***pull-up or pull-down resistors:*** Gives input pin a default state.  
**pull-up resistor:** Resistor pulling a wire to a high logic level in absence of input signal.  
**pull-down resistor:** Resistor pulling the pin to a logical low value in absence of input signal.  
 
```
   PULL UP                PULL DOWN

     o   VCC                o   VCC
     |                      |
     \                      |
     /   R                  o
     \       +-----+      \
     |-------| mcu |       \o        +-----+
     |       +-----+        | -------| mcu |
     o                      |        +-----+
   \                        \ 
    \o                      /   R
     |                      \
     |                      |  
    ---- GND               ---- GND
     --                     -- 
``` 

### DIGITAL BUFFER - [electronic turtorials](https://www.electronics-tutorials.ws/logic/logic_9.html) <a name="digital_buffer"></a>
**digital buffer:** Performs no decision making capabilities. Output matches input. 

```
     |\
A----| \----B
     | /
     |/

| input A | output B |
| ------- | -------- |
| 0       | 0        |
| 1       | 1        |
```

### TRISTATE BUFFER - [wiki](https://en.wikipedia.org/wiki/Three-state_logic), [electronic turtorials](https://www.electronics-tutorials.ws/logic/logic_9.html) <a name="tristate_buffer"></a>
**tristate:** Input controlled switch w/ an output that can be electrically turned ON or OFF by external enable signal input.  
***states:*** Logic LOW, logic HIGH, or high impedance state.  
**high impedance:** Circuit node allowing small amount of current per unit of applied voltage at that point.  
Low current & potentially high voltage.  
In digital circuit, (aka hi-z, tri-stated, or floating) output not being driven to any defined logic level.  
**low impedance:** Low voltage & potentially high current
***enable cleared:*** Switch is open producing an open circuit resulting in high-z output state. Buffer ouput is electrically disconnected & no current is drawn from the power supply.
***why use tristate buffers:*** Isolates devices & circuts from the data bus and one another.

```
       E
       |                    E
     |\|                    /
A----| \----C        A-----o  o------C
     | /      
     |/

| input A | input E | output C           |
| ------- | ------- | ------------------ |
| 0       | 0       | z (high-impedance) |
| 1       | 0       | z (high-impedance) |
| 0       | 1       | 0                  |
| 1       | 1       | 1                  |
```

## OUTPUT <a name="output"></a>
[YT](https://www.youtube.com/watch?v=IjKDKGqCm_4), [O4T](https://open4tech.com/open-drain-output-vs-push-pull-output/)  

### PUSH-PULL <a name="push_pull"></a>
**Push-Pull:** Capable of driving two output levels.  
Push to power supply voltage (pushing current to the load) or Pull to ground (pulling curring current from the load).  
Doesn't allow connecting multiple devices together in bus configuration.  
***commonly used w/ communication interfaces:*** With unidirectional lines such as SPI, UART, etc.  
**Push:** When Internal Signal set to *0*, PMOS transistor is activated & current flows through in from VDD to the Output pin. NMOS transistor is inactive.  
**Pull:** When Internal Signal set top *1*, NMOS transistor is activated & current starts to flow through it from output pin to GND. PMSOS transistor is inactive.  

<details>
<summary><b>PICTURES OF PUSH-PULL</b></summary>

![Picture of Push Phase](Images/Push_Phase.png) ![Picture of Pull Phase](Images/Pull_Phase.png) ![Picture of Push-Pull](Images/Push_Pull.png)  
</details>

<details>
<summary><b>PICTURES OF PUSH-PULL LED EXAMPLE</b></summary>

![Picture of Push-Pull LED On](Images/Push_Pull_Led_On.png)  
![Picture of Push-Pull LED Off](Images/Push_Pull_Led_Off.png)   
</details>

## OPEN DRAIN <a name="open_drain"></a>
**Open Drain:** The output logic is either ground (logic 0) or high impedance (floating state) when there is only one transistor involved.  
If drain is left open, device is off, the pin is left floating to Hi-Z.   
Driving to output logic high requires additional ciruitry (either external pull-up resistor or uC contains internal pull-up resistor).  
***commonly used in communication interfaces:*** Where multiple devices are connected on the same line (e.g. I2C).  

<details>
<summary><b>PICTURES OF OPEN DRAIN</b></summary>

![Picture of Open-Drain](Images/Open_Drain2.png) ![Picture of Open-Drain](Images/Open_Drain.png)  
</details>

<details>
<summary><b>PICTURES OF OPEN DRAIN EXAMPLE</b></summary>

![Picture of Open Drain Transistor On](Images/Open_Drain_Transistor_On.png)  
![Picture of Open Drain Transistor Off](Images/Open_Drain_Transistor_Off.png)
</details>

<details>
<summary><b>PICTURES OF OPEN DRAIN W/ INTERAL PULL-UP RESISTOR LED EXAMPLE</b></summary>

![Picture of Open Drain W/ Internal Pull-Up Resistor Transistor Off, LED On](Images/Open_Drain_Internal_Pull_Up_Resistor_Transistor_Off.png)  
![Picture of Open Drain W/ Internal Pull-Up Resistor Transistor On, LED Off](Images/Open_Drain_Internal_Pull_Up_Resistor_Transistor_Off.png)
</details>





