# LOOP SYSTEMS

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [OPEN LOOP SYSTEM](#open_loop_system)
2. [CLOSED LOOP SYSTEM](#closed_loop_system)
</details>

## [OPEN LOOP SYSTEM](https://www.electronics-tutorials.ws/systems/open-loop-system.html)
* Referred to as non-feedback system. Output has no effect on the control action of the input signal. 
* Manual electrical control system. 
* Does not monitor or measure the output signal.
* Main disadvantage:
    * Cannot self-correct any errors when the preset value drifts
    * Poorly equiped to handle disturbances or changes in the signal. Can be solved by feed forward control or predictive control.

## [CLOSED LOOP SYSTEM](https://www.electronics-tutorials.ws/systems/closed-loop-system.html)
* Fully automatic control system in which its control action being dependent on the output in some way.  
* Desired output condition is compared w/ the actual condition by generating an error signal which is the difference between output and reference input.  
* Error signal is difference between input & feedback signals.  
* Always implies use of a feedback control action in order to reduce any errors w/in the system. 
* Main advantages:
    * reduce errors by automatically adjusting system's input
    * improve stability of an unstable system
    * increase or reduce systems sensitivity
    * enhance robustness against external disturbances to the process
    * produce reliable & repeatable performance.
* Main disadvantages:
    * More complex due to a feedback path(s).
* ***summing point/comparison element:*** Regulates control signal by determining the error between actual output & desired output.  
Is is between feedback loop & systems input.  
    * ***summing point types:*** Can be positive or negative. 
* ***transfer function:*** Mathematical relationship between systems input & output.  
The ratio of the output to its input represents its gain.  
