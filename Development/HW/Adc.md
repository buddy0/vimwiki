# ADC - [link](https://www.youtube.com/watch?v=HicZcgdGxZY)

# WHY USE ADC
Analog signals are susceptible to noise and difficult to process and store in analog domain.  
Digital signals are less susceptible to noise and easy to process & store in digital domain.  
These signals are not lossless; some of the analog signal will be lost.  
Analog signal is continuous in time & amplitude, which can have an infinite resolution, while a digital signal is discrete in time, amplitude, & resolution. 

## CONVERION STEPS FOR ADC
1. **sampling:** Analog signal is sampled at a particular rate. 
2. **quantized:** Amplitude restriced into a discrete set of values. 
3. **binary encoded:** Convert into binary format.  

**Ex.**  
![Picture of ADC Step 1](Images/adc_sample_1.png) ![Picture of ADC Step 2](Images/adc_sample_2.png) ![Picture of ADC Step 3](Images/adc_quantized.png)  
The sampled signal is quantized into 16 different levels & is assigned a nearest value from these 16 levels.   

## QUANTIZATION, RESOLUTION, STEP SIZE, SAMPLING RATE, & ANTI ALIASING FILTER
**quantization:** Process of assigning a sampled signal a value from a discrete set of values.   
**resolution:** Descides how the assigned or quantized value is close to the actual value. Defined in # of bits which the quantized signal is going to get encoded.  
If resolution is *n bits*, then the total # of discrete levels is equaled to *2<sup>n</sup>*. Thus, input signal will be quantized into *2<sup>n</sup>* levels.  
![Equation of Resolution](Images/equation_resolution.png)  
**step size:** Defines min. change in the input signal which can be detected by the ADC.  
![Equation of Step Size](Images/equation_stepsize.png) by *Mathcha*.  
**quantization error:** 1 lsb.  
**sampling rate:** Should follow *Nyquist Sampling Theorem* (fs=2*fmax). So, after sampling the signal can be reconstructed.  
**anti aliaing filter:** Low pass filter. Before sampling a signal it is passed here. 
**sample & hold cicrcuit:** Samples the signal and holds the output to the same value until next sample is taken. Signal remains constant b/c ADC will take time for quantization & encoding. 

## ADC ALL STEPS
![Picture of ADC Steps](Images/adc_steps.png) 


