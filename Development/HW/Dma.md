# DMA - [link1](https://www.youtube.com/watch?v=KwAOINqNwyU), [link2](https://www.techopedia.com/definition/2767/direct-memory-access-dma) 

## MOTIVATION
* In general, most computing applications require some transfer of data (whether in bits or bytes). Who is the entity responsible for performing this data transfer? 
    * Up until now, the CPU/processor handles both data transfer & execution of a program.  

* If a CPU is handling the data transfer, the relevant application may not progress until the data transfer is complete. 

    * Now, there may be interrupts or events configured causing the processor to have something else occur. However, most of the time applications have some amount of data transfer resulting in a wait in the program.
   
* Thus, depending on the amount & frequency of data transferred, data transmission may easily casue a bottleneck on *program execution*.

* ***solution:*** change the entity.

## INTRODUCING DMA
**Direct Memory Access (DMA):** System designed for independently transferrring data between two sets of memories.  
***goals:***  
* Remove need for CPU to perform data transfer.
* Minimize data transfer time. 
* Maximize amount of data being transfered. 
* Provide generic, configurable interface for performing data transfer. 

## EVALUATING DMA
**negatives:** 
* Memory components does not have same fast processing speed of a CPU, resulting in a bottleneck on data transfer, thus affecting program execution as well. 
* Increase complexity for both HW & SW designs.  
***effectively utilze DMA system:*** DMA synchronized w/ other relevant components. 
   * Thus, DMA is *not* entirely independent, however, synchronization may not require a CPU, a dedicated interrupt signal would suffice. 

## INTERFACING W/ DMA
**controller:** Provide configuration capabilities to controll relevant device. 

