# ELECTRICAL EQUATIONS
* [LaTeX Equation Generator](https://latex.codecogs.com/eqneditor/editor.php)  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>
 
</details>


* **velocity:** Speed of something in a given direction.  
Physical vector quantity: magnitude & direction.  
Scalar absolute value (magnitude) aka speed measured in meters per secod (m/s).  
$`\bar{v}=\frac{\Delta x}{\Delta t}\;m/s`$.  

* **period:** Length in time that a waveform starts to repeat itself from start to finish.  
$`T=\frac{1}{F}\;s`$  
**ex:** $`T=\frac{1}{F}\rightarrow \frac{1}{300MHz}\rightarrow \frac{1}{(3\cdot 10^{2}\cdot 10^{6})Hz}\rightarrow \frac{1}{(3*10^{8})Hz}\rightarrow \frac{10^{-8}s}{3}\rightarrow \frac{10^{1}\cdot 10^{-9}s}{3}\rightarrow\frac{10}{3}ns`$

* ***pulse width:*** Pulse active time. 

* **frequency:** Rate at which something occurs, how often something repeats.  
Number of waves pers second. Measured in Hertz (Hz).  
$`f=\frac{1}{T}\;Hz`$

* ***duty cycle (based of frequency):*** Measures fraction of time a given transmitter is transmitting the signal.  
*PW* is pulse width & *T* is period.  
    * ***ratio:*** $`D = \frac{PW}{T}`$
    * ***percentage:*** $`D = \frac{PW}{T}*100`$  

* **electrical resistance:** Measure of opposition of current flow. Measured in Ohms.  
$`R = \frac{V}{I}\;Ohms`$  

* **electrical conductance:** Reciprocal, inverse of resistance. Measures ease of passing current flow. Measured in Siemens.  
$`G = \frac{I}{V}\;Siemens`$  
Ex: Rubber has high resistance & low conductivity and metals have low resistance & high conductance.   

## MATH
***slope intercept:***  
$`y=mx+b`$  
***point slope:***  
$`y-y_{1}=m(x-x_{1})`$  
***slope:***  
$`m = \frac{rise}{run}=\frac{y_{2}-y_{1}}{x_{2}-x_{1}}`$  


