# INFINEON

## BOARDS
* [XMC4700](Boards/Xmc4700.md)

## DAVE INSTALL
1. **download DAVE IDE:**  
    a. Goto [link](https://www.infineon.com/cms/en/product/microcontroller/32-bit-industrial-microcontroller-based-on-arm-cortex-m/#!tools) -> *Tools & Software* -> *DAVE V#*, select *Download*.
2. **download segger:**  
    a. Goto [link](https://www.segger.com/downloads/jlink/) -> *section J-Link Software & Documentation Pack*, select *Windows 64-bit installer*.  
    b. Open DAVE IDE -> *Window* -> *Preferences* -> *Run/Debug* -> *String Substitution*. Change *jlink_path* value to `C:/Program Files/SEGGER/JLink_V770a`.
