# ARCHITECTURE

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [OVERVIEW](#overview)
2. [MEMRIES](#memories)
3. [BUS INTERFACES](#bus_interfaces)
</details>

<details>
<summary><b>ACRONYMS</b></summary>

| acronym | meaning                            |
| ------- | ---------------------------------- |
| BROM    | Boot ROM                           |
| EBU     | External Bus Interface             |
| DSRAM1  | System RAM                         |
| DSRAM2  | Communication RAM                  |
| PSRAM   | Code RAM                           |
| USIC    | Universal Serial Interface Channel |

</details>

## OVERVIEW <a name="overview"></a>
* 32-bit ARM Cortex-M4 procesor core
* Little Endian
* | mem size | on-chip mem       |  
  | -------- | ----------------- |  
  | 16 KB    | boot ROM          |  
  | 96 KB    | program           |  
  | 128 KB   | data              |  
  | 128 KB   | communication     |  
  | 2 MB     | flash             |  
  | 8 KB     | instruction cache |  
* ***3 USIC units:*** providing UART, double-SPI, quad-SPI, IIC, IIS, & LIN interfaces.
* ***External memory interfaces w/ EBU:*** multi-media card and SD interface.

## MEMORIES <a name="memories"></a>
***BROM:*** Contains boot code & exception vector table. Basic system initialization sequence code, aka firmware, executed after reset release.  
***Flash:*** For non volatile code or constant data storage.  
***PSRAM:*** For user code or OS data storage. Accessed via Bus Matrix & provides zero-wait-state access for CPU code execution or data access.  
***DSRAM1:*** For general user data storage. Accessed via Bus Matrix & provides zero-wait-state access for data.  
***DSRAM2:*** For use w/ communication interface units such as USB & ETH modules.  

## BUS INTERFACES <a name="bus_interfaces"></a>
* All on-chip peripherals & memories are attached to Bus Matrix (pg. 54).  
***memory interface:*** On-chip memories capable to accept transfer request w/ each bus clock cycle.  
Data bus width is 32-bit.  
***peripheral interface:*** Each slave supports 32-bit accesses.
