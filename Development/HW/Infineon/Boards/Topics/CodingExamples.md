# CODING EXAMPLES

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [BLINKY](#blinky)  
    a. [APPS](#apps)  
    b. [XMC LIBS](#xmc_libs)  
    c. [REGISTERS](#registers)
</details>

# BLINKY <a name="blinky"></a>

## APPS <a name="apps"></a>
***app created(s):*** DIGITAL_IO.  
1. ***Configure app instance:*** Pin Direction to Input/Output.
2. ***Rename instance label:*** To LED1.
3. ***Manual Pin Allocator:*** Pin Number (Port) to #57 (P5.9) / LED1.
4. ***Generate Code:*** Generated file path is `Dave/Generated/DIGITAL_IO`.  

<details>
<summary><b>code</b></summary>

```c
/* file: main.c */
#include "DAVE.h"                 //Declarations from DAVE Code Generation (includes SFR declaration)

/**

 * @brief main() - Application entry point
 *
 * <b>Details of function</b><br>
 * This routine is the application entry point. It is invoked by the device startup code. It is responsible for
 * invoking the APP initialization dispatcher routine - DAVE_Init() and hosting the place-holder for user application
 * code.
 */

int main(void)
{
  DAVE_STATUS_t status;

  status = DAVE_Init();           /* Initialization of DAVE APPs  */

  if (status != DAVE_STATUS_SUCCESS)
  {
    /* Placeholder for error handler code. The while loop below can be replaced with an user error handler. */
    XMC_DEBUG("DAVE APPs initialization failed\n");

    while(1U)
    {

    }
  }

  /* Placeholder for user application code. The while loop below can be replaced with user application code. */
  while(1U)
  {
	  DIGITAL_IO_SetOutputHigh(&LED1);
	  for(float i = 0; i < 9000000; i++){};
	  DIGITAL_IO_SetOutputLow(&LED1);
	  for(float i = 0; i < 9000000; i++){};
  }
}
```
</details>

## XMC LIBS <a name="xmc_libs"></a>

<details>
<summary><b>code</b></summary>

```c
/* file: main.c */
#include "DAVE.h"    // Declarations from DAVE Code Generation (includes SFR declaration)
#include "main.h"

int main(void)
{
    XMC_GPIO_Init(XMC_GPIO_PORT5, 9u, &DIGITAL_IO_0);

    while(1u)
    {
        XMC_GPIO_ToggleOutput(XMC_GPIO_PORT5, 9u);
        for(int i = 0; i < 9000000; i++);
        XMC_GPIO_ToggleOutput(XMC_GPIO_PORT5, 9u);
        for(int i = 0; i < 9000000; i++);
    }

}
```

```c
/* file: main.h */
#ifndef _MAIN_H_
#define _MAIN_H_

#include "xmc_gpio.h"

XMC_GPIO_CONFIG_t DIGITAL_IO_0 =
{
    .mode = XMC_GPIO_MODE_OUTPUT_PUSH_PULL,
    .output_level = XMC_GPIO_OUTPUT_LEVEL_LOW,
    .output_strength = XMC_GPIO_OUTPUT_STRENGTH_MEDIUM
};

#endif /* _MAIN_H_ */
```
</details>

## REGISTERS <a name="registers"></a>

<details>
<summary><b>code</b></summary>

```c
/* file: main.c */
#include "main.h"
#include "XMC4700.h"

void LEDInitPort5Pin8And9(void);
unsigned int ChangeOutputstate(const unsigned char mask,const unsigned char pin);

PORT0_Type port_0_structure;

void main(void)
{
    LEDInitPort5Pin8And9();

    port_0_structure.OMR =  ChangeOutputstate(SET, PIN8); /* set P5.8 */
}

void LEDInitPort5Pin8And9(void)
{
    port_0_structure.OMR   = ChangeOutputstate(CLEAR, PIN8);/* clear P5.8 & P5.9 */
    port_0_structure.PDR1  = 0x00000004; /* medium driver P5.8 & P5.9 (A2) */
    port_0_structure.IOCR8 = 0x00000080; /* General-purpose Push-pull output driver mode P5.8 & P5.9 as */
}

unsigned int ChangeOutputstate(const unsigned char mask, const unsigned char pin)
{
    unsigned int temp = 0;

    switch (mask)
    {
        case SET:
        {
            temp = (1 << pin);
            break;
        }
        case CLEAR:
        {
            temp = (1 << (pin + 16));
            break;
        }
        case TOGGLE:
        {
            temp = ((1 << pin) | (1 << (pin + 16)));
            break;
        }
    }

    return (temp);
}


```

```c
/* file: main.h */
#ifndef _MAIN_
#define _MAIN_

#define SET 0
#define CLEAR 1
#define TOGGLE 2

enum
{
    PORT0,
    PORT1,
    PORT2,
    PORT3,
    PORT4,
    PORT5,
    PORT6,
    PORT7,
    PORT8,
    PORT9,
    PORT10,
    PORT11,
    PORT12,
    PORT13,
    PORT14,
    PORT15
};

enum
{
    PIN0,
    PIN1,
    PIN2,
    PIN3,
    PIN4,
    PIN5,
    PIN6,
    PIN7,
    PIN8,
    PIN9,
    PIN10,
    PIN11,
    PIN12,
    PIN13,
    PIN14,
    PIN15
};

enum
{
    IOCR_PC0,

};

#endif /* _MAIN_ */




/*
 * PnIOCR
 * PnOMR
 *
 *
 *
 *
 *
 * It is recommended to complete the Port and peripheral configuration with respect
 * to driver strength, operating mode and inital values before the port pin is switched
 * to output mode.
 *
 * Before activating the push-pull driver, it is recommended to configure its driver strength
 * and slew rate according to its pad class and the application need by the Pad Driver Mode
 * register Pn_PDR.
 * driver strength: Allows optimization to the outputs for the needed interface performance.
 *                  Help to reduce power consumption and limit noise, crosstalk & electromagnetic emissions.
 *
 * P5.8 pad type: A2 (strong, medium, or weak)
 * P5.9 pad type: A2 (strong, medium, or weak)
 * Actual logic level at the pin can be examined through reading Pn_IN
 * Test before setting port pin then after!
 *
 *
 * See 26.7 - Initialization and System Dependencies
 *
 *
 *
 *
 */

```
</details>

