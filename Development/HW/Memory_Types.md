# MEMORY TYPES
[**volatile**](https://en.wikipedia.org/wiki/Volatile_memory)**:** Computer storage that only maintains its data while the device is powered.  
[**nonvolatile**](https://en.wikipedia.org/wiki/Non-volatile_memory)**:** Computer memory that can retain stored information even after power is removed.  

## RAM (RANDOM ACCESS MEMORY)
Stores constant & variable values during program execution.  
Mostly *volatile*, but can be *nonvolatile*.

## ROM (Read Only Memory)
Like the microcontrollers hard drive. Has 2 partitions reserved for program code & permanent data used by the chip.   
Mostly *nonvolatile*. 

### EEPROM
[**EEPROM (Electrically Erasable Programmable Read-Only Memory)**](https://en.wikipedia.org/wiki/EEPROM)**:** Can be programmed and erased in-circuit. Were limited to single byte operations, but have increased to multibyte. Have a limited life erasing & programming, often million operations. 

### FLASH
[**flash**](https://en.wikipedia.org/wiki/Flash_memory)**:** Electronic non-volatile computer memory storage medium that can be electrically erased and reprogrammed. Used for storing & transferring data between devices. Developed from EEPROM.  
Can erased or write data in entire blocks. Has fast read access times, but not as fast as RAM. 
Have a limited life erasing & programming, often 10,000 operations. 
Example: [firmware](https://en.wikipedia.org/wiki/Firmware) containing BIOS.  

**pflash:** Program flash that stores program code & data constants.  
**dflash:** Data flash that stores data.  

## VIRTUAL MEMORY - [YT](https://www.youtube.com/watch?v=2quKyPnUShQ)
* Avoids memory fragmentation
* Each app/program thinks its the only program running. Has access to all the address space. 
1. Each app is self contained. Doesn't write onto other apps address space.
2. Doesn't matter where app is in memory. MMU does the mapping of the virtual addresses to the physical addresses.
3. App doesn't need to be in one continous block in physical memory

## QUESTIONS
<details>
<summary><b>Q:</b> Why have <b>NVRAM</b>?</summary>

***A:*** If data needs to be saved when power is off/on w/ fast access, then NVRAM can be used. Since, RAM belongs to primary memory.   
</details>

<details>
<summary><b>Q:</b> Differnce between <b>registers</b> & <b>memory</b>?</summary>

**A:** [G4G](https://www.geeksforgeeks.org/difference-between-register-and-memory/), [TD](https://techdifferences.com/difference-between-register-and-memory.html)  
**register:** Smallest data holding storage locations built into the CPU. Hold instructions or operands in which CPU is currently processing. Are memory locations directly accessible by the processor.  
**memory:** HW device used for storing computer programs, instructions, & data. Holds data that will be required for processing.   
 - **primary memory:** Internal to the CPU, e.g. RAM.
 - **secondary memory:** External to the CPU, e.g. hard drive.

![Picture of Register & Memory](Images/Registers_Memory.png) ![Picture of Register & Memory2](Images/Registers_Memory2.png)  
</details>
