#include <stdio.h>
#include <stdlib.h>

typedef struct CONV_ARGS_TAG
{
  int *I;
} CONV_ARGS_TYPE;

void foo (CONV_ARGS_TYPE conv_args);

int main ()
{
    int idx;
    CONV_ARGS_TYPE conv;
    conv.I = (int *) calloc (5, sizeof (int));
    
    printf("address of conv.I = %x\n", &conv.I);
    for (idx = 0; idx < 5; idx++)
    {
      printf ("conv.I[%d] = %d\n", idx, conv.I[idx]);
    }
    
    foo(conv);
    
    printf("address of conv.I = %x\n", &conv.I);
    for (idx = 0; idx < 5; idx++)
    {
      printf ("conv.I[%d] = %d\n", idx, conv.I[idx]);
    }

  return 0;
}

void foo (CONV_ARGS_TYPE conv_args)
{
    int idx;

    /*for (idx = 0; idx < 5; idx++)
    {
      conv_args.I[idx] = 1;
    }*/
    
    conv_args.I[0] = 1;
    conv_args.I[1] = 1;
    conv_args.I[2] = 1;
    conv_args.I[3] = 1;
    conv_args.I[4] = 1;

    printf("address of conv_args.I = %x\n", &conv_args.I);
    for (idx = 0; idx < 5; idx++)
    {
      printf ("conv_args.I[%d] = %d\n", idx, conv_args.I[idx]);
    }
}

/* OUTPUT

address of conv.I = 4e6fda80
conv.I[0] = 0
conv.I[1] = 0
conv.I[2] = 0
conv.I[3] = 0
conv.I[4] = 0
address of conv_args.I = 4e6fda48
conv_args.I[0] = 1
conv_args.I[1] = 1
conv_args.I[2] = 1
conv_args.I[3] = 1
conv_args.I[4] = 1
address of conv.I = 4e6fda80
conv.I[0] = 1
conv.I[1] = 1
conv.I[2] = 1
conv.I[3] = 1
conv.I[4] = 1

*/