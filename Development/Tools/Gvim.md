# GVIM

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INSTALL GVIM](#install_gvim)
2. [VIMRC](#vimrc)
</details>

## INSTALL GVIM <a name="install_gvim"></a>
1. Goto [link](https://www.vim.org/download.php), under section *Downloading Vim MS-Windows* select stable version executable. 
2. Vim VX Setup window opens up. Type of install: *Typical*.
3. Used default cofiguration settings.

## VIRMC <a name="vimrc"></a>
***help:*** Vim command mode type `:h vimrc`.

## FILE LOCATION
***Vim:*** `C:\Program Files (x86)`  
***vimrc:*** `C:\Users\buddy\vimfiles\`  