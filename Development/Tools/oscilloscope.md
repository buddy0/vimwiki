# OSCILLOSCOPE

## KEY PARAMETERS & SPECIFICATIONS - [link](https://www.youtube.com/watch?v=Sb6rlDApgUI&t=206s) [link](https://www.youtube.com/watch?v=r-NjuDN4oAc&t=140s)  
1. BANDWIDTH
    * At least 4x the fundamental of the signal in order to capture the signal properly.  
      If bandwidth is not high enough then sqaure waves become rounded.   
![Bandwidth picture](bandwidth.png)
      
2. SAMPLE RATE
    * Digital bendwidth. At least double the bandwidth to satisfy the Nyguist frequency. 
    * Ex. 200 MHz bandwidth, then 400 million samples. If the scoeps sampel rate = 1 Gsamples and using 2 channles then each channel has 500 Msampels which is greater than 400 Msamples.  
      
3. MEMORY DEPTH
    * More memory depth, the more times you can capture at the max sample rate enabling the user to obtain the full
    bandwidth of the oscilloscope over a wider range of time bases.  
    * *Memory Depth = Acquisition Time Window x Sample Rate*  
    A 1 MSa per channel a scope can capture 1 ms of time with a 1 GSa/s sample rate. 
    * How many samples can it save from the snalog to digital. Basically zoom out to a low frequency graph, capture the waveform, stop the trigger, now all that data is in memory. And I can now zoom in.  
    * Normally entry level scopes with 300MHz can only produce 1Mpoints (not really deep memory, can't look around with alot of data points). A step above would be 10Mpoints.  
    
5. SCOPES RESOLUTION
    * Number of levels an ADC converter. 8-bit oscilloscope is 2^8 = 256 level. 10-bit oscilloscope is 2^10 = 1024 levels. The more levels corresponds to more the oscilloscope is able to represent small details.  

6. WAVEFORM UPDATE RATE
    * How fast the scope can actually capture, trigger the signal and put it on display.  
    * Numer of acquistions per second that the scope can process and display. 
    * Faster update reate = more response, less blind time.   
    
7. DIGITAL SCOPES
    * DIGITAL STORAGE OSCILLOSCOPE (DSO)
    * MIXED SIGNAL OSCILLOSCOPE (MSO): Add 16 channles for a logic analyzer. Might be better in price to go with DSO and go with an external logic analyzer.    
    * MIXED DOMAIN OSCILLOSCOPE (MDO): Separate RF input on the front of the scopes.  
    
## IDEAL SCOPE
Bandwidth: at least 50 MHz  
GS/s: 1GS/s  
Deep Meemory: 10 M points  
FFT: 1 million  
[GW Instek GDS-1102R](https://www.amazon.com/GW-Instek-GDS-1102R-Oscilloscope-2-Channel/dp/B083TJNXPQ/ref=sr_1_5?crid=1AU66K8CHLB1N&dchild=1&keywords=gw%2Binstek%2Boscilloscope&qid=1627849247&s=industrial&sprefix=GW%2BInstek%2Cindustrial%2C180&sr=1-5&th=1)  
[Siglent Technologies SDS1102X](https://www.amazon.com/Siglent-Technologies-SDS1102X-Digital-Oscilloscope/dp/B01410O5D2/ref=dp_prsubs_1?pd_rd_i=B01410O5D2&psc=1)  

## TRIGGERING - [link](https://www.youtube.com/watch?v=H0Czb2zBzsQ)  
## PROBES - [link](https://www.youtube.com/watch?v=XQlPSFqhG08)


