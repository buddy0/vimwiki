# GIT
*Wherever HEAD is, is where action takes place.*  

## USEFUL COMMANDS
| action                                  | command                                      |
| --------------------------------------- | -------------------------------------------- |
| set up name                             | `$ git config --global user.name "<name>"`   |
| set up email                            | `$ git config --global user.email "<email>"` |
| set up diff tool                        | `$ git config --global diff.tool bc4` `$ git config --global difftool.bc4.path "C:\Program Files\Beyond Compare 4\BCompare.exe"` |
| initialize git repo in working directoy | `$ git init .`                               |
| clone existing repo                     | `$ git clone <url>`                          |
| add untracked file to staging area      | `$ git add <file_name>`                      |
| see status of modified files            | `$ git status`                               |
| commit tracked files in staging area    | `$ git commit -m "<message>"`                |
| see commit log                          | `$ git log`                                  |
| see diff of modified file               | `$ git diff <file_name>`                     |
| see diff of branches using diff tool    | `git difftool --dir-diff <branch_name> <branch_name>` |
| see local branch names                  | `git branch`                                 |
| see remote branch names                 | `git branch -r`                              |
| see local & remote branch names         | `git branch -a`                              |

**.gitconfig:** lives in home directory. The *git config* commands are put the results into this file.

## COMMIT
**commit:** Snapshot of the project. Records a snapchat of all tracked files in a directory.

## BRANCH
**branch:** Pointer to a specific commit. "Includes work of this commit & all parent commits.  
***make a new branch:*** `$ git branch BugFix`  
***switch to a different branch:*** `$ git checkout BugFix`  
***make & switch to the branch:*** `$git checkout -b BugFix`   
```
        git checkout -b BugFix 

+--+            +--+
|c0|            |c0|
+--+            +--+
 ^               ^
 |               |
+--+ main*      +--+ main 
|c1|            |c1| BugFix*
+--+            +--+
```

## MERGE 
**merge:** Combine 2 different branches together. Creates special commit that has 2 unique parents.  
***syntax:*** `$ git merge <branch_name/commit>`. Merge the branch_name/commit towards the HEAD.  
If the HEAD is located at a parent commit, then merge at a child commit. No new branches form. Just fast forward to the child commit.  
```
                                 git merge BugFix         git checkout BugFix, git merge main

         +--+                         +--+                       +--+           Since BugFix was ancestor of main
         |c0|                         |c0|                       |c0|           git didn't have to do any work
         +--+                         +--+                       +--+
          ^                            ^                          ^
       ---|---                      ---|---                    ---|---
       |     |                      |     |                    |     | 
      +--+  +--+                   +--+  +--+                 +--+  +--+
main* |c1|  |c2| BugFix            |c1|  |c2| BugFix          |c1|  |c2|
      +--+  +--+                   +--+  +--+                 +--+  +--+
                                    ^     ^                    ^     ^
                                    |     |                    |     |
                                   +--+ ---                   +--+ ---
                             main* |c3|               main    |c3| 
                                   +--+               BugFix* +--+ 
```

## REBASE
**rebase:** Another way of combining 2 branches together. Makes 2 branches act developed sequentially instead of parallel.  
Takes set of commits, copies them, and plops them down somewhere else.  
Used to make nice linear sequence of commits. Commit log/history is cleaner.  
***syntax_1:*** `$ git rebase <branch_name/commit>`. Merges HEAD towards the branch_name/commit.
***syntax_2:*** `$ git rebase <branch/commit_1> <branch_name_commit_2>`. *_1 is the target, while *_2 is whats being copied over.  
```
                                git rebase main                  git checkout main, git rebase BugFix 
        +--+                          +--+                          +--+
        |c0|                          |c0|                          |c0|             Since main was ancestor of BugFix,
        +--+                          +--+                          +--+             git moved branch ref forward in history.
         ^                             ^                             ^ 
      ---|---                       ---|---                       ---|---
      |     |                       |     |                       |     |
     +--+  +--+                    +--+  +--+                    +--+  +--+
main |c1|  |c2| BugFix*       main |c1|  |c2| (dotted)           |c1|  |c2| (dotted)
     +--+  +--+                    +--+  +--+                    +--+  +--+
                                    ^                             ^
                                    |                             |
                                   +--+                          +--+
                           BugFix* |c3|                   BugFix |c3|
                                   +--+                    main* +--+
```

```
                                git rebase main BugFix         git rebase BugFix main
         +--+                          +--+                          +--+
         |c0|                          |c0|                          |c0|             Since main was ancestor of BugFix,
         +--+                          +--+                          +--+             git moved branch ref forward in history.
          ^                             ^                             ^ 
       ---|---                       ---|---                       ---|---
       |     |                       |     |                       |     |
      +--+  +--+                    +--+  +--+                    +--+  +--+
main* |c1|  |c2| BugFix        main |c1|  |c2| (dotted)           |c1|  |c2| (dotted)
      +--+  +--+                    +--+  +--+                    +--+  +--+
                                    ^                             ^
                                    |                             |
                                   +--+                          +--+
                           BugFix* |c3|                   BugFix |c3|
                                   +--+                    main* +--+
```

## INTERACTIVE REBASE
**interactive rebase:** Best way to review series of commits when about to rebase.  
Similar to rebase but instead of all the commits in that branch being copied over, a selection of commits can be copied.  
***syntax:*** `$ git rebase -i <commit/branh_name>`. Git opens a UI showing which commits are about to be copied below the target of the rebase.  
```
                                 git rebase -i main
         +--+                           +--+
         |c0|                           |c0|
         +--+                           +--+  
          ^                              ^
       ---|---                        ---|---
       |     |                        |     |
     +--+   +--+                    +--+   +--+
main |c1|   |c2|               main |c1|   |c2| (dotted)
     +--+   +--+                    +--+   +--+
             ^                       ^      ^
             |                       |      |
            +--+                    +---+  +--+
            |c3| BugFix*    BugFix* |c3`|  |c3| (dotted)
            +--+                    +---+  +--+
```

## HEAD
**head:** Currently checked out commit. Always points to most recent commit reflected in working tree.  
***detaching head:*** Attaching to a commit instead of a branch.  
```
             git checkout c1    git checkout main
+--+               +--+              +--+
|c0|               |c0|              |c0|
+--+               +--+              +--+
 ^                  ^                 ^
 |                  |                 |
+--+               +--+              +--+
|c1| main*    head |c1| main         |c1|
+--+               +--+              +--+
```

## RELATIVE REFS
**relative refs:** Help move around in git rather than using the commit hash. Give concise way to refer to a commit.  
***^ operator:*** Move upwards 1 commit at a time.  
***~<num> operator:*** Move upwards # of times. Specifies # of parents to ascend.  
***HEAD:*** Used as a relative ref.  
```
             git checkout HEAD^   
+--+              +--+                      
|c0|              |c0| HEAD                   
+--+              +--+                   
 ^                 ^                         
 |                 |
+--+              +--+
|c1| main*        |c1| main
+--+              +--+
```

## BRANCH FORCING
**branch forcing:** Directly assign a branch to a commit w/ *-f* option. Done using relative refs. Done using relative refs.
Gives quickly to move branch to specific location.  
***syntax:*** `$ git branch -f <branch_name> <commit>`  
```
                git branch -f BugFix HEAD^ 
+--+                  +--+
|c0|                  |c0| BugFix
+--+                  +--+
 ^                     ^
 |                     |
+--+                 +--+
|c1| main*           |c1| main*
+--+ BugFix          +--+
```

Good Example showing branch is forced where ever the HEAD is located (if using HEAD as ref).  
```
                     git branch -f BugFix HEAD^
                        (what to happen)              (what really happens)
   +--+                 +--+                                +--+
   |c0|                 |c0| BugFix                         |c0|
   +--+                 +--+                                +--+
    ^                    ^                                   ^
 ---|---              ---|---                             ---|---
 |     |              |     |                             |     |
+--+  +--+           +--+  +--+                          +--+  +--+
|c1|  |c2| BugFix    |c1|  |c2|                   BugFix |c1|  |c2|
+--+  +--+           +--+  +--+                          +--+  +--+
 ^                    ^                                   ^
 |                    |                                   |
+--+                 +--+                                +--+
|c3| main*           |c3| main*                          |c3| main*
+--+                 +--+                                +--+
```
## REVERSE CHANGES

### RESET
***reset:*** Reverse changes by moving branch ref to older commit. As if the leaving commit had never been made.  
***syntax:*** `$ git reset <commit/branch_name>`. Attaching the HEAD to this spot and leaving the current spot.  
Good for local branches, but not for remote branches others are using.
```
              git reset HEAD^
+--+              +--+
|c0|              |c0| main*
+--+              +--+
 ^                 ^
 |                 |
+--+              +--+
|c1| main*        |c1| (dotted)
+--+              +--+
```

### REVERT
***revert:*** Reverse changes & share them with others.  
***syntax:*** `$ git revert <commit/branch_name>`. Always add a commit where the HEAD is located.  
```
              git revert HEAD      |              git revert HEAD    |               git revert main
+--+              +--+             |    +--+          +--+           |    +--+            +--+
|c0|              |c0|             |    |c0|          |c0|           |    |c0| HEAD       |c0|
+--+              +--+             |    +--+          +--+           |    +--+            +--+
 ^                 ^               |     ^             ^             |     ^               ^----------
 |                 |               |     |             |             |     |               |          |
+--+              +--+             |    +--+          +--+           |    +--+            +--+       +---+
|c1| main*        |c1|             |    |c1| main     |c1| HEAD      |    |c1| main       |c1| main* |c1'| HEAD
+--+              +--+             |    +--+          +--+           |    +--+            +--+       +---+
                   ^               |                   ^             |
                   |               |                   |             |
                  +---+            |                  +---+          |
                  |c1'| main*      |                  |c0'| main     |
                  +---+            |                  +---+          |
``` 

## CHERRY PICKING
**cherry picking:** Copy series of commits below current location (HEAD). Great when one knows which commits one wants.    
***syntax:*** `$ git cherry-pick <c1> <c2> <...>`.  
```
                          git cherry-pick c1 c3
    +--+                       +--+
    |c0|                       |c0|
    +--+                       +--+
     ^                          ^
  ---|---                    ---|---
  |     |                    |     |
+--+   +--+                 +--+   +--+
|c1|   |c2| main*           |c1|   |c2|
+--+   +--|                 +--+   +--+
 ^                           ^      ^
 |                           |      |
+--+                        +--+   +---+
|c3| BugFix          BugFix |c3|   |c1'|
+--+                        +--+   +---+
                                    ^
                                    |
                                   +---+
                                   |c3'| main*
                                   +---+
``` 

## ADVANCED TOPICS
***selecting 1 commit:*** cherry-pick or rebase -i  
***juggling commits:*** rearrange commits, commit ammend, rearrange them back  

## TAGS
**tags:** Permanently mark certain commits as "milestones" that can be ref like a branch.   
They never move.  Exist as anchors designating certian spots.  
***syntax:*** `$ git tag <tag_name> <commit>`. Creates a tag at the HEAD or specified commit.  

## DESCRIBE
**describe:** Desrcribes where one is to the closest tag.   
***syntax:*** `$ git describe <ref>`.  

##
