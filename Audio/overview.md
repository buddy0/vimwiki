# CABLES OVERVIEW - [link](https://www.youtube.com/watch?v=ACCTcYCvuZM)  

## LEVELS
**microphone:** Lowest signal level, thus will require preamp to get signal coming out of microphone to a decent recording level.  
Most auido interfaces have a built in preamp.
**line output:** Much higher signal level, doesn't require any amplification. Examples such as drums & keyboard. 
**instrument level:** Will require a bit of gain from the mixer & guitar itself w/ volume knob. Example includes guitar. 

## BALANCED 
***3 total wires:*** 2 wires carring the signal, while 1 wire for ground. 
* Both signal wires are carrying the same signal, but one signal is 180 degrees out of phase w/ other signal. As the signal being carried thoughout the calble it is liable to pick up noise as it travels. When the signal gets to audio mixer, interface input, the 2 signals are flipped and added to each other. Resulting in the noise built on the signal to be canceled out.  
* If I want to make a balanced connection, every single item in the signal chain must be balanced.
![Picture of Balanced Wiring](Images/balanced_wiring.png)  

## UNBALANCED
***2 total wires:*** 1 wire for signal & 1 wire for ground. 
* Unbalanced b/c only carrying 1 signal & might pick up noise that will not get canceled out. As a result, keep cables short.  
![Picture of Unbalanced Wiring](Images/unbalanced_wiring.png) 

## XLR MICROPHONE LEAD
* 3-pin lead w/ male and female ends. 
* Male end normally goes into microphoen while female end goes into audio interface or mixer. 
* Is MONO, not stereo. 
* **Balanced cable**.

## QUARTER INCH (1/4) OR 6.35mm JACK 
* Used for conencting guitar or mono output from a line level instrument like a keyboard. 
* **MONO/TS plug:** 2 wires, signal & ground resulting in an ... 
* **Unbalanced cable.**  
* **mono signal:** Only carry 1 signal that is unbalanced.  
![Picture of MONO/TS 1/4 Cable](Images/quarterinch_mono_ts.png)  
* **Stereo/TRS cable:*** 3 wires. Use it in 2 ways:
1. **stereo unbalanced signal:** 2 signals, left & right, of are transmitted down the cable & 3rd wire for ground. 
2. **mono balanced signal:** Now the 2 wires are carrying the same signal instead of the left & right signals & 3rd wire is ground.  
![Picture of Stereo/TRS 1/4 Cable](Images/quarterinch_stereo_trs.png)  

### SUMMARY 
***TRS:*** Balanced mono or unbalanced stereo.  
***TS:*** Unbalanced mono.  

## ONE EIGHTH (1/8) OR 3.5mm JACK
* little brother of 1/4 TRS
![Picture of 1/8 TRS Cable](Images/oneeighth_trs.png)

## RCA (PHONO)
* Each cable runs 1 signal down throughout resulting in unbalanced mono. 

## TRRS
* Mostly used for phone, laptop, tablet.
* Its an input & output at the same time. The headphone signals are going one way while the microphone signal is going opposite way. 

## Y-CABLE (BREAKOUT CABLE) 3.5mm STEREO to 2xRCA MONO
* Common for stereo output & want to connect left & right to audio interface.  
![Picture of Y-Cable 3.5mm Stereo TO 2 RCA Mono](Images/ycable_oneeighth_stereo_to_2RCA_mono.png) 

## Y-CABLE (BREAKOUT CABLE) 3.5mm stereo to 2xTS 6.35mm MONO
* Splits stereo signal into a left & right unbalanced mono  
![Picture of Y-Cable 3.5mm Stereo to 2 TS 6.35mm mono](Images/ycable_oneeighth_stereo_to_2TS_quarterinch_mono.png)  

## AUDIO INTERFACE
* ***headphone output:*** Normally a TRS Stereo 1/4 which is unbalanced. 








 
