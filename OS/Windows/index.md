# WINDOWS

* [Uninstall Linux](uninstall_linux.md)
* [Uninstall Windows Programs Completely](uninstall_windows_programs.md)
* [File Explorer](File_Explorer.md)
* [Wipe Hard Drive](Wipe_Hard_Drive.md)
* [File System Types](File_System_Types.md)
* [After Building PC](After_Building_Pc.md)
