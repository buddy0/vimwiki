# UNINSTALL LINUX - [HDG](https://helpdeskgeek.com/linux-tips/how-to-uninstall-ubuntu-in-a-windows-10-dual-boot-system/)

<details>
<summary><b>ENTER BIOS</b></summary>

**Upon Start up/Restart & F-Key**  
&emsp;&emsp;**1.** Restart computer & click one of the keys to load into BIOS.  
**In Windows 10 & Recovery Blue Screen**   
&emsp;&emsp;**1.** Click on *Troubleshoot* > *Advanced options* > *UEFI Firmware Settings*. *UEFI Firmware Settings* screen opens up.  
&emsp;&emsp;**2.** Click *Restart*. Computer will restart & load into BIOS.  
</details>

<details>
<summary><b>RECOVERY BLUE SCREEN</b></summary>

**In Windows Settings**   
&emsp;&emsp;**1.** Click on *Start* > *Settings* > *Updates & Security*. *Updates & Security* window opens up.  
&emsp;&emsp;**2.** Click on *Recovery*. Under *Advanced Startup* section, click on *Restart now*.  
**Using Power Icon**  
&emsp;&emsp;**1.** Windows Start menu, click on power icon.  
&emsp;&emsp;**2.** Hold down shift key & select *Restart*.  
</details>

<details>
<summary><b>ADMINISTRATOR COMMAND PROMPT</b></summary>

**In Windows 10**  
&emsp;&emsp;**1.** Search *cmd* in windows search bar. Right click & select *Run as administrator*. *Administrator: command prompt* window opens up. ***OR***   
&emsp;&emsp;**1.** Click on *Start* button. Scroll down & select Folder *Windows System*. Right click on *Command Prompt*. Select *More* > *Run as administrator*.  
&emsp;&emsp;&emsp;*Administrator: command prompt* window opens up.  
**In Recovery Blue Screen**  
&emsp;&emsp;**1.** Get to Recovery Blue Screen.  
&emsp;&emsp;**2.** Click on *Troubleshoot* > *Advanced options* > *Command Prompt*.  
</details>

## CHANGE BOOT ORDER USING UEFI
## OVERWRITE LINUX BOOT LOADER W/ WINDOWS BOOT LOADER
## REMOVE THE GRUB BOOTLOADER
## DELETE LINUX DISK/PARTITION IN WINDOWS
## REPAIR MASTER BOOT RECORD (MBR)

## QUESTIONS
* What partitions automatically come upon new used HDD & SSD? 
* Allowed to delete everying in drive including Recovery partition, etc.?
