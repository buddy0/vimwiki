# FILE EXPLORER

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. CHANGE OPEN FILE APPLICATION
</details>

# CHANGE OPEN FILE APPLICATION EXCEL EXAMPLE
1. Right click on file File Explorer -> *Properties* -> under *General* tab, field *Opens With*, left click change button.  
Window *how do you want to open .xml files from now on* opens up. Scroll down to bottom -> left click on *More apps* -> scroll down to bottom -> left click on *Look for more apps on this pc*.  
2.  Browse to `C:\Program Files (x86)\Microsoft Office\root\Office 16`-> left click on *Open* -> left click on *ok*.  
