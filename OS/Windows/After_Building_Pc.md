# AFTER BUILDING PC

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INSTALLING WINDOWS](#installing_windows)
4. [DRIVER INSTALLATION](#driver_installation)
5. [FINAL SETUP TWEAKS](#final_setup_tweaks)
</details>

## INSTALLING WINDOWS <a name="installing_windows"></a>
1. Stick windows media into system -> turn on PC -> go into BIOS by pressing DELETE key.
2. Make sure drive containing windows media & drive to be stalled on have boot option priority #1 & #2, respectively.  
Turn off all the other drives which will make it easier to distinguish the drive we want during windows installation. 
3. F10 - Save configuration & Exit. Windows media setup window opens.
4. Custom Insall, don't have internet connected when intalling.
5. 'Lets Get Started Screen': Unplug media install drive.  
Select *I don't have internet* -> *Continue with limited setup* -> *Do more across devices* - No -> *Get help from digital assistant* - Decline -> Choose privacy settings for your device - No to all.
6. At windows desktop for first time. Connect to internet. Windows will now try to intsall all the *outdated* drivers. See progress by clicking windows key -> update. Install all these updates before installing new drivers.

## DRIVER INSTALLATION <a name="driver_intallation"></a>
1. nvidia graphics card
    * Install wizard:
        * select *NVIDIA Graphics Driver*
        * select *Custom (Advanced)*
        * select *Perform a clean Installation*
    * Open NVIDIA CONTROL PANEL.
        * 3D Settings -> Manage 3D Settings -> Set *Power management mode* to *Prefer maximum performance* & set *Texture filtering - Quality* to High Performance.
2. Goto manufacture website & download latest drivers: chipset, ethernet, audio, rgb

## FINAL SETUP TWEAKS <a name="final_setup_tweaks"></a>
1. Go into BIOS. Enable *Extreme Memory Profile (X.M.P.) OR (DOCP). F10. 
2. Set max refresh rate for monitor.
