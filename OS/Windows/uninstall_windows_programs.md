# UNINSTALL WINDOW PROGRAMS COMPLETELY - [link](https://www.auslogics.com/en/articles/how-to-remove-software-leftovers/#:~:text=Type%20%25programfiles%25%20into%20the%20Search,Delete%20such%20folders.)

## STEPS
1. ***uninstall program***
2. ***delete uninstalled program folder in these folders:***  
windows search -> `%programfiles%` -> remove uninstalled program folder  
windows search -> `%appdata%` -> remove uninstalled program folder  
windows search -> `%temp%` -> remove uninstalled program folder
3. ***clean windows registry keys that have uninstalled program folder in these folders:***  
`HKEY_CURRENT_USER\Software`  
`HKEY_LOCAL_MACHINE\SOFTWARE`  
`HKEY_USERS\.DEFAULT\Software`    
`HKEY_LOCAL_MACHINE\SOFTWARE\Wow6432Node`  
