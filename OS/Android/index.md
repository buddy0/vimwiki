# ANDROID

## SETTINGS

### NOTIFICATIONS
***brief pop up notify:*** Notifications -> Brief pop-up settings
***app icon badges:*** Notifications -> Advanced settings -> App icon badges

### DISPLAY
Screen timeout
Navigation bar
Touch sensitivity

### HOME SCREEN
App icon badges

### ADVANCED FEATURES
* SPEN
    * When S Pen is removed
* Side key
* Video Brightness

### BATTERY AND DEVICE CARE
* Optimize

### ABOUT PHONE
* Name of phone

