# FILE SYSTEM TYPES
*[youtube: Explaining File Systems](https://www.youtube.com/watch?v=_h30HBYxtws)*

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpose)
2. [FAT](#fat)
3. [NTFS](#ntfs)
4. [EXFAT](#exfat)
5. [EXT](#ext)
6. [HFS, HFS+, & APFS](#hfs_hfs+_apfs)
7. [ZFS](#zfs)
8. [CHOOSING FILE SYSTEM](#choosing_file_system)
9. [QUESTIONS](#questions)
</details>

## PURPOSE <a name="purpose"></a>
**file system:** Standards for organizing data on storage device. Applied when format a partition/drive.  
Divide storage space on a drive into virtual compartments aka clusters & mantain an index of where individual files are located & avilable free space. 

## FAT <a name="fat"></a>
* File allocation table (FAT)
* ***variants:*** FAT12, FAT16, FAT32 w/ each one increasing # of clusters and max file & volume sizes.
* FAT32 remains popular due to wide OS compatibility. 
* Still widely used for removable media.

| variant | max file size        | max volume size                                               |
| ------- | -------------------- | ------------------------------------------------------------- |
| FAT12   | 32MB (8KB clusters)* | 32MB (8kb clusters)                                           |
| FAT16   | 2GB/4GB              | 16GB (256kb clusters)                                         |
| FAT32   | 4GB                  | 32GB (Windows format)<br>2TB (other OS)<br>16TB (theoretical) |

*16MB w/ 4KB clusters.  
**Different FAT16 versions limited to max file size of 2GB, 4GB, or vol siz of 2GB, 4GB, 8GB, 16GB depending on cluster & vector size.   

## NTFS <a name="ntfs"></a>
* New technology file system (NTFS). Introduced in 1993 to overcome limitations of fat32.
* File size limit of 16 exabyters (EB).
* Journaling file system (to avoid corruption).
* Supports file permissions and encryption.
* Windows must be installed on NTFS drive.
* Limited non-Windows OS compatibility - eg read-only in macOS & older Linux distros. 

## EXFAT <a name="exfat"></a>
* Extended file alloation table (exFAT).
* Introduced by Microsoft in 2006 as a file system optimized for high-capacity USB flash drives & memory cards.
* Less sophistaicated than NTFS but has significant benefits over fat32.
* Max. file size = 16EB. Thus best choice for formatting memory cards for recording video.
* Default file system for SDXC cards.
* Broader non-Windows OS support than NFTS - including read & write on macOS. Many linux distros requires extra drivers to be able to run exfat devices.

## EXT <a name="ext"></a>
* Extended file system (ext)
* Launched in In 1992 for Linux. In 1993, ext2 was released. In 2001, ext3 released & introduced journaling to protect against corruption in event of crashes or power failures.
In 2008, ext4 became Linux default FS.
* ***variants:*** ext2, ext3, ext4.
* ext4 max file size of 16TB & volumes up to 1EB.
* No native Windows or macOS support.

## HFS, HFS+, & APFS <a name="hfs_hfs+_apfs"</a>
* Hierarchical file system (HFS). Aka macOS standard.
* Introduced by Apple in 1985 for macOS.
* Max file & volume size = 2GB & 2TB, respectively.
* In 1998, HSF Extended (HSF+). Aka macOS Extended which introduced journaling.  
Max file & volume size of 8EB when using macOS 10.4 or above.
In 2017, Apple file system (APFS). Optimized for SSDs.
* No native Windows or Linux support.

## ZFS <a name="zfs"></a>
* zed file system (ZFA).
* Introduced in 2006 by Sun Microsystems & now developed by the OpenZFS project.
* Integrates volume manager to control storage HW.
* Hence provides increased data protection.
* Supports Linux, FreeBSD, and TrueOS.

## CHOOSING FILE SYSTEM <a name="choosing_file_system"></a>
* ***system drive:*** NTFS (Windows), ext4 (Linux), or HFS+/APFS (macOS)
* ***usb drives & memory cards:*** use FAT32 for capacities under 32GB. USse exFAT for 32GB+ or 4GB+ files. 
* ***external HDD or SSD:*** NTFS (Windows), exFAT if PC/Mac based, or FAT32.

## QUESTIONS <a name="questions"></a>
<details>
<summary><b>Q1:</b> How to check the file system type of a drive?</summary>

**A:** Windows - file explorere -> right click on drive -> properties
</details>

<details>
<summary><b>Q2:</b> How to change the files system type?</summary>

**A:** Only way to change is reformatting the drive which loses all data on the drive.
</details>

<details>
<summary><b>Q3:</b> So, in my case Windows, Linux, HDD/SDD, microSD/eMMC, USB would all be installed on _?</summary>

**A:**  
***Windows:*** NTFS  
***Linux:*** EXT4
***HDD/SDD:*** ?
***microSD/eMMC:*** ?
***USB flash drive:*** ?
</details>

<details>
<summary><b>Q4:</b> What file system type works with both OS Windows & Linux?</summary>

**A:** Best would be FAT32 or NTFS.
</details>

