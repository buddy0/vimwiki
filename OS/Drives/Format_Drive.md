# FORMAT DRIVE

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [WINDOWS - DISK MANAGEMENT](#windows_disk_management)
2. [QUESTIONS](#questions)
</details>

<details>
<summary><b>DEFINITIONS</b></summary>

* **partitions:** Sections on drive that store data. Always need at least one partition on a drive. 
    * **logical partition:** Cannot be used as boot
* ***boot interfaces:*** BIOS (older) & UEFI. 
* ***store info on drive:*** MBR & GPT.
    * **Master Boot Record (MBR)**: Only create 4 primary partitions. Can be avoided by creating 3 primary partitions, plus an extended partition made up of logical partitions. Only 32 bits available to represent logical sectors -> resulting to 2TB of storage space (anything larger, the extra disk space marked unalocated & unusable). Stores all partitions & boot data together which is terrible for redundacy.
    * **GUID Partition Table (GPT):** Create upto 128+ partitions on single drive. Allows 64 bits resulting in 9.4ZB of storage space. Stores duplicates of boot data across several partitions at beginning & end of table headers. If one partition gets corrupted can use other partition to recover -> more resilient to errors.
</details>

## [WINDOWS - DISK MANAGEMENT](https://support.microsoft.com/en-us/windows/create-and-format-a-hard-disk-partition-bbb8e185-1bda-ecd1-3465-c9728f7d7d2e) <a name="windows_disk_management"></a>
* ***opening disk management:*** Windows button -> *Control Panel* -> *Administrative Tools* -> *Computer Management* -> *Disk Management* (under *Storage*).
* ***create & format new volume/partition:*** Right-click unallocated region on hard disk -> *New Simple Volume*,  
*New Simple Volume Wizard* opens up: *Specify Volume Size*, *Assign Drive Letter or Path*, *Format Partition*.
    * To unallocate storage, right-click -> *Shrink Volume*, windows *Shrink* opens up.
* ***format existing volume/partition:*** Top section right-click volume/partition -> *Format*, *Format* window opens.
* ***initialize disk:*** If format option doesn't pop up, then disk needs to be initalized. Right click on disk -> *Initialize* -> *GPT* or *MBR* -> OK -> now format the disk.
* ***note:*** Formating a disk will erase all its data.  
***note:*** Cannot format a disk or partition currently in use, including the partition that contains Windows.  
***note:*** Can also format drive directly from file explorer.

![Picture of My Disk Management](Images/my_disk_management.png)


## QUESTIONS <a name="questions"></a>
<details>
<summary><b>Q1:</b> Does formatting a parition delete all of the data?</summary>

**A:** Formatting a drive only removes the pointers to the data as partition table is created or rebuilt. Once partition table is cleared or rebuilt, it can now accept new data in the place of old data aka *overwriting* which causes true & absolute data loss.
</details>

<details>
<summary><b>Q2:</b> Which is best MBR or GPT?</summary>

GPT is best. MBR is good for older OS while GPT better for modern computers. GPT is a must have if drive is more than 2TB and using Windows 11. 
</details>
