# OS

* [Windows](Windows/index.md)
* [Arch Linux](ArchLinux/index.md)
* [Android](Android/index.mf)
* [Move Steam Games From One PC To Another](#Move_Steam_Games_From_One_PC_To_Another.md)

## FILES/FORMAT
* [File System Types](Drives/File_System_Types.md)
* [Format Drive](Drives/Format_Drive.md)

