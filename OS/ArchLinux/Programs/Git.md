# <span style="color:navy">GIT</span>

## GITLAB
First create a Gitlab account.  
`username: buddy0`  
`email: mredoute1@gmail.comi`   

### SSH KEY
Create a key in *.ssh/id_rsa* `#ssh-keygen -t rsa -b 2048 -C "mredoute1@gmail.com"`  
1. copy key
2. goto gitlab *page/settings/sshkeys*  
3. paste the key  
4. add key  

* Now git push will ask for username and password: standard https way  
* `$ git push git@gitlab.com:buddy0/nameofrepo.git`: forces to use ssh

## GIT BARE REPO
A git folder contains 1. workspace 2. repo: in the *.git* folder  
Git bare repo is the repo w/o the workspace. A non-working directory.  
* `git clone --bare where_to_clone_from the_name`  
* Since this is a git bare repo containgin bo FILES -> git commands won't work (git status, git checkout) b/c it's not a workspace.  
* Seems pointless b/c can't do anyting ... BUT  
* This can now be used by another repo to push and clone
* Won't add anything to this directory

### PROS
NO simlinking from dotfiles folder to home directory.  
Don't have to place anything in the dotfiles directory.  

### STEPS
1. `$ mkdir ~/dotfiles`  
2. `$ git init --bare ~/dotfiles`  
3. In *.baschrc* add: `alias config='/usr/bin/git --git-dir=~/dotfiles --work-tree=~'  // replace ~ w/ $HOME`  
* anytime I type **config** @ the shell, it is going to run the git command with flags --git-dir and --work-tree  
* Git directory: dotfiles directory  
* Working tree: HOME directory  
* Will take effect on a new bash instance.  
4. `$ config config --local status.showUntrackedFiles no`  
* Going to be a lot of untracked files in home directory.  

### EXAMPLE
`$ cd dotfiles`  
`$ git remote add origin https://gitlab.com/~/dotfiles.git`  
`$ cd ~`  
`$ config add .bashrc`  
`$ config commit -m "Add my bashrc."`  
`$ config push -u origin master`  
* type in username & password  

## SUCKLESS PATCHES w/ GIT
1. Clean up Master before making any branch. Want branches to be default dwm, have clean code.  
`$ make clean && rm -f config.h && git reset --hard origin/master`  
2. Now we can make a new branch.  
`$ git branch config`  
`$ git checkout config`  
`$ git add config.def.h`  
`$ git commit -m "Add config branch"`  
`$ git checkout master`  
`$ git merge config -m "Merging config branch to master"`  
`$ make && sudo make clean install`  
3. Another branch.  
`$ make clean && rm -f config.h && && git reset --hard origin/master`  
`$ git branch pertag`  
`$ git checkout pertag`  
`$ git apply location_of_where_patch_is_downloaded`  
`$ git add file_that_was_change`  
`$ git commit -m "Add pertag branch"`  
`$ git checkout master`  
`$ git merge config -m "Merging config branch to master"`  
`$ git merge pertag -m "Merging pertag branch to master"`  
`$ make && sudo make clean install`  



