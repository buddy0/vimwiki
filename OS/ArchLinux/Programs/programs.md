# PROGRAMS

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [XORG](#xorg)  
&emsp;**a.** [AVOID SCREEN TEARING](#avoid_screen_tearing)  
&emsp;**b.** [VM FULLSCREEN](#vm_fullscreen)
2. [WALLPAPERS](#wallpapers)
3. [SOUND](#sound)
4. [SUCKLESS](#suckless)
5. [FONTS](#fonts)
6. [YAY](#yay)  
&emsp;**a.** [YAY ERROR](#yay_error)  
7. [VIA CONFIGURATOR](#via_configurator)
8. [STARSHIP](#starship)
9. [FIREFOX](#firefox)  
&emsp;**a.** [EXTENSIONS FOR MARKDOWN](#extensions_for_markdown)  
10. [ICONS IN THE TERMINAL](#icons_in_the_terminal)  
&emsp;**a.** [FOR NNN](#for_nnn)  
&emsp;**b.** [NNN PLUGINS](#nnn_plugins)  
&emsp;**c.** [PACKAGE](#package)  
11. [XDG MIME](#xdg_mime)
12. [ZATHURA](#zathura)
13. [BLUETOOTH](#bluetooth)
14. [INSTALLING PROGRAMS UNIX WAY](#installing_programs_unix_way)
15. [SNAP](#snap)
16. [ANBOX](#anbox)  
&emsp;**a.** [INSTALLING APPS TRHOUGH ADB](#installing_apps_through_adb)  
17. [MOUSE ACCELERATION](#mouse_acceleration)
18. [VIM](#vim)  
&emsp;**a.** [CREATE FOLDS](#create_folds)

</details>

## XORG <a name="xorg"></a>
`sudo pacman -S xorg xorg-server xorg-init`  
Find what GPU is installed on system and installed necessary drivers.  
`$ lspci | grep -e VGA -e 3D`  
`$ sudo pacman -S nvidia nvidia-utils`  
Copy xinitrc file to the home directory. Add *exec* window manager.  
`$ cp /etc/X11/xinit/xinitrc ~/.xinitrc`  
`$ vim .xinitrc`  
Add exec dwm. Save and exit vim.  
`$ startx`: start xorg server with dwm  
`$ exit`: remove xorg server  

## AVOID SCREEN TEARING <a name="avoid_screen_tearing"></a>
`$ sudo pacman -S nvidia-settings`  

## VM FULLSCREEN <a name="vm_fullscreen"></a>
xrandr: permanently adding undetected resoltuions  
`$ X -configure` This generates an almost completed configuration file for xorg in root folder.  
`cp xorg.conf.new /etc/X11/xorg.conf`  
In */etc/X11/xorg.conf*, under Section 'Monitor' write `Modeline and Option`  

## WALLPAPERS <a name="wallpapers"></a>
`$ sudo pacman -S feh`  
`$ feh --no-fehbg --bg-scale --randomize '/path/to/image/file'`  
In ~/.xinitrc add this line before exec dwm to restore background image.  
`~/.fehbg &`

## SOUND <a name="sound"></a>
`$ sudo pacman -S alsa-utils`  
`$ alsamixer`  

## SUCKLESS <a name="suckless"></a>
`$ sudo pacman -S git make gcc pkg-config`  
In slsstatus -> `(%x %r)`  
To have DWM status bar slsstatus, add this line to .xinitrc.  
`~/programs/suckless/slstatus/slstatus &`  

## FONTS <a name="fonts"></a>
`$ sudo mv ~/Downloads/font.ttf /usr/share/fonts/ttf`  
Change font for *st* in config.def.h.  
To figre out the name: `$ fc-list | grep "name"`  

## YAY <a name="yay"></a>
Install  
1. `$ git clone https://aur.archlinux.org/yay.git`  
2. `$ cd yay`  
3. `$ makepkg -si` error -> solution: `$ sudo pacman -S binutils fakeroot`  
* Never have to include `sudo`. If it wants privileges it will ask.  

### yay: error while loading shared libraries: libalpm.so.12: cannot open shared object file: No such file or directory <a name="yay_error"></a>
**solutin:** Removing yay & installing latest verion manually. [link](https://joshtronic.com/2021/06/06/yay-error-while-loading-shared-libraries-libalpm/)
1. `$ sudo pacman -R yay`  
2. `$ mkdir tmp/yay` && `$ ch tmp/yay`
3. `$ curl -OJ 'https://aur.archlinux.org/cgit/aur.git/plain/PKGBUILD?h=yay'`: Dowland latest PKGBUILD. `$ ls`: To see PKGBUILD.  
4. `$ makepkg -si`: Build & install package.  
5. `$ rm -rf /tmp/yay`: Get rid of evidence.  
  
## VIA CONFIGURATOR <a name="via_configurator"></a>
`$ yay -S via-bin`  
`$ sudo via-bin --no-sandbox`  

## STARSHIP <a name="starship"></a>
[Install](https://starship.rs)  
1. `$ surl -fsSL https://starship.rs/install.sh | bash`  
2. In .bashrc add `eval "$(starship init bash)"`  

Configuration  
`$ mkdir -p ~/config` and `$ touch ~/.config/startship.toml`  
```linux
# Don't print a new line at the start of the prompt
add_newline = false

# Make promt 1 line instead of 2 lines
[line_break]
disabled=true

# Replace the "❯" symbol in the prompt with "➜"
[character]                            # The name of the module we are configuring is "character"
success_symbol = "[➜ ](bold green)"    
```

## FIREFOX <a name="firefox"></a>
### EXTENSIONS FOR MARKDOWN <a name="extensions_for_markdown"></a>
[Markdown viewer install link](https://addons.mozilla.org/en-US/firefox/addon/markdown-viewer-webext/?utm_source=addons.mozilla.org&utm_medium=referral&utm_content=search)  
`$ vim .mime.types`  
add `text/plain md markdown`  

## INCONS IN THE TERMINAL <a name="icons_in_the_terminal"></a>
[Install](https://github.com/sebastiencs/icons-in-terminal#installation)  
1. `$ git clone https://github.com/~in-terminal.git`  
2. `$ ./install.sh`  

### FOR NNN <a name="for_nnn"></a>
[Install](https://github.com/~#file-icons)  
1. clone the repo
2. compile nnn / make O_ICONS=1 with sudo  
3. running `./nnn` will show icons, but need to *move the newly compiled nnn to where your programs go*  
4. `sudo mv nnn /bin`  
* nnn does not have a config file. Environment variables are used. I put mine in .bash_profile  

### NNN PLUGINS <a name="nnn_plugins"></a>
In *~/.config/nnn/plugins* run `$ curl -Ls https://raw.githubusercontent.com/jarun/nnn/master/plugins/getplugs | sh`  

### PACKAGE <a name="package"></a>
`$ sudo pacman -S xdg-utils`  

## XDG MIME <a name="xdg_mime"></a>
`$ xdg-mime query default image/png` -> feh.desktop  
`$ xdg-mime query default text/plain` -> vim.desktop  
`$ xdg-mime query filetype .vimrc` -> image/png  

To find applications, goto */usr/share/applications
```bash
feh.desktop
vim.desktop
firefox.desktop
org.pwmt.zathura/desktop
nnn.desktop
```

## ZATHURA <a name="zathura"></a>
`$ sudo pacman -S zathura zathura-pdf-poppler`  

## BLUETOOTH <a name="bluetooth"></a>
`$ sudo pacman -S bluez bluez-utils`  
Make sure Kernal module *btusb* is loaded. [link](https://wiki.archlinux.org/title/Kernel_module#Obtaining_information)  
`$ lsmod | grep btusb`  
If not, have to use modprobe to load the module.  
`$ sudo pacman -S kmod` && `$ modprobe btusb`  
Start the bluetooth service: `$ sudo systemctl start bluetooth.service`  
Start evertime at bootup: `$ sudo systemctl enable bluetooth.service`  

## INSTALLING PROGRAMS UNIX WAY <a name="installing_programs_unix_way"></a>
**Normally Package Managers:**  
* Automate installation, removal, and management of SW applications.  
  
**Installing the source manually:**    
* Required to keep track of installed location, version of SW, and remove maually.  

**make:**
* Automates compilation of programming source code for target system.  
* "Makefile" define steps to build the application. 
* commands: 
* `$ configure` A local script inside directory of soruce code downloaded which probes the system to make sure the correct componenets are installed.
  `$ make` Do the actual compilation of the program. Source code into machine code.  
  `$ make install` Need elevated privalages suc has *sudo*. Copies the binary file to the apprropiate place on the system so that it can found within your path and can be installed 

## SNAP <a name="snap"></a>
1. `$ whereis snapd`: Check to see if I have *snapd*: required to install snaps.    
2. `$ yay -S snapd`: Didn't work.   
***ERROR:*** A failure occurred in build(). Aborting...  
**solution [link](https://unix.stackexchange.com/questions/626215/cant-install-snap-on-arch-linux):** `$ sudo pacman -S --needed base-devel`. Select **1** for *autoconf*.  
***SAME ERROR:*** Can't exec "aclocal": No such file or directory at /usr/share/autoconf/Autom4te/FileUtils.pm line 274. autoreconf: error: aclocal failed with exit status: 2.  
**solution:** `$ sudo pacman -S automake`. :)
3. `sudo systemctl enable snapd.socket --now`:. Enable and start the service.  
4. `$ snap --version`: Make sure that snap service is running.  
5. `$ snap --list`: Check installed snaps on linux.   

## ANBOX - [install](https://www.youtube.com/watch?v=ElNOJlEELEI), [archwiki](https://wiki.archlinux.org/title/Anbox), [link](https://forum.garudalinux.org/t/ultimate-guide-to-install-anbox-in-any-arch-based-distro-especially-garuda/7453) <a name="anbox"></a>
1. `$ sudo snap install --devmode --beta anbox`
2. NOT YET. `$ sudo snap remove anbox`.  
3. HAVE TO INSTALL A KERNEL w/ ashmem & binder modules.

### INSTALLING APPS TRHOUGH ADB <a name="installing_apps_through_adb"></a>
1. `$ sudo pacman -S android-tools`
2. `$ adb install /path/to/app.apk` 

## [MOUSE ACCELERATION](https://wiki.archlinux.org/title/Mouse_acceleration) <a name="mouse_acceleration"></a>
1. ***see input devices:*** `$ xinput`  
2. ***see properties for an input device:*** `& xinput list-props <id>`  
3. ***create file, make it executable, & place in .xinitrc:***  
&emsp;a. `$ touch .xprofile`  
&emsp;b. `$ chmod +x .xprofile`  
&emsp;c. in .xinitrc before launching dwm add `~/.xprofile &`

***note:*** For some reason creating bash script in a normal file and adding it to .xinitrc doesn't change mouse speed. Reason why file .xprofile is used.

```bash
#!/bin/bash

xinput --set-prop 'pointer:''Ploopy Corporation Trackball Mouse' 'libinput Accel Profile Enabled' 0, 1
xinput --set-prop 'pointer:''Ploopy Corporation Trackball Mouse' 'libinput Accel Speed' -0.5
```

## VIM <a name="vim"></a>

### CREATE FOLDS <a name="create_folds"></a>
***fold method manual:*** `set foldmethod=manual` Have to manual create folds. Folds are not saved when reopening files. Kind of like RAM.  
To save folds:  
1. `$ mkdir ~/.vim/view`
2. Goto vim file, `:mkview`
3. `:loadview`  
OR  
Add following lines to .vimrc  
`autocmd BufWinLeave *.* mkview`  
`autocmd BufWinEnter *.* silent loadview`  

***create fold:*** highlight lines in visual mode then press `zf`.  
***close fold:*** `zc`    
***open fold:*** `zo`  
***close all folds:*** `zM`  
***delete fold:*** `zd`  

## DEVELOPMENT

### CTAGS
***install:*** sudo pacman -S ctags  
***create tags file for c library:***  
1. ***create tags:*** `$ cd ../../usr/include/` -> `$ ctags -R`
2. ***add newly created ctags file in .vimrc:*** `set tags+=/usr/include/tags`

| keyboard                          | action                                          |
| --------------------------------- | ----------------------------------------------- |
| CTRL + ] or g] or :ta[g] CTRL + rw | jump to tag underneath cursor                     |
| CTRL + T                           | jump back in the tag stack                        |
| :tn                                | jump to next matching tag                         |
| :tp                                | jump to previous matching tag                     |
| :tags                              | shows contents of tag stack, > marks active entry |
