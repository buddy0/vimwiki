# <span style="color:navy">POSTINSTALLATION</span>

## 64 BIT LIBRARY
Since I installed Arch Linux 64-bit, need to make sure pacman is utilizing the 64-bit library.  
Edit pacman.conf file. Uncomment.  
`# vim /etc/pacman.conf`  
Uncomment `#[multllib] & lines below`  

## SYNCHRONIZE
Synchronize local package database with the remote package database, and update any packages that have any version differences.  
`# pacman -Syu`  

## ADD USER
Add user. Create password. Install sudo. Add user to appropriate group. Add user to sudoers file. Uncomment.  
`# useradd -m buddy` or `# useradd -m -g users -s /bin/bash buddy`  
`# passwd buddy`  
`# pacman -S sudo`  
`# usermod -aG wheel,audio,video,optical,storage buddy`  
`# groups buddy` 
`# vim /etc/sudoers`  
Uncomment `# %wheel ALL=(ALL) ALL` or/and add `buddy ALL=(ALL) ALL`  

## AUTOMOUNT EXTERNAL DRIVE
`# lsblk`  
`# sudo mkdir /mnt/seagate`  
`# mount /dev/sdb1 /mnt/seagate`  
`# blkid`  
`# cat /proc/mounts`  
`# sudo vim /etc/fstab`  
UUID=####### /mnt/seagate ext4 rw,relatime 0 0  

