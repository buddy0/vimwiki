# <span style="color:navy">PREINSTALLATION</span>

## CHECK INTERNET STATUS
`# ip a`  
`# ping google.com`  
^c to kill the command  

## UPDATE SYSTEM CLOCK
`# timedatectl set -ntp true`  
`# timedatectl status`  

## PARTITIONING

### UEFI w/ GPT | BIOS w/ MBR
Guild Partition Table (GPT): part of the Unified Extensible Firmware Interface  
Master Boot Record (MBR): first 512 bytes of storage  

fdisk: edits, manipulates partition maps  
Need to foromat partitions w/ appropriate file system  

| Partition | Size      | File System  |
|-----------|-----------|--------------|
| UEFI      | 512MB     | EFI          |
| root      | remainder | linux (ext4) |

`# fdisk /dev/sda`  
`# g` Creates a new empty GPT partition table label.  
`# n` Add a new partition.  
`# +512MB` EFI partition.  
`# t` Change the partition type.  
`# 1` EFI system.  
`# n` Add a 2nd partition.  
`# p` Print the partition table.  
`# w` Write tabel to disk and exit.  

## FORMATTING
Formatting: [definittion](https://www.partitionwizard.com/partitionmagic/what-does-formatting-a-hard-drive-do.html)
           
UEFI partition: `# mkfs.fat -F32 /dev/sda#`  
root partition: `# mkfs.ext4 /dev/sda#`  

## MOUNTING
Root partition mounting to the *mnt* directory b/c this is where the system is going to be installed.  
`# mount /dev/sda#/ /mnt`  

