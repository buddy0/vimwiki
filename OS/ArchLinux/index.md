# ARCH LINUX

## INSTALL
* [PreInstallation](Install/PreInstallation.md)
* [Installation](Install/Installation.md)
* [PostInstallation](Install/PostInstallation.md)

## PROGRAMS
* [Programs](Programs/programs.md)
