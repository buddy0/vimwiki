# ERRORS

<details>
<summary><b>error: failed to commit transaction (invalid or corrupted package), no packages were upgraded.</b></summary>

**solution [[SO]](https://unix.stackexchange.com/questions/574493/sudo-pacman-syu-fails-with-error-failed-to-commit-transaction-invalid-or-cor):** Update keyring since some arch developeres may have changed their keyrings and updates are signed with new PGP keys.  
Perform following commands:  
1. `$ sudo pacman -Sy archlinux-keyring`
2. `$ sudo pacman -Syu`  
</detail>
