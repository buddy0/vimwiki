# SOLENOID
*SA-DDD-Solenoid.doc*  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [DETAILED DESIGN](#detailed_design)  
    a. [SOLENOID CONTROL](#solenoid_control)  
        &emsp; I. [CONTROL STRATEGIES FOR ABS IOS SOLENOIDS](#control_strategies_for_abs_ios_solenoids)  
    b. [SOLENOID FAILSAFE](#solenoid_failsafe)  
    c. [STATE DEFINITIONS](#state_definitions)  
</details>

**solenoid:** Electromagnet formed by a helical coil which generates a controlled magnetic field when an electric current passes through it.   
Coil of wire acting like a magnet when carring electrical current.  
An engineering, solenoid refers to complete apparatus which includes an actuator.  
**actuator:** Component responsible for moving & controlling a mechanism of a system. Simply a mover.  
Ex: opening a valve.   

## DETAILED DESIGN <a name="detailed_design"></a>
* SCS includes different types of solenoid:  
    * ABS isolation, digitally controlled
    * ABS dump, digitally controlled
    * circuit isolation
    * circuit supply
    * Electronically Controlled Actuator (ECA), current controlled
    * Variable Effort Steering (VES)

### SOLENOID CONTROL <a name="solenoid_control"></a> 
2 types:  
1. ***on-off control:*** Digitally controlled: closing/opening valve.  
For normally open solenoid, opening the valve means the command is fully OFF. Closing the valve means turning solenoid fully ON.  
For normally closed solenoid, opening the valve means the command is fully ON. Closing the valve means turning solenoid fully OFF.  
2. ***current control:*** More precise linear control. Specific current output is desired. Designed for both close/open loop current control. Calculates solenoid duty cycle.  
* ***closed loop:*** Aka Feedback control system. Output is fed back to the input.  
* ***open loop:*** Current feedback circuitry not available. 

#### CONTROL STRATEGIES FOR ABS IOS SOLENOIDS <a name="control_strategies_for_abs_ios_solenoids"></a>
* ***Iso-Apply:*** Fully digital control of solenoid for a pressure apply pulse. Switched to 0% duty cycle to open open ISO valve & allow brake fluid flow into the caliper. Valve is closed w/ 100% duty cycle.  
* ***dual current:*** 
* ***Soft-Close:*** Reduce noise even more  

### SOLENOID FAILSAFE <a name="solenoid_failsafe"></a>
* ***known fault conditions:*** open solenoid, shorted solenoid, open solenoid driver cicruit, shorted solenoid driver circuit.  
* ***naming tests:***  

| test    | meaning                                                                                                                              |
| ------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| active  | Sol switched to known state, either ON or OFF.                                                                                       |
| passive | Sol is used in the state as it is & not actively changed by the test. Normally only 1 sol using active test, rest using passive test |
| ON      | Sol is considered to be ON = energized. Could be result of active or passive test.                                                   |
| OFF     | Sol is considered to be OFF = not energized. Could be result of active or passive test.                                              |

* ***detection of the faulty solenoids principles:***  
    * ***ASIC driven solenoids:*** Controlled by the ASIC itself. Circuit does the failsafe. Fault conditions are detected by this external HW module & reported by status flags. Since some faults are only detectable if coil is engaged, each solenoid should be periodically switched on for a min test duration.  
    * ***discrete driven solenoids:*** Connected to the (N)HET at driver circuit side w/ an analog feedback to the ADC (either V or I based).  
    Takes much longer due to: solenoid settling time, trigger ADC, & ADC converison.  
    During Test frame, solenoid state must *not* change.    
    * ***solenoid test  phases:***   

    | solenoid test phase                              | meaning                                                                                                                               |
    | ------------------------------------------------ | ------------------------------------------------------------------------------------------------------------------------------------- |
    | passive tests                                    | No active test pulse generated & test window set to OFF portion of PWM period.                                                        |
    | power on solenoid self test (initial test)       | During system self test after power on or "Clear DTC" diag event, all solenoids should be checked. Every solenoid is commanded on for min time & amount of test pulses (10 after power on & 7 for Clear DTC). |
    | periodic solenoid self test (slow test)          | In normal operating mode when no performance subsystem is active & no solenoid commands are issued. Sol switched on w/ test pulses periodically (~same as initial state). Only 1 sol tested for each cycle, every 5 seconds. |
    | solenoid self test during regulation (fast test) | If solenoid already commanded due to performance subsystem activation & it has a discrete control circuit. Use existing PWM signal to do failsafe. Sol command may be usable for either ON or OFF testing in a passive way. |
***Failsafe Relay current:*** 
***Over temperature:*** Based on thermal model only.  
***static driver monitor:*** 
    

### STATE DEFINITIONS <a name="state_definitions"></a>
| state             | definition                                                                                                                               |
| ----------------- | ---------------------------------------------------------------------------------------------------------------------------------------- |
| Disabled          | Initial state of solenoid state machine                                                                                                  |
| Enabled           | When conditions are correct to allow solenoid activation, but no request for activation is present.                                      |
| SolenoidOff       | Solenoid is commanded off, due to a request or timeout event                                                                             |
| SolenoidOn        | Fully energizes solenoid when request is made                                                                                            |
| IsoApply          | Only be entered for NO solenoids that are desired to be open specified period of time.                                                   |
| IsoEndOfApply     | Entered when apply pulse for a channel iso solenoid has expired and an isolation pulse for the remainder of the control loop is desired. |
| CurrentRegulation | Entered when solenoid needs to be commanded to a duty cycle that corresponds to desired current level being commanded.                   |
| RegulateStage     | For the second part of dual current commands.                                                                                            |
| InhibitOffset     | Current has been commanded to 0 Amps after it was previously set to a non-zero current.                                                  |
| SoftClosePhase1   | Similar to CurrentRegulation, but used for soft close feature.                                                                           |
| SoftClosePhase2   | Special state of the soft close feature where current request is ramped up to 2nd current request w/in 1ms OS task.                      |
| CurrentTable      | Somehow similar to Soft-Close feature, but allows more flexible way in controlling coil current.                                         |
| CLCC Test         | Necessary since CLCC needs low current for a long time period to test the solenoid.                                                      |
| Shunt             |                                                                                                                                           


| events                  | meaning |
| ----------------------- | ------- |
| Disable                 | Power switch commanded off or being disabled, due to DTC condition being latched. Set FALSE when calling EnableSolenoid |
| Enable                  | Power switch commanded on. Set TRUE when calling EnableSolenoid. |
| CommandOff              | Occurs when solenoid is commanded off. When CommandSolenoid is called w/ active time set to 0 msec & CommandSolenoidCurrent call w/ request 0 Amps or active time of 0 msec. |
| CommandOn               | Generated when CommandSolenoid is called w/ non-zero active time & ApplyIsoSolenoid is called w/ active time of 0. |
| Apply                   | Generated when ApplyIsoSolenoid is called w/ non-zero active time. Opens iso solenoid to allow pressure to be added to the wheel(S). Commands sol open for amount of time. |
| ControlCurrentImmediate | Occurs when CommandSolenoidCurrent is called for a sol configured to immediately enter current regulation upon request. |
| SoftClose               | Generated when CommandSolenoidSoftClose is called w/ non-zero active time. Opens iso solenoid to allow pressure to be added to wheel(s). Command sol open for amount of time. Causes less noise than Apply. |
| CurrentTable            | Occurs when CommandSolenoidCurrentTable is called for a sol configured to immediately execute current regulation requests organized in a table. |
| CLCC Test               | Occurs when ComandSolenoidClccTest is called for a disengaged CLCC sol. Used only interal to generated an active ON test pulse. |
| Timeout                 | Generated when TimeoutSolenoid is called by sol ISR. Happens when time specified to reamin in current state has elapsed. |

***Solenoid Test State Machine:*** 
| state       | meaning                                                                                                                                                                        |
| ----------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ |
| passive     | Only passive sol tests are allowed & performed. Initial state of state machine.                                                                                                |
| InitInitial |  Entered when commanded from system self test to start initial sol tests. All variables needed reset to start values.                                                          |
| Initial     | Initial sol self test is performed. Means scheduling for next sol to test & requesting active ON test pulses. When all sol have performed, system self test is informed & state machine jumps back to passive state auto. If amount of requested sol tests differ to # of performed tests, failure SOLENOID_TIMEOUT_FAILURE is set.                                            |
| InitSlow    | Entered when commanded from self test to start slow periodic solenoid tests. Should be done if no other module is requesting sol activity. If there is, system self test should switch to passive or fast testing. Timer for inter-test delay is reset as start condition of the slow test.                                                                                    |
| Slow        | One sol per loop is commanded to get an active OFF test. Done cyclically for all solenoids. If inter-test delay timer has expired, state machine switches to SlowActive state. |
| SlowActive  | Actual sol to be active tested is requested to do an active ON test pulse. If all required test pulse are performed, inter-test delay timer is reset & state machine switched back to Slow state.                                                                                                                                                                            |
| InitFast    | Entered when commanded from system self test to start fast periodic sol tests. Done if other modules like performance subsystems are requesting sol activity.                  |
| Fast        | Next requested test is determined from test ages of sols & last test method performed.                                                                                         |
| Abort       | Entered when commanded from system self test to stop sol tests. Done if other modules like diagnostic are requesting sol activity or failsafe relay is switched off.           |

| event    | meaning                                                                            |
| -------- | ---------------------------------------------------------------------------------- |
| Update   | Generated when UpdateSolenoidMonitor is scheduled from OS.                         |
| Transfer | Generated from interal logic if a move to another state is needed.                 |
| Initial  | Occurs when CommandSolenoidTestMethod is called to switch into initial test phase. |
| Passive  | Occurs when CommandSolenoidTestMethod is called to switch off sol tests.           |
| Slow     | Occurs when CommandSolenoidTestMethod is called to switch into slow test phase.    |
| Fast     | Occurs when CommandSolenoidTestMethod is called to switch into fast test phase.    |
