# SA

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

</details>

**system asic:** ASIC aka Application System Integrated Circuit is a chip customized for a particular use rather than general purpose use.  
Does one job: built for only one customer, used in only one product line.   

| asic                  | uC               |
| --------------------- | ---------------- |
| specific app          | generic          |
| fully custom          | not fully custom |
| faster                | slower           |
| nonreprogrammable     | reprogrammable   |
| expensive             | cheaper          |
| performance increases |                  |
| space decreases       |                  |

## SA COMPONENTS & MODULES
| component | module                      |
| --------- | --------------------------- |
| WhlSpd    | [FastMissingWssDetect]      |
|           | [WheelSpeed](WheelSpeed.md) |
|           | [WssAdditionalInfo]         |
| Solenoid  | [LeakySolDriver]            |
|           | [Solenoid](Solenoid.md)     |
|           | [SolenoidCompensations]     |
|           | [SolenoidFreqHopp]          |
