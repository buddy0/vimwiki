# WHEELSPEED

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [BACKGROUND](#background)
2. [DETAILED DESIGN](#detailed_design)  
&emsp;a. [WHEEL SPEED CALCULATION DESIGN](#wheel_speed_calculation_design)  
&emsp;b. [EQUATIONS](#equations)  
&emsp;c. [STATES & EVENTS](#states_and_events)  
</details>

## BACKGROUND <a name="background"></a>
Wheel speed sensors typically include a toothed shaft.
The sensor counts the rate at which the teeth pass.

## DETAILED DESIGN <a name="detailed_design"></a>
* Design consists of 2 parts:  
1. Getting data from I/O
2. Analyzes raw data, determining if new speed is to be computed.  
TRUE: logic must account for tire size, sensor pulses per revolution, etc.  

### WHEEL SPEED CALCULATION DESIGN <a name="wheel_speed_calculation_design"></a>
* As wheel decclerates/accelerates, time between the edges will increase/decrease.
* At each update, I/O will provide # of sensor edges that were detected since the last update & time stamps for the last 2 detected sensor edges.
* Wheel speed velocity can be calculated based on:
    * number of sensor edges detected on this update
    * time stamps for latest 2 edges for current update * previous update (4 edges total)

### EQUATIONS <a name="equations"></a>
* $`sensor\;edge = \frac{tire\;circumference}{tooth\;count * 2}`$ each tooth count has 2 edges

* $`velocity=\frac{sensor\;edge}{\frac{1}{2}period}`$
$`velocity=\frac{sensor\;edge}{avg\;time\;between\;sensor\;edges}`$  

### STATES & EVENTS <a name="states_and_events"></a>

| state           | definition          |
| --------------- | ------------------- |
| Disabled        | WSS is not enabled. |
| Idle            | Wheel is not moving. Wheel speed may still be ramping down to zero if wheel was previously moving. |
| PreparingToMove | Wheel not moving, but appears to have started to move. Wheel speed cannot be calculated yet b/c timers used to calculate delta times may have rolled over. |
| StartingToMove  | Edges have been revceived for this wheel on this update. Wheel speed is calculated based on limited "real" edges.
| Moving          | Edges have been received for this wheel on this update. Wheel speed and wheel acceleration calculated based on limted "real" edges. | 
| WheelWasMoving  | No edges have been received this update. WS accleration will not be calculated based on "virtual" edges & thus is set to 0. |

| event           | definition |
| --------------- | ---------- |
| EdgesReceived   | Generated in Update function when sensor edges have been received this update for current wheel. |
| NoEdgesReceived | Generated in Update function when no sensor edges have been received this update for the current wheel. |
| Timeout         | Generated while in state WheelWasMoving if delta time > Wheel_Speed_Idle_Time. |
| Disable         | Generated to put WSS in state Disabled. |

### SUPPORT OF WSS
* ***standard active wss:*** Supported by wss module if they use 7 and 14mA current signaling.  
* ***intelligent wss:*** Can provide driving direction, air gap between sensor & pole ring, etc.
    * ***PWM:*** Use 7/14 mA. Each wss pulse represents a rising edge of the sensor. Additoinal info encoded in pulse length HIGH such as air gap or driving direction. PWM standstill pulses are filterd out.  
    * ***VDA:*** Use 7/14/28 mA. Transmits wws pulses & data. WS pulse represented by short 7/28 mA pulse. 8 data bits & 1 partity bit transmitted by 7/14 mA pulses. Standstill uses 7/14 mA pulses of 7/28 mA pulses used by WSS. Thus, there is no problems differianting between wss & standstill pulses.  
Additional wheel speed sensor data is decoded by the wss additional info module which reads raw data from miram asic. 

### FAILSAFES
### SENSOR FAULT MAPPING
These failures can be mapped to fault codes:  
Open Sensor  
High Side Over Current  
High Side Leakage Current
Low Side OVer Current  
High Side Shorted To UBat  
Low Side Shorted to GND  
Low Grey Zone  
High Grey Zone  
Erratic Sensor  
Missing Sensor  
Dropout  

### ELECTRICAL FAILSAFE


