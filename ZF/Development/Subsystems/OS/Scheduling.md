# SCHEDULING

**[scheduling](https://en.wikipedia.org/wiki/Scheduling_(computing)#task_queue):** Action of assigning resources to perform tasks. *Resources* may be processors. *Tasks* may be threads, processes, or data flows. Makes it possible to have computer multitasking with a single CPU.  
**[task](https://en.wikipedia.org/wiki/Task_(computing)):** Unit of execution or unit of work. Can be ambigous to these terms: process, step, request, thread (for execution)  
**[process](https://en.wikipedia.org/wiki/Process_(computing)):**  Instance of a computer program being executed by thread(s). Contains program code & its activity. Is the execution of instructions after being loaded from the disk into memory.  
**[request](https://en.wikipedia.org/wiki/Request%E2%80%93response):** Message exchange pattern. requestor sends a request message to a replier system, which receives and processes the request, ultimately returning a message in response.  
**[thread](https://en.wikipedia.org/wiki/Thread_(computing)):** 
* Smallest sequence of programmed instructions that can be managed independently by a scheduler. Most cases, component in a process. 
* Path of execution within a process. Has its own PC, register set, & stack [[G4G]](https://www.geeksforgeeks.org/thread-in-operating-system/).  
* Basic unit of CPU utilization consisting of a PC, stack, & set of registers [[EDU]](https://www.cs.uic.edu/~jbell/CourseNotes/OperatingSystems/4_Threads.html).  
![Picutre of Threads](Images/Threads.png)  

![Program VS Process VS Thread](Images/Program_vs_Process_vs_Thread.jpg)  
![Task & Thread](Images/Task_Thread.png)  

## QUESTIONS
<details>
<summary><b>Q:</b> Difference between <b>task</b> & <b>thread</b>?</summary>

**A:** [SO](https://stackoverflow.com/questions/4130194/what-is-the-difference-between-task-and-thread)  
**task:** Something you want done.  
**thread:** Workers that perform the task. 
</details>
