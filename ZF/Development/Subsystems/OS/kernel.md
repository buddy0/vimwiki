# Kernel - [wiki](https://en.wikipedia.org/wiki/Kernel_(operating_system)), [techopedia](https://www.techopedia.com/definition/3277/kernel) 

**kernel:**  Computer program at the core of a computer's OS & has complete control over everything in the system. Connects the application SW to the HW of the system.  
![Picture of Kernel](Images/kernel.png)
