# LINKER
*Linker User Manual V6.3r1*  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PROCESS FLOW](#process_flow)
2. [MEMORY](#memory)
3. [MAPFILE](#mapfile)
4. [GENERATE-LINKER-TRICORE-TASKING.PL](#generate_linker_tricore_tasking.pl)
5. [LINKING PROCESS](#linking_process)
6. [CONTROLLOING THE LINKER](#controlling_the_linker)
7. [LSL](#lsl)
8. [SEMANTICS](#semantics)
9. [MSC](#msc)
</details>

<details>
<summary><b>DEFINES</b></summary>

| word             | definitoin                               |
| ---------------- | ---------------------------------------- |
| physcial address | Offset on given internal or external bus |
</details>

## PROCESS FLOW <a name="process_flow"></a>
1. **generate_linker.bat**
2. **generate_linker_cmd_file.bat** (called from generate_linker.bat)
3. **Linker.mk** (called from gernerate_linker_cmd_file.bat)  
When *generate-Linker-TRICORE-tasking.pl* is executed, recipe below are used as arguements.  
```makefile
Linker.lsl: generate-Linker-TRICORE-tasking.pl
    ..\..\..\..\..\..\10_Tools\Perl\bin\perl.exe
    generate-Linker-TRICORE-tasking.pl 
    Linker-Prolog.lsl # in var prologLinkerScript
    Linker-Epilog.lsl # in var epilogLinkerScript 
    Linker.lsl # in var outname 
    TRICORE # in var sysname  
    32 # in var groupalign
    ..\..\..\..\..\BUILD\TriCore_MM_APP\partitioning.xml # in var reg_spec_file  
    ..\..\..\..\ThirdParty\Vector\BswAutocore_Vector\Appl\GenData # in var gen_data_path
    ..\..\..\..\Services\Os\LinkerCmd\Source\memory_region_config.xml # in var os_config_file
```

## MEMORY <a name="memory"></a>
***order files are used:***  
1. partition.partwizconf & memory_region_config.xml
2. partitioning.xml & memory_region.pm 
3. generate-Linker-TRICORE-tasking.pl  
4. linker.lsl 

***memory regions creation:*** By files generate-Linker-TRICORE-tasking.pl & memory_region.pm which parses file partitioning.xml which is created by partition.partwizconf which uses file memory_region_conifg.xml. File partitioning.xml contains: name, type, memory segment, files, libs, pragmas_i, pragmas_u, has init section, comment.  

## MAPFILE <a name="mapfile"></a>
**mapfile:** Output file containing info about section locations & symbols.

## GENERATE-LINKER-TRICORE-TASKING.PL <a name="generate_linker_tricore_tasking.pl"></a>
1. creates vairables to house command line arguments.
2. Parses file partitioning.xml for memory region configuration & for OS application.
3. Writes to output file.
4. Opens Linker Prolog, reads it, writes lines to output.
5. Parses ROM, RAM
6. Opens Linker Epilog, reads it, writes lines to output.
7. Close outfile

## LINKING PROCESS <a name="linking_process"></a>
1. ***linking:*** Input -> relocatable object .o file(s). Output -> relocatable object .out file.  
Relocatable object has:  
&emsp;***header info:*** Overal info about file such as code size, name of source file was assembled from, ..  
&emsp;***object code:*** Binary code & data, divided into various named sections. Sections are blocks of code that have to be placed in specific parts in memory.  
&emsp;***symbols:*** Exported or imported. Generally names of routines or names of data objects.  
&emsp;***relocation info:*** List of places w/ symbolic references that linker has to replace w/ actual addresses.  
&emsp;***debug info:*** Object code used by debugger.  
2. ***locating:*** Assigns absolute addresses to object code, placing each section in specific part of target memory.

## CONTROLLOING THE LINKER <a name="controlling_the_linker"></a>
Linker script file (LSL file). Has its own syntax, however C keywords can be used b/c script sent to the C preprocessor before it starts interpreting script.
An LSL file can define:  
1. ***memory in tagert:*** linker must know what memory device & its start-address, size, & if R/W accessible (RAM) or R only accessible (ROM).
2. ***how & where code & data placed in memory***
3. ***HW architecture of target processor***

LSL proveds linker w/:  
1. Definition of target's core architecture (supplied w/ toolset).
2. Specification of memory attached to target processor.
3. Info on how application loaded in memory.

## LSL <a name="lsl"></a>
***purpose:*** Specifies characteristics of tagert board & how sections located in memory.

***structure of LSL file:*** Definitions can appear in any order:  
&emsp;***architecture (required):*** How linker converts logical addresses into physical addresses.  
&emsp;Also contains info about hardware (stack) & interrupt vector table.  
&emsp;***derivative:*** Describes configuration  of internal (on-chip) bus & memory system.  
&emsp;Converts offsets on the buses (specified in architecture definition) into offsets in internal memory.  
&emsp;***processsor:*** Describes instance of a derivative.  
&emsp;***memory & bus (optional):*** Specifies internal/external memory & on-chip/off-chip buses (context of derivative/board specification definitions).  
&emsp;***board specification:*** Combines the processor & memory & bus definitions.  
&emsp;Describes all characteristics of target board's system buses, memory devices, I/O subsystems, & cores.  
&emsp;The linker for each core: convert logical address to offset w/in memory device, locate sections in memory, & view used & free physical memory.  
&emsp;***section layout:*** Control where input sections are located.  

***compilation process:*** 1. Preprocesses using C preprocessor. 2. Interprets using scanner & parser.  

***lsl lexicon syntax example (section 17.2 pg. 1019):***  `A ::= B` *A* is defined as *B*.  
***identifier:*** character sequence starting w/ a-z, A-Z followed letters, digits, & dots.  
***comments, exppresions:*** Same as C language.  
***functions:*** All return 64-bit signed integer.  

### SEMANTICS <a name="semantics"></a>
***semantics of section layout:*** Section 17.9 pg. 1057
Defines section layout for one address space.  
```lsl
section_layout <povessor>:<core>:space
{
    /* section statement(s) */
    group group_name (group specifications)
    {
        /* section statement(s) */

    }
}
```

**section_layout** <*processor*>:<*core*>:*space name* { *section_statements* }  
* ***clone sections:*** Placed in special address spaces characterized by set of regular address spaces (have different local memory). Created in main core of the task.  
Ex. `section layout mpe:vtc:tc1_abs18|tc2_abs18|tc0_abs18`  
* ***group:*** Where sections are located. Contain input section(s) & possibly other groups.  
***ls:*** **group** (*group_specifications*) { *section_statements* }  
sections

## MISC <a name="msc"></a>
* ***lsl select statements:*** select object file sections/segments.  
