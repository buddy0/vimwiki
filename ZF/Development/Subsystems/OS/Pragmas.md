# PRAGMAS
**pragma:** Keywords in C source that control behavior of the compiler.  

## COMMON PRAGMAS FOR TASKING COMPILER - [UM](http://www.tasking.com/support/tricore/tc_reference_guide_v2.2.pdf)
**`#pragma message "<string>"` (pg.43):** Print the message string on standard output. 

**`#pragme align <n>` (pg.41,220):** Based on *--align*. "By default the TriCore compiler aligns objects to the minimum alignment required by the architecture. 
With this option you can increase this alignment for objects of four bytes or larger. The value must be a power of two."  

**`#pragma align restore` (pg.41,220):** Returns to the previous pragma level if the same pragma is used more than once. Return to the previous alignment setting.  

**`#pragma section <section_type> "<section_name>"` (pg.44,261):** Based on *-R*.  
Compiler defaults to a section naming convetion: `section_type_pref.module_name.symbol_name`.  
**Ex.** For code sections: `.text.module_name.symbol_name`  
**"** If a module must be loaded at a fixed address or if a data section needs a special place in memory, *-R* option generates a different section name: `section_type_pref.name` where *name* replaces *module_name.symbol_name*. The new unique section name can now be used in linker script file for locating. **"**    

**`#pragma clear/noclear` (pg.42):** Performs 'clearing' or no 'clearing' of non-initialized static/public variables.  

**`#pragma warning/error` (not in TASKING):*** Issues a warning/error message.  


