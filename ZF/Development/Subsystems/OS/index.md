# OS
[**OS**](https://en.wikipedia.org/wiki/Operating_system)**:** System SW that manages computer HW, SW resources, & provides common services for computer programs.  
![Picture of OS](Images/Operating_System.png)  
* [scheduling](Scheduling.md)
* [kernel](kernel.md)
* [pragmas](Pragmas.md) 
* [program memory](Program_Memory_Layout.md)
* memory segmentation
* [Vector](Vector.md)
* [Linker](Linker.md)

## DEVELOPMENT PATHS

<details>
<summary><b>DP</b></summary>

***location:*** `/SCS/SCS_Core_Based/08_Software/Application/OS/OS_VECTOR_3xx/OS/project.pj`  
| program | development path          |
| ------- | ------------------------- |
| DT_PBM  | Os_Vector_3xx_CHR_DT_MY24 |
</details>

