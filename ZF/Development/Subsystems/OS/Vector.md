# VECTOR

## SANDBOXES

| name               | comment                         | Customer | Integrity path                                                                                            | 
| ------------------ | ------------------------------- | -------- | --------------------------------------------------------------------------------------------------------- |
| CBD1900139_Install | Needed Install to create ...D02 | CHR      | `/IBC/Customer/Chrysler/Common/05_Software/04_From_Supplier/Vector/MSR_CBD1900139/D02/install/project.pj` |
| CBD1900178_D00     | Was able to just extract ...D00 | GM       | `/SCS/Customer/GM/Common/05_Software/04_From_Supplier/Vector/Microsar_CBD1900178/D00/extract/project.pj`  |

1. Above sandboxes were created at `C:\Vector`  
2. Go into CBD1900139_Install and run CBD1900139_D02.exe.  
I forget what happens but this is how we create folder CBD1900139_D02.  
3. Go into `C:\Vector\CBD1900139_D02\DaVinciConfigurator\Core` -> right click hold *DaVinciCFG.exe* & -> Drag into `C:\Vector` -> Create shortcuts here -> Rename as Davinci *CBD1900139_D02 (CHR)*
4. Do same thing for *CBD1900178_D00*.  

***WL PBM SIP:*** `IBC/Customer/Chrysler/WL/05_Software/04_From_Supplier/Vector_Autocore/WL_PBM_CBD1900139_Project/project.pj`  
## DRIVERS
***location:*** `G:\Software\VECTOR\Drivers\`  
Need *ExternalComponentsSetup.zip* & *Vectr_Driver_Setup_21_10_3*.  
1. Copied above zips to `C:\Downloads\Installers` & ziped them here.  
2. Run `C:..\..\Downloads\Installers\Vector_Driver_Setup_21_10_3\Drivers\setup.exe`  
***note:*** Vector Driver Setup_21, VN1610, Vector Keyman.  
***note:*** When running them there will be error popup windows. Unblock and rerun multiple times for C runtime.  
3. Run `C:..\..\Downloads\Installers\ExternalComponentsSetup\DaVinci_ExternalComponentsSetup_2_18_0_1` ***I don't think I ran this!***  



