# PROGRAM MEMORY

![Picture of Typical Program Memory Layout](Images/Program_Memory_Layout.jpg)

**stack:** Contains the call stack, LIFO structure.  
**heap:** Contains dynamically allocated memory.  
[**bss**](https://en.wikipedia.org/wiki/.bss)**:** Aka block starting symbol. Portion of object file, executable, or assembly line code that contains statically allocated variables that are declared but no assigned value yet. They are initialized to zero or do not have explicit initialization in source code.   
**"** Peter van der Linden, a C programmer and author, says, "Some people like to remember it as 'Better Save Space.' Since the BSS segment only holds variables that don't have any value yet, it doesn't actually need to store the image of these variables. The size that BSS will require at runtime is recorded in the object file, but BSS (unlike the data segment) doesn't take up any actual space in the object file. **"**  
```c
int i;
char a[12];
```
[**data**](https://en.wikipedia.org/wiki/Data_segment)**:** Portion of an object file or corresponding address space for a program that contains *initialized* static variables, i.e. global variables & static local variables. Have a defined value & can be modified. Thus, *Read*&*Write*.  
```c
int i = 3;
char a[] = "Hello World";
```
**rodata:** Contains static constants rather than variables.  
[**text**](https://en.wikipedia.org/wiki/Code_segment)**:** Portion of an object file or the corresponding section of the program's virtual address space that contains executable instructions. Generally, *Read* only & fixed size.  
