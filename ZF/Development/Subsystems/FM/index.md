# FM
* [Training](Training.md)

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [IMPORTANT](#important)  
2. [CHR COMPONENTS & MODULES](#chr_components_&_modules)
3. [GM COMPONENTS & MODULES](#gm_components_&_modules)
4. [OTHER](#other)
</details>

## IMPORTANT <a name="important"></a>
* Each program has a different DP. DPs are used for any model year, however when a bug/update is needed a new development path is branched at the lastest checkpoint of that given model year.
* ***multi-core fault mapping FM programs:*** BT1XX, BV1XX, T1XX HD, C1XX.2, 31XX.2, T1XX PU/SUV Global B

## VOCAB
| term                           | definition                                                                                                                                                                             |
| ------------------------------ | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| SCS function                   | A functionality in the SCS-related parts of the performance SW, e.g. ABS, TC, YSC                                                                                                      |
| Control Mode (CM)              | Degration of a specific SCS function, i.e. SCS function can be NORMAL (good state, SCS function fully available), INHIBITED (bad state, SCS function completely inhibited), or in between. Affects next regulation activity, not the one currently ongoing.                                                                                                                                                 |
| Partial System Shutdown (PSSD) | Similar to CM, but affects the current ongoing regulation activity, i.e. SCS function can be NORMAL (good state, SCS function fully available & may continue current regulation process unaffected), INHIBIT (bad state, SCS function completely inhibited & must abort current regulation process immediately), or in between.                                                                                   |
| EPB function                   | A functionality in the EPB-related parts of the performance SW, e.g. ECD, RWU.                                                                                                         |
| EPB function inhibit           | Degration of a specific EBP function, i.e. EPB function can be NORMAL or INHIBITED (no inbetween).                                                                                     |
| Input Validation               | Indication whether a specific input signal can be used for its intended purposes. Can be VALID (good state), INVALID (bad state), & other states not important for system degration.   |
| Harware Supply                 | Indication whether a specific HW supply line should be on or off. Can be ENABLED (good state) or DISABLED (bad state).                                                                 |

## CHR COMPONENTS & MODULES <a name="chr_components_&_modules"></a>
| Component         | Module                                    |
| ----------------- | ----------------------------------------- |
| DevData           | [DevData](DevData.md)                     |
|                   | [FaultStackHil](FaultStackHil.md)         |
|                   | [Fmm](Fmm.md)                             |
| DiagData          | [DiagInterface](DiagInterface.md)         |
|                   | [FaultLogging](FaultLogging.md)           |
| DiagData_Cfg      | [DiagInterface_Cfg](DiagInterface_Cfg.md) |
|                   | [FaultLogging_Cfg](FaultLogging_Cfg.md)   |
| DriverAlert       | [DriverAlert](DriverAlert.md)             |
| FaultDatabase     | [FaultDatabase](FaultDatabase.md)         |
| InhibitManager    | [InhibitManager](InhibitManager.md)       |
| MonitorStatus     | [MonitorStatus](MonitorStatus.md)         |
| FmMain            | [FaultManagement](FaultManagement.md)     |
| InputValidation   | [InputValidation](InputValidation.md)     |
| SystemDegradation | [SupplyManagement](SupplyManagement.md)   |
|                   | [SystemReaction](SystemReaction.md)       |

## GM COMPONENTS & MODULES <a name="gm_components_&_modules"></a>
| Component             | Module                                                     |
| --------------------- | ---------------------------------------------------------- |
| DriverAlert           | [DriverAlert](Global_B/DriverAlert.md)                     |
| FmDevDataFmm          | [Fmm](Global_B/Fmm.md)                                     |
| FmDiagData            | [FmDiagData](Global_B/FmDiagData.md)                       |
|                       | [FmHistoryBuffer](Global_B/FmHistoryBuffer.md)             |
|                       | [FmFaultLogging](Global_B/FmFaultLogging.md)               |
| FmFaultStorageControl | [FmFaultStorageControl](Global_B/FmFaultStorageControl.md) |
| FmAutosarAdapter      | [FmAutosarAdapter](Global_B/FmAutosarAdapter.md)           |
| FmSystemDegradation   | [FmSystemDegradation](Global_B/FmSystemDegradation.md)     |
| FmSync                | [FmSync](Global_B/FmSync.md)                               |

[GM Code](Gm_Code.md)


## OTHER <a name="other"></a>

### SUPRESS FAULTS (PUT SOMEWHERE ELSE)
Act as internal faults with no action. They can still be latched, but don't report to the customer or driver.
