# DRIVERALERT
*DriverAlert.doc*  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpse)
2. [DETAILED DESIGN](#detailed_design)
</details>

## PURPOSE <a name="purpsoe"></a>
* Determines if warning lamps should be illuminated, text be shown, & chime to be heard.  
* Lamps may be illuminated due to latched failures or gov. regulations.

## DETAILED DESIGN <a name="detailed_design"></a>
* The ABS, TC, YSC subsystems provide the base brake warning (red lamp), ABS warning lamp (yellow lamp), & the TC/YSC Disabled lamp.  
* Lamp control is subject to gov. regulations, thus customer input is limited.  

If lamp is illuminated, then a failure has occurred.
| lamp       | active when PFV is active      |
| ---------- | ------------------------------ | 
| Base Brake | May be active                  | 
| ABS        | Must be requested to be active |
| TC/YSC     | Must be requested to be active |

* ***text output:*** Indicates actions might be required to emphasize the lamp handling.  
* ***acoustic warning:*** Acoustivally warn driver & informk him of different system CMs.  
