# INPUT VALIDATION
*Fm-DDD-InputValidation.doc*

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpse)
2. [INPUT VALIDATION TYPES](#input_validation_types)
</details>

## PURPOSE <a name="purpose"></a>
* **purpose:** Centralizes validation under one module since there are multiple ways to validate whether an input to a system is valid.  
Fault Database Table which is stored in the calibration section provides # of inputs & their value (TRUE/FALSE).
* ***code generated files:***   
    * *InputValidation_Cfg.h:* Defines id macros for the inputs, 0 - N.
    * *InputValidation_Cfg_Sub.h:* Defines mask macros that convert id macros into masks to make a byte.
    * *FaultDatabase_Cfg.c:* Stores mask macro or zero for each input.
* ***why store inputs bits, bytes:*** Less storage. Now can store one U8 variable instead of N input U8 variables. One U8 varaible can hold max. of 32 bytes (currently only using 6ish bytes).

## Input Validation Type <a name="input_validation_types"></a>
| Is input disabled? | Is input linked to latched fault? | Is input linked to prior faulted fault? | Is input Indererminate? | `IsInputValid`  | `GetInputValidationState` |
| ------------------ | --------------------------------- | --------------------------------------- | ----------------------- | --------------- | ------------------------- |
| Yes                | Ignore                            | Ignore                                  | Ignore                  | input invalid   | input invalid             |
| No                 | Yes                               | Ignore                                  | Ignore                  | input invalid   | input invalid             |
| No                 | No                                | Yes                                     | Ignore                  | input invalid   | input prior faulted       |
| No                 | No                                | No                                      | Yes                     | input valid     | input indeterminate       |
| No                 | No                                | No                                      | No                      | input valid     | input valid               |

Based on `GetInputValidationState`.
| state         | description - input linked to fault is:                                                                                              |
| ------------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| invalid       | disable or latched                                                                                                                   |
| prior fault   | enabled & prior faulted (not latched)                                                                                                |
| indeterminate | enabled, indeterminate fault (currently detected) (not latched or not prior faulted)                                                 |
| valid         | enabled, none of above flags are set (not disabled, not latched, no prior faulted, & not detecting)<br>Fault not active w/ any flags |

## CODE EXPLAINED
```c
FAULT_DATABASE_TYPE_TAG
{
    ...;
    U8 Input_Validation[NUMBER_OF_INPUT_VALID_BYTES];
    ...;
} FAULT_DATABASE_TYPE;

const FAULT_DATABASE_TYPE Fault_Database[INTERNAL_FAULT_ID_MAX] = 
{ 
    ...,
    /*----------------------------------------------------------------------------------------------*/
    /* Input Validation configuration for WSS_OPEN_FRONT_LEFT */
    /*----------------------------------------------------------------------------------------------*/
    {
    /* INPUT_VALID_WSS_FRONT_LEFT                   (TRUE/FALSE) */ 0                                 |
    /* INPUT_VALID_WSS_FRONT_RIGHT                  (TRUE/FALSE) */ IV_WSS_FRONT_RIGHT_MASK           |
    /* INPUT_VALID_WSS_REAR_LEFT                    (TRUE/FALSE) */ IV_WSS_REAR_LEFT_MASK             |
    /* INPUT_VALID_WSS_REAR_RIGHT                   (TRUE/FALSE) */ IV_WSS_REAR_RIGHT_MASK            |
    /* INPUT_VALID_WSS_DIR_FRONT_LEFT               (TRUE/FALSE) */ 0                                 |
    /* INPUT_VALID_WSS_DIR_FRONT_RIGHT              (TRUE/FALSE) */ IV_WSS_DIR_FRONT_RIGHT_MASK       |
    /* INPUT_VALID_WSS_DIR_REAR_LEFT                (TRUE/FALSE) */ IV_WSS_DIR_REAR_LEFT_MASK         |
    /* INPUT_VALID_WSS_DIR_REAR_RIGHT               (TRUE/FALSE) */ IV_WSS_DIR_REAR_RIGHT_MASK        ,
    ...,
    }
};

-----

LOCAL IV_TYPE In_Val;

typedef struct IV_TYPE_TAG
{
    U8 Input_Validation[NUMBER_OF_INPUT_VALIDATIONS][NUMBER_OF_INPUT_VALID_COPIES];
    U8 Prior_Fault_Validation_Orred[NUMBER_OF_INPUT_VALID_BYTES];
    U8 Latched_Validation_Orred[NUMBER_OF_INPUT_VALID_BYTES];
    U8 Detecting_Validation_Orred[NUMBER_OF_INPUT_VALID_BYTES];
} IV_TYPE;

/* 
   Gets called from FmInternal when a fault is detecting.
   Calls function UpdateInputValidFromMask w/ INPUT_VALID_INDETERMINATE_MASK.
*/
void UpdateInputValDetectedFault(const FAULT_ID_TYPE fault_id);

/* 
    Gets called from FmInternal when a fault is latched.
    Calls function UpdateInputValidFromMask w/ LATCHED_FAULT_INVALID_MASK.
*/
void UpdateInputValLatchedFault(const FAULT_ID_TYPE fault_id);  
```
## QUESTIONS
<details>
<summary><b>Q1: </b> Each fault has different values for Input_Validations? Does this mean we need an array of 900 elements to store validity of each fault: INPUT_VALID, INPUT_INVALID, INPUT_PRIOR_FAULT_ONLY, INPUT_INDETERMINATE?</summary>

**A:** Valid question since `FAULT_STATUS_TABLE_TYPE Fault_Status_Table[INTERNAL_FAULT_ID_MAX];` holds an entry for each fault.
The only time the Fault Database Table input validation values (stored in calibration section) are compared is in funtion *UpdateInputValidationMaskForFault*. 
Since the validity of the faults are not stored in buffers, the states are determined by the state of the faults.
For example, if the fault is Detecting, then *In_Val.Input_Validation* will set macro *INPUT_VALID_INDETERMINATE_MASK*.

* *UpdateInputValidationMaskForFault* is called by:  
    * UpdateInputValDetectedFault  
    * UpdateInputValidDetectingMasks  
    * UpdateInputValidLatchedMasks  
    * UpdateInputValidPriorFaultMasks
    * UpdateInputValLatchedFault
</details>
