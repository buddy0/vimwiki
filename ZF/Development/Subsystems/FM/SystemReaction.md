# SYSTEMREACTION
*Fm-DDD-SystemReaction.doc*

<details>
<summary><b>ACRONYMS</b></summary>

| acronym | meaning                    |
| ------- | -------------------------- |
| CM      | Control Mode               |
| PSSD    | Partial System Shutdown    |
| DRP     | Dynamic Rear Proportioning |

</details>

* **purpose:** Provides methods to determine the CM & PSSD requests based on latched failures and the fault database.    
Both are predefined by the Performance System on each fault in the database.  
* **CM:** FM request to a specific performance subsystem that will limit operational mode of that subsystem.  
Limits functionality of the performance system. May be to perform or isolate DRP control.  
Requests a mode of operation of the Performance System.  
* **PSSD:** FM request to a specific performance subsystem that  will limit operational mode of that subsystem if fault occurs during activation.  
Returns system to normal breaking performance in case of failure. May be to dump all pressure from rear channels or apply full pressure to front channels.  
Requests a recovery mode that should be used when a failure occurs during control.  
* ***preparing System Reaction:*** Info. on fault reactions & faults present in the system are needed.  
Fault database provides reactions & fault status table provides info. on each failure.  
* ***requests are generated:*** When `UpdateSystemReaction` is called which is generally when: 
    * Fault just latched or when latched conditional fault gone from faulted condition to test passed condition. 
    * FM is initialized, `EnabledNormalLampControl` & `EnableNormalInhibitControl` are called.
    * If FM `Force_Update` flag is TRUE then function `InitSystemReaction` is called and CM & PSSDs reset NORMAL state.  
    Faults are then rescanned & system reactions are updated to reflect current state of faults.
