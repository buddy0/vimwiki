# FM TRAINING

## OVERVIEW
**purpose:** Provide diagnostic on sensor inputs.  
* Sensor normally has 2 ports/inputs. Comparisons are made with these readings, & if perhaps they are off by 2%, 5 consecutive times then a successful non fault occurs.  
* Example below, inteligent pressure sensor performs comparison at sensor level instead of uC level allowing: to distantiate what is wrong, either sensor or uC & another level of diagnostic.    
* Sensor could then send a spi message w/ first 8 bits being pressure & next 8 bits being the diagnostic.  
![Picture of Pressure Sensor](Images/Pressure_Sensor.png)
* Every fault has a Flag (16-bits) &  Status_Counter
```c
/* MonitorStatus_Sub.h */
typedef struct FAULT_STATUS_TABLE_TYPE_TAG
{
	FAULT_STATUS_FLAG Flag;
	U16 Status_Counter;
} FAULT_STATUS_TABLE_TYPE;

/* MonitorStatus.c */
FAULT_STATUS_TABLE_TYPE Fault_Status_Table[INTERNAL_FAULT_ID_MAX];
```
* `void UpdateMonitorStatus (const FAULT_ID_TYPE fault_id, const BOOLEAN monitor_passed, const U16 weight, const U16 goal)`: counter resets if *FALSE* to *TURE*.  

## DMTCONFIGURATION 
* location: `MMC_APPL\Layers\Services\DmtConfiguration`
* Placed in ROM
* Automatically generates code into *Shared\Fm\FaultDatabase_Cfg\FaultDatabase_Cfg_Shr.h & FaultDatabase_Cfg_Shr_Sub.h*. 
* IGNITION_LATCHED: Fault occurs during the entire time car is on. If fault goes away, there still exists a fault.


## FAULT ALARM DECODE
* ***2xx:*** **1.** Fault happens. **2.** uC is reset. **3.** Second cyle is updated, thus the data is bad.  
* ***3xx:*** Fault data is copied form NVRAM into the fault record so data is good

## FAULT FROM UPDATE STATUS 
1. Put info in logger
2. NVRAM expanded fault record bytes

