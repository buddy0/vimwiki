# SUPPLYMANAGEMENT
*Fm-DDD-SupplyManagement.doc*

**purpose:** Manages the supplies used to power various system components.  
Based on failure conditions, supplies may need to be turned off or even on if failure conditions have been removed.  
Usually though if no faults are present, supplies are initialized & turned on by other subsystems.
