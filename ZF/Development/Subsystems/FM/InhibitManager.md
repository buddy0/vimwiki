# INHIBITMANAGER
*Fm-DDD-InhibitManager.doc*  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpse)
</details>

## PURPOSE <a name="purpose"></a>
* Determines if performance brake control is enabled/disabled based on AC subystem reactions & state of slip control power switch.  
* Determines if performance drive control is enabled/disabled on subsystem reactions of TC, YSC, & EDTR.  
* Thus, if Inhibit Manager is enbaled, then it makes decisions about whether brake & drive control arbitrators can pass performance request to the actuator. 
