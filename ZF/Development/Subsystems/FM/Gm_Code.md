# GM CODE (31XX)

***fault macros:*** `StateManagemen\DmtConfiguration\MonitorConfiguration\Cfg/SourceCal\MonitorConfigurationGen_Cfg.h`  
***entire fault database:*** `StateManagement\DmtConfiguration\MonitorConfiguration\Cfg\SourceCal\MonitorConfigurationGen_Cfg.c`  
***global fault core macros:*** `StateManagement\DmtConfiguration\MonitorConfiguration\Cfg\SourceCal\MonitorConfigurationGen_Cfg_Sub.h`  
***GetMonitorConfig macros/functions:*** `StateManagement\DmtConfiguration\DmtConfigurationMain\Cfg\Source\DmtConfigurationMain_Cfg.h`  
***declared calibration pointers & function delcarations of GetMonitorConfig:*** `StateManagement\DmtConfiguration\DmtConfigurationMain\Cfg\Source\DmtConfigurationMain_Cfg.c`  
***PSSD & CM macros:*** `StateManagement\Fm\FmSystemDegradation\Cfg\Source\FmSystemDegradation_Cfg.h`  
***SR: PSSD & CM, Input Validation, Supply Management, Lamp Config database:*** `StateManagement\DmtConfiguration\DegradationConfiguration\Cfg\SourceCal\DegradationConfigurationGen_Cfg.c`  
***BITSHIFT CM & PSSD macros, DmtConvertSystemReactionToCm & Pssd:*** `StateManagement\DmtConfiguration\DmtConfigurationMain\Main\Source\DmtConfigurationMain.h`

***set, reset, get, copy flags:*** `MMC_INTEGRATION\Common\Sc\ScMain\Source\Macros.c`  
***git word_pos, bit_pos, bit_from_bitfield:*** `StateManagement\Fm\FmMain\Main\Source\FmMain_Sub.h`  

## FAULTS STORED
DMT stores confiugrations of faults in:  
1. Struct array named *DmtMonitorConfig*. Each element (fault) contains struct members:
    * FaultStorageGroup
    * FailedDetectableIndex
    * DemEvent
    * NextFault
    * Zf_Fault_Id
    * FDL Enable Count
    * Ecu_Id
    * Comms_Fault_Type
    * OBD_CLASS
    * Storage_Type
    * Failure Record Type

2. Struct variable named *DmtMonitorConfigFlags*. Each member is an array w/ size of max. # of fault status words.
    * Enabled
    * Latching
    * PriorFaultValidation
    * Indeterminate
    * PciRelevant
    * BrakeArbitrator
    * DriveArbitrator
    * ActiveCouplingArbitrator
    * Excessive_Voltage_Enable
    * MecNonZeroDisable;
    * DiagOutputControl
    * PbDeviceControlDisable
    * ElectricDriveArbitrator
    * ElectricDriveSafeOff
    * CommsFault

## OBTAINING DATA FROM DATABASE
Since fault database stored in calibration section, pointers declared in application code points to calibration code.

```
1. Call function GetMonitorConfigPriorFaultValidation(<fault_name>) uses pointer parameter DmtMonitorConfigFlagsCalPtr.
2. Assignment for DmtMonitorConfigFlagsCalPtr occurs in function InitDmtConfigurationCalibrationPointers and   
calls function GetCalibrationSet which returns an address from function pointer variable call GetCalibrationSetPtr.

/* SoftwareExecution\Os\CalInterface\SOURCE\CalInterface.c */
void* GetCalibrationSet(const U32 subsystem, const U32 requested_group)
{
   return ((*GetCalibrationSetPtr)(subsystem, requested_group));
}

Assignment of Function pointer variable GetCalibrationSetPtr is &GetCalibrationSet.
3. Function GetCalibrationSet returns address of struct array at element Calibration_Database[subsystem].Get_Cal_Service(requested_group).

/* VehicleControl\Lc\SupervisedRemoteParkAssist\Model\CalHandlerTLSil.c */
void *GetCalibrationSet(const U32 subsystem, const U32 requested_group)
{
    void* ptr = NULL;

    if (NUM_CALIBRATED_SUBSYSTEMS != 0)
    {
        /*
            Call the "get" service associated with the requested subsystem
        */
        ptr = Calibration_Database[subsystem].Get_Cal_Service(requested_group);
    }

    return(ptr);
}
```

## FAULT STATUS TABLE
The fault status flags are stored in a struct array named *Fault_Status_Table* w/ size of max. # of fault status words.  
Each element has members:
* Latched
* Detecting
* GoalAchieved
* PriorFault
* FaultExpected
* Modified

### OBTAINING BOOLEAN DATA FROM ARRAY
Call function *GitBitFromBitfield* w/ arguements bitfield & item_id. Ex. GitBitFromBitfield(Fault_Status_Table[word_1].enabled, fault_id);


* Struct array Fault_Status_Table is the official holding place of the fault status flags. If flags need to be set or cleared, it should happen here.  
Can be achieved by creating a pointer poiniting to Fault_Status_Table.
* Variable GetStatusFlagFromArray used for reading purposes. Provide the flags in one variable for a specific fault id.

```
Fault_Status_Table[max # of words] =
{
    {   /* WORD 0 */
        {   /* LATCHED */
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    f_id 31 |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  | f_id 0
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        },
        {   /* DETECTING */
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    f_id 31 |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  | f_id 0
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        },
        {  /* GOAL ACHIEVED */
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    f_id 31 |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  | f_id 0
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        }
        {  /* PRIOR FAULT */
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    f_id 31 |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  | f_id 0
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        }
        {  /* FAULT EXPECTED */
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    f_id 31 |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  | f_id 0
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        }
        {  /* MODIFIED */
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
    f_id 31 |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  |  | f_id 0
            +--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+--+
        }
    },
    ...
}

U8 GetStatusFlagFromArray =
{
    /* 
       b0: NO FLAG
       b1: LATCHED
       b2: DETECTING
       b3: GOAL_ACHIEVED
       b4: PRIOR_FAULT
       b5: FAULT_EXPECTED
       b6: MODIFIED
       b7:
    */
    +--+--+--+--+--+--+--+--+
    |  |  |  |  |  |  |  |  |
    +--+--+--+--+--+--+--+--+
}
```

## SYSTEM DEGRADATION
***latched:*** Peform LoadSystemDegration: system reaction, epb, input val, supply management, flags.  
***detecting:*** Pefrom input val.  

```
UpdateMonitorStatus
    UpdateMonitorStatusLatched
        ApplyLocalFmSystemDegradation
            LoadSystemDegradation                 /* Obtaining degradation for specified fault into */
                LoadSystemDegradationScs          /* Obtaining system reaction degradation (CM & PSSD) for specified fault, inserted into structure additionalSystemDegradation */
                    GetDegradationConfigRawScs
                    LoadSystemDegradationScsFlags /* Only if fault is latched & expected */
                LoadSystemDegradationEpb
                    GetDegradationConfigRawEbp
                LoadSystemDegradationInput
                    GetDegradationConfigRawInput
                    LoadSystemDegradationInputPermanent
                LoadSystemDegradationSupply
                    GetDegradationRawSupply      
                LoadSystemDegradationFlags
                    GetMonitorConfigRawFlags

/* In file FmSystemDegradation.c, function Apply ApplyLocalFmSystemDegradation. */
local struct var additionalSystemDegration
    * struct var SystemDegradationScs 
        * array ControlModeUnexpected
        * array ControlModePermanent
        * array ControlMode
        * array PartialShutdown
    * struct var SystemDegradationEpb
    * struct var SystemDegradationInput
    * struct var SystemDegradationSupply
    * struct var SystemDegradationFlags
```


### SYSTEM REACTION STORED IN DATABASE
* Struct array named *DmtDegradationConfigScs* w/ max number of faults. Each element(fault) has 1 member, U8 array named SystemReaction w/ size max nubmer of SRs.
***each element w/in SystemReaction:*** 1st 4 bits represets CM, while last 4 bits represent PSSD.

### OBTAINING SYSTEM REACTION DATA FROM DMT
Done by function calls LoadSystemDegradationScs & LoadSystemDegradationScsFlags.
* ***LoadSystemDegradationScs:***
1. Created pointer pointing to DMT calibration code for Scs system reaction at a specific fault. Assigned value is function call GetDegradationConfigRawScs(faultId).  
2. DMT data manipulated by converting CM & PSSD in separate local arrays.
3. Values loaded onto local struct var additionalSystemDegration->SystemDegration->Control Mode/PartialShutdown.
4. Call function LoadSystemDegradationScsFlags to modify additionalSystemDegration->SystemDegration->ControlModeUnexpected/ControlModePermanent



