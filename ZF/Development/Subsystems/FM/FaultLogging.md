# FAULTLOGGING
*Fm-DDD-FaultLogging.doc*  

***purpose:*** Handles collection of data (FreezeFrames) to faults & events. Initializes Fault Status Table during system initialization based on faults stored in NVRAM.  
***note:*** All code defined in Cfg component b/c no internal use interface specified yet.  
