# FAULTLOGGING_CFG
*Fm-DDD-FaultLogging.doc* 

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpose)
2. [QUESTIONS](#questions)
</details>

<details>
<summary><b>ACRONYMS</b></summary>

| acronym | word                              |
| ------- | --------------------------------- |
| DTC     | Diagnostic Trouble Code           |
| NVRAM   | Non-Volatile Random Access Memory |
</details>

## PURPOSE 
* Simply log persisent faults into storage - NVRAM by fault blocks: base or extended.  
* Initializes Fault Status Table during system initialization based on faults stored in NVRAM.  
If ignition turned back on & previous faults were latched, they are latching them through `UpdateFaultStatusFlag` which updates Fault Status Table,  
*not* through `UpdateMonitorStatus` which would be latching the already latched faults! 

Fault Database includes several attributes pertaining to fault logging:   
* if fault ID should be stored in NVRAM if failure occurs  
* if fault ID subject to aging  
* if fault response warrants currently latching a fault based on faults stored in NVRAM  
* if current prior fault validatoin status should be set based on faults stored in NVRAM

<details>
<summary><b>STORAGE TYPE</b></summary>

**storage type:** How fault stored in NVM.
* ***Not Persisent:*** Fault *not* stored in NVM.
    * <details>
      <summary><b>Restrictions</b></summary>

      | type                               | description                                                                                          |
      | ---------------------------------- | ---------------------------------------------------------------------------------------------------- |
      | Not Persistent storage type        | Permanent<br>hard-latched<br>configuration response types<br>faults requiring Prior Fault Validation |
      | Not require Prior Fault Validation | Permanent<br>hard-latched<br>configuration response types<br>                                        |
      | No Age                             | Non-persisent faults                                                                                 |
      </details>
* ***Persisent:*** Fault *is* stored in NVM as "Fault Block" either as *extended fault blocks* or *base fault blocks*.
    * <details>
      <summary><b>Example of Base Block Configuration</b></summary>

      ***configurable:*** number of bytes  
      ***not configurable:*** fault identifier, fault aging counter, & flags  

      | byte identifier           | number of bytes       | description                                                                                                             |
      | ------------------------- | --------------------- | ----------------------------------------------------------------------------------------------------------------------- |
      | FAULT_BLOCK_FAULT_ID      | sizeof(FAULT_ID_TYPE) | Internal fault identifier                                                                                               |
      | FAULT_BLOCK_AGE_COUNTER   | 1                     | Fault Aging Counter (ignition count)                                                                                    |
      | FAULT_BLOCK_GENERAL_FLAGS | 1                     | Flags<br>Bit 0 - Prior Fault Validation<br>Bit 1 – History DTC flag<br>Bit 2 – Test Passed Since DTC Cleared<br>Bit 3 – Low System Voltage<br>Bit 4 – High System Voltage<br>Bit 5 – Excessive High System Voltage<br>                                                                                      |
      </details>
</details>

<details>
<summary><b>AGING</b></summary>

* **aging:** Specifies aged or not if fault stored in NVM. All aging counters incremented regardless of status aging flag in database.  
If fault configured to age & age counter meets customer defined min. age, then "DTC_HISTORY_MASK" bit is cleared in NVM & fault no longer reported to diagnostics as "history fault". History flag cleared from Fault Status Table.
* ***restriction:***  
    * permanent, hard-latched, & configuration response type faults are recommended to not allow aging
    * non-persistent storage type faults cannot require aging.
</details>

<details>
<summary><b>RESPONSE</b></summary>

**response:** Determines response of fault.  
Current Fault Status Table is cleared & then initialized at system initialization (each new ignition cyle).    
If fault logged in NVRAM & fault response type is Permanent, Hard, Configuration latched, then Latched flag is set in Fault Status Table (exception if DEBUG or DEVEL compiler switches are used).  
</details>

<details>
<summary><b>PIOR FAULT VALIDATION</b></summary>

**prior fault validaton:**  
During system initializatin, if fault stored in NVRAM has its PFV flag set, then PFV flag is set in the Faul Status Table (exception if DEBUG or DEVEL compiler switches are used).
</details>

<details>
<summary><b>TEST PASSED SINCE DTC CLEARED</b></summary>

**test passed since DTC cleared:**    
During system initialization, if fault stored in NVRAM has its "Test Passed Since DTC Cleared Flag" set, then flag is set in Fault Status Table.
</details>

<details>
<summary><b>DTC HISTORY MASK</b></summary>

**dtc history mask:** If fault stored in NVRAM has been cleared or aged, "DTC_HISTORY_MASK" will be cleared in NVRAM, otherwise it will be set.
</details>

<details>
<summary><b>CONFIGURATION</b></summary>

**configuration:** Persistent data (aka environmental or ambient) has been configured. Applicaton engineer can tailor this data. 
</details>

# QUESTIONS <a name="questions"></a>
<details>
<summary><b>Q1:</b> Difference between FaultLogging_Cfg & FaultStackHil?</summary>

**A:** FaultStackHil is only used when the DEVEL compile switch is turned on. Used for in house debugging.  
FaultLogging_Cfg is release code to be used by the customer. 
</details>

