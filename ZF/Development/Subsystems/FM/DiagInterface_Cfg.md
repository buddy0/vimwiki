# DIAGINTERFACE_CFG
*Fm-DDD-DiagInterface_Cfg.doc*

<details>
<summary><b>TABLE OF CONTENTES</b></summary>

1. [ACRONYMS](#acronyms)
2. [PURPOSE](#purpose)
</details>

## ACRONYMS
<details>
<summary><b>TABLE</b></summary>

| acronym                  | word |
| ------------------------ | ---- |
| Diagnostic Event Manager | DEM  |
</details>

## PURPOSE
* Provide mechanism by which faults can be enabled/disabled or inhibited and cleared.  
* ***in autosar environment:*** DEM handles all customer specific fault information. FM only handles internal fault data to synchronize with DEM data.  
Autosar interfaces implemented by FM for DEM to use for data synchronization.

