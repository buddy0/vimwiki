# DEVDATA
*FM-DDD-DevData.doc*  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpose)
2. [APPLICATION](#application)
</details>

## PURPOSE <a name="purpose"></a>
Provides funcionalities that are used during applicaton development.

## APPLICATION <a name="application"></a>
* **FaultStackHil:**
    * Only available in DEVEL builds.
    * Fault stack buffer updated when fault transitions to passed or latched.  
    Empty slots filled w/ FAULT_STACK_HIL_EMPTY_SLOT.  
    Faults that are PFV are not added to the buffer.

* **Fault maturity monitor (Fmm):**
    * Only available in DEVEL builds.
    * Feature that provides how close detecting faults are to be latched & how many faults are being detected totally in the system w/in current ignition cycle.
    * Uses a threshold to decide if detecting fault should be entering monitoring state & if so stores them in a buffer.  
    Threshold is the % of S8_MAX (127).  
    Each fault has a counter to detect how close to being latched.  
    Once counter reaches threshold, fault stores in FMM buffer w/ counter.
    * When fault in buffer is latched, leaves the Fmm buffer & enters FaultStackHil buffer.
    * Threshold is user configurable parameter:  
    0 means detecting faults enter monitoring state,  
    S8_MAX means only latched faults enter monitoring state (techinally disables Fmm feature).

* **FM test interface:**
    * Only available in DEVEL builds.

* **FaultInsertion:**
    * Only available in SIMULATION_CIAT_HIL switch.
