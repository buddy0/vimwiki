# FAULTMANAGEMENT
*Fm-DDD-FaultManagement.doc*

<details>
<summary>TABLE OF CONTENTS</summary>

1. [FAULT DATABASE TABLE](#fault_database_table)  
    a. [INDEX](#index)  
    b. [ENABLE](#enable)  
    c. [LOW IGNITION VOLTAGE ENABLE](#low_ignition_voltage_enable)  
    d. [RESPONSE](#response)  
    e. [PRIOR FAULT VALIDATION](#prior_fault_validation)  
    f. [STORAGE CONTROL](#storage_control)  
    g. [SUPPLY MANAGEMENT](#supply_management)  
    h. [INPUT VALIDATION](#input_validation)  
    j. [OUTPUT VALIDATION](#output_validation)  
    k. [FAULT CODES](#fault_codes)  
l. [SYSTEM REACTION](#system_reaction)  
2. [FAULT STATUS TABLE](#fault_status_table)  
    a. [FLAGS](#flags)  
        &emsp;i. [LATCHED FLAG](#latched_flag)  
        &emsp;ii. [DETECTING FLAG](#detecting_flag)  
        &emsp;iii. [TEST PASSED FLAG](#test_passed_flag)  
        &emsp;iv. [INHIBIT FLAG](#inhibit_flag)  
        &emsp;v. [PRIOR FAULT VALIDATION FLAG](#prior_fault_validation_flag)  
        &emsp;vi. [HISTORY FLAG](#history_flag)  
        &emsp;vii. [LATCHED SINCE POWER-UP FLAG](#latched_since_power-up_flag)  
        &emsp;viii. [GOAL ACHIEVED FLAG](#goal_achieved_flag)  
        &emsp;ix. [SENT FLAG](#sent_flag)  
        &emsp;x. [TEST PASSED SINCE DTC CLEARED FLAG](#test_passed_since_dtc_cleared_flag)  
    b. [STATUS_COUNTER](#status_counter)  
3. [FAULT HISTORY TABLE](#fault_history_table)  
4. [OPERATIONAL OVERVIEW](#operational_overview)  
    a. [UPDATEFMINTERAL](#update_fm_internal)  

</details>

**purpose:** Keep track of all fault information in system. Does not perform any failsafe monitoring, only records & acts upon results the monitors pass to it.  

## FAULT DATABASE TABLE <a name="fault_database_table"></a>
**Fault Database:** Heart of architecture for the FM subsystem. Contains one entry for each fault (enabled or disabled).  
* ***stored:*** Access database called "CoreFaultDatabase.mdb". Fault database stored in ROM.  
Currenlty stored in calibration section. Thus, all access done via `Fault_Database_Ptr`, which is initialized using calibration handler.
* ***configured:*** "Fault Database Generation Tool.mbd"
* In Integrity seems all of the members in FaultDatabase_Cfg/Shr/Docs/ generate code into FaultDatbase_Cfg/Calibration/Source/FaultDatabase_Cfg.c

### INDEX <a name="index"></a>
Internal fault code identifier. Used in fault database & fault status table. Only used internally, debugging purposes.  
Represented in database spreadsheets, but not stored in code in ROM.  
Codebase: found in `FaultDatabase_Cfg/FaultDatabase_Cfg_Shr.h`

### ENABLE <a name="enable"></a>
Flag determines if fault is enabled or disabled.  
Fault database stored in flash (ROM).  

### LOW IGNITION VOLTAGE ENABLE <a name="low_ignition_voltage_enable"></a>
Flag determines if fault enabled or inhibited when there is an assumed low ignition voltage system condition.

### RESPONSE <a name="response"></a>
Flag determines longevity of a latched fault. Controls how the fault will be dealt w/ once it is found. Holds info. if fault is a permanent, hard, ignition, condition, or configuration latched.  
| term                  | definition                                                                                                                                                                                      |
| --------------------- | ----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Permanent latched     | Fault permanently latched at time of occurence. Only way to restore is cause of fault to be corrected & system reset using plant or development tool. Dealer service tool cannot remove this fault. If flag is set, consequences of the fault (e.g. lamps turn on, system reaction) remains whenever ignition turned on even if fault goes away. Generally reserved for catastrophic faults i.e. ROM or RAM failure when requested by customer (kind of like ROM).                                                                                                                                                                            |
| Hard latched          | Similar to permanetly latched, but only way to restore is cause of fault to be corrected & system reset using plant, development, or dealer service tool (kind of like ROM).                    |
| Ignition latched      | Fault latched at time of occurence to end of ignition cycle. No longer latched at the start of the ingition cycle. If flag is set, consequences of the fault (e.g. lamps turn on, system reaction) remain (even if cause of fault goes away) until iginition is turned **off**. When iginition is turned on again, system will not be disabled unless subsequent fault is detected (kind of like ROM).             |
| Condition latched     | Fault latched at time of occurence to when it is no longer active, happening. If flag is set, consequences of the fault remain only as **long as fault condition persists**. Normal system operation resumes auto (case of no other faults) as soon as fault condition no longer present (kind of like ROM).                                                                                                         |
| Configuration latched | Similary to Condition Latched Fault Response, but also the fault code cleared out of NVRAM. The LATCHED, HISTORY, & PRIOR_FAULT_VALIDATION flags cleared out of fault status table in RAM as soon as fault condition is no longer present. This fault cannot be cleared with the diagnostic tool (kind of like ROM).                                                                                                   |

### PRIOR FAULT VALIDATION <a name="prior_fault_validation"></a>
Determines if fault must comply w/ any gov. regulations: ECE 13, FMVSS 105, or FMVSS 135.  
Determines if system must return to the state that it was at end of ignition cycle that fault occurred in.  
?Could be useful to check if Ingition latched fault occur?  

### STORAGE CONTROL <a name="storage_control"></a>
Determines if fault stored in NVM. Yes, then  
***type:*** Fault block must be specified. Not Persistent (stored for a short time) & Persistent (stored for a long time).  
***aging:*** Permanent latched, hard-latched, & configuration response type faults are recommended to not allow aging.

### SUPPLY MANAGEMENT <a name="supply_management"></a>
Determines which supplies should be enabled or disabled in response to fault conditions.  
E.g. if fault WSS_FRONT_LEFT_OPEN is latched, then Supply Management WSS_FRONT_LEFT is not supplied since it was preconfigured to FALSE.  
| supplie name                          | description                                    | 
| ------------------------------------- | ---------------------------------------------- | 
| Front Left Wheel Speed Sensor Supply  | For applications using active sensors          |
| Front Right Wheel Speed Sensor Supply | For applications using active sensors          |
| Rear Left Wheel Speed Sensor Supply   | For 4 sensor applications using active sensors |
| Rear Right Wheel Speed Sensor Supply  | For 4 sensor applications using active sensors |
| Rear Wheel Speed Sensor Supply        | For 3 sensor applications using active sensors |
| 5V Sensor Supply                      | For applications having a 5V sensor supply     |
| Power Management System               | For all applications                           |
| Pressure Boost Supply                 | For applications using pressure boost supplies | 

### INPUT VALIDATION <a name="input_validation"></a>
Determines which inputs to system are properly functioning & which should not be used for decision-making.  
E.g. front right wheel speed sensor is checked for opens, shorts, erratic behavior, & against other sensors for missing or unmatched speeds. Each of these faults would have the input validation flag for the WSS_FRONT_RIGHT disabled in their database entry. If any of these faults were to occur, then fault manager would use info. in database to notify rest of the system that right fron wheel sensor should not be used in decision making.  
| input name for YSC system                 |
| ----------------------------------------- |
| Front Left Wheel Speed Sensor             |
| Front Right Wheel Speed Sensor            |
| Rear Left Wheel Speed Sensor              |
| Rear Right Wheel Speed Sensor             |
| Brake Pedal                               |
| Drivetrain                                |
| Delivered Torque                          |
| Engine Speed                              |
| Throttle Position                         |
| Gear Position                             |
| Engine Off Time                           |
| Ambient Temperature Sensor                |
| Lateral Accelerometer                     |
| Longitudinal Accelerometer                |
| Yaw Rate                                  |
| Steering Angle                            |
| Brake Pressure (Master Cylinder Pressure) |
| ECA Release Switch                        |

### OUTPUT VALIDATION <a name="output_validation"></a>
Determines which system outputs can be reliably controlled & which should not be commanded.

### FAULT CODES <a name="fault_codes"></a>
Contains entries for customer specific fault code.

### SYSTEM REACTION <a name="system_reaction"></a>
Determines what portions of system should be requested to shut down when fault is present. Only requested to system performance manager since all the systems are performance related.    
**Control Mode (CM):** Specifies operational mode that performance system should enter: *_INHIBIT, *_NORMAL, etc.  
Mode activated when fault occurs when system not active or after current system activation & PSSD is complete.  
Actions may be: DRP only or isolate only DRP.  
**Partial System Shutdown (PSSD):** Specifies action to be taken when fault occurs during system control.   
Actions may be: dump all pressure from wheel, allow full pressure on wheel, or no action.

## FAULT STATUS TABLE <a name="fault_status_table"></a>
Contains information about each fault in system.  
From the monitors & other external resources, there are interface functions provided to interface to these tables.

### FLAGS <a name="flags"></a>

```c
/* file location: MonitorStatus_Shr.h */
#define LATCHED                             	(U16)0x0001
#define DETECTING                           	(U16)0x0002
#define GOAL_ACHIEVED                       	(U16)0x0004
#define TEST_NOT_PASSED_SINCE_POWERUP       	(U16)0x0008 /* Useful for PFV */

#define LATCHED_SINCE_POWERUP           		(U16)0x0010 /* Useful for Repsonse Conditional Latched */ 
#define TEST_NOT_COMPLETED_SINCE_DTC_CLEARED	(U16)0x0020
#define PRIOR_FAULT_VALIDATION              	(U16)0x0040
#define GOAL_NOT_ACHIEVED_SINCE_POWERUP     	(U16)0x0080

#define ONSTAR_TRIGGER_SENT                 	(U16)0x0100
#define HISTORY                             	(U16)0x0200
#define COUNTED_FOR_REPORTING                 	(U16)0x0400
#define ENABLED_IN_CONFIGURATION            	(U16)0x0800

#define FAULT_SETS_OBD_MIL              		(U16)0x1000
#define TEST_FAILED_SINCE_DTC_CLEARED       	(U16)0x2000
#define DTC_COUNTED                         	(U16)0x4000
#define DISABLED                            	(U16)0x8000
```

#### LATCHED FLAG <a name="latched_flag"></a> 
Indicates if fault is currently latched.    
***is set when:*** fault been detected & status counter of fault reaches the *goal*.  

#### DETECTING FLAG <a name="detecting_flag"></a>
Indicates if fault is currently being detected.  
***is set when:*** Only set when conditions are correct for monitor to detect fault & fault is being detected.  
***flag:*** Not filtered or debounced, allowing quick state change.

#### TEST PASSED FLAG <a name="test_passed_flag"></a>
Indicates fault passed this ignition cycle or since last request to clear DTCs.  
***test passes:*** When conditions are correct to monitor the failure & no failure been detected for min. amount of time or activation.  

#### INHIBIT FLAG <a name="inhibit_flag"></a>
Indicates fault not allowed to be detected or latched.  
Thus, detecting & latched flags are cleared.  
***note:*** kind of like disabled, but can be changed to enabledish if wanted.  

#### PRIOR FAULT VALIDATION FLAG <a name="prior_fault_validation_flag"></a>
Indicates fault handled by prior fault validation logic.  
***set:*** Fault handled as if it were latched. System reaction is recreated until Test Not Passed flag is cleared, indicating fault is no longer present.  
***difference between Latched fault:*** Supply Management: Supplies will not be disabled by a fault that is only Prior_Fault, so passing of the monitor routine will be possible for faults that disable supplies. When the monitor routine fails, the fault will be Latched again and supplies will be disabled, if configured.  
***Force Update flag set:*** When any Prior Validation flag transitions from set to cleared. 

#### HISTORY FLAG <a name="history_flag"></a>
Indicates if fault is stored in NVM.  
***cleared when :*** fault must be overwritten in NVM.

#### LATCHED SINCE POWER-UP FLAG <a name="latched_since_power-up_flag"></a>
Indicates if fault latched this ignition cycle.  
***for non conditionally latched response:*** Follows latched flag.  
***for conditionally latched response:*** Flag not reset if conditions that caused fault are no longer present.  

#### GOAL ACHIEVED FLAG <a name="goal_achieved_flag"></a>
Indicates fault status counter reached its goal. Internal use only, not reported to customer.  
***goal achieved:*** Can be fault passing or fault failing its failsafe monitor.  

#### SENT FLAG <a name="sent_flag"></a>
Indicates that this fault has been sent over communications bus for a Request Codes request.  
***start of Request Codes request:*** This flag cleared for all faults.  
This flag insures that data only created once.  

#### TEST PASSED SINCE DTC CLEARED FLAG <a name="test_passed_since_dtc_cleared_flag"></a>
Indicates fault stored in NVRAM has passed its failsafe monitor at least once since last time DTCs were cleared in fault status table in NVRAM

### STATUS COUNTER <a name="status_counter"></a>
Tracks status of fault.  
***unit measured fault:*** time, occurences of failure, or other means.  
***tracks:*** Tracks both passing & failing of a failure.    

## FAULT HISTORY TABLE <a name="fault_history_table"></a>
User configurable based on customer needs & amount of NVM available. Each row contains info for one fault.  
***stored:*** RAM & NVM. During initialization, NVM manager reads fault history table from NVM. Any changes to this table will be made in RAM.  
***min data contained in fault row:*** fault code, prior fault validation flag, fault-aging counter.
*  ***internal fault code stored:*** In customer flags section. Provides quick method determine which fault was stored.
*  ***validate prior fault:*** Determine if the fault must comply with any government regulations.  
* ***fault aging counter:*** Number of ignition cycles until a fault can be removed from Fault History table if the fault is not continually being detected each ignition cycle.  

## OPERATIONAL OVERVIEW
***interfaces w/:*** Subsystems, communicates w/ NVM manager, fault monitors, performance systems, diagnostic communications.  
***basic operation:*** Collect data from monitors, determine system response to info. provided by monitors, & distribute this info. to the system.  
**scheduler:*** Task provided to the scheduler that operates periodically updating Fault Management system.  
Task updates: input validation info., performance system reaction, inhibit management, supply management, & lamp status.  
Scheduled tasks scan dirty bits & info. bits that force an update; if any are set then info. must be updated.  
Lamp status relies on performance system reaction & prior fault validation info. to update.

<details>
<summary><b>UpdateFmInternal</b></summary> <a name="update_fm_internal"></a>

**`RESCAN == FALSE`:** fault latched in system.  
**`RESCAN != FALSE`:** fault passed in system. Thus, reset entire system reaction and recalculate based on all latched and pfv fualts.  

</details>

## QUESTIONS <a name="questions"></a>
<details>
<summary><b>Q:</b> Why is Input Validation needed when we can just check if a fault is latched to determine if an input should be used?</summary>

**A:** Becuase some inputs don't care about a particular fault status.  
E.g. Input WSS_DIR_FRONT_LEFT doesn't care if fault WSS_MISSING_FRONT_LEFT is latched, but input WSS_FRONT_LEFT (meaning that it cannot be used in decision making process if the fault is latched).  
</details>

<details>
<summary><b>Q:</b> Different ways to see if a fault is latched, detected?</summary>

**A:**  
***LATCHED***  
* Use function IsFaultLatched
* FaultStackHil

***DETECTED***
* IsFaultDetected
* Fault Maturity Monitor
</details>

<details>
<summary><b>Q:</b> Ways to validate a fault?</summary>

**A:**  
* Fault Status Table
</details>

