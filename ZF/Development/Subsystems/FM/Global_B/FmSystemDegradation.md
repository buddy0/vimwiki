# FMSYSTEMDEGRADATION
*DD_CS_FmSystemDegradation*, *DD_TS_FmSystemDegradation*, *DD_TS_FmSystemDegradation_Cfg*, *DD_TS_FmSystemReaction*  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. OVERVIEW
2. DMT GEN FILE LOCATIONS
3. CALL HIERARCHY
4. INTERFACES POPULAR
5. STATE TRANSITION TABLES
6. QUESTIONS
</details>

## OVERVIEW
* System degradation involves reaction (CM & PSSD), EPB, IV (status inputs: Enabled, Latched, Prior, Detected, & Permanent), SM, and flags (Degration & FailedDetectable)
* Module has 3 cloning structres: SystemDegrationLocal/Remote/Total. Data objects local & remote are internal, while total read externally.
* Module manages system degradation states (owns & updates logic)
* Module provides only subsystem interfaces, no global interfaces, while modules FmInputValidation, FmSupplyManagement, & FmSystemReaction is reverse.
* ***NOTE:*** Larger numerical values mean "worse" degradation states.
* ***NOTE:*** FmSystemDegradation.h (FM) includes FmSystemDegradation_Cfg.h (FM) includes FmSystemDegradationGen_Cfg.h (DMT - stored here to keep all auto generated files under DMT, violates COBRA Style Guide).  

## DMT DATA OBJECTS & FILE LOCATIONS
* ***system reactions stored in DMT:*** Struct array named `DmtDegradationConfigScs` w/ size max number of faults. Each element(fault) has 1 member, U8 array named `SystemReaction` w/ size max nubmer of SRs. Each element w/in SystemReaction: 1st 4 bits represets CM, while last 4 bits represent PSSD.

| name                                                           | location                                                                                                    |
| -------------------------------------------------------------- | ----------------------------------------------------------------------------------------------------------- |
| system reactions (gen)                                         | *StateManagement\DmtConfiguration\DegradationConfiguration\Cfg\Source\FmSystemDegradationGen_Cfg.h*         |
| system degradation fault configurations (gen)                  | *StateManagement\DmtConfiguration\DegradationConfiguration\Cfg\SourceCal\DegradationConfigurationGen_Cfg.c* |
| bitshift CM & PSSD macros, DmtConvertSystemReactionToCm & Pssd | *StateManagement\DmtConfiguration\DmtConfigurationMain\Main\Source\DmtConfigurationMain.h*                  |
| CM & PSSD request macros (gen)                                 | *StateManagement\Fm\FmSystemDegradation\Cfg\Source\FmSystemDegradation_Cfg.h*                               |

## CALL HIERARCHY
```
1. Obtain additionalSystemDegradation for specified fault.
2. Compare additionalSystemDegradation with SystemDegradationLocal outputting to SystemDegradationLocal.
3. Compare SystemDegradationLocal with SystemDegradationRemote outputting to SystemDegradationTotal.

UpdateMonitorStatus
    UpdateMonitorStatusLatched
        ApplyLocalFmSystemDegradation
            LoadSystemDegradation                 /* Obtaining degradation for specified fault */
                LoadSystemDegradationScs          /* Obtaining system reaction degradation (CM & PSSD) for specified fault, inserted into structure additionalSystemDegradation */
                    GetDegradationConfigRawScs
                    LoadSystemDegradationScsFlags /* Only call if fault is LATCHED & not EXPECTED (not suppressed) then inside function if 
                                                        CM != NORMAL, then set array element additionalSystemDegradation.ControlModeUnexpected
                                                            If fault is LATCHING, then set array element additionalSystemDegradation.ControlModePermanent */
                LoadSystemDegradationEpb  
                LoadSystemDegradationInput  
                LoadSystemDegradationSupply  
                LoadSystemDegradationFlags  
            OverlaySystemDegradation                    /* SystemDegradationLocal = SystemDegradationLocal (static global structure) + additionalSystemDegradation (local structure) */
                TransitionTableLoop                     /* For CM */
                    stateResult = TransitionTableLookup
                    destList[systemId] = stateResult    /* Finally, inserting new CM into destination (SystemDegradationLocal) */
                TransitionTableLoop                     /* for PSSD */
                OverlaySystemDegradationScsFlags
        PublishTotalFmSystemDegradation                 /* SytemDegradationTotal = SystemDegradationLocal + SystemDegradationRemote */ 
```

## INTERFACES POPULAR
pg. 6 - *DD_TS_FmSystemDegradation*
* ***initialization:*** Initalizes structures SystemDegradationLocal/Remote/Total to default states.   
* ***update:*** Not needed on multicore systems b/c inter-core communication already triggers all actions that should be executed cyclically.  
* ***event handling - external interfaces:*** Translation layer between service request from FM components & data processing for upgrading interal degradation states. Thin.
* ***getting:*** Read only. Provide output of SystemDegradationTotal, evaluated by modeules FmInputValidation, FmSupplyManagement, & SystemReaction. '
* ***setting:*** Special modifications of internal state not covered in core services. Affect only specific part of system degradation. Disturb the standard signal flow driven by latched and passing faults.
* ***internal - data handling:*** Correspond to core services that module provides internally to initalizase its data objects. Work on poiners but do not know specific meaning of pointed-to data objects.
* ***project specific callouts for internal data handling:***  
* ***internal helper functions:*** For better code structuring & avoiding code duplication.

## STATE TRANSITION TABLES
* Answers the question when multiple degradation states are requested for the SR, then what is the degradation state.
* Is an "addition table" of A[i][j] (where i & j are different degradation states requested).  
* A CM/PSSD_X_Tranisition_Table consists of A[CM/PSSD_X_MAX][CM/PSSD_X_MAX].
* Stored officialy in const static structure named FmTransitionTableInfo. Declared in file FmSystemDegradation_Cfg.c.
* If element consist of FM_TRANSITION_TABLE_DEFAULT, then correct degradation state is based on the worse logic (higher number).  
If element consist of CM/PSSD_X_INHIBIT, then correct degradation state is based on the CM/PSS Transistion Tables.
* Each SR has table of A[CM/PSSD_X_MAX][CM/PSSD_X_MAX], but not necessary to store all elements b/c of implicient properties, majority tables vanish.

```
FM_TRANSITION_TABLE_INFO_TYPE contains:  
ControlModeInhibit     - Worst case logic
PartialShutdownInhibit - ^
ControlModeTable       - Fragments of "addition tables" for those SCS functions that do not use simple-worst case logic.
PartialShutdownTable   - ^
```

### RULES
For correct degradation state, found in file *DD_TS_FmSystemDegradation_Cfg.docm* section 6.4 *Configuration Options*.   
1. If all table elements fulfill A[i][j] >= max(i, j) then the ControlModeInhibit is FM_TRANSITION_TABLE_DEFAULT & no data addition table stored explicitly.

2. Degradation state request **CM_X_NORMAL** has no influence on current state.  
Therefore, Row 0, Col 0 omitted.  
Ex: A[i][CM_X_NORMAL] = i  
    A[CM_X_NORMAL][j] = j

3. Degradation state request **CM_X_INHIBIT** has no influence on current state.  
Therefore, last Row & Col omitted.  
Ex: A[i][CM_X_INHIBITED] = CM_X_INHIBITED  
    A[CM_X_INHIBITED][j] = CM_X_INHIBITED

4. Degradation state requests **identical** is idempotent and already know the result.  
Therefore, row diagonals are ommited.  
Ex: A[CM_X_Y][CM_X_Y] = CM_X_Y

5. Upper right triagle can be ommitted b/c of symmetry.  
Ex: A[i][j] == A[j][i]

6. Only left triagle stored.  
Therefore, A[2][1] is always 1st element.  
SCS function w/ CM_X_MAX as 2 or 3 will always have no non-trivial table elements. As 4 only has 1 non-trivial element.

7. The determination of non-trivial table elements is based on Gaussian sum formula: sum(k) = n(n+1) / 2 (where n = CM_X_INHIBIT).

```
ABS CM TRANSITION TABLE EXAMPLE
CM_ABS_NORMAL    0
CM_ABS_INHIBIT   6
CM_ABS_MAX       7

non-trivial table elements = ((6-2)(6-1))/2 = 10

/* ID */   /* ID */   /* ID */   /* ID */   /* ID */   /* ID */   /* ID */
/* ID */   /* D  */   /* SY */   /* SY */   /* SY */   /* SY */   /* IN */
/* ID */              /* D  */   /* SY */   /* SY */   /* SY */   /* IN */
/* ID */                         /* D  */   /* SY */   /* SY */   /* IN */
/* ID */                                    /* D */    /* SY */   /* IN */
/* ID */                                               /* D  */   /* IN */
/* ID */   /* IN */   /* IN */   /* IN */   /* IN */   /* IN */   /* IN */
```

## QUESTIONS
<details>
<summary><b>Q1:</b> How many SRs are stored for each fault?</summary>

**A:** 1 CM & PSSD for each SR.  
Every fault consists of degradation states for all the SRs stored in the system.  
A particular SR can only have one state occuring at a time: meaning it can only be NORMAL, INBHIBITED, etc.  
</details>

<details>
<summary><b>Q2:</b> If multiple degradation states are requested for the same SR, then what is the degradation state?</summary>

**A:** See section STATE TRANSITION TABLES.
</details>
