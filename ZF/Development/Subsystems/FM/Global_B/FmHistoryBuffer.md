# FMHISTORYBUFFER

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpose)
</details>

## PURPOSE <a name="purpose"></a>
Holds functionality of maintaining FM history buffer for holding already occured faults which are not currently present.

## IMPORTANT INTERFACES
**SetHistoryBufferFault:** Called when a fault is detected. Ex. Interface is called in fuction CheckAndProcessStorageUpdates when:
1. Fault is updated, failed, not toggled, not expected (meaning not suppressed), not in PFV buffer (which is read at startup)
2. Fault is updated, not failed, is toggled, not expected (meaning not suppressed), not in PFV buffer (which is read at startup) 

<details>
<summary><b>NOTE:</b> Fault is toggled in function:</summary>

1. StoreMonitorInBuffer when fault is updated, (monitorPassed == isFaultLatched and if fault is not expected or fault is not latched)  

| monitorPassed == isFaultLachted | toggle | meaning                                             |
| ------------------------------- | ------ | --------------------------------------------------- |
| TRUE  == TRUE                   | yes    | Fault prev latch & current monitor is passing       |
| FALSE == FALSE                  | yes    | Fault prev not latched & current monitor is failing | 
| TRUE  == FALSE                  | no     | Fault prev not latched & current monitor is passing |  
| FALSE == TRUE                   | no     | Fault prev latched & current monitor is failing     |
2. StoreEventInBuffer when fault is updated, (EventStatus != isFaultLatched)
</details>
