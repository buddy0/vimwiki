# DRIVERALERT
*Fm\DriverAlert\Doc\Fm-DDD_DriverAlert.docm*  
*Fm\DriverAlert_Cfg\Doc\Fm-DDD_DriverAlert_Cfg.docm*  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpose)
</details>

## PURPOSE <a name="purpose"></a>
During normal modes of operation, a number of lamps are controlled by faults latching/passing.  
Lamps are only updated when control over warning lamps is enabled to FM.
The lamp status is then sent to SA to turn them ON/OFF.  
Sa function GetFmLampRequestData (returns structure Fm_To_Sa_Lamp_Data) pulls lamp info from FM. 

## IMPORTANT VARIABLES

| var                                    | type    |  meaning                                         |
| -------------------------------------- | ------- |------------------------------------------------- |
| `DriverAlert.Lamp_Status[#]`           | BOOLEAN | Current On/OFF status of the FM controlled lamps |
| `DriverAlert.Lamp_Req_Mode[#][#]`      | BOOLEAN | Lamp request mode during system reconfigurations |
| `DriverAlert.Lamp_Control_Enabled`     | BOOLEAN | Tracks if FM has control over the lamps          |
| `Fm_To_Sa_Lamp_Data[<#>].Sa_Lamp_Id`   | U8      | Sa lamp id                                       |
| `Fm_To_Sa_Lamp_Data[<#>].Lamp_Command` | BOOLENA | Lamp command                                     |
 
## IMPORTANT INTERFACES
***`InitFmLampControl`:*** Inits var Driver_Alert.Lamp_Req_Mode & Fm_To_Sa_Lamp_Data[#].Sa_Lamp_Id.  
***`UpdateFmLampControl`:*** Calls function UpdateFmLampControlCallOut.  
***`UpdateFmLampControlData`:*** Copies Temp to Official for var Driver_Alert.Lamp_Req_Mode.  
***`UpdateLampStatus`:*** Updates warning lamp status only if Driver_Alert.Lamp_Status[#] != expected_lamp_status (from Lamp_Status_Fnct_Ptr[#]).  
***`EnableNormalLampControl:`*** If Mode Manager allows FM to control the lamps, store ON/OFF lamp status in Driver_Alert.Lamp_Status[#].  
***CFG `UpdateFmLampControlCallOut`:*** Driver_Alert.Lamp_Req_Mode[#][#] is filled with DMT System Reaction lamp status for a particular fault.  
