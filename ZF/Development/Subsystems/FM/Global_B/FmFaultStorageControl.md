# FMFAULTSTORAGECONTROL
*Fm\FmFaultStorageControl\Doc\DD_CS_FmFaultStorageControl.docm*  
*Fm\FmFaultStorageControl\Main\Doc\DD_TS_FmFaultStorageControl.docm*  
*Fm\FmFaultStorageControl\Cfg\Doc\DD_TS_FmFaultStorageControl_Cfg.docx*  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpose)
</details>

## PURPOSE <a name="purpose"></a>
Manages upto 32 groups each having 32 conditions. Each fault code assigned to one group. Depending on system state a group can be fulfilled.
If fulfilled, then external logging is allowed -> reported to DEM meaning DTC visible via Diagnostic & HMI requests to trigger lamp.
If not fulfilled, then no lamp is requested, fualt will be suppressed, and not reported to DEM.

## CFG
Currenlty the DMT has all faults configured to fault storage group FLT_STO_GRP_DEFAULT.

<details>
<summary><b>details</b></summary>


```
/* file FmFaultStorageControl_Cfg_Sub.h */
#define MAX_STORAGE_GROUPS                  1U
#define FLT_STO_GRP_DEFAULT                 0U

/* file DmtConfigurationMain_Cfg.h */
typedef struct DMT_MONITOR_CONFIG_TYPE_TAG
{
    BITFIELD FaultStorageGroup :4; /* actually FAULT_STORAGE_CONTROL_TYPE */
    U8 FailedDetectableIndex;
    U16 DemEvent;
    FAULT_ID_TYPE NextFault; /* next fault that is mapped to the same DEM event (cyclic list) */
    FAULT_ID_TYPE ZF_FAULT_ID;
	U16 FDL_Enable_Count;
    U8 Ecu_Id;
    U8 Comms_Fault_Type;
	U8 OBD_Type;
	U8 Storage_Type;
    U8 Failure_Record_Type;
} DMT_MONITOR_CONFIG_TYPE;

/* file MonitorConfigurationGen_Cfg.c */
const DMT_MONITOR_CONFIG_TYPE DmtMonitorConfig[DMTCONFIGURATION_NUMBER_OF_ENTRIES] =
{
    /* NVRAM_WRITE_FAILURE */
    {
        /* FaultStorageGroup      */ FLT_STO_GRP_DEFAULT,
        /* FailedDetectableIndex  */ FM_FAILED_DETECTABLE_DEFAULT,
        /* DemEvent               */ DemConf_DemEventParameter_DTC_0x062f00,
        /* NextFault              */ NVM_REQ_FAILED,
        /* Zf_Fault_Id (hex)      */ 0x0409,
        /* FDL Enable Count       */ 0x0000,
        /* Ecu_Id (hex)           */ (U8)e_Ecu_Id_None,
        /* Comms_Fault_Type       */ NON_COMMS_FAULT,
        /* OBD_CLASS              */ OBD_CLASS_A,
        /* Storage_Type           */ PERSISTENT_STORAGE_TYPE,
        /* Failure Record Type    */ DEFAULT_FR_GROUP
    },

```

## IMPORTANT VARS
**FaultStorageControlCloned:** Cloned structure with members: U32 GroupFulfilledData, BOOLEAN RunUpdate, & BOOLEAN UpdateAllowed.
FaultStorageControlCloned.RunUpdate set to TRUE in functions:
1. ReCalculateGroupsAndConditions (called from InitFmFaultStorageControl & UpdateFmFaultStorageControl, both when on master core) when

FaultStorageControlCloned.RunUpdate cleared to FALSE in functions:
1. InitFmFaultStorageCloningData
2. ProcessFaultExpectedData

FaultStorageControlCloned.RunUpdate set or cleared in functions:
1. ProccessClonedStorageData
