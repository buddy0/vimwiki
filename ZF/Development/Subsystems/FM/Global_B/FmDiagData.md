# FMDIAGDATA
*Fm\FmDiagData\Main\Doc\DD_TS_FmDiagData.docx*  
*Fm\FmDiagData\Doc\DD_CS_FmDiagData.docm*  
*Fm\FmDiagData\Cfg\Doc\DD_TS_FmOnstar.docx*  

<details>
<summar><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpose)
</details>

## PURPOSE <a name="purpose"></a>
Handles general requests for fault logging and clearing. Thus, interacts with subsystems Diagnostic & NVRAM.  
Utilzes History Buffer for storing faults.

## INTERFACES
**InitFmDiagData:** Calls FmHistoryBuffer functions InitOperationTime & InitFmHistoryBuffer. And calls InitFmFaultLogging.
