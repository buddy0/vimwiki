# FMFAULTLOGGING

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpose)
</details>

## PURPOSE <a name="purpose"></a>

## IMPORTANT INTERFACES
**InitFaultStatusTablePrior:** Copies over FmPfvBuffer into Fault_Status_Table[wordPosition].PriorFault

## QUESTIONS
<details>
<summary><b>Q: Difference between interfaces IsPriorFaultValidPfvBuffer & IsPriorFaultValid?</b></summary>


</details>
