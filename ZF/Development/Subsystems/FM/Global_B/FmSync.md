# FMSYNC
*Fm\FmSync\Doc\DDD-FmSync.docm*  
*Fm\FmSync\Main\Doc\DD_TS_FmSync.docm*

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpose)
</details>

<details>
<summary><b>ACRONYMS/GLOSSARY</b></summary>

| acronyms/glossary | meaning                                                                                                     |
| ----------------- | ----------------------------------------------------------------------------------------------------------- |
| ICC               | Inter Core Communication. Exchange of RAM data between different cores in multicore system.                 |
| Cloning           | Instantiating same variables (same symbols) on all the cores. Values are managed by each core individually. | 
</details>

## PURPOSE
Provides ICC between FM satellites in multicore system.  
Data needed on different cores is exchanged via FmSync in block size NOT via OS ICC (saves runtime savings).  
The data is FaultStatus Flags, IV states, SM info., CMs & PSSDs.  
Provides Tx & Rx functions for sending & receiving data from other cores.

Exchanges FM data between cores in a multicore subsystem.  
"Current implementation uses multiple memcpy() calls to copy individual data structures as it is still not guaranteed that these data variables are arrianged continuously in memory."

## IMPORTANT VAR
**FmSync_TxBuffer** & **FmSync_TxBuffer** are arrays holding pointers of core buffers.

## IMPORTANT INTERFACES
**InitFmSync:** Initializes pointers to core local buffers.
**FmSyncRxProcess:** Recieves data from remote core(s) to local core. Cyclic every 10ms.
**FmSyncTxTransfer:** Transmits data from local core to remote core(s). Cyclic ever 10ms.

<details>
<summary><b>Details</b></summary>


```
/* Core 0 */                                      /* Core 1 */
FmSync_TxBuffer[SAT0] = &FmRxBufferC1_From_C0; -> FmSync_RxBuffer[SAT0] = &FmRxBufferC1_From_C0; 
FmSync_RxBuffer[SAT0] = &FmRxBufferC0_From_C1; -> FmSync_TxBuffer[SAT0] = &FmRxBufferC0_From_C1;

ANOTHRE WAY TO LOOL AT IT

/* Core 0 */
FmSync_TxBuffer[SAT0] = &FmRxBufferC1_From_C0; 
FmSync_TxBuffer[SAT1] = &FmRxBufferC2_From_C0;
FmSync_RxBuffer[SAT0] = &FmRxBufferC0_From_C1; /* Core 1 is remote sat */
FmSync_RxBuffer[SAT1] = &FmRxBufferC0_From_C2; /* Core 2 is remote sat */

/* Core 1 */
FmSync_TxBuffer[SAT0] = &FmRxBufferC0_From_C1;
FmSync_TxBuffer[SAT1] = &FmRxBufferC2_From_C1;
FmSync_RxBuffer[SAT0] = &FmRxBufferC1_From_C0; /* Core 0 is remote sat */
FmSync_RxBuffer[SAT1] = &FmRxBufferC1_From_C2; /* Core 2 is remote sat */

/* Core 2 */
FmSync_TxBuffer[SAT0] = &FmRxBufferC0_From_C2;
FmSync_TxBuffer[SAT1] = &FmRxBufferC1_From_C2;
FmSync_RxBuffer[SAT0] = &FmRxBufferC2_From_C0; /* Core 0 is remote sat */
FmSync_RxBuffer[SAT1] = &FmRxBufferC2_From_C1; /* Core 1 is remote sat */


```

</details>

## QUESTION
<details>
<summary><b>Q: Why does FM data need to be ICC? Who needs/using this data?</b></summary>

**A:** One reason is that applying system degradation, FM needs to account for all the faults to see which has the worst state. 
</details>

<details>
<summary><b>Q: Difference between local & remote cores?</b></summary>

**A:** Local core is the core currently being run.  
Remote core is the core not currently being run. So if core 0 is running then core 1 & 2 are remote cores.


</details>
