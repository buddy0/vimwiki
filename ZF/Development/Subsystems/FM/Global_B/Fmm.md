# FAULT MATURITY MONITORING (FMM)

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpose)
</details>

## PURPOSE <a name="purpose"></a>
Determines figure of merit representing how close a diagnostic was detecting a failure.

## IMPORTANT INTERFACES
**InitFigureOfMeritData:** Initizalizes ptr var fmm_block_base_address[nvram_block_list_idx] with correct beginning NVRAM block pointers/addresses.  
**CalculateFigureOfMerit:** Called in UpdateMonitorStatus when:  
1. Prev fault was detecting & current monitor is failing.
2. Prev fault was not detecting & current monitor is failing.  

`FOM = (U8)((status_counter * FMM_RESOLUTION) / status_goal)`. If, latched then set bit7, FMM_LATCHED_BIT_MASK.  
Set FOM into core vars Fault_Status_Table_Extnd_CoreX[relative_index].  
**UpdateFigureOfMerit:** Called in functions:
1. FmProcessDetectingFlagChange when detecting flag is TRUE.  
Called from FmProcessRemoteFaultStatusFlags when 
2. FmProcessMasterCoreModifiedFlag
3. UpdateExtendedFaultBlock
