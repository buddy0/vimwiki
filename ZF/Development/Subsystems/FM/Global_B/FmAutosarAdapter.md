# FMAUTOSARADAPTER

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpose)
2. [DATA TYPE - DMT_MONITOR_CONFIG_TYPE](#data_type_dmt_monitor_config_type)
3. [INTERFACES](#interfaces)
4. [QUESTIONS](#questions)
</details>

<details>
<summary><b>ACRONYMS</b></summary>

| acronyms | meaning                  |
| -------- | ------------------------ |
| DEM      | Diagnostic Event Manager |
</details>

## PURPOSE <a name="purpose"></a>
* Recieve fault status results (passed/failed) & forward them to Autosar DEM.  
    * Not done synchronously b/c FM is ASIL D while DEM is ASIL QM.
* Ensures partitioning of SW & freedom of interference between Autosar & nonAutosar components.
* Cyclic part only on master core.

## DATA TYPE - DMT_MONITOR_CONFIG_TYPE <a name="data_type_dmt_monitor_config_type"></a>
* DMT_MONITOR_CONFIG_TYPE has members DemEvent & NextFault which are used in this component.

<details>
<summary><b>codebase</b></summary>

```c
typedef struct DMT_MONITOR_CONFIG_TYPE_TAG
{
    BITFIELD FaultStorageGroup :4; /* actually FAULT_STORAGE_CONTROL_TYPE */
    U8 FailedDetectableIndex;
    U16 DemEvent;
    FAULT_ID_TYPE NextFault; /* next fault that is mapped to the same DEM event (cyclic list) */
    FAULT_ID_TYPE ZF_FAULT_ID;
	U16 FDL_Enable_Count;
    U8 Ecu_Id;
    U8 Comms_Fault_Type;
	U8 OBD_Type;
	U8 Storage_Type;
    U8 Failure_Record_Type;
} DMT_MONITOR_CONFIG_TYPE;

/* file locations: /StateManagement/DmtConfiguration/MonitorConfiguration/Cfg/SourceCal/MonitorConfigurationGen_Cfg.c */
const DMT_MONITOR_CONFIG_TYPE DmtMonitorConfig[DMTCONFIGURATION_NUMBER_OF_ENTRIES] =
{
	...
	    /* WSS_OPEN_FRONT_LEFT */
    {
        /* FaultStorageGroup      */ FLT_STO_GRP_DEFAULT,                         /* Seems all faults have this value, unsure where declared? */             
        /* FailedDetectableIndex  */ FM_FAILED_DETECTABLE_DEFAULT,                /* Seems all faults have this value, unsure where declared? */ 
        /* DemEvent               */ DemConf_DemEventParameter_DTC_0x450200,      /* Generated from DaVinci Configurator & placed at /MMC_INTEGRATION/ThirdParty/Vector/BswAutocore/Appl/GenData/Dem_Lcfg.h */
        /* NextFault              */ WSS_LS_SHORT_TO_GND_FRONT_LEFT,
        /* Zf_Fault_Id (hex)      */ 0x0101,                                      /* _DTC_List.xml */
        /* FDL Enable Count       */ 0x0000,                                      /* _DMT.xml */
        /* Ecu_Id (hex)           */ (U8)e_Ecu_Id_None,                           /* _DMT.xml */
        /* Comms_Fault_Type       */ NON_COMMS_FAULT,                             /* _DMT.xml */
        /* OBD_CLASS              */ OBD_CLASS_A,                                 /* _DMT.xml */
        /* Storage_Type           */ PERSISTENT_STORAGE_TYPE,                     /* _DMT.xml */
        /* Failure Record Type    */ DEFAULT_FR_GROUP                             /* _DMT.xml */
    },
	...
	/* WSS_LS_SHORT_TO_GND_FRONT_LEFT */
    {
        /* DemEvent               */ DemConf_DemEventParameter_DTC_0x450200,
        /* NextFault              */ WSS_OPEN_FRONT_LEFT, /* back to top */   
    },
	
}
```
</details>

## INTERFACES <a name="interfaces"></a>
**`SetEventStatusPassed`:** Fowards passed fault to Autosar DEM. Every fault in dem group must pass to be passing.  
**`SetEventStatusFailed`:** Fowards failing fault to Autosar DEM. Only 1 fault in dem group must fail to be failing.  

## QUESTIONS <a name="questions"></a>
<details>
<summary><b>How does DEM get event status from FM?</b></summary>

1. Functions UpdateMonitorStatusPassed, UpdateMonitorStatusLatched, ProcessLatchedAndGoalAchievedFlagChange, ProcessExpectedFlagChange
   call function ***StoreMonitorInBuffer***.
2. Function StoreMonitorInBuffer modifies structure FmMonitorBufferInterface (FaultUpdated, FaultFailed, FaultToggled) based on values monitorPassed and FmMonitorBufferInterface.  
Function StoreMonitorInBuffer calls function StoreMonitorInBufferCallout which calls FmUpdateStatusExtndFlags modifing FmFault_Status_Table_Extnd_Flags (TestFailedThisOperationCyclet, TestFailedSinceDtcCleared, TestNotCompleteThisOperationCycle, AnyLampOn) based on monitorPassed.
3. Function ***CheckAndProcessStorageUpdates*** calls functions SetEventStatusPassed or SetEventStatusFailed based on values FmMonitorBufferInterface.
4. Functions SetEventStatusPassed or SetEventStatusFailed calls function StoreEventInBuffer.
5. Function StoreEventInBuffer modifies structure FmDemEventBufferInterface (FaultUpdated, FaultFailed, FaultToggled).
   Fowarded to DEM.
6. Function FmReadEventBuffer reads structure FmDemEventBufferInterface and may call DEM function ***Dem_ReportErrorStatus***.

```
When are we modifing structure FmMonitorBufferInterface?
ResetFmMonitorBufferInterface; Reset 3 flags
CheckAndProcessStorageUpdates; Reset 3 flags
ClearFmFaults_Cfg;             Reset 3 flags
StoreMonitorInBuffer; May set FaultToggled, FaultFailed, FaultUpdated. May reset FaultFailed.

When are we modifing structure FmDemEventBufferInterface?
UpdateFmDemInteraction; Modify ClearFaultState
FmReadEventBuffer;      Reset 1st 3 flags.
FmReadEventBuffer;      Resett 1st 4 flags.
FmSaveDemSingleFaultId; Set flag ClearFaultBuffer.
```
</details>

