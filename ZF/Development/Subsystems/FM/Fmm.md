# FMM
*FM-DDD-Fmm.doc*  

<details>
<summary><b>TABLE OF CONTENS</b></summary>

1. [PURPOSE](#purpose)
</details>

## PURPOSE <a name="purpose"></a>
* Provides how mature a fault is becoming latched.  
Provides the functions to update fault maturity monitor buffer such as updating, clearing, & providing status of a particular fault in the buffer.  
* Counts the total number of faults that enter the detecting state in the current ignition cycle.  
* Faults will be added to FMM buffer when the detection counter is above a given threshold & it will be removed once it is latched.  
* ***fmm buffer parameters:*** fault id, detection counter, detection counter max, detection flag.  

## FUNCTION INITFIGUREOFMERIT
* Obtains block address of fmm nvram into array Fmm_Nvram_Block_address.

## FUNCTION UPDATEFMMDATA
* Updates fmm data.
1. Find current_maturity_data for specific fault. Need block_idx & record_idx. Achieved by calling array Fmm_Nvram_Block_address and dereferencing.
2. Create new_maturity_data by ((status_counter * FMM_RESOLUTION) / status_goal).
3. Write new_maturity_data into Fmm_Nvram_Block_address by Fmm_Nvram_Block_address[block_idx][record_idx], only if new maturity greater than old maturity.
4. ?Call function WriteNvramBlock. Not sure why, doesn't step 3 update nvram?
5. Call function UpdateExtendedFaultBlock if following coniditions are true:  
       fault is not a comms fault, new maturity state >= threshold, & fault is not latched.
    && there is an open slot & specific fault does not already exist   
