# MONITORSTATUS 
*Fm-DDD-MonitorStatus.doc* 

* **purpose:** Provide interface functions to collect data from monitor functions & provide that data to other features.  
* ***monitor functions:*** Report current status of monitor, reset monitor.  
* ***query functions:*** Ask question about fault group or group of faults.  
* ***fault latching dection - debounce method:*** Tracks passing & failing time of a fault. Done to prevent ratcheting or drifting that may cause false fault.
<details>
<summary><b>FAULT STATUS TABLE</b></summary>

* **fault status table:** Info about faults stored. Every fault has:
    * **status flag:**
    * **status counter:** Tracks if a monitor has passed or failed.  
    If fault is latched/unlatched, then status counter compared against goal to determine if latched fault can be removed or if fault should be latched, respectively.
```c
/* MonitorStatus_Sub.h */
typedef struct FAULT_STATUS_TABLE_TYPE_TAG
{
    FAULT_STATUS_FLAG Flag; /* var_name: Fault_Status_Table[INTERNAL_FAULT_ID_MAX].Flag           */
    U16 Status_Counter;     /* var_name: Fault_Status_Table[INTERNAL_FAULT_ID_MAX].Status_Counter */
} FAULT_STATUS_TABLE_TYPE;

extern FAULT_STATUS_TABLE_TYPE Fault_Status_Table[INTERNAL_FAULT_ID_MAX];
```

**FAULT STATUS FLAG**  
```c
/* MonitorStatus_Shr.h */`
#define LATCHED                             	(U16)0x0001 /* Indicates if fault has been latched during ignition cycle. ?Latched if equal to goal? */
#define DETECTING                           	(U16)0x0002 /* Track current trend of the fault, i.e. currently passing or failing. */
#define GOAL_ACHIEVED                       	(U16)0x0004 /* Indicates that a fault monitor has reached its goal. Goal can be passing or failing. */
#define TEST_NOT_PASSED_SINCE_POWERUP       	(U16)0x0008

#define LATCHED_SINCE_POWERUP           		(U16)0x0010 /* Indicates if a fault was latched this ignition cyle. */
#define TEST_NOT_COMPLETED_SINCE_DTC_CLEARED	(U16)0x0020
#define PRIOR_FAULT_VALIDATION              	(U16)0x0040 /* Indicates if fault was latched on prev ignition cycle but yet to pass fault monitors on subsequent ignition cycle. */
#define GOAL_NOT_ACHIEVED_SINCE_POWERUP     	(U16)0x0080

#define ONSTAR_TRIGGER_SENT                 	(U16)0x0100 /* Fault ID status has been queried & sent out over the application specific in-vehicle communication network. */
#define HISTORY                             	(U16)0x0200 /* Indicates that fault has been stored in NVRAM. */
#define COUNTED_FOR_REPORTING                 	(U16)0x0400
#define ENABLED_IN_CONFIGURATION            	(U16)0x0800

#define FAULT_SETS_OBD_MIL              		(U16)0x1000
#define TEST_FAILED_SINCE_DTC_CLEARED       	(U16)0x2000
#define DTC_COUNTED                         	(U16)0x4000
#define DISABLED                            	(U16)0x8000 /* Determine if fault detection allowed or not. Set, then fault not allowed to mature. */
``` 
</details>


<details>
<summary><b><code>UpdateMonitorStatus</code> - DEBOUNCE METHOD</b></summary>

* `void UpdateMonitorStatus(const FAULT_ID_TYPE fault_id, const BOOLEAN monitor_passed, const U16 weight, const U16 goal);`  
* ***`UpdateMonitorStatus`:*** Tracks if monitor has completed testing during ignition cycle, is presently detecting a fault, & if fault is currently latched.  
If a change does occur, then data will be updated which will determine system reaction & info. stored in NVM.
* ***parameters:***  
    * ***`const FAULT_ID_TYPE fault_id:`*** Uses internal fault code.
    * ***`const BOOLEAN monitor_passed`:*** System operating correctly then MONITOR_PASSED or TRUE (no fault), otherwise MONITOR_FAILED or FALSE (fault).
    * ***`const U16 weight`:*** Modify status counter. 
    * ***`const U16 goal`:*** Compared against status counter to determine if monitor conditions have existed long enough to latch or clear the fault.  
    Determines if monitor has completed its test.

### FLOWCHART
**1a.** *check valid fault id:* `fault id < max`  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**yes:** get *fm_flag*  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**no:** do nothing  

**1b.** *check fm_flag not disabled && fault database enabled*:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**yes:** get *status_counter*   
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;get *flag_state* (TRUE: if *DETECTING*. FALSE: not *DETECTING*)  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**no:**  monitor is inhibited, do nothing  

**1c.** *check fm_flag fault status has changed*: `monitor_passed == flag_state` *(rn flag_state represents DETECTING bit)*     
Since, detecting flag indicates previous trend of fault, comparing the 2 will show a change in status of the fault.  
However, status & detecting flag are inverted when compared to one another, so when flags are equal then fault status has changed.  
"Definition of monitor_passed and Detecting flags are inverted when compared to one another. That is, if the monitor_passed is true, then Detecting should be false and vice versa."  
***Thus, if `monitor_passed` equals `flag_state`, then something has changed resulting in `status_counter` to reset, toggle `fm_flag` detecting bit, clear `goal_achieved` bit, & `update_status_flag` to be true.***  
| current monitor_passed      | eq op | prev. flag_state decting bit | meaning                                      | action                                                          |
| --------------------------- | ----- | ---------------------------- | -------------------------------------------- | --------------------------------------------------------------- |
| FALSE (fault)               | `==`  | TRUE (detecting)             | nothing changed, don't go inside `if`        | nothing
| **TRUE (no fault)**         | `==`  | **TRUE (detecting)**         | changed, go inside if                        | status_counter=0<br>**fm_flag Detecting bit=FALSE**<br>goal_achieved=FALSE<br>update_status_flag = TRUE
| **FALSE (fault)**           | `==`  | **FALSE (not detecting)**    | changed, go inside if. First time Detecting. | status_counter=0<br>**fm_flag Detecting bit=TRUE**<br>goal_achieved=FALSE<br>update_status_flag = TRUE<br>add to Fault Detecting Stack
| TRUE (no fault)             | `==`  | FALSE (not detecting)        | nothing changed, don't go inside `if`        | nothing

&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**yes:** reset *status_counter*  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;toggle *fm_flag* detecting bit  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;clear *fm_flag* goal achieved bit  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;update_status_flag = TRUE;  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**no:** do nothing

**2a.** *check fm_flag has goal_achieved*:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**yes:** goal already reached, do nothing  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**no:** increment status counter by weight  

**2b.** *update fault maturity state by checking if monitor_passed == FALSE*
&nbsp;&nbsp;&nbsp;&nbsp;**yes:** call updateFmmData
&nbsp;&nbsp;&nbsp;&nbsp;**no:** nothing

**2c.** *check status_counter >= parameter goal*:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**yes:** set *fm_flag* goal achieved bit  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;update_status_flag = TRUE;  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**no:** goal not reached, no dothing  

**2ca.** *check parameter monitor_passed == FALSE*:  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**yes:** call `UpdateMonitorStatusLatched` service  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**no:** call `UpdateMonitorStatusPassed` service  

**3.** *check status_flag != FALSE*  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**yes:** call `UpdateFaultStatusFlag`  
&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;**no:**  do nothing
</details>

<details>
<summary><b><code>UpdateUpDownMonitorStatus</code> - UP/DOWN COUNTER METHOD</b></summary>

`void UpdateUpDownMonitorStatus (const FAULT_ID_TYPE fault_id, const BOOLEAN monitor_passed, const U16 weight, const U16 goal);` 
</details>

 
