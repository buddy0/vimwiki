# DIAGINTERFACE
*Fm-DDD-DiagInterface.doc*  

**purpose:** Provides mechanism by which faults can be enabled/disabled or inhibited & cleared.  

*Diagnostic_Monitors_Interaction_with_FM.doc*  
Specifies diagnostic monitor behaviors for UDS (Unified Diagnostic Services) & OBD(On Board Diagnos) rules.

### GENERAL RULES FOR ALL MONITORS
* All monitors shall support a *Fail* & *Pass* path.
* All reporting to FM shall only be made from scheduled Tasks.
* All monitors shall report to FM periodically. Done by calling:   `ResetMonitorStatus`: if monitor is disabled.  
`UpdateMonitorStatus/UpdateUpDownMonitorStatus`: if monitor is enabled & is either passing or failing.
* UpdateMonitorStatus/UpdateUpDownMonitorStatus shall only be called on passed/failed determination.
* Following a System Re-Init or a Clear DTCs, all monitors shall re-start, compute new result, & report status to FM.
* All components which support Diagnostic Monitors shall provide globally available *Re-Init* & *Clear DTCs Notification* callback function.

### MONITOR TYPES
***Type 1:*** Execute only during Bootloader or Micro/Application initialization.  
***Type 2:*** Execute only during System Self-Test.  
***Type 3:*** Execute periodically (at their own rate) while system is ON & before system begins shutdown sequence.
***Type 4:*** Execute only after end of Drive Cycle or in Keep Alive state of the system.

