# FAULTDATABASE
*Fm-DDD-FaultDatabase.doc*  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [PURPOSE](#purpose)
</details>

## PURPOSE <a name="purpose"></a>
Provide and initialize pointer to fault database which is accessed by serveral modules w/ FM subsystem.
