# BOOLEANSIGNALS MODULE: IO-DDD-BooleanSignals

## PURPOSE
* Provide services for commanding discrete output signals w/ simple on/off logic & getting state of discrete input signals.   
* These signals are mapped to specific IO pins by configuring the Boolean Signals module.  

## ALGORITHM
* Module has generic functions for setting & getting state of any IO port pin.  
***generic function purpose:*** Route commands/requests to specifc services provided by each port.  
***ports:*** Common peripherals of the micro (e.g. TIM, CCU6) or external devices (e.g. ASIC).  

* Each signal has a service & set of definitons specifing what port & pin it is mapped to.  
***signal service purpose:*** Calls generic service passing along the definitons.  

* In Autosar application, pin & port mapping is configured through DIO & Port configuration of the Tresos tool.  

* ```
   +------------------+
   | GENERIC FUNCTION |
   +------------------+
            |             Routes commands/requests
            v
   +------------------+
   |     SERVICE      |
   +------------------+
            |             Definitions
            v
   +------------------+
   | GENERIC SERVICE  |
   +------------------|
```
