# ADCIF MODULE: IO-DDD-ADC

## PURPOSE
* Provide basic fuctional interface to access ADC peripheral on all AURIX platform devices.

## 3 PARTS TO ANALOG SUBSYSTEM
1. ***Analog Inputs module:*** Provides application interfaces to access analog input data. Provide conversion of raw analog input into application format. Contains initial configurations of ADC peripheral & ADC group conversion parameters. 
* *AdcIf* module acts like a bridge between *AnalogInputs* module & Infineon MCAL. 
2. ***System ASIC driver:*** Responsible for setting up & retrieving results of internal ASIC ADC. 
3. ***Epb driver:*** Responsible for retrieving results from S12 micro & proved services to *AnalogInputs* module.  

## ADC SIGNALS GROUPED INTO 3 MAIN GROUPS
Each ADC group can have multiple subgroups.
1. Group Event
2. Solenoid Group
3. SW Group

Due to differences in *TI* & *AURIX* ADC architecture, the channels were spread across **AdcHwUnit/kernels**.  
As a result, there exists mutliple SW or HW triggered logical groups in the Eb configuration.  
***solution:*** *Adc_Cfg* component was created to form a bridge between the *Adc* component & the Eb configuration to retain same logic of *TI* in *AURIX* implementation.  
**Ep tool:** Generates channel offsets literaly from 0 to 7 for each kernel regardless of ADC channel numbers (An0 to An64).

## ADC GROUP CONVERSIONS
Converted ADC results can be transferred from ADC result registers to application buffers by using DMA & w/o DMA by Ifc MCAL.  
***CASE 1: w/o DMA***  
Result buffers should be initialized for every logical subgroup.  
`SetAdcGroupResultBuffer();` initializes the following 3 result buffers for each main group based on the existenece of the subgroup.
```c
/* Adc main group result buffers declaration. */
Adc_ValueGroupType Adc_GroupSw_Result[ADC_MAX_KERNELS][NUM_MAX_CHANNELS_ADC_SW_SUBGROUPS];
Adc_ValueGroupType Adc_GroupSol_Result[ADC_MAX_KERNELS][NUM_MAX_CHANNELS_ADC_SOL_SUBGROUPS];
Adc_ValueGroupType Adc_GroupEvt_Result[ADC_MAX_KERNELS][NUM_MAX_CHANNELS_ADC_EVT_SUBGROUPS];
```
`Adc_SetupResultBuffer(subgroup_id,result_buffer);` Infineon MCAL function that configures result buffer for each logical group. 
![Table of Setting up ADC Group Conversions Result Buffers](Images/Result_Buffers.png)  

***CASE 2: USING DMA***  
Similary to w/o using DMA, ADC results are still transferred from ADC result registers into application buffers.  
However, upon completion of ADC conversion, a interrupt will trigger a DMA channel to do the above result transfer process. 

### SW GROUP CONVERSION
No callback function will be enabled for SW groups.  
SW group utilizes the sequential offset mapping configured by *Adc_Cfg* component.  
![Table of SW Group Conversion](Images/Sw_Group_Conversion.png)  

### EVENT GROUP CONVERSION
**purpose:** Perform *simultaneous* voltage reads by system signals ready by the ADC. Perform sigle shot group conversions.  
Internal TOM can trigger group conversions either rising or falling edge & freq of the trigger.  
Event Group conversions always in *sync* w/ Open Loop Coil Freq. configured for this group.  
Adc results stored in *circular buffer* `Adc_GroupEvt_Result[][]`. [circular buffer](https://en.wikipedia.org/wiki/Circular_buffer)

***CASE 1: W/O USING DMA***  
Call back function can be configured for each logical subgroup and update conversion status & conversion count; used to read results from cirucular buffer.

***CASE 2: W/ USING DMA***  
No callback function & results stored in circular buffer.  
Last converted result read this way: 
```c
dma_destination_addr = (U32)GetDmaDestAddr(Adc_Sr0_Dma_Ch_Config[kernel_index]);
offset_index = (U8)(((dma_destination_addr - (U32)&Adc_GroupEvt_Result[kernel_index][0]) /(MAX_NUM_CH_PER_KERNEL*ADC_RESULTS_SIZE_IN_BYTES)));

```
### SOLENOID GROUP CONVERSION
Similar to Event Group, TOM channel trigger Sol Group based on freq. & edge.  
Trigger only enabled when conversions are required & then after completion of converison trigger will be disabled.  
Callback function can be enabled for last subgroup so results can be stored in application buffers: `Solenoid_Feedback[]` & `Solenoid_Supplies[]`.  
Solenoid group conversion triggered from SA subsystem during solenoid self testing.





