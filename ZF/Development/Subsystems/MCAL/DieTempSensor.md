# DIETEMPSENSOR MODULE: IO-DDD-SystemControlUnit
DDD focuses only on External Request Unit (ERU).  
**ERU:** Event & pattern detection unit.  

## PURPOSE
* Generate an external interrupt request if an edge is detected on an input pin.  
Detected event can trigger other modules or provide notification.  
* ERU can accept external interrupt requests from CCU6, STM, GTM-TOM, & REQx ports (General purpose input lines as request sorces).  
* 8 independent ERU channels (using 6 of them).

## ERU SERVICE REQUEST ROUTING
* Each ERU channel is mapped to Output Gating Unit (OGU), which is the configuration parameter & routing of output lines.  
* ***OGU purpose:*** Combine available...
    * ***trigger combination:*** trigger events (`TRxy` where x=0-7) from Input Channels
    * ***pattern detection:*** status flags (`EIFR.INTFx` where x=0-7) from Input Channels
* ***OGU outputs:*** Generates 4 output signals distributed to the system: **ERU_PDOUTy**, **ERU_GOUTy**, **ERU_TOUTy**, & **ERU_IOUTy**.  

