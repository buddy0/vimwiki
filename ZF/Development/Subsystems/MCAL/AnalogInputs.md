# ANALOGINPUTS MODULE: IO-DDD-ANALOGINPUTS

## PURPOSE
* Provide services for getting the values of analog signals read by ADCs by mapping these signals to specific IO pins. 

### ALGORITHM
* Module has generic functions for getting analog voltages of any pin on any port.  
The port can be common peripherals of the microcontroller (ADC1, ADC2) or external devices (ASIC w/ADC).  
The generic function routes requests to services, provided by each port.
* Each signal has a service & definitions specifing the mapped pin and port & the gain as well.  
Signal's service calls a generic service, passing in its defitions of its signal. 
* Module controls the logic of uC ADC1 & ADC2, using services provided by the ADC driver to perform periodic conversions & buffer results retrieved by the generic service.  

## SOLENOID
**feedback:** Outputs of a system are routed back as inputs.  
**failsafe:** Feature in an event of failure, system will go to a safe status never affecting safety.  
* Solenoid contains 2 different blocks of analog data:
1. Feedback of the coil itself. The coil supply voltage is measured by uC ADC. Can be triggered by HW or SW event. 
2. The current through failsafe relay FET is measured by system ASIC. Can be triggered by SPI command or rising edge of *ADEVT* pin. 
* Depending on processor type, there is an internal connection between the TOM & ADC where rising & falling edge can be used.  
Thus, possible for 1 TOM channel pin to trigger both ADC at different times occupying falling edge by uC & rissing edge by ASIC.  

### ANALOG FEEDBACK
***Requirements for capturing analog feedbacks of solenoids:***  
1. Feedbacks need to be converted at a specific time, to be able to perform failsafe monitoring.  
Thus, they will be synchronized w/ the TOM according to PWM period when the test is done. ***WHY PWM PERIOD?***  
Solenoid failsafe requires the following channels to be converted:
    * first coil supply
    * then sall solenoid feedbacks
    * coil supply again at last
2. Perform all actions as fast as possible, preferably w/o SW interaction.  
Achieved by configuring VADC to do this:
   * configure an ADC group to contain coil supply & all discrete solenoid feedbacks
   * configure this group to be triggered by TOM channel falling edge
   * configure this group to generate an interrupt (Callback function) at completion of Adc conversions & transferring results from Adc RAM to application buffer

* Therefore, the result handeling of this module:  
   * TOM channel triggers ADC conversion accoriding measure point of the test
   * ADC starts conversion of all channels
   * In callback function, store solenoid & solenoid supply results from ADC group result buffer to application buffers.  

### FAILSAFE RELAY CURRENT
Another time constant needed for the TOM channel solenoid driver; conversion time of system ASIC for failsafe relay current.  
Achieved through the rising edge of the TOM channel pin & must occur before the active ON test pulse is finished.  

### GROUP HANDELING RESTRICTIONS






