# MCAL

## INTEGRITY PROJECT PATHS
***Regular Sandbox:*** `/SCS/SCS_Core_Based/08_Software/Application/IO/IO_GEN1`  

## COMPONENTS & MODULES
| Component         | Module                              | 
| ----------------- | ----------------------------------- |
| AnalogInputs      | [AnalogInputs](AnalogInputs.md)     |
|                   | [AdcIf](AdcIf.md)                   |
| BooleanSignals    | [BooleanSignals](BooleanSignals.md) |
| SystemControlUnit | [DieTempSensor](DieTempSensor.md)   |
|                   | [EruIf](EruIf.md)                   |
