# FILE STUFF

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [FILE STRUCTURE](#file_structure)
2. [FILE EXTENSIONS](#file_extensions)  
&emsp;a. [ini](#ini)  
&emsp;b. [bat](#bat)  
&emsp;c. [lsl](#lsl)  
&emsp;d. [pl](#pl)  
&emsp;e. [pm](#pm)  
&emsp;f. [xml](#xml)  
&emsp;g. [xls](#xls)  
&emsp;h. [mk](#mk)  
&emsp;i. [elf](#elf)  
&emsp;j. [s37](#s37) 
&emsp;k. [bak](#bak)  
</details>

## FILE STRUCTURE <a name="file_structure"></a>
* In Cobra, there are two main folders: APPLICATION & CALIBRATION  
***appliction:*** For program flash. Most of the code resides here. Almost the same for each vehicle.  
***calibration:*** For data flash. Is the trim of the vehicle. Ex, the Fault Database Table resides here.  

## FILE EXTENSIONS <a name="file_extensions"></a>
[fileinfo](https://fileinfo.com)  

<details>
<summary><b>.ini</b></summary> <a name="ini"></a>

**ini file:** Configuration file for computer software. Stands for initialization.  
Consists of text-based content w/ a structure & sytnax comprising *key-value pairs* for properties & sections that organize these properties.  
Stored in a plain/text file.  

## KEYS (PROPERTIES)
Every key has a name and value, delimited by an equal sign. 
```ini
name=value
```

## SECTIONS
Keys can be grouped into sections. Sections end at next section declaration or at end of file & cannot be nested. 
```ini
[section]
a=a
b=b
```
## OTHER
* Sections & property names are *not* case sensitivity.   
* Comment: Semicolon followed by a space & at the begining of a line by themselves. Ex. `; comment`.  
* INI files have no hieracrchy of sections.    
</details>

<details>
<summary><b>.bat</b></summary> <a name="bat"></a>

**bat file:** Aka batch file. A script file for Windows OS that automates regular tasks.  
Consists of a series of commands to be executed by the command-line interpreter. 
When file is run, shell program reads the file & executes its commands
Stored in a plain/text file. 

**batch processing:** Running of "jobs that can run w/o end user interactions or can be scheduled to run as resources permit." 
</details>

<details>
<summary><b>.lsl</b></summary> <a name="lsl"></a>

**documentation:** Chapter 16 in TASKING UM.  
**lsl file:** Aka Linker Script Language used for TASKING compiler.  
</details>

<details>
<summary><b>.pl</b></summary> <a name="pl"></a>

**pl file:** Contains source code written in [Perl](https://www.perl.org/docs.html) which is a scripting language.
</details>

<details>
<summary><b>.pm</b></summary> <a name="pm"></a>

**pm file:** Module containing [Perl](https://www.perl.org/) that can be referenced & used by Perl Programs. May be:
1. *Traditional:* Defines subroutines & variables for the function feferencing the module to import & use.
2. *Object-oriented:* Functions as a class definition & is accessed through method calls.
</details>

<details>
<summary><b>.xml</b></summary> <a name="xml"></a>

**xml file:** Extensible Markup Language (xml) is a markup language that defines set of rules for encoding documents that is both human & machine readable.
</details>

<details>
<summary><b>.xls</b></summary> <a name="xls"></a>

**xls file:** Is a Microsoft Excel Spreadsheet (legacy) which store & display data in table format. 
</details>

<details>
<summary><b>.mk</b></summary> <a name="mk"></a>

**mk file:** Makefile used by SW compilers & linkers for building executable from source files.  
**documentation:** [GNU make](https://www.gnu.org/software/make/manual/make.html)

## SYNTAX
```make
# rule
target : prerequisites
    recipe
```
</details>

<details>
<summary><b>.elf</b></summary> <a name="elf"></a>

**documentation:** [The 101 of ELF files on Linux: Understanding and Analysis](https://fileinfo.com/extension/elf)  
**elf file:** Executable & Linkable Format file. A system file that can store executable programs, memory dumps, & shared libraries. 
Defines the structure for binaries, libraries, and core files & typically output of compiler or linker in binary format.
</details>

<details>
<summary><b>.s37</b></summary> <a name="s37"></a>

**s37 file:** Aka a Motorola S-record (SREC) file format. Conveys binary information as hex values in ASCII Text form.  
Commonly used in programming flash memory in uC. 
Typical application: 
| name     | input | output        |
| -------- | ----- | ------------- |
| compiler | .c    | .o            |
| linker   | .o    | binary -> .s37|
</details>

<details>
<summary><b>.bak</b></summary> <a name="bak"></a>

**bak file:** Signifies backup file.  
</details>
