# CODING STANDARD - [link](Images/Coding_Standard.pdf) 

## ARCHITECTURE SCOPES
1. **global scope:** Global interface from a subsystem to other subsystems.  
Located in `subsystem.h` file.  
2. **task scope:** Reserved for functions allowing global access only b/c must be executed by OS scheduler.  
In the form `void FunctionName (void)`.  
3. **subsystem scope:** Interface to other modules w/in same subsystem & will *not* be used outside the subsystem.  
Located in `module_sub.h` file.  
4. **private scope:** Provide info. used only w/in a single module of subsystem. Only C source code can include this file.  
Can't include in header file b/c it can only belong to one file, module.  
Located in `module_priv.h` file.  

### EXAMPLE
***subsystems:*** `Ha`, `MCAL`, `Microsar`, etc.  
***modules:*** `AnalogInputs`, `DMA`, `Iom`, etc.  
`AnalogInputs.h`: Access any subsystem.  
`AnalogInputs_Sub.h`: Access any module w/in the *MCAL* subsystem.  
`AnalogInputs_Priv.h`: Access only the module *AnalogInputs*.  
![Picture of File Explorer Scope](Images/file_explorer_scope.png)  

### LOCAL & INLINE
Both located in *SC* subsystem in *StdTypes.h*.  
**LOCAL:** Used on private scope data and functions, allows symbolic info. during *debug* and *development*, & does *NOT* compromise data hiding. `#define LOCAL static`.  
**INLINE:** Reduce overhead of normal function call. `#define INLINE inline`.  

## SOURCE FILE GUIDE LINES

### EDITOR SETTINGS
***line length:*** 105  
***tab stops:*** 4 character spaces  

### TYPES
`.c`, `subsystem.h`, `.h`, `_sub.h`, `_priv.h`, and Assembly files.  

### INCLUSION OF HEADER FILES
`sc.h` (if this is a C)  
any `subsystem.h` files  
any `_sub.h` files  
`_priv.h` file (if it exists) 

## C RULES & GUIDELINES

### BOOLEAN OPERATIONS
* All boolean-type comparisons *must* be against *TRUE*, *FALSE*, or another variable of boolen-type.  
* Booelan type variables *must* be compared against *TRUE*, *FALSE*, or another boolean type.  
* Boolean assignments should be explicitly assigned *TRUE* or *FALSE*. 

### NAMING CONVENTIONS
* ***function:*** First character of each word is capitalized w/ no separation of words. **Ex.** `FunctionName`.  
Reader functions shall start w/ the word *Is*, *Get*, or *Read*.
* ***non-temporary data:*** First character of each word is capitalized w/ an underscore separating inndividual words. **Ex.** `Wheel_Speed`.  
* ***temporary data:*** No capital letters are used & words are separated by an underscore. **Ex.** `temp_velocity`.  
* ***define, typedef, & macro:*** Declared using all capital letters w/ underscores separating individual words. 
* ***numbers in names:*** Shall be treated as a new word & therefore be separated by an underscore.  **Ex.** `temp_velocity_1`.  

### VARIABLE INITIALIZATION
* Variables that are *not* automatic & are to be initialized to zero should *not* be explicitly initialized to zero as the RAM will all be set to zero when application is initialized.  

### MATH OPERATIONS
* Floating point variables may not be used. 

### MACROS
* Use parentheses

### CONSTANTS
* Use of constant numeric values w/in the code is prohibited. If desired, value should be declared in a *#define* statement in a header file.
* Symbol assigned to numeric constant should be descriptive, not to its numeric value tho.  
* Not allowed in comments

### READ ONLY REGISTER REFERENCE
* Declared with both *volatile* & *const* modifiers.  

### FILE INCLUSION
* Format `#include "file_path\file". *file_path* always goes out to the root of the project & then back into the directory that contains the header file. 
* Ordered from global scope to private scope.  
* All C source code files shall include `sc.h` & thus no header file shall include `sc.h`.  
* **Ex. retrieving file in another subsystem:** In `Serivces\Os\Rtos_Cfg\Source`. `#include <Sc\ScMain\Source\Sc.h>`.   
* **Ex. retrieving file in same subsystem, same module:** In `Serivces\Os\Rtos_Cfg\Source`. `#include "Rtos_Cfg.h"`. 
* ***NOTE:*** Always try to include header files in C files, avoiding including header files in header files whenever possible.  

### EXTERN
* Symbols w/ this keyword *must* *not* be initialized.  

### PARAMETER PASSING
* *#* of parameters should be limited as much as possible & Whenever possible, pass by reference.
* Parameters that are passed shall be declared *const*. 
* parameters shall remain in same order.  

### PROJECT DEFINITIONS
* Global definitions, can be found in the *Sc* (system common) subsystem.  

### IF STATEMENTS
* Every *if* statement needs an *else*. 

### DATA TYPES
![Table of Data Types](Images/Data_Types.png)





