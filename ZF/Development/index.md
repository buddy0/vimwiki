# DEVELOPMENT

## SUBSYSTEMS
* [**MCAL**](Subsystems/MCAL/index.md), [**OS**](Subsystems/OS/index.md), [**FM**](Subsystems/FM/index.md), [**SA**](Subsystems/SA/index.md)

## AURIX DOCUMENTS
* [**TC3xx User Manuel**](Aurix/User_Manuel/TC3xx.md)
* [**Architecture**](Aurix/Architecture/index.md)

## OTHER
* [**Coding Standard**](coding_standard.md)  
* [**AUTOSAR**](autosar.md)
* [**Tresos**](Tresos/index.md)  
* [**File Stuff**](File_Stuff/index.md)
