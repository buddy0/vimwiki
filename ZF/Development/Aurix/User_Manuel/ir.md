# INTERRUPT ROUTER (IR)
*Infineon Aurix TC3xx Part 1 UM - Chap 16*

<details>
<summary><b>ABBREVIATIONS</b></summary>

| abbreviation | full name                             |
| ------------ | ------------------------------------- |
| IR           | Interrupt Router                      |
| SR           | Service Request                       |
| SRN          | Service Request Node                  |
| SRC          | Service Request Control Register      |
| ICU          | Interrupt Control Unit                |
| ICR          | Interrupt Control Register            |
| ISP          | Interrupt Service Provider            |
| IVT          | Interrupt Vector Table                |
| SPB          | System Peripheral Bus                 |
| GPSRxy       | General Purpose Service Request       |
| SRBx         | Service Request Broadcast             |
| ERU          | External Request Unit                 |

**SRC**  
| abbreviation | full name                           | comment                                                                                                                      |
| ------------ | ----------------------------------- | ---------------------------------------------------------------------------------------------------------------------------- |
| SRE          | Service Request Enable/disable      | Enables interrupt to take part in arbitration selected Service Provider. Does *not* enable setting SRR.                      |
| SRR          | Service Request Flag                | Indicates that a SR is pending. Set indirectly by HW or directly through SW via SETR & CLRR bits. Auto reset by HW when SR is acknowledged and serviced.                                                                                                                                                          |
| SETR         | Request Set bit                     | Sets SRR & SWS to one if SETR is written to 1.                                                                               |
| CLRR         | Request Clear bit                   | Clears SRR if CLRR is written to 1.                                                                                          |
| TOS          | Type of Service Control             | Each Tri-core CPU and DMA can act as an ISP.                                                                                 |
| SRPN         | Service Request Priority Number     | Defines priority of SR w/ respect to other sources service from same ISP.                                                    |
| ECC          | ECC Encoding                        |                                                                                                                              |
| IOV          | Interrupt Trigger Overflow Bit      | Set by HW if SR is pending & new SR is triggered via interrupt trigger or SETR bit.                                          |
| IOVCLR       | Interupt Trigger Overflow Clear Bit | Used to clear IOV bit by writing 1 to IOVCLR bit.                                                                            |
| SWS          | SW Sticky Bit                       | Set when SETR written to 1.                                                                                                  |
| SWSCLR       | SW Sticky Clear Bit                 | Used to clear SWS bit by writing 1 5o SWSCLR bit.                 

**ICR**  
| abbreviation | full name                             |
| ------------ | ------------------------------------- |
| CCPN         | Current CPU Priority Number           |
| IE           | Global Interrupt Enable/Disable Bit   |
| PIPN         | Pending Interrupt Priority Number     |                                                           |
</details>

<details>
<summary><b>REGISTERS</b></summary>

![Picture of Registers](Images/Ir_Registers.png)  
</details>

<details>
<summary><b>FEATURES</b></summary>

* Interrupt System w/ support of 1024 service requests.
* Support of 255 service request priority levels per ICU / Service Provider.
* Support of 8 ICUs/ Service Providers
* Each peripheral interrupt w/ dedicated Service Request Node (SRN)
* Each SRN w/ programmable 8-bit priority vector.
* SRNs cleared automatically by HW on interrupt acknowledge by configured Service Provider.
* 8 General Purpose Service Requests (GPSR) per CPU that can be used as SW interrupts.
* Service Request Broadcast Registers (SRB) to signal General Purpose Service Requests (Software Interrupts) simultaneously to multiple Service Providers.
* External interrupts w/ filter modes & trigger modes (falling edge, rising edge, high or low level).
</details>

<details>
<summary><b>BLOCK DIAGRAMS</b></summary>

![Picture of IR Block Diagram](Images/Block_Diagram_Ir.png)  
![Picture of Interrupt System](Images/Interrupt_System.png)  
</details>

* **IR:** Schedules interrupts (here called service requests) from external resources & internal resources, & SW to CPU & DMA modules (here called Service Provider). Called Service Request instead of Interrupt Request b/c interrupts can be serivced by either one of service providers, CPU or DMA.  
* ***interrupt system:*** Composed of Interrupt Router module which includes SRNs, Interrupt Control Units (ICUs), & additional functionality for SW development.    
    * Each module that can generate service request is connected to a SRN in the central IR module.  
    * Each SRN contains SRC configuring priority, mapping to one of the available service providers.  
    * Each ICU is connected to one service provider (CPU or DMA module). ICU gets the winning SRN of an arbitration round & service provider signals back when     &nbsp;&nbsp;&nbsp;&& which service request it is processing.  
* **SRN:** Contains SRC & interface logic connecting it to the triggering unit outside of IR module & interrupt arbitration buses inside IR module.  
* **SRC:** Contains enable/disable info, service request set and clear bit, sw stick bit (SWS), service request priority level vector, service request destination / service provider, service request status bit, integrity errors, & interrupt overflow bits.  
* ***SRC protection:*** Is write protected via an On Chip Bus Master TAG-ID protection which is controlled by the IR control registers ACCEN_CONFIG & ACCEN_TOSx.  
    * SRC[31:16] write protected by ACCEN_TOSx (x=SRC.TOS).  
    * SRC[15:0] write protected by ACCEN_CONFIG.  
* ***Request Set and Clear Bits (SETR, CLRR):*** Writing 1 to SETR causes bit SRR to be set to 1. Writing 1 to CLRR causes bit SRR to be cleared to 0. Value written to SETR or CLRR is ***not*** stored, thus these bits always return 0 when read. Writing a 0 to these bits has no effect.  
* ***SRPN CPU:*** SR associated w/ SRPN by an IVT located in each CPU. CPU IVT ordered by priority number. Allows single peripheral to have multiple priorities for different purposes.  
* ***SRPN DMA:*** SR associated w/ SRPN to DMA channel numbers: SPRN=x will trigger DMA channel x.
* ***mapping of SR:*** All SRs mapped to SRNs in IR. IR has one 1024 interrupt trigger input vector. Each interrupt trigger input vector bit related to one SRN w/ the SRN index number.  A trigger pulse on interrupt trigger input vector bit *x* wll trigger SRN *x*. 
* ***timing characteristics of SR trigger signals:*** IR clocked w/ SPB.
* **ICU:** One ICU per ISP. SRNs can be mapped to ICU by SRCx.TOS register bit field.
* **GPSR:** Each GPSR group consist of 8 SRNs. Each GPSR named *SRC_GPSRxy*. Intended for SW Interrupts b/c they are not mapped to a HW interrupt trigger event. Only be triggered by writing 1 to related SRC_GPSRxy.SETR bit or SRBx[y] bit.
* **SRBx:** Used to set SRs to mutiple ISPs in parallel. One SRBx implemented for each GPSRxy. Used to trigger multiple SRs w/in SRC_GPSRx group in parallel. Each SRBx register is write protected via dedicated ACCEN_SRx0 & ACCEN_SRBx1.
* ***system registers:*** IR module does *not* support the CLC, OCS, & KRSTx registers. 
* ***usage of interrupt system:*** 
1. CPU to ICU interface. Contains ICR that holds CCPN, IE, & PIPN.
2. DMA to ICU interface. 
3. SW-initiated interrupts. Any SRN can be used, just have to making SRR to 1. However, once SRR bit is set, no way to distinguish between SW or HW initiated SR. Thus, SW should only use SRNs & interrupt priority numbers that are not being used for HW SRs. Also, device contains GPSR SRNs that support SW-initiated interrupts. One group per Tricore CPU w/ each group containing 8 SRNs.  
4. Exteranl interrupts are reserved by 8 SRNs. Setup for external GPIO port input signals (edge/elvel triggering, gating, etc.) are able to generate interrupt request which is controlled in the ERU.
