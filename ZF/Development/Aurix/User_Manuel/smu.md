# Safety Management Unit (SMU)

*Infineon Aurix TC3xx Part 1 UM - Chap 15*
<details>
<summary><b>FEATURES</b></summary>

* Collects every alarm signal generated from safety mechanisms.
* Implements Fault Signaling Protocol (FSP) reporting internal faults to external environment.
</details>

<details>
<summary><b>BLOCK DIAGRAM & ARCHITECTURE</b></summary>

![Picture of SMU Block Diagram](Images/Block_Diagram_Smu.png)  
![Picture of SMU Architecture](Images/Architecture_Smu.png)  
</details>

**SMU:** Provides generic interface to manage behavior of the uC under presence of faults.  
Centralizes all alarm signals related to different HW & SW safety mechanisms.  
Each alarm can be configured to trigger internal actions and/or notify presence of faults vis FSP.  
***SMU partitioned into 2 parts:*** Physically separated. Located in different clocks & power domains.  
&nbsp;&nbsp;&nbsp;&nbsp;***SMU core:*** Collects majority of alarm signals from HW monitors & safety mechanisms.  
&nbsp;&nbsp;&nbsp;&nbsp;***SMU_stdby:*** Collects alarms from modules which detect clock (no clock), power(under/over voltage) & temperature failures (under/over temperatures). Also collects SMU_alive signal notifing when SMU_core is not triggering a reaction.  

