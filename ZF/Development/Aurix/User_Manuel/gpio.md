# GENERAL PURPOSE I/O PORTS AND PERIPHERALS I/O LINES (PORTS)

<details>
<summary><b>REGISTERS</b></summary>

| type/mnemonic                              | name                                          | comment                                                                              |
| ------------------------------------------ | --------------------------------------------- | ------------------------------------------------------------------------------------ |
| **Module Identification Register**         |                                               |                                                                                      |
| ID                                         | Port n Identification Register                |                                                                                      |
| **Port Input/Output Control Registers**    |                                               | Determines Input as Pull-Up/Down or Tri-State & Output as Push-Pull, Open Drain.     |
| IOCR0                                      | Port n Input/Output Control Register 0        | Ports 0-3.                                                                           |
| IOCR4                                      | Port n Input/Output Control Register 4        | Ports 4-7.                                                                           |
| IOCR8                                      | Port n Input/Output Control Register 8        | Ports 8-11.                                                                          |
| IOCR12                                     | Port n Input/Output Control Register 12       | Ports 12-15.                                                                         |
| **Pad Driver Mode Register**               |                                               | Ouputs driver strength & slew rate.                                                  |
| PDR0                                       | Port n Pad Driver Mode Register 0             | Ports 0-7.                                                                           |
| PDR1                                       | Port n Pad Driver Mode Register 1             | Ports 8-15.                                                                          |
| **LVDS Pad Control Register**              |                                               |                                                                                      |
| LPCRx (x=0-7)                              | Port n LVDS Pad Control Register x            | RX pad, bit field [7:0], TX pad, bit field [15:8].                                   |
| **Pin Function Decision Control Register** |                                               | Possibility of EN/DIS port pad, select digital input or analog ADC input func.       |
| PDISC                                      | Port n Pin Function Decision Control Register | Bit field PDISx (x=0-15) represents each port.                                       |
| **Pin Controller Select Register**         |                                               | Different functionality in each port.                                                |
| PCSR                                       | Port n Pin Controller Select Register         |                                                                                      |
| **Port Output Register**                   |                                               | Determines value of GPIO pin when it's selected by Pn_IOCRx as output.               |
| OUT                                        | Port n Output Register                        | Bit field [15:0] represents each port.                                               |
| **Port Output Modification Register**      |                                               | Set, clear, or toggle logic state of single port line by manipulating output reg.    |
| OMR                                        | Port n Output Modification Register           | Bit field [15:0] for setting, toggeling Pn_OUT. Bit field [31:16] for clearing togeling Pn_OUT.                                                                                                                                                                    |
| OMSR                                       | Port n Output Modification Set Register       | Sets logic state of a single port line in Pn_OUT. Bit field [15:0] for each port.    |
| OMSR0                                      | Port n Output Modification Set Register 0     | Sets logic state of Pn.[3:0] port lines in Pn_OUT.                                   |
| OMSR4                                      | Port n Output Modification Set Register 4     | Sets logic state of Pn.[7:4] port lines in Pn_OUT.                                   | 
| OMSR8                                      | Port n Output Modification Set Register 8     | Sets logic state of Pn.[11:8] port lines in Pn_OUT.                                  |
| OMSR12                                     | Port n Output Modification Set Register 12    | Sets logic state of Pn.[15:12] port lines in Pn_OUT.                                 |
| OMCR                                       | Port n Output Modification Clear Register     | Clears logic state of a single port line in Pn_OUT. Bit field [31:16] for each port. |
| OMCR0                                      | Port n Output Modification Clear Register 0   | Clears the logic state of Pn.[3:0] port lines in Pn_OUT.                             |
| OMCR4                                      | Port n Output Modification Clear Register 4   | Clears the logic state of Pn.[7:4] port lines in Pn_OUT.                             |
| OMCR8                                      | Port n Output Modification Clear Register 8   | Clears the logic state of Pn.[11:8] port lines in Pn_OUT.                            |
| OMCR12                                     | Port n Output Modification Clear Register 12  | Clears the logic state of Pn.[15:12] port lines in Pn_OUT.                           |
| ESR                                        | Port n Emergency Stop Register                | Bit field [15:0] represents each port.                                               |
| **Port Input Register**                    |                                               | Logoc level of a GPIO pin can read in this reg.                                      |
| IN                                         | Port n Input Register                         | Bit field [15:0] represents each port.                                               |
| **Access Protection Registers**            |                                               | Controls write access for transactions w/ on chip master bus TAG ID #-# .            |
| ACCEN0                                     | Port n Access Enable Register 0               | EN0 -> TAG ID 000000B, EN1 -> TAG ID 000001B , ... ,EN31 -> TAG ID 011111B.          |
| ACCEN1                                     | Port n Access Enable Register 1               | Read as 0, written as 0.                                                             |
</details>

<details>
<summary><b>GPIO BLOCK DIAGRAM & TABLE OF PCx CODING</b></summary>

![Picture of GPIO Block Diagram](Images/Block_Diagram_Gpio.png)  
* ***Pn_IOCR register:*** Switches between input & output mode.  
Controls driver type of output driver and determines if internal weak pull-up, pull-down, or w/o input pull device is connected to input pin.  
* SW can set, clear, toggle bit in Pn_OUT through Pn_OMSR, Pn_OMCR, Pn_OMR, or just through Pn_OUT itself.
* If pin is configured as output, alternate output lines (ALT1 - ALT7) can be switched.
* Data written to Pn_OUT by SW can be used as input data to on-chip peripheral.
* When output line selected as general-purpose output line, logic state of each port pin can be changed individually by programming bits in the Pn_OMSR, Pn_OMCR, or Pn_OMR -> Pn_OUT.  
Logic level at pin can be examined by reading Pn_IN.

Describes the coding of the *PCx* bit fields in *Pn_IOCR* registers that determine the port line functionality.  
![Table of PCx Coding](Images/Pcx_coding.png)  
**tri-state:** logic LOW, logic HIGHT, & a high im
**input pull up:** Input pin pulled HIGH by default.
**input pull down:** Input pin pulled LOW by default.
</details>

<details>
<summary><b>FEATURES</b></summary>

* Controls up to 16 port lines.
* Multiplexes up to 7 alternate functions to each output.
* Controls pad characteristics of assigned pads like drive strength, slew rate, pull-up/down, push/pull, or open-drain operation, selection of TTL or CMOS/automotive input levels.  
* More on page 1251.
</details>


*Infineon Aurix TC3xx Part 1 UM - Chap 14*  
**port module (port slice):** Controls set of assigned GPIO port lines which are connected to **pads** connected to device **pins/balls**.  
**pad:** Configured as input (default after reset) or output.  
**input mode:** Function of input line from from pin to register *Pn_IN* & to peripheral is independent of whether port pin operates as input or output. 
Meaning that if port is in output mode, level of pin can read by software via *Pn_IN* or peripheral can use pin level.  
**output mode:** Output driver activated & drives value supplied through multiplexer to port pin.  
**low-voltage differential signaling (LVDS) port operation:** Offer RX or TX functionality for a pair of pins.  
**system connectivity of ports:** Described in the pinning tables in the datasheet (TC38x_DS_06.pdf).  
**control of port lines by peripheral (HW_DIR/HW_EN/HW_OUT):** Peripheral takes control of output path of port line. When peripheral activates HW_EN its data line connected to HW_OUT is driven to output driver.



