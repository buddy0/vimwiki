# QUEUED SYNCHRONOUS PERIPHERAL INTERFACE (QSPI)
*Infineon Aurix TC3xx Part 2 UM - Chap 29*

<details>
<summary><b>FEATURES</b></summary>
* Master and Slave mode operation 
    * Full-duplex, half-duplex, automatic slave select control, four-wire & three-wire type connection
* Flexible data format
* Baud rate generation
* Interrupt generation
* QSPI supports control & data handling by the DMA controller
* Flexible QSPI pin configuration
* HW support parity mode
* Seven slave select inputs SLSIB...H in Slave Mode
* 16 programmable slave select outputs SLSO[15:0] in Master mode
* Several modular reset options.
* Loop-back mode.
* Communication stop on RxFIFO full
* High Speed Input Capture (HSIC)
</details>

<details>BLOCK DIAGRAMS</b></summary>

</details>

<details>
<summary><b>Registers</b></summary>

</details>

* **QSPI:** Provide synchronous serial communication w/ external devices using clock, data-in, data-out, & slave select signals.
* ****external signals:***
    * Serial clock SCLK
    * Data in master to slave direction MTSR (Master Transmit Slave Receive)
    * Data in slave to master direction MRST (Master Receive Slave Transmit)
    * Slave select SLS
* ***2 operating modes regarding serial clock & SSL signals:*** Master mode wich generates & drives clock signals & Slave mode which receives these signals.  
* ***3 opearting modes regarding direction of communication:*** Full Duplex, Half Duplex, Simplex. Thus, 6 operating modes: 3 for master, 3 for slave.
* ***queue support:*** Describes the functionality implemented for comfortable switching of the timing configuration of the QSPI frames, depending on SSL signal.
    * The QSPI module expects 32 basic configuration bits to be moved with one move (for example DMA move) from some on-chip general purpose RAM to the TXFIFO.
