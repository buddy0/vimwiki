# SYSTEM TIMER (STM)
*Infineon Aurix TC3xx Part 2 UM - Chap 27*

<details>
<summary><b>BLOCK DIAGRAMS</b></summary>

![Picture of STM Block Diagram](Images/Block_Diagram_Stm.png)  
![Picture of Compare Mode Operation](Images/Stm_Compare_Mode_Operation.png)  
![Picture of Compare Match Interrupt Control](Images/Stm_Compare_Match_Interrupt_Control.png)  
</details>

<details>
<summary><b>FEATURES</b></summary>

* Free running 64-bit counter.
* All 64 bits can be read synchronously
* Flexible service request generation based on compare match w/ partial STM content.
* Counting starts automatically after an Application Reset.
* STM registers are reset by an Application Reset if bit ARSTDIS.STMxDIS is cleared. If bit ARSTDIS.STMxDIS is set, the STM registers are not reset by application reset.
</details>

<details>
<summary><b>REGISTERS</b></summary>

| short name   | long name                          |
| ------------ | ---------------------------------- |
| CLC          | Clock Control Register             |
| ID           | Module Identification              |
| TIMx (x=0-6) | Timer Register x                   |
| CAP          | Timer Capture Register             |
| CMPx (x=0-1) | Compare Register x                 |
| CMCON        | Compare Match Control Register     |
| ICR          | Interrupt Control Register         |
| ISCR         | Interrupt Set/Clear Register       |
| OCS          | OCDS Control and Status Register   |
| ACCEN0       | Access Enable Register 0           |
| ACCEN1       | Access Enable Register 1           |
| KRST0        | Kernel Reset Register 0            |
| KRST1        | Kernel Reset Register 1            |
| KRSTCLR      | Kernel Reset Status Clear Register |
</details>

<details>
<summary><b>ACRONYM</b></summary>

| acronym | description             |
| ------- | ----------------------- |



</details>

* **STM:** Designed for global system timing applications requiring both high precision & long period.  
    * Upward counter, running at frequency Fstm.
    * Not possible to affect content of timer during normal operation. Timer registers can only be read.
    * Can be disabled or suspended.
    * Due to its 64-bit width, not possible to read entire content w/ one instrucion. Needs to be read by 2 load instructions.  
    To enabele synchronous & consistent reading, CAP is implemented which lataches hight part of STM each time when one of the regisers TIM0 to TIM5 is read.  
    Thus, CAP holds upper value of timer at exactly same time when lower part is read. 2nd read operation would get value of CAP to get complete timer value.
    * Can be read in sections from 7 registers, TIM0-TIM6 which can be viewed as individual 32-bit timers each w/ different resolution & timing range.
* ***compare mode operation:*** Content can be compared against 2 compare values stored in CMP0 & CMP1. Service requests can be generated on complete match.
* ***compare match interrupt control:*** Each CMPx has its own interrupt request flag that is set by HW on complete match event. Flag can also be set and cleared by SW. Setting ICR.CMPxIR by writing 1 to ICR.CMPxIRS does *not* generate an interrupt at STMIRx. They can be further directed by ICR.CMPxOS to either STMIR0 or STMIR1.
* ***using multiple STMs:*** Available b/c of multiple CPUs. All modules are connected & controlled by Fstm. 
