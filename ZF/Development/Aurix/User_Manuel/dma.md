# DIRECT MEMORY ACCESS (DMA)
*Infineon Aurix TC3xx Part 1 UM - Chap 18*

<details>
<summary><b>BLOCK DIAGRAMS</b></summary>

![Picture of DMA Block Diagram](Images/Block_Diagram_Dma.png)  
![Picture of DMA Channel Request Control](Images/Dma_Channel_Request_Control.png)   
</details>

<details>
<summary><b>FEATURES</b></summary>

* Resource partitions.
* DMA channels.
* Double buffering options.
* DMA Linked List.
* DMA Channel Request Control.
* Move Engine.
* DMA On Chip Bus Switch.
* Interrupt Triggers.
* Operating frequencies.
</details>

<details>
<summary><b>ACRONYM</b></summary>

| acronym | description             |
| ------- | ----------------------- |
| TCS     | Transaction Control Set |
| ME      | Move Engine             |
| RP      | Resource Partition      |
</details>

* **DMA:** Moves data from a source module to destination module w/o CPU intervention.  
* ***DMA data move:*** Controlled by the TCS of an active DMA channel excuted by a ME. DMA channel activated by DMA request. 
* ***configration interface:*** FPI interface on the SPB bus. Supports single data transfers & does not support block transfers.  
* ***resource partition:*** Each DMA channel assigned a RP. Each RP has its own access enable protection. Each RP has unique master tag identifier driven on to on chip bus during DMA read or write move.  
* ***DMA channels:*** Each DMA channel assigned a RP & stores context of independent DMA operation. Each channel has a DMA channel request control (following requests are possible: DMA SW request, DMA HW request, DMA daisy chain request, & DMA auto start request). DMA channel status flag TSR.CH indicates if DMA request is pending
