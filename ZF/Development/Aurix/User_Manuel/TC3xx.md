# TC3xx UM

## PART 1
**9.** [System Control Unit (SCU)](scu.md)  
**10.** [Clocking System](clocking_system.md)  
**14.** [General Purpose I/O Ports and Peripheral I/O Lines (Ports)](gpio.md)  
**15.** [Safety Management Unit (SMU)](smu.md)  
**16.** [Interrupt Router (IR)](ir.md)  
**18.** [Direct Memory Access](dma.md)

## PART 2
**27.** [System Timer (STM)](stm.md)    
**29.** [Capture/Compare Unit 6 (CCU6)](ccu6.md)  
**32.** [Enhanced Versatile Analog-to-Digital Converter (EDSADC)]  
**36.** [Asynchronous/Synchronous Interface (ASCLIN)]  
**37.** [Queued Synchronous Peripheral Interface (QSPI)](qspi.md)  
**39.** [Single Edge Nibble Transmission (SENT)]  
**48.** [Input Output Monitor (IOM)]  
