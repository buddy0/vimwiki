# SYSTEM CONTROL UNIT 
*Infineon Aurix TC3xx Part 1 UM - Chap 9*  
**System Control Unit (SCU):** Cluster of sub-modules controlling various functions. Modules include:
* Reset Control Unit (RCU)
* Trap Generation (TR)
* System Registers for miscellaneous functions (SRU)
* Watchdog Timer (WDT)
* External Request Handeling (ERU)
* Emergency Stop (ES)
* Power Management Control (PMC)

<details>
<summary><b>WATCHDOG TIMER (WDT)</b></summary>

<details>
<summary><b>REGISTERS</b></summary>

| mnemonic              | name                                    |
| --------------------- | --------------------------------------- |
| `WDTSCON0`            | Safety WDT Control Register 0           |
| `WDTCPUyCON0` (y=0-2) | CPUy WDT Control Register 0             |
| `WDTSCON1`            | Safety WDT Control Register 1           |
| `WDTCPUyCON1` (y=0-2) | CPUy WDT Control Register 1             |
| `WDTSSR`              | Safety WDT Status Register              |
| `WDTCPUySR` (y=0-2)   | CPUy WDT Status Register                |
| `EICON0`              | ENDINIT Global Control Register 0       |
| `EICON1`              |  ENDINIT Global Control Register 1      |
| `EISR`                | ENDINIT Timeout Counter Status Register |
| `SEICON0`             | Safety ENDINIT Control Register 0       |       
| `SEICON1`             | Safety ENDINIT Control Register 1       |
| `SEISR`               | Safety ENDINIT Timeout Status Register  |       

</details>

* **watchdog timer, computer operating properly (COP):** Detects and recovers from HW or SW failure. Watches over uC. 2 kinds:
    * ***Safety Watchdog Timer:*** Overall system level watchdog. 
    * ***CPU Watchdog Timers:*** Monitor separate CPU execution threads. 
* ***servicing WDT:*** Prevents timer overflow avoiding an alarm. 
* ***2 steps:*** *Valid Password Access* followed by a *Modify Access*. Automatically switches WDT to Time-Out Mode.    
    * ***requirement:*** During next Modify Access, `WDTxCN0.ENDINIT` is written w/ 1.  
* ***successful sercive:*** WDT ends Time-Out mode switching back to its former mode of operation.  
* ***failed service:*** TriCore is reset.  
</details>

<details>
<summary><b>EXTERNAL REQUEST UNIT (ERU)</b></summary>

<details>
<summary><b>REGISTERS</b></summary>

| mnemonic        | name                              |
| --------------- | --------------------------------- |
| `EIFILT`        | External Input Filter Register    |
| `EICRi` (i=0-3) | External Input Channel Register i |  
| `EIFR`          | External Input Flag Register      |
| `FMR`           | Flag Modification Register        |
| `PDRR`          | PAttern Detection Result Register |
| `IGCRj` (j=0-3) | Flag Gating Register j            |
</details>

* **ERU:** Event & pattern detection unit. 8 channels.
* ***task:*** Generation of interrupts based on selectable trigger events (e.g. edge on an input pin)
* ***ERU split into 3 main groups:***  
    * **1.** Input Channels x
        * ***REQxy Digital PORT Input Glitch Filter (FILT):*** Calculates the area of a signal. Only available on the REQxy inputs coming directly from PORTS.
        * **External Request Selection Unit (ERSx):** Per input channel, selects 1 out of 6 possible available inputs.  
          `EICRy.EXISx` (y=0-3, x=0-1 thus 4 regs containing 2 channels each)
        * **External Trigger Logic Unit (ETLx):** Per input channel, allows the defintion of the transition that lead to a trigger event & can store status.  
           Based on an edge detection block.        
            ***pattern detection capility of all OGUz:*** Output of `EIFR.INTFx` is sent to all OGUs in parrellel 
            * If edge is detected, status flag `EIFR.INTFx` becomes set cleared if opposite event is detected (only if `EICRyLDENx` is set).   
            ***trigger actions in one of the OGU:*** Target is selected by `EICRy.INPx`
            * If `EICRy.EIENx` is set, then a trigger pulse output can be used.  
            * Trigger becomes active when edge is detected. 
    * **2.** Connecting Matrix
        * ***Connecting MAtrix:*** Distributes trigger signals (TRxy) & status signals (EIFR.INPFx) ffrom different ETLx between OGUy units.  
    * **3.** Output Channels y. Generates 4 ouput signals.  
        * **Output Gating Unit (OGUy):** Per output channel, combines trigger events & status flags from the input channels to the system.  
        1. ***trigger combination:*** All TRxy that are enabled & directed to OGUy & pattern change even (if enabled) are logically OR-combined. 
        2. ***pattern detection:*** Status flags `EIFR.INTFx` can be enabled to take part in pattern detection.  
        * ***output signals:***  
            * **`ERU_PDOUTy`:** Directly outputs pattern match information in defined target modules.  
            * **`ERU_GOUTy`:** Output pattern match or miss info.  
            * **`ERU_TOUTy`:** Combo of peripheral trigger, pattern detection result change event, or ETLx trigger outputs to trigger actions in other modules
            * **`ERU_IOUTy`:** Gated trigger output to trigger interrupts.  
</details>

<details>
<summary><b>TRAP GENERATION (TR)</b></summary>

<details>
<summary>REGISTERS</summary>

 | mnemonic | name                    |
 | -------- | ----------------------- |
 | TRAPSTAT | Trap Status Register    |
 | TRAPSET  | Trap Set Register       |
 | TRAPCLR  | Trap Clear Register     |
 | TRAPDIS0 | Trap Disable Register 0 |
 | TRAPDIS1 | Trap Disable Register 1 |
</details>
</details>



