# CLOCKING SYSTEM

<details>
<summary><b>REGISTERS</b></summary>

| type/mnemonic                           | name                                    |
| --------------------------------------- | --------------------------------------- |
| **Oscillator Ciccuit control Register** |                                         |
| OSCON                                   | Oscillator Circuit Control Register     |
| **System PLL Registers**                |                                         |
| SYSPLLSTAT                              | System PLL Status Register              |
| SYSPLLCON0                              | System PLL Configuration 0 Register     |
| SYSPLLCON1                              | System PLL Configuration 1 Register     |
| **Peripheral PLL Registers**            |                                         |
| PERPLLSTAT                              | Peripheral PLL Status REgister          |
| PERPLLCON0                              | Peripheral PLL Configuration 0 Register |
| PERPLLCON1                              | Peripheral PLL Configuration 1 Register |
| **CCU Registers**                       |                                         |
| CCUCON0                                 | Clock Control Register 0                |
| CCUCON1                                 | Clock Control Register 1                |
| CCUCON2                                 | Clock Control Register 2                |
| CCUCON5                                 | Clock Control Register 5                |
| CCUCON6                                 | Clock Control Register 6                |
| CCUCON7                                 | Clock Control Register 7                |
| CCUCON8                                 | Clock Control Register 8                |
| CCUCON9                                 | Clock Control Register 9                |
| CCUCON10                                | Clock Control Register 10               |
| CCUCON11                                | Clock Control Register 11               |
| **Clock Output Control Register**       |                                         |
| EXTCON                                  | External Clock Control Register         |
| FDR                                     | Fractional Divider Register             |
| **Clock Monitor Registers**             |                                         |
| CCUCON3                                 | Clock Control Register 3                |
| CCUCON4                                 | Clock Control Register 4                |
</details>

*Infineon Aurix TC3xx Part 1 UM - Chap 10*  
**clocking system:** Invloves different building blocks:  
* Basic Clock Generation (clock source)
* Clock Speed-Up Scaling (PLL)
* Clock Distribution (CCU)
* Individual Clock Configurations (Peripherals)

![Picture of Clocking System](Images/Clocking_System.png)

## CLOCK SOURCES
1. **Oscillator Soure**: The oscillator circuit, Pierce Oscillator, designed to work w/ both external crystal/ceramic resonator or external stable clock source.
Circuit consists of an inverting amplifier w/ *XTAL1* as input & *XTAL2* as output & an inverting feedback resistor. [difference between crystal & ceramic](https://ecsxtal.com/ecs-electronic-component-new-product-announcements/334-quartz-crystals-vs-ceramic-resonator) 
2. **Back-up Source** 

## PLL
**Phase Locked-Loop (PLL):** Needed b/c typical CPU operating speeds are 10x the frequency of osc freq. There are 2 PLLs available. Both can convert a low freq external clock signal to a high speed internal clock for max. performance. 
1. **System Phase Locked-Loop (System PLL) Module:**  
![Block Diagram of System PLL](Images/Block_Diagram_System_Pll.png)
2. **Peripheral Phase Locked-Loop (Peripheral PLL) Module:**  
![Block Diagram of Peripheral PLL](Images/Block_Diagram_Peripheral_Pll.png)

## CCU / ?PERIPHERALS?
**clock Control Unit (CCU):** Receives the clocks that are created by the oscillator, backup, & the 2 PLLs (PLL0 & PLL1/2). 
![Block Diagram of Clock Control Unit](Images/Block_Diagram_Clock_Control_Unit.png)  

## CGU
**Clock Generation Unit (CGU):** Involves the basic clock generation, 2 PLLs, & the CCU. 
![Diagram of Clock Generation Unit](Images/Block_Diagram_Clock_Generation_Unit.png)  

