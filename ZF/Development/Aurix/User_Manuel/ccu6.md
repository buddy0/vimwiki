# CAPTURE/COMPARE UNIT 6 (CCU6)
*Infineon Aurix TC3xx Part 2 UM - Chap 29*

<details>
<summary><b>FEATURES</b></summary>

</details>

<details>BLOCK DIAGRAMS</b></summary>

</details>

<details>
<summary><b>Registers</b></summary>

</details>

* **CCU6:** High Resolution 16-bit capture and compare unit w/ application specific modes, mainly for AC drive control.
    * Consists of 2 identical kernals CCU60 & CCU61. 
