# INTERRUPT SYSTEM

* Sources such as peripherals or external inputs can generate an interrupt signal to the CPU to request for service.

* Additional units (aka service providers) such as second CPU, DMA unit, or a Peripheral Control Processor (PCP) can handle the interrupt requests (aka service requests).

* The main CPU and upto 3 additional service providers can handle an interrupt Service Request Node (SRN).

* Each interrupt or service request from a module connects to a *Service Request Node*, containing a *Service Request Control Register (SRC)*.
Interrupt arbitration busses conntect SRNs w/ Interrupt Control Units (ICUs) of service providers.

![Picture of Interrupt System](Images/interrupt_system.png)

* **Service Request Node:** Each one contains SRC & logic for commincation w/ requesting source module & interrupt arbitration busses.  
4 CPU SRNs for SW interrupts which can only be set by SW.

* **SRC:** Holds the control bits to enable or disable the request, assign a priority number, direct request to one of the service providers. 
Each request can be set or reset through SW.

    * **Service Request Flag (SRR):** Directly set or reset by associated HW. Ex. trigger event.  
    Can be set or cleared (HW or SW)

        * SRE == 1, pending service request takes part in the interrupt arbitration of the service provider via TOS bit field.  
        Bit SRR auto reset by HW when service request is acknowledged & serviced.

        * SRE == 0, pending service request is excluded from interrupt arbitrations.  
        Bit SRR must be reset by SW (write 1 to CLRR).
        
    * **Service Requeust Enable Control (SRE):** Controls whether an active interrupt request is passed to the designated interrupt service provider.

    * **Type-of-Service Control (TOS):** Selects service provider for request. Interrupt system can manage up to 4 Service Providers.
    
    * **Serice Request Priority Numebr (SRPN):** Indicates priority.  Permits upto 255 sources.

* **ICU:** Manages interrupt system, arbitrates incoming interrupt requests to find one w/ highest priority, & determines whether or not to interrupt service provider.

* **ICR:** Holds *Current CPU Priority Number (CCPN)*, *global interrupt enable/disable bit (IE)*, & *Pending Interrupt Priority Number (PIPN)*.

* Entering & Exiting Interrupt Service Routine (ISR): Page 68-69. Exits w/ Return From Exception (RFE) instruction. HW auto restores upper context

* **Interrupt Vector Table (IVT):** Array of ISR entry points.  
Interrupt Vectors (IVs) ordered by increasing priority.  
Possible have multiple IVTs.  
**Base of Interrupt Vector Table (BIV) register:** Stores base address into IVT. 
    * "When interrupted, CPU calaculates entry point of ISR from PIPN & contents of BIV register. PIPN is:  
        * left-shifted by five bits 
        * ORed w/ address in the BIV register
        ![Picture of BIV](Images/BIV_or_IVT_Address.png)  
        
![Picture of Interrupt Vector Table](Images/Interrupt_Vector_Table.png)  

* "When the CPU takes an interrupt, it calculates an address in the Interrupt Vector Table that corresponds w/ the priority of the interrupt (ICR.PIPN bit field). This address is loaded into the PC. The CPU begins executing instructions at this address in the Interrupt Vector Table."  

* **Interrupt Service Routine (ISR):** Program related to the interrupt in memory. The ISR address is the interrupt vector.  

## SPANNING INTERRUPT SERVICE ROUTINES ACROSS VECTOR ENTRIES
* Elimnates need of a jump to rest of interrupt handler if it would not fit into the available 8 words between entry locations.

## INTERRUPT PRIORITY GROUPS
* Set of interrupts which cannot interrupt each others service routine.

## INTERRUPT CONTROL REGISTERS
2 CSFRs support interrupt handling:
* **Interrupt Control Register (ICR)**
* **Base Interrupt Vector Table  Pointer (BIV)**
