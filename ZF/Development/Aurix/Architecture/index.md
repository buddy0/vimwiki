# ARCHITECTURE - [pdf](Images/tc1_6__architecture_vol1.pdf)

1. [Overview](Overview.md)
2. [Programming Model](Programming_Model.md)
3. [General Purpose & System Registers](General_Purpose_System_Registers.md)
4. [Tasks & Functions](Tasks_Functions.md)
5. [Interrupt System](Interrupt_System.md)
6. [Trap System](Trap_System.md)
9. [Memory Protection System](Memory_Protection_System.md)
