# PROGRAMMING MODEL

## MEMORY MODEL
* 4GB of memory w/ address width of 32 bits.
* Address space divided into 16 segments (0-F), represented by four uppermost bits.
* ![Picture of Physical Address Space](Images/Physical_Address_Space.png)  
* ![Picture of Byte Ordering](Images/Byte_Ordering.png)  
* ![Picture of Mutlibyte Units](Images/Multiple_Byte_Units.png)  
* ![Picture of Memory Example](Images/Memory_Example.png)  
