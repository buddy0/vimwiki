# MEMORY PROTECTION SYSTEM
* Following subsystems are involved w/ Memory Protection
    * Trap System
    * I/O Privilege Level
    * Memory Protection: Provides control over which regions of memory a task is allowed to access & what types are permitted
        * **range based:** Designed for small & low cost applications that do not require virtual memory.
        * **page based:** Require virtual memory, optional Memory Management Unit (MMU) supports this.

## EFFECTIVE ADDRESSES
* **effective addresses:** Translated into phyical addresses using:  
    * direct translation
    * **Page Table Entry (PTE):** (optional MMU only)



