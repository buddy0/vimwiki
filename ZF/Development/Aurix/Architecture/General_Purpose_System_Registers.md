# GENERAL PURPOSE AND GENERAL SYSTEM REGISTERS

***2 types of core registers:*** **1.** GPSRs: 32 (16 data & 16 address), **2.** CSFRs.  
**system global registers:** Registers A[0], A[1], A[8], A[9] contents are not saved or restored across traps, function calls, or interrupts.  
***program state information registers:***  
* **PC (Program Counter):** holds address of the instruction currently running.
* **PSW (Program Status Word):** Task-specific architectural state.
* **PCXI (Previous Context Information):** Contains linkage information to previous execution context.
 

## ENDINIT PROTECTED
Architecture supports initialization state prior to operational state.  
**ENDINIT protected:** CSFRs that are only writable in the initialization state. Can be modified using the MTCR instruction.  

## STACK MANAGEMENT - <span style="color:red">GO OVER</span>
***stack management is done through:***  
* **A[10] (SP):** Initial contents set by RTOS when task is created, allowing private stack area to be assigned to individual stacks. 
* **Interrupt Stack Pointer (ISP):** Helps prevent ISRs from accessing private stack areas & possibly interfering w/ SMTs. Automatic switch to the use of the ISP instead of the private stack pointer is implemented in the architecture. 
* **PSW IS field (represents 2 stack types):**  
    * **0** for user stack
    * **1** for shared global interrupt stack.

### EXAMPLE
*If* an interrupt is taken & interrupted task was using its ...  
* private stack (PSW.IS == 0), *then* contents are saved w/ upper context of the interrupted task & A[10] (SP) is loaded w/ current contents of the ISP.
* interrupt stack (PSW.IS == 1), *then* no pre-loading of A[10] (SP). ISR continues to use interrupt stack at point where IR has left it.

## QUESTIONS
<details>
<summary><b>Q:</b> If PC holds the current instruction to be executed, how does it find out the next instruction?</summary>

**A:** ?
</details>

<details>
<summary><b>Q:</b> Why have a private task stack, global stack, interrupt stack?</summary>

**A:** ?
</details>







