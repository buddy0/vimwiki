# OVERVIEW
<details>
<summary><b>TRICORE ARCHITECTURE</b></summary>

* 32-bit uC
* Little-endian byte ordering for data memory & CPU
* ***Instruciton Set Architecture (ISA) consists:***  
1. real-time capability of uC 
2. computational power of DSP
3. RISC load/store
 registers (LSB at the lowest address)
</details>

<details>
<summary><b>REGISTERS</b></summary>

* 32 GPRs, PC, 2 status flags. Also, have CSFRs. 
* ***lower registers:*** Reg [0-7]  
* ***upper registers:*** Reg [8-15]  
![Picture of Architecural Registers](Images/Registers.png)  
* ***note:*** Regs A[0], A[1], A[8], A[9] are not included in either upper or lower context & are not saved or restored across calls or interrupts. These regs are normally used by OS to reduce overhead.  
</details>

<details>
<summary><b>DATA TYPES</b></summary>

* boolean, bit string, byte, signed fraction, address, signed & unsigned integers, IEEE-754 single-precision floating point format.  
</details>

<details>
<summary><b>MEMORY MODEL</b></summary>

* 4 GBytes.
* Address space is divided into 16 regions/segments each of 256 MBytes. Upper four bits of an address selects specific segment. 
</details>

<details>
<summary><b>ADDRESSING MODES</b></summary>

* absolute, base + short offset, base + long offset, pre-increment, post-increment, circular, bit-reverse
* Data is stored in **little-edian byte order:** least significant bytes are at lower addresses.  
***Little Endian Byte Order:*** The least significant byte (the "little end") of the data is placed at the byte with the lowest address.  
***Big Endian Byte Order:*** The most significant byte (the "big end") of the data is placed at the byte with the lowest address.  
</details>

<details>
<summary><b>TASKS & CONTEXTS</b></summary>

**task:** Independent thread of control. 2 types:
1. **Software Managed Tasks (SMTs):** Created through services of real-time kernel or OS & dispatched under the control of scheduling SW. Each task has a mode:  
**User-0 Mode:** Tasks that don't access peripheral devices. Mode cannot enable or disable interrupts.  
**User-1 Mode:** Tasks that access common, unprotected peripherals. Mode disable interrupts for a short time.  
**Supervisor Mode:** Permits r/w access to system registers and all peripheral devices. Mode may disable interrupts.  
  
2. **Interrupt Service Routines (ISRs):** Dispatched by HW in response to an interrupt. Code invoked directly by the processor on receipt of an interrupt.

**context:** Everything processor needs to define state of associated task: CPU general registers, task's PC, & its Program status info. (PCXI & PSW). Supdivided into upper & lower context.  
**Context Save Areas (CSAs):** Consists of 16 words of memory storage aligned on 16 word boundary. Each holds exactuly 1 upper or lower context. Made up of linked lists & linked together by a linked word.  
**context switching:** Occurs when an event or instruction causes break in program execution: interrupt service requests, traps, & function calls.  
</details>

<details>
<summary><b>INTERRUPT SYSTEM & PRIORITY</b></summary>

* Built around Service Request Nodes (SRNs).  
* **service request:** Interrupt request or DMA request. 
* Jump to vectors in code memory. Entry code for ISR is block w/in vector of code blocks.
* May come from:
1. on chip peripheral
2. external HW
3. SW.  

* Service requests are prioritized. Every service request assigned SRPN. Also, all ISRs have a priority number.  
* Max. # of interrupt sources is 255, thus 255 priority levels.  
* Rules:  
    * Service request can interrupt servicing of lower priority interrupt.
    * Interrupt soruces w/ same priority cannot interrupt each other.
    * Interrupt Control Unit (ICU) determines which source will win based on priority number.
</details>

<details>
<summary><b>TRAP SYSTEM</b></summary>

* **trap:** Occurs as a result of an event such as: (NMI), instruction exception, illegal access, etc. 
* They can be synchronous, asynchronous, HW, or SW  
* Each trap is assigned Trap Identification Number (TIN) identifing trap w/in its class.
* Entry code for trap handler is comprised of a vector of code blocks. When trap is taken, TIN placed in data register *D[15]*. 
* 8 Trap classes:
1. Memmory Management Unit (MMU)
2. Internal Protection
3. Instruction Error
4. Context Management
5. System Bus & Peripherals
6. Assertion Trap
7. System Call
8. Non-Maskable Interrupt (NMI)
</details>

<details>
<summary><b>PROTECTION SYSTEM</b></summary>

* Composed into 3 main subsystems:
1. **Trap System**
2. **I/O Privilege Level:** User-0 mode, User-1 mode, & Supervisor mode. 
3. **Memory Protection System:** Controls which regions of memory task allowed to access.
</details>

## QUESTIONS
<details>
<summary><b>Q:</b> Difference between <b>trap</b> & <b>interrupt</b>?</summary>

**A:** [SO](https://stackoverflow.com/questions/3149175/what-is-the-difference-between-trap-and-interrupt)  
**trap:** Execption in a user process. Kind of like a CPU internal interrupt. **Ex.** Division by zero or invalid memory access.  
**interrupt:** generated by HW devices. **Ex.** Switch, button, overflow.  
</details>

<details>
<summary><b>Q:</b> What is <b>virtual memory</b>?</summary>

**A:** [TM](https://techmonitor.ai/what-is/what-is-virtual-memory-4929986)  
**virtual memory:** Enables computer to compensate shortages of physical memory by transferring pages of data from RAM to disk storage.  
Thus, when RAM runs low virtual memory can move RAM data into secondary storage such as a hard drive freeing up the RAM to complete the task.  
Similar to ***swap memory*** in linux.  
</details>

<details>
<summary><b>Q:</b> Difference between <b>MI</b> & <b>NMI</b>?</summary>

**A:** [G4G](https://www.geeksforgeeks.org/difference-between-maskable-and-non-maskable-interrupt/)  
**maskable interrupt (MI):** Can be enabled or disabled by instructions of the CPU.  
**nonmaskable interrupt (NMI):** Cannot be enabled or disabled by instructions of the CPU. Used for HW errors, etc.  
![Picture of MI Vs NMI](Images/Mi_Vs_Nmi.png)  
</details>
 




