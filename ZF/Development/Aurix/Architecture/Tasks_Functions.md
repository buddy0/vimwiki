# TASKS & FUNCTIONS
*  RTOS manages interrupt handlers & SMTs.  

**task:** Independent thread of control.  
***context:*** Task state. Context types:  
![Picture of Upper & Lower Context](Images/Upper_Lower_Context.png)    
**Context Save Areas (CSAs):** Contexts saved into memory occuping 16 word blocks of storage. The architecture uses linked lists where each CSA, whether upper or lower context, is linked together through a *Link Word*.  
* **Free context List (FCX):** Unused CSAs are linked together here.  
* **Previous Context List (PCX):** CSAs that contain saved upper or lower contexts are linked together here. 

## TASK SWITCHING
**task switching:** Saving (store) a context & restoring(loading) another.Occurs when there is an interrupt, trap, function call, etc.
* Upper context is saved automatically.
* Lower context is saved explicitly through instructions
![Picture of Context Save Areas](Images/Context_Save_Areas.png)

### CONTEXT SAVE & RESTORE
***context management registers:*** FCX, PCX, LCX
1. **Context Save:** The first CSA in the FCX (CSA 3) is pulled off & placed on the front of the PCX.  
![Picture Context Save](Images/Context_Save.png)  
![Picture Context Save 2](Images/Context_Save_2.png)  
![Picture Context Save Steps](Images/Context_Save_Steps.png)   
![Picture context Save After](Images/Context_Save_After.png)  

2. **Context Restore:** The first CSA in the PCX (CSA 3) is pulled off & placed on the front of the FCX. 
![Picture Context Restore Before](Images/Context_Restore_Before.png)  
![Picture Context Restore 2](Images/Context_Restore_2.png)  
![Picture Context Restore Steps](Images/Context_Restore_Steps.png)   
![Picture context Restoer After](Images/Context_Restore_After.png)  

## QUESTIONS
<details>
<summary><b>Q:</b> Difference between <b>SMTs</b> & <b>interrupts</b>?</summary>

**A:**  
**task:** Indepent thread of control, created by a real time kernel or OS. Dispatched by the control of a scheduler.  
**interrupt:** Break or in the program. Dispatched normally by a HW device.
</details>

<details>
<summary><b>Q:</b> Simply, what is <b>RTOS</b>?</summary>

**A:** [wiki](https://en.wikipedia.org/wiki/Real-time_operating_system), [G99](https://www.guru99.com/real-time-operating-system.html)  
**RTOS:** OS for real-time applications that processes data and events that have critically defined time constraints.  
</details>
