# IT

<details>
<summary><b>TABLE OF CONTETNS</b></summary>

1. [PERMISSION ISSUE](#permission_issue)  
    a. [C DRIVE PERMISSION FIX](#c_drive permission_fix)  
2. [UYOD](#uyod)
3. [LINKS](#links)
4. [ZIM CHANGE PASSWORD](#zim_change_password)
5. [UAC FIX](#uac_fix)
</details>

## PERMISSION ISSUE <a name="permission_issue"></a>

**Background Story:**  
Trying to install Cobra IDE into C:\ProgramFiles(x86). Admin rights username & password popped up. Putting my information worked fine. 
Win Zipping the file into that location created errors such has "unable to process *file_name*" and "corruption" occured on each file. However, Win Zipping to my Downloads or Documents folder worked fine. Therefore, the problem seemed to be something with the admin rights. Therefore, I created a folder named *sw* in my drive and did this:
1. Right click goto *properites*
2. Click on tab *security*
3. click on *Edit*
4. On Group or usernames click on *Users*
5. On Permission for SYSTEM check Allow for *Full control* & *Modify*

However, I accidentaly did this on my entire C: Drive, not on the folder *sw*. Tried changing it back but I can still create files on my C: drive. 
Called IT, and they said this was fine. Time will only tell.

**SOLUTION:** 
1. Just WinZip the zip file to Downloads folder then move to `C:\app\tools\cobra`. 
2. Create a folder named *sw* in `C:` drive. Right click, goto properties, click on security, and change User group to Full Control.

### C DRIVE PERMISSION FIX <a name="c_drive permission_fix"></a>
Needed for Vector.  
1. In File Explorer, right click on C: drive -> Properties -> Security -> Advance Settings
2. Modify Permissions. I had to Edit principal *SYSTEM* & add 2 principal Authenticated Users (in Principal section click on *Select a principal*, in Enter the object name to select type *Authenticated Users* -> click OK).  
3. Below reveals picture settings that were implemented:  
4. Click on Apply. Click *Continue* on window pop up *Security Error*.  

<details>
<summary><b>PICS</b></summary>

![C Drive SYSTEM Permission Entry](Images/SYSTEM_Permission_Entry.png)  
![C Drive Authenticated Users Permission Entry](Images/Authenticated_Users_Permission_Entry_1.png) ![C Drive Authenticated Users Permission Entry](Images/Authenticated_Users_Permission_Entry_2.png)  
![C Drive Advanced Security Settings](Images/Advanced_Security_Settings_for_Windows.png)  
</details>

## UYOD <a name="uyod"></a>

1. UYOD request permission: [Mobile Device Request Portal](http://mrf.america.zf-world.com/).  
2. [Request to add DIV ARU user to permission group for MobileIron](https://trw1.sharepoint.com/sites/site1808/IT%20Workplace%20HowTos/Add%20mobile%20user%20group%20in%20AURM.pdf).  
3. Mobile Iron Registration to download all the Work apps: Android_Onboarding_UYOD.pdf 
4. Register device for PingID: [PingID 2FA link 1](https://trw1.sharepoint.com/sites/site1808/IT%20Workplace%20HowTos/PingID%20%E2%80%93%20How%20to%20register%20your%20device%20for%20Two-Factor%20Authentication.pdf) [PingID 2FA link 2](https://trw1.sharepoint.com/sites/site87/Documents/User%20Manuals/PingID_DeviceRegistrationManual_EN.pdf)  
5. Set up Microsoft Authenticator.
6. Register for Microsoft 365 Access: [Microsoft](https://trw1.sharepoint.com/sites/site1808/IT%20Workplace%20HowTos/HowTo_Enrolling_Android_Azure+Updates%20EN.pdf)  
 
Main PDFS used: Android_Onboarding_UYOD.pdf, HowTo_Enrolling_Android_Azure+Updates EN.pdf

## LINKS <a name="links"></a>

* [IT Self Service Portal](https://zf-bmcp-dwp.emea.zf-world.com:9030/dwp/app/#/catalog)
* [Install SW](https://zf-bmcp-dwp.emea.zf-world.com:9030/dwp/app/#/srm/profile/SRGAA5V0GEUCIAQ4EGRUQ3GB7544FC/srm)
    * PTC Integrity, Beyond Compare, Notepad++, Google Chrome  
* [Administrative full control](https://zf-bmcp-dwp.emea.zf-world.com:9030/dwp/app/#/srm/profile/SRGAA5V0GEUCIAQ8STDEQ7U6PY3YO5/srm)
    * **Duration:** 1 year, **HW:** PC_Name, **Justification:** SW Developer & Special HW/SW, **Reason:** Admin rights for using PC as developer
* [CI Portal](https://ci-portal.zf.com/ciportal/en/applications/digital_media/zf_wallpaper/zf_wallpaper.html#text_with_image___gallery_192734)

## ZIM CHANGE PASSWORD <a name="zim_change_password"></a>
Login password was expiring, so had to change it using this [link](https://zim.zf-world.com/IdentityManagerPWPortal-ZFWorld/page.axd?RuntimeFormID=fb5a2c25-a802-4925-870b-f9583909dd59&aeweb_handler=p&aeweb_rp=&wproj=0&ContextID=QER_PasswordWeb_Session).  
Wait like 15 minutes then restart computer.  
Also, if Windows login reminder: Please lock this computer, then unlock it using you most recent password or smart card.  
Then, lock computer using "WIN key + L" then login using newly created password.  

<details>
<summary><b>ZIM Requests</b></summary>

1. Goto this [link](https://zim.zf-world.com/IdentityManager-ZFWorld/page.axd?RuntimeFormID=542248c4-f24a-4862-bb38-3d5fbd7bb5f6&aeweb_handler=p&aeweb_rp=&wproj=0&MenuID=Portal_IAM_MyIdentity&ContextID=VI_Start) -> Start a new request.  
2. **requests made:** `ZF-WORLD - LIV-MCS_SW_Tools-R` for EB100 Training videos.  
</details>

## UAC FIX <a name="uac_fix"></a>
1. Create a folder to house zip. I did `C:\Utils\UCA_fix`.
2. In `G:\Software\Ortbals\Windows 10 Stuff`, go into winzip for UCA_Fix.zip -> copy UCA_Fix.reg into folder created above.  
3. Click on windows key -> type Task Scheduler -> Right pane Actions -> Create Task  
Below are pictures to create the Task:  
<details>
<summary><b>PICS</b></summary>

![Creat Task General](Images/Task_Scheduler_Create_Task_General.png)  
![Create Task New Trigger 1](Images/Task_Scheduler_Create_Task_New_Trigger_1.png) ![Create Task New Trigger](Images/Task_Scheduler_Create_Task_New_Trigger_2.png) ![Create Task Triggers](Images/Task_Scheduler_Create_Task_Triggers.png)  
![Create Task Actions](Images/Task_Scheduler_Create_Task_Actions.png)  
![Create Task Conditions](Images/Task_Scheduler_Create_Task_Conditions.png)  
![Create Task Settings](Images/Task_Scheduler_Create_Task_Settings.png)  
</details>



