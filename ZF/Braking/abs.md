# ABS

## INTRO - [link](https://www.youtube.com/watch?v=irXpOfo4Y3E&list=PLNi9btYL4Cc8k0XcRlGrZkeGDe-CM8_S6&index=4&t=191s) 

***2 main brakes:*** **1.** Brake pedal for vechicle in motion. **2.** Parking brake for vechicle stationary.  
***principles of braking hydraulic system:*** **1.** Brake assemblies at the wheels. **2.** The hydraulic system that applies them. Hydraulic pressure is transmitted through *liquid* b/c it's *not* incompressable, thus no loss when pressure is applied.  
***divided systems:*** Safer if partial failure happens. Fluid loss in one half of the system still leaves other half able to hold the vechile braking, tho w/ an increase in stopping distance.  

## ABS 
**ABS (Anti-lock Braking System):** Prevents wheel locking or skidding no matter how hard brakes are applied or how slippery the surface. 
***advantages:*** Controled steering & stopping distances are reduced. 

### COMPONENTS
***brake pedal:*** Brake pedal uses leverage to transfer effort from drivers foot to the master cylinder.   
***master cylinder:*** Conencted to the brake pedal via a push rod.  
***brake lines:*** Carry brak fluid from the master cylinder to the brake assemblies. Made of steel attached to the body in white. Flexible lines are needed between body and suspension for steering and suspension movement.  
***brake fluid:*** Fluid to trasmit force from master cylinder to the wheel cylinders of the brake assemblies.  
***proportining valve:*** Adjust braking force to allow brake transfer.   
***power booster:*** Uses a vacuum to multiply the driver's pedal effort & apply that to the master cylinder, increasing the pressure availble for the master cylinder.   
***hydraulic control unit (hydraulic modulator) (HCU):*** Modular of the ABS to provide hydraulic pressure. Signals are sent here from the ABS ECU. This unit executes them by using solenoid valves connected in series w. master cylinder & brake circuits.  
***wheel speed sensors:*** Consist of a tooth rodor that rotates w/ the wheels and the pickup. As each tooth of the rodor passes the pickup a small voltage is induced in the pickup. These pulses are sent as signals to the ECU which processes them to operate the HCU.  
![Picture of Components](Images/abs_components.png)   

## BRAKING STEPS - ENTRY
1. Driver pushes on brake pedal applying force to the piston on the master cylinder.  
2. Piston applies pressure to the fluid in the cylinder.  
3. The brake lines transfer the pressur to the wheel cylinders.  
4. Wheel cylinders at the wheel assemblies apply the brakes.  

## BRAKINGSTEPS - INTERMEDIATE
1. Start of a journey, ABS auto checks itself. Any failure in system lights up a warning light onto the dash panel. 
2. Wheel speed sensor signals are *INPUT* to the ECU. 
3. When brakes are applied, the wheel speed changes sending a new signal to the ECU. 
4. If ECU detects a wheel might lock, it sends a signal to the HCU. In a 3 channel system the HCU uses 3 solenoid valves to control brake pressure & prevent locking. The valves are in series w/ brake master cylinder & brake circuits. 1 for each FW and 1 for th RWs.  

![Picture of ABS Example](Images/abs_example.png)  

## BRAKING STEPS - ADVANCED
1. Brake pedal forces are transmitted to the master cylinder then through the solenoid valve to the brake unit at the wheel. 
2. Signals from the wheel speed sensor show no tendancy for the wheel to lock up, therefore the ECU is *not* sending any control current to the solenoid coil. The Solenoid valve is *not* energized & the hydraulic pressure from the master cylinder is supplied to the brake unit at the wheel. 
3. When the sensor detects possible lock ups, the ECU sends a command current to the solenoid coil (part of the HCU) energizing it & causing the armature and valve to move upward isloating the brake circuit from the master cylinder. This keeps the brake pressure from the solenoid & brake circuit constant whether or not the master cylinder hydraulic pressure rises.  
4. If the wheel sensor still sensors wheel deacceleration, the ECU sends a larger current to the solenoid valve resulting in the armature to be moved further. This lowers the braking pressure and opens a passage from the brake circuit to the accumulator. A temp reservoir for any brake fluid that flows out of the wheel brake cylinders due to fall in pressure. A return pump sends this brake fluid back to the master cylinder. Pressure in the brake caliber circuit is now reduced.
5. If the wheel sensors indidcate that lowering brake pressure is letting wheel accelerate again, then ECU stops sending current to the HCU and deenergizes the solenoid valve. This lets the pressure increase making the wheel deaccelerate again.   

![Picture of ABS Example W/ 1 Wheel](Images/abs_example_one_wheel.png)  

***NOTE:*** During normal braking as the rotational wheel speed falls, no elctric current flows from the ECU to HCU, and the solenoid valve is not energized. The brake master cylinder hydraulic pressure is applied to the brake unit and the ABS is *not* involved. However, even tho ABS is passive during normal braking, its ECU is constanlty monitoring for rapid deacceleration from any of the wheels.  
