# SOLENOID
[video: Solenoid Basics Explained](https://www.youtube.com/watch?v=BbmocfETTFo&list=PLNi9btYL4Cc8k0XcRlGrZkeGDe-CM8_S6&index=6)

## MAGNETS
* A typical bar magnet has a North and South pole. Putting an iron nail close to the North pole will move the nail towards the magnet. However, the magnetic field can not be turned off, thus the nail will *only* deattach by phsyically removing the nail.  
* A compass will be affected by a magnets magnetic field.  
* Opposites attract & similar polar ends repel each other. Lines always form closed loops and run from N to the S.    
![Picture of Magnetic Field](Images/magnetic_field.png)  
* Problem with permanet magnets: always on and can't be turned off quickly or controlled.  
* However, we can control an electric magnetic field with standard wire. Placing a compass near a wire will have no effect. But, connecting a power source to each end of wire allows current flow which generats a magentic field changing direction of the compass. The electrical magnetic field is operating in a circular pattern around the wire.  
***NOTE:*** When current flows through a wire it creates a circular magnetic field around the wire.  
![Picture of Electrical Magnetic Field](Images/electrical_magnetic_field.png) ![Picture of Electrical Magnetic Field Reverse](Images/electrical_magentc_field_reverse.png)  
![Picture of Wire Magetic Field](Images/wire_magnetic_field.png)  
* Wrapping a wire into a coil will intesify the magnetic field. Conncting power supply to the coil and passing a current through it affects the compass by the N pointing at the end of the coil similar to a compass & bar magnet.  
***NOTE:*** When current flows throgh a coiled wire, each wire still produces a magnetic field, except the field lines will merge together creating a large magneic field. Tell which is N or S by using the right hand grip rule. Grip the solenoid and point thumb in direction of **Conventional Current Flow** (+ to -). Thumb points to N end &  current will be flowing in direction of your fingers.  
![Picture of Compass & Coil 1](Images/coil_compass1.png) ![Picture of Compass & Coil 2](Images/coil_compass2.png) ![Picture of Compass & Coil 3](Images/coil_compass3.png) ![Picture of Compass & Coil 4](Images/coil_compass4.png)    
![Picture of Coil Wire Magnetic Field 1](Images/coil_wire_magnetic_field1.png) ![Picture of Coil Wire Magnetic Field 2](Images/coil_wire_magnetic_field2.png) ![Picture of Right Hand Rule](Images/right_hand_rule.png) ![Picture of Magnetic Field Sources](Images/magnetic_field_sources.png)  

## SOLENOID - [What is a solenoid?](https://www.tlxtech.com/articles/solenoid-101-what-is-a-solenoid)
**solenoid:** Device comprised of a coil of wire, a housing, and a moveable plunger (armature) which converts electrical energy into mechanical work.  
When an electrical current is introduced, a magnetic field forms around the coil drawing the plunger in.  
### TYPES
**Latching:** Holds armature in fixed position w/o constant source of power with components such as permanent magnet and spring return.  
**On-Off:** Moves the armature when power is supplied. Coninuous power is required to hold armature in position. Removing power returns armature to original position via spring return.  
**Proportional:** Convert an electric signal into proportional mechanical force, allowing variance in armature position and/or force relative to current level. Spring return is needed to aid armature to original position.  

## SLOENOID VALVES - [How Solenoid Valves Work](https://www.youtube.com/watch?v=-MLGr1_Fw0c&list=PLNi9btYL4Cc8k0XcRlGrZkeGDe-CM8_S6&index=7&t=129s) 
**solenoid valves:** Allow engineers to automously & remotley control flow of fluid through a system with the help of a controller. The solenoid coil at top is used to oeprate the valve by passing an electrical current through it to create an electrical magnetic field. 
### TYPES
**NC (Normally Closed):** Plunger is pushed down by the spring sitting in a down position to close the valve indefintely.  
If the coil receives an electrically current, then it will generate an electrically magnetic field causing plunger to move upwards therefore opening the valve.  

**NO (Normally Open):** Plunger is pushed upwards by the spring sitting so that the valve is always open unless the solenoid coil is powered on.  
If the coil receives an electrically current, then it will generate an electrically magnetic field causing the the plunger to be pushed instead of pulled closing the valve.  
***NOTE:*** Direction of current flowing into the coil is what determines whether the coil produces a pulling/pushing force on the plunger.  








 
