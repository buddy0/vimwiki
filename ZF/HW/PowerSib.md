# POWERSIB

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INSTALL](#install)
2. [HW](#hw)
3. [CONFIGURE](#configure_example)
4. [MAIN DISPLAY](#main_display)
</details>

## INSTALL <a name="install"></a>
1. Goto `G:\Software\PowerSIB\Install\PowerSIB_ Install_121818.zip`.
2. Copy & paste zip to my local machine location `C:\Users\z0118277\Downloads\Installers`.
3. Goto `C:\Users\z0118277\Downloads\Installers\PowerSIB_ Install_121818` & double left click setup.exe.
4. Goto `G:\Software\Frantz\for_Others\for_Matthew`. Copy & paste folder `Configs_MY19` into `C:\Program Files (x86)\PowerSIB\Configs`

## HW <a name="hw"></a>
***rear panel run/prog switch:*** Default is run position. Prog posititon for updating PowerSIB firmware.

## CONFIGURE EXAMPLE <a name="configure_example"></a>
1. Launch PowerSib. Main menu goto *Configure* -> *PS System*.
2. PowerSib Configuration: `C:\Program Files (x86)\PowerSIB\Configs\GM_IBC_091718.psb`
3. Status Monitor Layout: `C:\Program Files (x86)\PowerSIB\Configs\GM_IBC_091718.ini`
4. Module: `GM_C1XX_Up`.
5. Click *Configure Device*.
6. Close the window *Configure*.
7. Main menu goto *View* -> *Main Display*.  

## MAIN DISPLAY <a name="main_display"></a>
***digital ctrl - vbatt switch:*** Turns on/off VBATT. Similar to taking the battery cables out of the battery.  
***digital ctrl - comm enable/ignition switch:***  Turns on/off ignition. Similar to pushing button or turning keys to turn on/off the ignition.  

***clr comm messages:***  
1. Select *Show Tx Ctrl*. Window *Tx Msg Ctrl* opens up.
2. Under section *CAN Analyzer Mode* select *Bus1*, *Bus2*, & *Bus3*. Select *Clear All*.
3. In window *CAN Msgs*, push button *Clr_Tx*.
