# MINIWIGGLER

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INSTALL](#install)
2. [ABOUT](#about)
3. [FLASHING FIRST TIME](#flashing_first_time)
4. [HSM](#hsm)
</details>

## INSTALL <a name="install"></a>
1. Goto website: `www.infineon.com/miniwriggler.com`.
2. Under Applications and Supported Tools, Infineon MemTool, click on *Tools & Software* -> *Software Downloads* -> click on *MemTool v4.8.1 exe download*.
3. Put exe into `C:\Users\z0118277\Downloads\Installers`. Double left click on exe.

## ABOUT <a name="about"></a>
* ***define:*** The Infineon On-Chip Memory Programming Tool (IMT).  
Handles the on-chip FLASH and OTP memory devices on specific Infineon uCs.  
Allows to erase, program, verify, & protect the module.  
Can completely or partly writes its contents into the memory device. 
Provides ready to use configuration files.  
Coverts from the PC/USB to the debug interface of Infineon uC.  
ZF mainly uses it for MemTool.  
* ***software interface tool:*** Device Access Server (DAS) DLL.
* ***hints:***  
    * Never connect DAP cable to EXT connector of the miniWiggler. This will destroy target and/or miniWiggler.  
    * Never connect both JTAG & DAP cables to targets at the same time.  
    * In case of JTAG, make sure GPD pin is connected to GND on both sides.  

## FLASHING FIRST TIME <a name="flashing_first_time"></a>
Each memory type will follow below steps:  
1. In section *FLASH/OTP - Memory Device*, select memory type.
2. In section *File*, *Open File* -> *Select All* -> *Add Sel*.
3. In section *FLASH/OTP - Memory Device*, click *Program*.

| topic   | memory type | file location                                                          | file name                                          |
| ------- | ----------- | ---------------------------------------------------------------------- | -------------------------------------------------- |
| UCB     | UCB         | `G:\Software\Frantz\for_Others\for_Matthew`                            | `UCB_SETTINGS_C38x.s37`                            |
| FEE     | EEPROM      | `C:\...\MMC_APPL_PBM\MMC_PRODUCT\PersistentStorage\Mem\Nvram_Cfg\Docs` | `TC3xx_SW_MCAL_FEE_Initial_Data.s37`               |
| HW_PROG | PFLASH      | `C:\...\MMC_APPL_PBM\00_BUILD\TriCore`                                 | `TriCore_MM_APP_ZFCAL_3PCALS_HW_PROG.s37`          |
| BOOT    | PFLASH      | `C:\...\MMC_BOOT_PBM\Binaries`                                         | Select one of the .s37, not sure which one though. |

* **NOTE:** UCB usually only done 1 time when recieving new nonprogrammed ECU.  

## HSM <a name="hsm"></a>
* ***Hardware Security Module (HSM):*** Key value for each CAN message to prevent anyone from sending messages over CAN. HSM needs a new UCB file.  
* ***HSM files:***
    * ***for debugging:*** `C:\sw\GM\31XX_MY24\MMC_APPL_PBM\MMC_INTEGRATION\ThirdParty\CycurHSM\ecy_hsm_TC39x_HT_SE\bin\HSM.hex`
    * ***for UCB:*** `C:\sw\GM\31XX_MY24\10_Tools\UCB\UCB_GM_31xx_HSM_DISABLE.sre` 
    * ***for UCB:*** `C:\sw\GM\31XX_MY24\10_Tools\UCB\UCB_GM_31xx_HSM_ENABLE.sre`


