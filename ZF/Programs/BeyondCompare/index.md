# BEYOND COMPARE
*[video link](https://www.youtube.com/watch?v=eGAyWvstKCI)*  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [OPEN A COMPARISON](#open_a_comparison)
2. [COMPARISON](#comparison)
3. [TOOLBAR BUTTONS](#toolbar_buttons)
4. [IMPORTANT TRICKS](#important_tricks)
</details>

## OPEN A COMPARISON <a name="open_a_comparison"></a>
1. Drag and drop from file explorer
2. Right click on selected 2 item & click *Compare*.
3. File menu in BC4

## COMPARISON
* Folders line up based on names. Automatically scans subfolders in the background. Results are indicated by the coloration of file names & icons.  
* Orphaned, file exists only on 1 side, has color blue.   
* File exists on both sides has color red for newer & color gray for older.  
* Actions include *copy*, *move*, or *synchronize* files using items on the *Actions* tab on the main menu.  
* ![Picture of Legend](Images/beyondcompare_legend.png) 
* **content comparison results:** Indicated w/ an equal or not equal incon in the center column. Double click on pair of files to view specific differences. Files open in Text Compare. Matching text color black, different text color red w/ a red background. To view only lines w/ differences click *Diffs* button. Can also include context w/ differences by clicking *Context* toolbar button.  
* **copy a section of different lines between files:** Click yellow arrow button next to a section of differences. Notice how text changes from red to black (comparison colors update as text is edited). 

## TOOLBAR BUTTONS <a name="toolbar_buttons"></a>
| button          | decription (upon click)                                                                                                              | 
| --------------- | -----------------------------------------------------------------------------------------------------------------------------------  |
| **All**         | Shows everything.                                                                                                                    |
| **Diffs**       | Show only differences.                                                                                                               |
| **Same**        | Show what's the same.                                                                                                                |
| **Folder/File** | Either: always show folders (all), compare files & folder structure (structure), only compare files (files), ignore folder structure (flatten).                                                                                                                                               |
| **Minor**       | Ignore unimportant differences.                                                                                                      |     
| **Rules**       | Settings, more thorough comparison. Make sure to check *Compare Contents* -> *Rules-based comparison*.                               |
| **Copy**        | Copy to either side of the file(s) selected.                                                                                         |
| **Expand**      | Expand all revealing files inside folders.                                                                                           |
| **Collapse**    | Collapse all hiding files inside folders.                                                                                            |
| **Select**      | Selects everything folders & files on the screen.                                                                                    |
| **Files**       | Selects only files on the screen.                                                                                                    |
| **Refresh**     | Refreshes screen to update files that have been updated.                                                                             |
</details>

## IMPORTANT TRICKS <a name="important_tricks"></a>
<details>
<summary><b>Always select <i>All</i> when opening Integrity on startup</b></summary>

To see if the diff package contains any orphans.
</details>

<details>
<summary><b>See differnces in all files</b></summary>

1. crtl A -> right click -> select *Compare Contents*. *Compare Contents* window opens up.
2. In *Actions* section select *Rules-based comparison*. Click *Start*.
</details>

<details>
<summary><b>Make a diff package</b></summary>

1. Left side: Select files -> right click -> copy to folder -> name Before.
2. Right side: Select files -> right click -> copy to folder -> name After.
</details>

<details>
<summary><b>Copy entire file(s) to one side instead of going inside & coping individual sections</b></summary>

1. Select file(s) -> Right click -> Copy to left/right -> Start.
</details>

<details>
<summary><b>Exclude & Ignored</b></summary>

**Exclude:** Excludes the file in Beyond Compare. Does not display when *All* button is selected. Select file -> right click -> Exclude.  
**Ignored:** Ignores the file in rule base comparsion. file is now black. Still shows when *All* button is selected. Select file -> right click -> Ignored.
</details>
