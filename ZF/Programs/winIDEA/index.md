# WINIDEA

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INSTALL & INFO](#install_and_info)
2. [WINIDEA LOCATIONS](#winidea_locations)  
    a. [INSERT INIT SCRIPTS](#insert_init_scripts)  
    b. [DISABLE HSM](#disable_hsm)  
    c. [TARGET DONWLOAD TARGET FILES](#target_download_target_files)  
    d. [ALLOW_MASS ERASE](#allow_mass_erase)  
    e. [FLASH HW PROG](#flash_hw_prog) 
3. [TEMPLATE](#template)
4. [MANUALLY](#manually)
5. [HSM_STUFF](#hsm_stuff) 
6. [FM DEBUGGING](#fm_debugging)
7. [QUESTIONS](#questions)
</details>

## INSTALL & INFO <a name="install_and_info"></a>
* ***install:*** Goto [https://www.isystem.com/downloads/winidea.html](https://www.isystem.com/downloads/winidea.html). Download latest winIDEA build. Either:
   * Select *File* for *Installer 64-bit*. Run installer in `C:\Downloads`.
   * Select *File* for *Portable* add zip from `C:\Downloads` into `C:\iSYSTEM`. Can essentially have 2 different versions on the PC.  
* ***winIDEA IDE w/ BlueBox:*** Tool for debugging, testing, & analyzing embedded systems. winIDEA runs on development PC. BlueBox is an on-chip analyzer tool. HW interface between development PC running winIDEA & target uC.

<details>
<summary><b>31XX MY24/23</b></summary>

**target:** Infineon TriBoard-TC3X7  
**device:** TC387TP 

| file                 | path (C:\sw\GM\31XX_MY24\..)                                                                                    |
| -------------------- | --------------------------------------------------------------------------------------------------------------- |
| symbol               | `MMC_APPL_PBM\00_BUILD\TriCore\TriCore_MM_APP.out` (CAN serial loaded `TriCore_MM_ZFTRW_APP_3P_APP_signed.s37`) |
| program              | `MMC_APPL_PBM\00_BUILD\TriCore\TriCore_MM_ZFTRW_APP_3P_APP_HW_PROG.s37` (also used for flash memTool)           |
| target - hsm         | `MMC_APPL_PBM\MMC_INTEGRATION\ThirdParty\CycurHSM\ecy_hsm_TC39x_HT_SE\bin\HSM.hex`                              |
| target - enable_ucb  | `10_Tools\UCB\UCB_GM_31xx_HSM_ENABLE.sre`                                                                       |
| target - disable ucb | `10_Tools\UCB\UCB_GM_31xx_HSM_DISABLE.sre`                                                                      |
| target - nvram fee   | `MMC_APPL_PBM\MMC_PRODUCT\PersistentStorage\Mem\Nvram_Cfg\Docs\TC3xx_SW_MCAL_FEE_Initial_Data.s37`              |
| target - boot        | `MMC_BOOT_PBM\Binaries\Aurix_38x_GM_31xx_Boot_Remapped.s37`                                                     |
</details>

<details>
<summary><b>A100 MY24</b></summary>

**target:** Infineon TriBoard-TC3X7  
**device:** TC387TP 

| file                 | path (C:\sw\GM\A100_PBM_MY24\..)                                                         |
| -------------------- | ---------------------------------------------------------------------------------------- |
| symbol               | `MMC_APPL_PBM\BUILD_WM\A100_PBM_APP\A100_PBM_APP.out`                                    |
| program              | `MMC_APPL_PBM\BUILD_WM\A100_PBM_APP\A100_PBM_APP.out`                                    |
| target - hsm         | `MMC_APPL_PBM\MMC_PRODUCT\ThirdParty\CycurHSM\ecy_hsm_TC39x_HT_SE\bin\HSM.hex`           |        
| target - enable_ucb  | `MMC_APPL_PBM\MMC_PRODUCT\MCAL\MCAL\UCB\UCBHex\UCB_GM_31xx_HSM_ENABLE.sre`               |
| target - disable ucb | `MMC_APPL_PBM\MMC_PRODUCT\MCAL\MCAL\UCB\UCBHex\UCB_GM_31xx_HSM_DISABLE.sre`              |
| target - nvram fee   | `MMC_APPL_PBM\MMC_PRODUCT\Services\Mem\Nvram_Cfg\Docs\Aurix_MC-ISAR_FEE_InitialData.s37` |
| target - boot        | `03_Integration\ExtBinaries\Bootloader\AURIX_TC38x_EBCM_Boot_PBM.hex`                    |
</details>

<details>
<summary><b>DT MY24</b></summary>

**target:** Infineon TriBoard-TC3X7  
**device:** TC387TP 

*have to program the HSM*  
DT module can be run on DT, KM, & 31XX (might have a problem with COMMS)

| file                          | path (C:\sw\CHR\DT_PBM_MY24\..)                                                                                         |
| ----------------------------- | ----------------------------------------------------------------------------------------------------------------------- |
| symbol                        | `MMC_APPL_PBM\00_BUILD\TriCore_MM_APP\TriCore_MM_APP.out`                                                               |
| program                       | `MMC_APPL_PBM\00_BUILD\TriCore\TriCore_MM_APP_ZFCAL_3PCALS_Signed_HW_PROG.s37`                                          |
| target - hsm                  | `MMC_APPL_PBM\MMC_PRODUCT\CyberSecurity\Hsm\ecy_hsm_TC39x_HT_SE\bin\HSM.hex`                                            |
| target - enable_ucb           | `10_Tools\UCB\UCB_GM_31xx_HSM_ENABLE.sre`                                                                               |
| target - disable ucb          | `10_Tools\UCB\UCB_GM_31xx_HSM_DISABLE.sre`                                                                              |
| target - nvram fee            | `MMC_APPL_PBM\MMC_PRODUCT\PersistentStorage\Mem\Nvram\Cfg\Doc\TC3xx_SW_MCAL_FEE_Initial_Data.s37` (also in 12_Manuuals) |
| target - boot (Order matters) | `MMC_BOOT_PBM\Binaries\Boot_Files\DT_IBC_BootManager.s37`                                                               |
|                               | `MMC_BOOT_PBM\Binaries\Boot_Files\Aurix_38x_DT_FOTA_Bootmanager.s37 or DT_IBC_Boot_ZFSigned.s37`                        |
|                               | `MMC_BOOT_PBM\Binaries\Certstore_Files\Stellantis_AND_Escrypt_AND_ZF_certstore.s37`                                     |
</details>

## WINIDEA LOCATIONS <a name="winidea_locations"></a>
| target                         | commands                                                                                                 |
| ------------------------------ | -------------------------------------------------------------------------------------------------------- |
| symbol file                    | *Debug* -> *Configure Session* -> select *Applications* & select button *Edit* -> select ...             |
| target & program files         | *Debug* -> *Configure Session* -> select *SoCs* & select button *Edit* -> select ...                     |
| target download                | *Debug* -> *Target Download* -> ...                                                                      |
| enable init script             | *Harware* -> *CPU options* -> secton *Initialization before programming* or <br>*Debug* -> *Configure Session* -> select *SoC* & *Edit* -> in Device field select *Configure* |
| enable/disable prg mem regions | *Hardware* -> *Options* -> ... UCB mem region enabled when dealing with UCB or HSM.hex                   |
| memory options                 | *Hardware* -> *x_memory* -> *Configure*                                                                  |
| add cpu cores                  | *Debug* -> *Configure Session* -> goto *SoCs* -> *Edit* -> *Memory Spaces* -> check other 2 cores -> select each core -> *edit* -> section *Applicaion* select *App* |
| display cpu cores              | *Debug* -> Core -> select core                                                                           |
| add script                     | *Tools* -> *Customize* -> *Local Tools* -> *Add* -> *Browse* -> select sript. Check *Use Output Window*. |

### INSERT INIT SCRIPTS <a name="insert_init_scripts"></a> 
1. Put scripts into winIDEA workspace folder.  
2. ***placing init scripts*** -> section *Initialization before Programming* select *Initialize* -> from dropdown menu *select custom* -> select scripts.
3. In sections *Initialization before Debug session* & *Initialization at Attach* check boxes *Same as Programming/Debug*

### MASS ERASE TO PROGRAM HSM <a name="disable_hsm"></a>
**WANT TO DISABLE THE UCB MEM REGION B/C THAT IS WHERE HSM CODE EXISTS when mass erasing**
1. At target file winIDEA location, add file UCB_HSM_DISABLE. This should be the only file enabled.
2. *Hardware* -> *Options* -> select *Programming* and check mark *Infineon TC3xx_UCB_24kB* -> click *Ok* twice & *Reset*
   I think also *Infineon TC3xx_DFLASH1_Cerberus_128kB* needs to be checked as well.
3. ***enable prg mem region*** -> *Infineon TC3xx_UCB_24kB* -> *Reset*.
4. Make sure chip is connected.
5. ***target download*** -> *UCB_HSM_DISABLE*
6. *mass erase* PFASH, DFASH, & DFLASH1_Cerberus.
7. ***disable prg mem region:*** -> *Infineon TC3xx_UCB_24kB* -> *Reset*.

### TARGET DONWLOAD TARGET FILES <a name="target_download_target_files"></a>
1. At ***target & program files*** enable the target files by checking checkboxes: fee, boot, hsm (downloading hsm code, but not opertional since disable programming memory *Infineon TC3xx_UCB_24kB*, have to flash UCB enable in order for code to be running)
2. run target download files

### ALLOW MASS ERASE <a name="allow_mass_erase"></a>
1. ***memory options for *Infineon TC38x_PFLASH_10MB*, *Infineon TC38x_PFLASH_10MB*, & *Infineon TC3xx_DFLASH1_Cerberus_128kB**** -> section *Options* check *Allow mass erase*. Reset.

## FLASH HW PROG <a name="flash_hw_prog"></a>
1. Make sure program file is added
2. Click button *Download* on main menu toolbar  

## TEMPLATE <a name="template"></a>
1. Place Dustin's extracted zip *ZFWorkSpaceTemplates* in `C:\iSYSTEM\`  
2. In `C:\iSYSTEM\ZFWorkSpaceTemplates\` run *Install.cmd* (only have to do this once to tell winIDEA where templates are). Enter 1 & press enter.
3. Create folder winIDEA (workspace) in codebase where ever file *TriCore_MM_APP.INI* is located. Copy workspace folder path to clipboard.  
4. Open up winIDEA -> *Create New Workspace* -> insert name, paste workspace path in *Location*, and select desired Template from dropdown.  
5. *Hardware* -> *Debugger Hardware* -> select ic5000. Hit reset.
6. ***make own template:*** Copy workspace folder & put it into `C:\iSYSTEM\ZFWorkSpaceTemplates`.  
   Have to change the file names to match the original template file names.  
   This template folder will now appear in the dropmenu.  

## MANUALLY <a name="manually"></a>
1. Create folder (winIDEA workspace) in codebase. Put init scripts in here:  
*TC_3Xx_OCDS_Initialization.ini*, *HSM_start.ini*, *Watchdog_PF_Unlock.ini*, *StellantisBypass.py*  
2. Open up winIDEA and create new workspace (*File* -> *Workspace* -> *New Workspace*). Window *New Workspace* opens.  
   Name workspace. Enter workspace folder created in codebase. -> *Create*. Window *Basic Session Configuration* opens.
3. Enter Device: TC387TP, Symbol file, Program file. -> Ok. Window *iSYSTEM BlueBox Selection* opens.
4. Connect Debugger to PC, select him -> *Accept*. Window *Select Target Board* opens.
5. Select *Infineon TriBoard-TC3X7* -> *Use the Selected Board*.
6. ***enable init script***
7. ***enable/disable prg mem regions***
8. Configure *App* -> ***symbol file***.  
   Configure *SoCs* -> ***target & program files*** (Move HSM_DISABLE to the top (make it enabled). Move HSM_ENABLE to the bottom (make it disabled)). Reset.
9. ***taget download*** `UCB_HSM_DISABLE`.  
   Once done safe to mass erase D/PFASH. Good to do when recieving a new/old ECU. Only have to do this once if not dealing with HSM.  
   There is a spot in memory where HSM written for enabled. If blank will allow to do more stuff. If written over when already been written -> ***brick***.
10. Mass erase mem regions (did DFLASH1_Cerberus_128kB (specific for DT) 1st, then DFLASH_640kB, then PFLASH_10MB).
11. ***target download*** boot, hsm.hex, fee (CHR need `UCB_HSM_ENABLE`). Downloading hsm.hex if needed (needed for CHR).
12. Select button *Download* (download app and symbols at same time).
13. In window *Watch* add variables *OsData_CpuX* and select button *Real Time Update*.
14. **SPECIFIC TO CHR:** CHR expects HSM to be running b/c it makes calls to HSM code.  
    Enable `UCB_HSM_ENABLE` -> ***taget download*** `UCB_HSM_ENABLE` (make sure *HSM_start.ini* is initialized).  
    Can check HSM in sectin below *HSM STUFF*.
15. **SPECIFIC TO CHR:** Select *Run* then *Stop*. In window *Assembly* click 3 dots and type *_START* -> right click on arrow & select *GoTo*.  
    Select *Reset*, *Run*, *Stop* -> Goto *_START* in codebase (if cstart.c file already open, right click on line 206 & select *GoTo*. Select *Run*.).  
16. **SPECIFIC TO CHR:** Or can use Bypass script (flashing through SerialLoader will auto jump to app, no need for script).  

## HSM STUFF <a name="hsm_stuff"></a>
***WAYS TO BRICK ECU***  
* Mass erasing D/PFLASH without flashing UCB_HSM_DISABLED (unsure brick occures even if mem regions are disabled) -> ***brick***
* Flash UCB_HSM_DISABLED -> mass erase D/PFLASH -> forget to flash *hsm.hex* -> flash UCB_HSM_ENABLED -> reset -> ***?***
* Flash UCB_HSM_ENABLED -> mass erase D/PFLASH -> ***brick***
* Mass erasing UCB mem region -> ***brick***
* Don't have HSM and flash UCB_ENABLE -> ***permanent brick***
* Don't have boot and flash UCB_ENABLE_HSM -> ***temp brick***

***WAYS TO NOT BRICK ECU***
* Flash UCB_HSM_DISABLED -> don't flash hsm.hex -> flash UCB_HSM_ENABLED
* Flash UCB_HSM_DISABLED -> flash hsm.hex -> don't flash UCB_HSM_ENABLED

***CHECK TO SEE HSM STATUS***  
* **1:** Check to see if *hsm.hex* is flashed/programmed:  
Window *Memory* goto hsm code address, make sure board is connected (found below).
* **2:** *View* -> *Aurix* -> *UCB*. Double click *UCB_HSMCOTP0_ORIG*. NEVER HIT OKAY HERE! ->  
   Plus out *PROCONHSMCFG*, look at *HSMBOOTEN*.  
   This reads out from HW (have to look at after RESET & must be connected). Decodes UCB HSM section in word.  
   Hit Cancel. HITTING OKAY WILL MAKE CHANGES, WILL WANT TO FLASH UCB!  
* ***note:*** From (1) can see that hsm is programmed. From (2) can see hsm is turned off so can turn it on by flashing UCB_HSM_ENABLE.
* ***note:*** There can always be something in the HSM mem region & not be correct so its good to program the one that comes with it.

## FM DEBUGGING <a name="fm_debugging"></a>
<details>
<summary><b>faults showing in Fault_Stack_Hil though they are disabled in DMT</b></summary>

Faults showing are in NVRAM and have bit PFV set.  
Since macro FAULT_DATABASE_ID did not change, function InitFaultDatabaseCompatibilityCheck will not clear NVRAM.  
In function UpdateFmInternal (line 163), faults are being added to buffer when bits LATCHED or PFV are set.  
Solution is to change macro in Cobra, however it does not change in winIDEA for some reason.  
Therefore, breakpoint at this function and change the local variable *stored_fualt_database_id* to a different value.  

*from Mounika debugging meeting*  
"Even though I disabled all the faults except one, PFV faults were still showing in Fault_Stack_Hil.  
I once flashed the SW and faults were set. Then I flashed same SW with disabling the faults, & the PFV ones were still in the buffer.  
So, CMs will be inhibited.  
So, changing FAULT_DATABASE_ID and flashing it will now be considered a new fault database so faults stored in NVRAM are reset.  
Changing the macro results in faults not being PFV and testing becomes a fault free environment.  
Every DMT udpate the macro is changed to avoid mixing up old & current SW data with different configurations."  

"Prev I had fault enabled, fault was latched, then I disabled the fault, and flashed the SW.  
Even though faults are being reported with monitor_passed as TRUE or FALSE, FM will not take any reation becasue the fualts are disable in the fault database.  
However, if the prev faults were configured as PFV, then they will stay as PFV because they are stored in NVRAM.  
PFV: Configured as TRUE in DMT. Fault is latched. In next ignition cycle, NVRAM faults are read. If fault has PFV TRUE in DMT, then bit PRIOR_FAULT_VALIDATION set in Fault_Status_Table.  
However, cannot test when PFV fautls are changing the system reactions.  
Hence, reason for this change, however faults sill PFV.  
Solution, change the macro FAULT_DATBASE_ID."  

"Faults showing in Fault_Stack_Hil b/c it is looking at PFV in NVRAM.  
If FAULT_DATABSE_ID is changed, PFV status in NVRAM is reset."    

"Ideally, in integration test or anywhere we have to test with all the faults enabled like the SW release.  
But testing system reactions for a specific fault cannot be done if PFV faults are present."  

**STEPS**  
Have to force all fualts to 'Enable Production' b/c structure SystemReaction holds the worst state based on all the faults.  
Therefore, if only 1 fault is active, creating a fault free bench setup where a speific fault can be tested.  
1. *Enable Production* set to TRUE for all faults. Compile cal file.
2. Faults are shown in Fault_Stack_Hil while running.
3. *Enable Production* set to FALSE for all faults except ALL_WSS_FAILURE. Compile cal file.
4. If faults are in the Fault_Stack_Hil they they were PFV.
5. To clear Fault_Stack_Hil, change the FAULT_DATABASE_ID to reset the NVRAM (might have to change manually in debugger).
</details>

<details>
<summary><b>hitting traps while running after initial breakpoint</b></summary>

The chip has many HW alarms. If enabled and hit -> trap -> handling.  
SMU alarms are handled by FSP reset, but a connected debugger can prevent ECU from shutting off.  
Therefore, handled by an infinite loop. Debugger likes to set alarm group 7 bit 17.  
*solution:* Disabled the alarm(s) in file *Smu_PBCfg.c*. Can do this becuase were debugging not running module normally.  

```c
/*AlarmConfig for SmuCore*/ /* represents config bits that enable/disable alarms */
  {
    0x1c06db5U,0x2U,0x1c06db5U,
    0x1c06db5U,0x2U,0x1c06db5U,
    0x1c06db4U,0x2U,0x1c06db4U,
    0x0U,0x0U,0x0U,
    0x0U,0x0U,0x0U,
    0x0U,0x0U,0x0U,
    0x2b619f7U,0x0U,0x2b619f7U,
    0xff117006U,0x8000U,0xff117006U, /* prev. 0xff137006U,0x8000U,0xff137006U, */
    0x2f41c3fU,0x2000000U,0xf41c3fU,
    0x38028U,0x0U,0x38028U,
    0x600006U,0x0U,0x600006U,
    0x373fU,0x0U,0x373fU
  },
  
  /*AlarmFspConfig for SmuCore*/ /* represents the config bits for tripping the FSP (HW pin) */
  {
    0x1c06db5U,0x1c06db5U,0x1c06db4U,0x0U,0x0U,0x0U,0x2b619f7U,0xff117006U,0xf41c3fU,0x38028U,0x600007U,0x373fU}, /* 0xff137006U */

```
</details>

## QUESTIONS <a name="questions"></a>
<details>
<summary><b>Q1:</b> What are the different ways of flashing?</summary>

**A:**  
1. ***BlueBox debugger:*** By JTAG/DAP. Mostly used. 31XX file used: `TriCore_MM_ZFTRW_APP_3P_APP_signed.s37`.
2. ***Serial Loader:*** For CAN. 31XX file used: `TriCore_MM_ZFTRW_APP_3P_APP_signed.s37`.
3. ***memTool, miniWriggler:*** By JTAG/DAP.
</details>
