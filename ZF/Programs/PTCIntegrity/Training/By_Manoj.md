# TRAINING BY MANOJ - [useful link](PTC_Source_Integrity_Training--Braking_V2.pdf)

## 2 PARTS OF INTEGRITY
1. **Change Management:** Records infomation. Major topic includes the workflow & state. Top half of Integrity application client.  
2. **Configuration Management:** File storage. Major topics are sandboxes, checkin, checkout, & checkpoints. Bottom half of Integrity application client.  

## INTEGRITY OVERALL WORKFLOW EXAMPLE
![Picture of Integrity Overall Workflow EXAMPLE](Images/integrity_workflow_example.png)  

## CHANGE REQUEST 
Created b/c analyzation did *not* match requirements. Found bug, known problem. Can be created by anyone.  
![Picture of ALM_Request Workflow 2](Images/alm_change_request_workflow2.png) ![Picture of ALM_Request Workflow 1](Images/alm_change_request_workflow.png) 

**Change Control Board (CCB):** Review of results out of analysis due to feasibility, impact, risk, dependencies, resource and cost analysis, scheduling.  
**Change Controller:** Approves/rejects *ALM_Change Request* based on analysis. Can create *ALM_Task* in state *ALM_Approved*.  

## TASK
Authorize, control, & track defined changes of a product. Assigned to a responsible person. Shows progress of implementation.  
![Picture of ALM_Task Workflow 1](Images/alm_task_workflow.png)
*Change Package* can only be created in the state *ALM_Started*.  
![Picture of ALM_Task Workflow 2](Images/alm_task_workflow2.png)  

## REVIEW
Review & approve changes to a product. Assigned to a responsible person. Shows progress of implementation.  
Item owner: reviews related *Item* including *Change Packages*, creates *ALM_Checklist* based on *ALM_Checklist Template*, & rates the result. *ok* -> *ALM_Completed* or *not ok* -> *ALM_Rework*. Finally gives a conclusion.  
![Picture of ALM_Review Workflow 1](Images/alm_review_workflow.png) ![Picture of ALM_Review Workflow 2](Images/alm_review_workflow2.png)

## CONFIGURATION MANAGEMENT
Integrity is a *static* repo meaning first have to download content by creating a *Sandbox* to the PC on a hard drive. Then *members* can be *Check Out* & *Check In*.  
3 types of sandboxes:  
1. **Build:** Restriction of unable to modify files.
2. **Regular:** Only checkpoints are created here. 
3. **Variant:** Branch, modified createdd only for *Subsystems* & *Components*Only checkpoints are created here.  

## OTHER
**Check Out:** Only 1 person can check out at a time, thus should only be used when modifing files real quick. *Check In* when done. Checking out a member can only be done when the *ALM_Task* is at state *ALM_Started, b/c a *Change Package* is needed when checking out.    
**Revert:** No longer in *Check Out* state.  
**Resynchronize:** Updated as in public server.  
**Checkpoint:** *Check In* fiels grouped together to create a version for the whole folder (Component, Subsystem). Act as monthly component/subsystem version release. Provided to the integrators.  

### CONFIGURATION MANAGEMENT ARCHITECTURE
Folder structure created by ZF. **Ex.**  
Io, Mcal -> Subsystems  
AnalogInputs -> Component
source files -> Modules


