# 13008 - SCCM - Expert CM (SCCM-EX) - [link](https://plm-se.emea.zf-world.com/training-material/13008_ILM_SCCM-EX/13008_ILM_SCCM-EX.htm) 

## SANBOXES FOR MAINLINE, VARIANT, & BUILD
Sandbox is needed to copy project members to your workstation. Sandbox ...   
* Points to a project on the Integrity server.  
* Gets automatically registered w/ the Integrity client.  
* Recreates the project's directory structure (recurvise).  
* Can be created as ...  
**Regular (Main Line):** Contains the member revisions of the main line.  
**Variant (Development path):** Contains the member revisions of the specified variant.  
**Build (Checkpoint / Label):** Contains the member revisions of the specified checkpoint. No Changes are possible.  
