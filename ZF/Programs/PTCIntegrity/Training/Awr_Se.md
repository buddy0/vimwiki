# 4084 - SYSTEMS ENGINEERING ROLL OUT - AWARENESS (AWR-SE) - [link](https://plm-se.emea.zf-world.com/training-material/4084-AWR-SE-Rollout-Awareness/4084_AWR-SE-PLM-Systems-Engineering-Roll-out-Awareness.htm)  

## INEGRITY MODULES
![Picture of Integrity Modules](Images/integrity_modules.png)  
PLM SE Tool based on 3 pillars:  
* ILM with modules in ***Problem Resolution Management (PRM)***, ***Change Management (CRM)***, ***Release Management (REL)***, ***Test Management (TM)***, & ***Requirement Management (RM)***  
* SCCM (Development projects documents management: SW Configuration Management)  
* GIT

## DOCUMENT MODELS & WORKFLOW IN INTEGRITY

### WORKFLOW OF ILM
**Problem Resolution Management (PRM):** Open Point & Action items.  
**Change Management (CRM):** Change requests, Task, & Review items.  
**Release Management (REL):** Delivery & Build items.  
**Risk Management (RiskM):** Phase 2.  

### DOCUMENT & WORKFLOW OF ILM
**Requirements Management (RM)** 2nd phase.    
**Test Management (TM)**: 2nd phase.  

***advantages:*** 1 database (centralized storing) for complete ZF development. Access controlling w/ permissions. Historical records, unique item IDs, traceability, and All My Work concept.  

## ALM WORKFLOW
**Application Lifecycle Management (ALM) workflow:** Allows management of all engineering tasks during *V-Life Cycle* from *Requirements Management* to *Test & Delivery Management*.  
![Picture of ALM Workflow](Images/ALM_workflow.png)  

## SW TOOL FOR SAFETY DEVELOPMENT
![Picture of Integrity Safety](Images/integrity_safety.png)

## TOOL SUIT BASICS
***Integrity consist of 2 parts:***  
1. **SCCM/SI - Configuration Management:** Version control system & configuration management.  
2. **IM - Workflow & Documents:** Change Management, Problem Resolution, & Release management.
* Both parts are integrated & linked by: 
1. **Change Packages:** Log of changes to members & subprojects committed to the server.  
2. **Checkpoints:** aka SI Projects / SI Builds w/ IM items. 

![Picture of Change Packages](Images/change_packages.png) 

## WORK W/ ITEMS (TABS)
**Fields:** Title, Project, Team, Owners, Description, Conclusion, State, Priority, etc.  
**Relationships:** Conenction between different item types.  
**Attachments:** Add up to 10MB per attactment. Add an attachment (any local file) or add a memberlink with Drag & Drop (member revision).  
**Workflow:** Shows workflow & state transitions of the opened *Item*.   
**History:** Every saved change of an *Item* is available here. Can open an *Item* in any previous state.  

## REVIEW
![Picture of Review for Integrity](Images/integrity_review.png)

