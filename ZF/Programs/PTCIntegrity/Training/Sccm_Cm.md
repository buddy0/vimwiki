# 4096 - SCCM - BASIC CM (SCCM-CM) - [link](https://plm-se.emea.zf-world.com/training-material/4096_ILM_SCCM_BasicCM/4096_SCCM-CM-PLM-Integrity-SCCM-BasicCM.htm)  

## ARCHITECTURE & WORKING METHOD & DEVELOPMENT CYCLE 
***PTC Integrity environment has 2 different servers:***
1. Integrity SCCM  
2. Integrity Manager (Workflow & Documents: PRM, CRM, REL, RM, TM)  

***NOTE:*** Reason for 2 different logins for *ALM_ZF Developer*, for SCCM and IM.  
![Picture of Architecture](Images/sccmArchitecture.png)  

**sandbox:** Private workspace mirroring the public **Project**. Access to projects and their members. A user's computer holds sandboxes and working files.  
Management of files here will change all files locally (no Integrity connection necessary).  
![1st Picture of Working Method](Images/workingMethod1.png)  
![2nd Picture of Working Method](Images/workingMethod2.png)  
![Picture of Development Cycle](Images/developmentCycle.png)  

## GLOSSARY
**members:** Files under version control in a project.  Update a member leads to new revision which has a new number for reference.  
Content of each member is stored in an archive with history of all submitted changes. Each revision can be individually viewed and worked on.  
![Picture of Member Example](Images/memberExample.png)  

**working file:** Local working copy of a revision. Changes are made here, not the revision itself.  
Changes can be tested w/in Sandbox environment using these. Modified working files can be resynchronized (roll back).  

**member revision:** Revision of the member file w/ which working file will be synchronized w/.

## VIEWSETS
Access Configuration Management (SCCM) and Workflows & Documents (IM) in same interface. 
**viewset:** Collection of views in specific configuration that persists each time open and close Integrity Client.  

## PROJECTS
**Projects:** In the Integrity root.  
**Subproject:** Child of the project. 

### VIEW
***Projects View:*** Lists all projects which current user is allocated.  
***Project View:*** Shows project content on subproject and member file level.  

## SANDBOX
**sandbox:** Private workspace that resides on client machine and mirrors the content of a project on a server.  
Collection of pointers to its real-life counterparts in master project.  
Allows to make a change to a local working file w/o affecting the main project. ***Types:***  
**Regular:** Useful for sequential development of a project over short/long term.  
**Variant:** Useful for branching off main development path.  
**Build:** Useful for testing out specific revision of the project.  
***NOTE:*** Creating a *Sandbox* registers it w/ Integrity SCCM.  

### CREATE SANDBOX
**1.** Goto *Projects* tab and navigate to a (sub)project.  
**2** Right Click on project and select *Create Sandbox*.  
***NOTE:*** Do *not* nest sandbox locations.  
***Confirm Recurse Into Subprojects:*** Select *No to All* which only the *directory structure* will be duplicated. The member files will have to be synchronized manually. This is preferred, since it only synchronizes the files that you need for your work.  

### VIEWS
***My Sandboxes:*** Tab on left side listing all my created sandboxes.  
***Sandbox:*** Tab on right side showing content of recently sandboxed project folder.  
***NOTE:*** These 2 views act independently. Right click on *Sandbox* tab & clicking *Dynamic* will change behavior.  
**static:** View locks originated Sandbox. Marking another Sandbox in the *My Sandboxes* view, the static *Sandbox* will *NOT* show content of originating Sandbox.  
**dynamic:** View automatically updates currently selected Sandbox in *My Sandboxes* view.  

## MEMBERS
New files must be explicitly added to become members of an Integrity (sub)project.  
An added member can be checked out for editing & checked in to preserve changes.  
***A file to be added must:*** Located in local sandbox (sub)directory, correct file name, proper format, and located appropriate.  

### ADD MEMBER
**1.** Select destination project w/in *Sandbox* view.  
**2.** Push *Explorer* button opening currently marked *Sandbox* view directory on local hard drive w/in file explorer.  
**3.** Create/Move/Copy new member file to this location.  
**4.** Switch back to Integrity and mark correct destination project w/in *Sandbox* view.  
**5.** Right click on (sub)project in *Sandbox* view and select *Add Member* or push *Add Member* button. 
***NOTE:*** Also, add member via Drag n Drop to specific (sub)project in *Sandbox* view.  
***NOTE:*** Also, add member via Non-members view. Select folder in *Sandbox* view. Goto main menu. Select *Sandbox*, then *Views*, then *View Non-Members*.  Confirm to search for new members recursively in subdirectories (Yes to All).  

## CHECK OUT
**check out:** Extracts contents of a revision in a member history and copies it to the working file.  
To make changes to a member, first must be checked out.  
Checked out revision is copied to local Sandbox as working file and is locked on server project. ***Operation:***  
**1.** Select file w/in sandbox structure in *Sandbox* view.  
**2.** Right click and select *Check Out*. Specific revision can be selected here (or **2.** Right click and select *View Member History*.   
**3.** Right click on a revision and select *Check Out*).  
***NOTE:*** Current member revision will be locked to your user account and becomes your local working revision.    
Lock lasts until *Check In*, *Revert*, or *Remove My Lock* is performed.  

## CHECK IN
**check in:** Preserve changes as a new revision in the member's history. Memebers should be checked in on a regular basis. Must include changes from before. ***Operation:***  
**1.** Select file w/in sandbox structure in *Sandbox* view.  
**2.** Right click and select *Check In* or by pressing *Check in member* button.  

## MEMBER HISTORY
***view differences:***  
**1.** Goto page of *View Member History*.  
**2.** Press CTRL and select 2 revisions.  
**3.** Right click and select *View Differences*.  

## LOCKS
**lock:** Controls how changes are made to revisions. Needed to check in an updated member and to prevent more than 1 user to *Check In* changes to same revision.  
***View My locks:*** **1.** Main menu tab, select *Member*, then *Locks*, then *View My Locks*.  

## REVERTING A MEMBER
**revert:** Discards any changes made to working file since it was checked out. Unlocks the revision & resets the write protection.   
***operation:*** **1.** Select working file in sandbox structure in *Sandbox* view. **2.** Right click and select *Revert Members*. **3.** Select *Yes* to overwrite it.  

## DROPPING MEMBERS
***Operation:***  
**1.** Select file in sandbox structure in *Sandbox* view.  
**2.** Right Click and select *Drop Members*.  

## DROPPING SANBOXES
Dropped sandbox is no longer registered w/ PTC Integrity & cannot be accessed w/ Integrity anymore.  
***Operation:***  
**1.** Select sandbox in *My Sandboxes* view.  
**2.** Right click and select *Drop Sandbox*.  
**3.** Affirm drop be selecting *Yes*.  
**4.** Select a *Drop Option*.

### DROP OPTIONS
**Nothing:** No files will be deleted from the Sandbox directory on your local drive.  
**Sandbox Members Only:** All Sandbox members on your local drive will be deleted, non-members will be left.  
**Entire Sandbox Directory:** Deletes the Sandbox directory on your local drive and all of its contents.  

## MOVE & RENAME MEMBER

## RESYNCHRONIZE
When users are working from Sandboxes based on same master project, common for members in an individual Sandbox to become out of sync with member revisions in the project.
**resynchronize:** Updating out of sync files to most current member revisions.  
***operation:*** **1.** Select member in sandbox structure in *Sandbox* view. **2.** Right click and select *Resynchronize*. **3.** Select *Yes*.  
***NOTE:*** To *NOT* use *Yes to All* option. Select *Yes*.  
***NOTE:*** All changes will be lost.  

## CHECKPOINT PROJECTS
**checkpointing:** Track evoltuion of entire project. 
***operation:*** **1.** Select project in *Projects* view. **2.** Right click and select *Create Checkpoint*  

## EXTERNAL LINKS
***operation:*** **1.** Mark member file/sub-project folder to be linked. **2.** Press *HTTP Link* button to create dyncamic link. **3.** Link is available in clipboard. 
