# Q/A  

<details>
<summary><b>Q1:</b> How is PTC Integrity differnt from Git?</summary>

Integrity is a configuration & resolution management software. The *file structure* is different:  
***Integrity:*** Members (files on public server) at first are shared among development paths. It is only when that member is branched (new DP) when it corresponds to a DP.  
A member w/ same member revision can belong to different projects.  
***Git:*** Cloning a project means the files are no way related to other branches.  
</details>

<details>
<summary><b>Q2:</b> Would deleting a sandbox on my local machine in the file explorer receive the same result as dropping the sandbox in Integrity?</summary>

**A:** If you delete sandbox from your local machine, it will not delete from integrity. Since it is deleted only from your local machine, you can click on resynchronize button to redownload. Dropping sandbox from integrity will delete your view from integrity and also from local machine.
</details>

<details>
<summary><b>Q3:</b> What do the different options mean for <i>Confirm Recurse Into Projects</i>?</summary>

**A:**  
**Yes to all:** Populate anything inside the sub-project whether that be members or other sub-projects with their members as well. This could take a long time.  
**No to all:** Populate the sub-project structure one layer above, no members. Members need to be synchronized manually.  
**Yes & No:** Same effect but only to the explicit stated sub-project. 
</details>

<details>
<summary><b>Q4:</b> Resynchronize is for mirroring the sub-projects &/OR members of a public sub-project. Adding a member is only for creating new files that are not in the public sub-project. Is this correct? </summary>

**A:**  
**Resynchronize:** Populates the public members into my local machine & updates out of sync working files to the most current member revisions.  
**Add a member:** Adds a new file(member) into the project that the public project does not already have. A new file has to be located in the local sandbox directory, correct file name & extension, & located appropiate. 
</details>


<details>
<summary><b>Q5:</b> When I add or drop a member, how does this affect the Sandbox and the public project (add member: do I have to manually check out & in)?</summary>

**A:**  
These operations affect both the local & public projects. 
Adding a member automatically checks the member out & in.  
Dropping a member drops the member from both projects.  
***NOTE:*** These operations will be in the record for Integrity forever. Only do so if needed. 
</details>

<details>
<summary><b>Q6:</b> What operations effect the public project?</summary>

**A:**  
Pretty much anything done on the *Project* or *Sandbox* views (lower right screen) affects the public project. Operations include *Check in*, *Add a member*, *Drop a member*, etc.  
Pretty much anything done on the *Projects* or *My Sandboxes* views (lower left screen) affects the local project. Operations include *Create Sandbox*, *Drop Sandbox*, etc. 
</details>

<details>
<summary><b>Q7:</b> What happens if I were to <i>Resynchronize</i> an outdated member that has been checked out?</summary>

**A:**  
Similar to *Resynchronize* a member that has not been checked out. The working file is replaced by the newly member revision (thus changes to working file are discarded), however there is still a lock on the member resulting in the *Check Out* operaion still being active.  
</details>
   
<details>
<summary><b>Q8:</b> If a member isn't checked out & there are no updated member revisions, then will Revert do the same action as Resynchronize? What about updated member revisions?</summary>

**A:**  
No, *Revert* can only be applied when a member is *check out*. *Revert* acts like an undo.  
*Resynchronize:* Always try to update to the newest member revision if possible.  
</details>   

<details>
<summary><b>Q9:</b> When do working files come into place?</summary>

**A:**  
? Modify a local file, a working file is created?  
?(Is this when we are checking out a member OR anything we resynchrinize now appears as working files in our Sandbox)?  
</details>

<details>
<summary><b>Q10:</b> What exactly is a change package?</summary>

**A:**  
**change package:** Link between the change & configuration management tool that contains the contents to be modified. Needed to be able to *check out, check in* files. Can only be created in the when *ALM_Task* is in the state *ALM_Started*.  
</details>

<details>
<summary><b>Q11:</b> Are we mostly modifing files, thus <i>Add a member</i> is rarely used?</summary>

**A:**  
Correct.
</details>

<details>
<summary><b>Q12:</b> If I add changes to a file on my local machine, execute the checkout command on that file, will Integrity know about these changes?</summary>

**A:**  
When the *Check out* command is selected on a member, a window pops up whether to ...
1. Keep the modified changes before the *Check out* command was selecting.  
2. Disregard the changes made to the working file since last Resynchronize.   
</details>

<details>
<summary><b>Q13:</b> How to retrieve the development path/branch/variant?</summary>

**A:**  
* On *Projects* view, RC on project, select *View Project History*.  
* On *My Sandboxes* view, RC on sandbox, select *View Project Information*.  
* On *Sandbox* view, RC on project, select *View Project History* or *View Project Information*. 
</details>
 
<details>
<summary><b>Q14</b> What do the Sandbox memebers state <i>draft</i> & <i>in work</i> mean?</summary>

**A:**  
They do not work correctly in Integrity
</details>

<details>
<summary><b>Q15</b> When Checkpointing, does the entire Sandbox need to be <i>Resynchronize</i>?</summary>

**A:**  
Yes.  
</details>

<details>
<summary><b>Q16:</b> Difference between <i>Adding a member</i> vs <i>Archiving a member</i>?</summary>

**A:**  
***Archiving a project:*** 1 file for all projects, be in same location.  
When any file has differences in future, we can branch it from one location for each program. 
***Adding members:**** Done to each project, thus different copies since in different locations. 
</details>

<details>
<summary><b>Q17:</b> Do I have to save a file when check out to see the modified working file icon?</summary>

**A:**  
Yes, have to save the file in order to see the change.
</details>

<details>
<summary><b>Q18:</b> Can I undo a <i>Check in</i> command? </summary>

**A:**  
No, check in files are in Integrity forever. However, can update member revision to previous revision. Check in new changes which will override existing member revsision. 
</details>

<details>
<summary><b>Q19:</b> Can I rename a sandbox after is has been created?</summary>

**A:**  
No.
</details>
 
<details>
<summary><b>Q20:</b> Confused on the folder structure when sanbox is created. Specifically, the folder structure on subprojects on why the project history includes multiple DPs when were already on a specifc DP?</summary>

**A:**
</details>

<details>
<summary><b>Q21</b> Do working files apply to all development paths of a project?</summary>

**A:** No, creating a sanbox w/ a particular DP has its own set of files for each DP.
I think the problem is arising when *Retarget Sandbox*, b/c were using the same file path in Windows file explorer so Integrity thinks that if there are working files it should be asked if they should carry over to the new sandbox at a DP. Working files do not apply to different sandboxes w/ same project but different DPs.
</details>

<details>
<summary><b>Q22:</b> Will save an edited item push changes to the server?</summary>

**A:** Yes.  
</details>

<details>
<summary><b>Q23:</b> Can you change a member revision to previous?</summary>

Yes, by *Update Member Revision* command.  
</details>

<details>
<summary><b>Q24:</b> I retarget a sandbox w/ locks on members, do they still appeare when I come nack to the sandbox?</summary>

Yes, I locks affect the public server. What you do on local machine has no affect on the public server. Have to be physically unlocked again for them to dissapear.  
</details>

<details>
<summary><b>Q25:</b> Does double clicking on a member in *Sandbox* view for the working revision or member revision></summary>

**A:** Working revision. Resychronize to match working revision to member revision.
</details>

<details>
<summary><b>Q26:</b> What happens when I drop a member from a development path but goto a previous checkpoint and create a new development path? Can I still access this member?</summary>

**A:** I assume so.
</details>

<details>
<summary><b>Q:27</b> Where & why can I create development paths?</summary>

**A:**  
***where:*** Only at project.pj. No member level, but members can branch & coexists in the same component development path.  
However, one needs to consider if another project uses same development path before the branch. Examples below:  

```
Program_1                | Program_2 branches member_1           | Program_1 sees                     | Program_1 checkpoints agian w/ member_2 (acquires member_1 branch)
------------------------ | ------------------------------------- |----------------------------------- | ---------------------------------------------------------- 
component     member_1   | component     member_1                | component    member_1              | component    member_1                   member_2
checkpoint    revision   | checkpoint    revision                | checkpoint   revision              | checkpoint   revision                   revision
                         |                                       |                                    |            
                         |                                       |                                    | [1.4 C]       
                         |                                       |                                    |   ^
                         | [1.3 C]                               | [1.3]                              | [1.3]
                         |   ^                     [1.1.2.1.1 C] |   ^                    [1.1.2.1.1] |   ^                    [1.1.2.1.1 C]
[1.2 C]       [1.1.2 C]  | [1.2]         [1.1.2] >    ^          | [1.2 C]      [1.1.2 C]   >  ^      | [1.2]         [1.1.2] >      ^           [2.2 C]
  ^              ^       |   ^              ^                    |   ^             ^                  |   ^              ^                         ^
[1.1]         [1.1.1]    | [1.1]         [1.1.1]                 | [1.1]        [1.1.1]               | [1.1]         [1.1.1]                    [2.1]
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
Program_2 sees
----------------------------------------- |
component   member_1             member_2 |
checkpoint  revision             revision |
                                          |
[1.4]                                     |
  ^                                       |
[1.3 C]                                   |
  ^               [1.1.2.1.1 C]           |
[1.2]    [1.1.2] >    ^          [2.2]    |
  ^         ^                      ^      |
[1.1]    [1.1.1]                 [2.1 C]  |
```

```
Program_1                | Program_2 branches member_1           | Program_1 sees                     | Program_1 checks in member_1         | Program_2 checks in member_2
------------------------ | ------------------------------------- |----------------------------------- | ------------------------------------ | ---------------------- 
component     member_1   | component     member_1                | component    member_1              | component    member_1                | component  
checkpoint    revision   | checkpoint    revision                | checkpoint   revision              | checkpoint   revision                | checkpoint
                         |                                       |                                    |                                      |
                         |                                       |                                    |                                      | [1.5 C]
                         |                                       |                                    |                                      |   ^
                         |                                       |                                    | [1.4 C]                              | [1.4]              [1.1.2.1.2 C]
                         |                                       |                                    |   ^                                  |   ^                     ^
                         | [1.3 C]             [1.1.2.1.1 C]     | [1.3]                 [1.1.2.1.1]  | [1.3]         [1.1.3 C] [1.1.2.1.1 ] | [1.3]     [1.1.3]  [1.1.2.1.1]
                         |   ^                       ^           |   ^                        ^       |   ^              ^           ^       |   ^          ^          ^
[1.2 C]       [1.1.2 C]  | [1.2]         [1.1.2] >               | [1.2 C]      [1.1.2 C] >           | [1.2]         [1.1.2] >              | [1.2]     [1.1.2] >    
  ^              ^       |   ^              ^                    |   ^             ^                  |   ^              ^                   |   ^          ^
[1.1]         [1.1.1]    | [1.1]         [1.1.1]                 | [1.1]        [1.1.1]               | [1.1]         [1.1.1]                | [1.1]     [1.1.1]
----------------------------------------------------------------------------------------------------------------------------------------------------------------------------------
```

```
Program_1                | Program_2 checks in member_1 | Program_1 sees         | Program_1 checks in member_1          | Program_2 checks in member_1
------------------------ | ---------------------------- | ---------------------- | ------------------------------------- | -----------------------------
component     member_1   | component     member_1       | component    member_1  | component    member_1                 | component
checkpoint    revision   | checkpoint    revision       | checkpoint   revision  | checkpoint   revision                 | checkoint
                         |                              |                        |                                       | 
                         |                              |                        |                                       | [1.5 C]
                         |                              |                        |                                       |   ^
                         |                              |                        | [1.4 C]                               | [1.4]      [1.1.4 C]
                         |                              |                        |   ^                                   |   ^           ^
                         | [1.3 C]       [1.1.3 C]      | [1.3]        [1.1.3]   | [1.3]         [1.1.3]   [1.1.2.1.1 C] | [1.3]      [1.1.3]    [1.1.2.1.1]
                         |   ^              ^           |   ^             ^      |   ^              ^          ^         |   ^           ^          ^
[1.2 C]       [1.1.2 C]  | [1.2]         [1.1.2]        | [1.2 C]      [1.1.2 C] | [1.2]         [1.1.2] >               | [1.2]      [1.1.2]
  ^              ^       |   ^              ^           |   ^             ^      |   ^              ^                    |   ^           ^
[1.1]         [1.1.1]    | [1.1]         [1.1.1]        | [1.1]        [1.1.1]   | [1.1]         [1.1.1]                 | [1.1]      [1.1.1]
-----------------------------------------------------------------------------------------------------------------------------------------------------------------------
```

</details>

<details>
<summary><b>Q:28</b> Why did Integrity not ask me to branch a member. It did it automatically?</summary>

**A:** Since we already made a new development path for a subproject, it knows the member should prolly want to be own its own path as well.
Not sure, though b/c it asked if I wanted to branch fault database xml file but not for example FaultSpecificData_Cfg.c?
</details>

<details>
<summary><b>Q:29</b> Error for transitioning OP to state <i>ALM_Rejected</i> results an error b/c linked CR is not in the following states: <i>ALM_Cancelled</i>, <i>ALM_Closed</i>, <i>ALM_Rejected</i>?</summary>

**A:** Remove linked CR.
</details>

<details>
<summary><b>Q:30</b> Why can I not change the summary for an OP in state defined with me as an owner?</summary>

**A:** 
</details>

<details>
<summary><b>Q31:</b> Can a component development path exist w/ 2 different checkpoint builds that have members in different branches?<br></summary>

**A:** Yes, component build from 2 different project-level DPs can have same component DP w/ different build revisions. Can increase or decrease but cannot move left or right.
In constrast, member revisions can be branched and still be part of the same development path.
However, if I branch a member and another component DP uses the non branch member and updates a file in that compenent, then the newly created checkpoint acquires the branch member!  This results in 2 projects fighting over component development path. Solution, create another component development path.
</detals>

<details>
<summary><b>Q:32</b> If a member is checked in that has a higher member assoicated to it, what happens? What about component builds?</summary>

**A:**  
***member:*** Branch.    
***component build checkpoint:*** Jump over the topmost checkpoint acquiring its changes.
***note:*** If this happens, think to yourself that another program might use the same development path and use this higher change.  
If I need to make a change on this file, then I would need to branch (think this does this auto.).  
</details>

<details>
<summary><b>Q:33</b> Can 2 different tasks check in the same member at the same member revision?</summary>

**A:** No, a change package only lets you select one Task change package. Thus, two different member revisions are needed.
</details>

# ALM_STATES
<details>
<summary><b>Q1:</b> What state in <i>ALM_Change Request</i> can an <i>ALM_Task</i> be created?</summary>

**A:**  
*ALM_Approved*. 
</details>

<details>
<summary><b>Q2:</b> What state in <i>ALM_Change Request</i> or <i>ALM_Task</i> item did <i>ALM_Review</i> get created (also in state <i>ALM_Defined</i>)?</summary>

**A:**  
When *ALM_Task* transition to state *ALM_Defined*. 
</details>

<details>
<summary><b>Q3:</b>  What state in <i>ALM_Task</i> can <i>Change Package</i> be created, closed?</summary>

**A:**  
Created & closed in *ALM_Started*.  
</details>
