# GLOSSARY 

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INTEGRITY DEFINITIONS](#integrity_definitions)
2. [VIEWS](#views)
3. [SANDBOX](#sandbox)
4. [MEMBER & WORKING FILES](#member_and_working_files)
5. [LOCKS](#locks)
6. [PROJECT](#project)
7. [ITEM TABS](#item_tabs)
8. [LINKS](#links)
</details>

## INTEGRITY DEFINITIONS <a name="integrity_definitions"></a>
**Integrity:** PLM Systems Engineering(SE) tool that based on 3 pillars: Integrity Management, Configuration Management, & GIT and based on 2 parts (server): Workflow & Documents (IM) and SCCM (SI).  
**Product Lifecyle Management (PLM):** Products life through design to launch.   
**Application Lifecyle Management (ALM):** Workflow that manages engineering tasks related to *v-diagram* from *Requirements Management (RM)* to *Test Management (TM)*.   
**item:** Basic unit of info. in Integrity. Make up the ALM workflow.  
**Query:** Allows to view and work w/ items that you are concerned with.

## VIEWS <a name="views"></a>
**viewset:** Collection of views in a specific configuration that persists each time you open and close the Integrity Client.  
***Managed Queries:*** View that searches for queries shared with me and ones I created.  
**My Sandboxes:** Lists the sandboxes that I created.   
**Sandbox:** Opens one of your local Sandboxes with more detail.  
**Projects:** Lists all projects available to me in Integrity.   
**Project:** Opens a project with more detail.  
**static:** View is locked to the originated Sandbox.  
**dynamic:**  View is automatically updated according the currently selected Sandbox in the My Sandboxes view.  

## SANDBOX <a name="sandbox"></a>
**Create Sandbox:** Private workspace on local machine mirroring a project on the public server. Allows working files to be created which have no affect on the main public project. Creating a sandbox registers it with Integrity SCCM. ***Types:***  
* **Regular:** Useful for the sequential development of a project over the long or short term. Contains the member revisions of the main line.  
* **Variant:** Useful for branching off the main development path. Contains the member revisions of the specified variant.  
* **Build:** Useful for testing a specific revision of the project. No changes are possible. Contains the member revisions of the specified checkpoint.  

**Retarget Sandbox:** Change the type of the created sandbox. **Ex.** From *Regular* to *Variant*. ***NOTE:*** Have to Resynchronize after, don't forget.  
**Drop Sandbox:** Unregistering sandbox from Integrity unabling viewing access & resulting it to be no longer connected to the public project. Adiitionaly, an option can be picked on how much stuff is deleted from the local project on the local disk: 
* **Nothing:** No files will be deleted from the Sandbox directory on your local drive.  
* **Sandbox Members Only:** All Sandbox members on your local drive will be deleted, non-members will be left. Integrity server needed. 
* **Entire Sandbox Directory:** Deletes the Sandbox directory on your local drive and all of its contents. Integrity server needed.  
***NOTE:*** *Nothing* (Server not needed)  can replace *Entire Sandbox Directory* (server needed) by just manually deleting the local files.  
**Development Path (Branch):** Path, branch name. Needed for creating *Variant* Sandbox.  

## MEMBER & WORKING FILES <a name="member_and_working_files"></a>
![Table of Member & Working Icon Key](Images/member_working_icon_key.png)    
**Add Member:** A new member being explicitly added to a Sandbox project. To be added must be located in a local sandbox directory. Auto checked in, out thus located in both local & public projects.  
**Drop Member:** Removing a member from both the local & public projects.  
**Revert:** Discards any working changes to the file since it was last checked out, unlocks the revision, & resets the write protection. Opertion transitions ![Picture of File Delta Icon](Images/working_file_delta_icon.png) to ![Picture of Working File Icon](Images/working_file_icon.png). Command only performed when member is *Checked Out*.  
**Resynchronize:** Updating out of sync members from *Sandbox* w/ the member revison of the public project. Should be performed when a ![Picture of Member Delta Icon](Images/member_delta_icon.png) or ![Picture of File Delta Icon](Images/working_file_delta_icon.png) appears, resulting in ![Picture of Member icon](Images/member_icon.png). Opertion causes all changes to be lost. Also, modified working files can be resynchronized (roll back).  

**Check Out:** Extracts the contents of a revision in a member history and copies it to the working file in the local sandbox. Changes to a member must first be checked out. Once checked out, it is locked on the server project.  
**Check In:** Preserves member changes as a new revision in the member’s history.  
**Promote/Demote:** Process of managing data as it moves through defined maturity levels. Each level is represented by a state that is defined by the administrator. Can promote the member state from "In Work" to "Checking" and from "Checking" to "Published". 
**Member History:** Detail information on any member revision.  
**Change Package:** Link from *Change Management* to *Configuration Management*. Collection of modified files for an *ALM_Task* item.  
**Work Package:** 
**Archive:** Whenever a member is added, it is located in the archive. 

## LOCKS <a name="locks"></a>
![Table of Lock Types](Images/lock_types.png)  
**lock:** Feature to control how changes are made to a revision. Check in an updated member & prevent more than one user from simultaneously "Check In" changes to the same revision.  
**exclusive lock:** Only one user can peform a *Check Out* on this revision. No simultaneously work on the same member file with different persons.  
**non-exclusive lock:** Multiple users can peform a *Check Out* on the same member file. Possibility to work on the same member revision with different persons at the same time.   
**unlock:** Lasts until a *Check In*, a *Revert* or a *Remove My Lock* is performed.  

## PROJECT <a name="project"></a>
**Checkpoint:** Tracking evolution of an entire project by saving a copy of the project in the project’s history as a revision. Includes all members & subprojects along w/ their revision numbers. Automatically created when a project is created. 2 Types: *Mainline* & *Variant*. Entire project at the current time.  
**Project History:** See available checkpoints & development paths.  

## ITEM TABS <a name="item_tabs"></a>
**Fields:** Title, Project, Team, Owners, Description, Conclusion, State, Priority, etc.  
**Relationships:** Conenction between different item types.  
**Attachments:** Add up to 10MB per attactment. Add an attachment (any local file) or add a memberlink with Drag & Drop (member revision).  
**Workflow:** Shows workflow & state transitions of the opened *Item*.   
**History:** Every saved change of an *Item* is available here. Can open an *Item* in any previous state.  

## LINKS <a name="links"></a>
**dynamic link:** Link where the member or project can change, as result going into the link changes. Marking a member or project in the *Project* view & selecting *HTTP Link* button.  
**static link:** Link where the member or project stays the same. Open the *Member History*, select a revision, and select *HTTP Link* button.  
**HTTP Link button ![Picture of HTTP Link](Images/link_http.png):** Link opens in web browser. Start w/ `http://skobde-mks.kobde.trw.com:7001...`.  
**GUI Link button ![Picture of GUI Link](Images/link_gui.png):** Link opens in Integrity. Start w/ `url:integrity://skobde-mks.kobde.trw.com:7001...`.  
