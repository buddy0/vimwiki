# ALM_BUILD 

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [ADDING CHECKPOINT IN IDENTITY SECTION EXAMPLE](#adding_checkpoint_in_identity_section_example)
2. [RELATIONSHIPS](#relationships)
</details>

## ADDING CHECKPOINT IN IDENTITY SECTION EXAMPLE <a name="adding_checkpoint_in_identity_section_example"></a>
1. On main menu, insert the Build ID number. 
2. On *ALM_Build* window, select *Edit* button. *Edit_ALM_Build* window opens up.
3. On *Overview* tab, under *General* section, change field *State* from *ALM_Planned* to *ALM_Defined*. In *Planning* section, the *Planned Completion Date* was set to today's date. Select *Apply*  
4. On *Identity* tab *Baseline* section, the *SI Build* was selected (look right). *Select a Source Project* window opens up. 
5. In *Project Name:** section, select project related to checkpoint. Click *Next >*. 
6. Check either ***Normal(mainline)***, ***Variant*** & select *Development Path name*, or ***Checkpoint*** & select either *Revision* or *Label*. Click *Finish*. 
7. On *ALM_Build* item click *OK* or *Apply*.  

## RELATIONSHIPS <a name = "relationships"></a>
| field                             | possible item type                                                                                               |
| --------------------------------- | ---------------------------------------------------------------------------------------------------------------  |
| **Build For (1..n)**              | Build, Delivery                                                                                                  |
| **Builds (0..n)**                 | Build                                                                                                            |
| **Checklists (0..n)**             | Checklist                                                                                                        |
| **Work Items (0..n)**             | Action, CR, Review, T, Work Package                                                                              |
| **Review (0..1)**                 | Review                                                                                                           |
| **Detected in Builds For (0..n)** | Open Point                                                                                                       |
| **Actions (0..n)**                | Action                                                                                                           |
| **Spawns (0..n)**                 | Action, Build, CR, Delivery, Feature, OP, Review, Risk, Task, Test Session, Work Package                         |
| **Tested By (0..n)**              | Objectives, Test Session                                                                                         |
| **Used By (0..n)**                | Test Session                                                                                                     |
| **Spawns (0..n)**                 | Action, Build, CR, Delivery, Feature, Open Point, Review, Risk, T, Work Package                                  |
| **Spawned By (0..n)**             | Action, Build, CR, Delivery, Feature, Input Document Design, OP, Review, Risk, T, Work Package, Partner Exchange |
| **Requires (0..n)**               | Action, Build, CR, Delivery, OP, Review, T, Work Package                                                         |
| **Required For (0..n)**           | Action, Build, CR, Delivery, OP, Review, T, Work Package                                                         |


