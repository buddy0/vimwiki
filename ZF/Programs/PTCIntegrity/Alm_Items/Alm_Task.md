# ALM_TASK

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [IMPORTANT](#important)
2. [CREATE TASK ITEM](#create_task_item)
3. [SWITCHING STATES](#switching_states)
4. [RELATIONSHIPS](#relationships))
</details>

## IMPORTANT <a name="important"></a>
* In *Overview* tab -> *Notes* field, say what checkpoint was created for future reference on where the change(s) were implemented.

## CREATE ALM_TASK ITEM <a name="create_task_item"></a>
***If*** creating an *ALM_Task* item directly from *ALM_Change Request*,  
***then*** CR's & task's *Relationship* section -> *Work Item(1...n)* is automatically filled w/ the related *ALM_Task* OR *ALM_Change Request*.  
1. On *ALM_Change Request* main menu, click *Item*, select *Create Related*. *Create Related Item* window opens up. ***OR***  
On viewset home page, right click on CR -> *Create Releated Item*. *Create Related Item* window opens up. ***OR***  
On viewset home page, select CR -> select main menu *Item* -> *Create Related*. *Create Related Item* window opens up.  
2. Select *ALM_Task*, click *OK*. *ALM_Task* item opens up in the state *ALM_Initiated*.  

***error:*** Cannot Create Releated Task item in relationship tab of CR b/c it is trying to create a Task through an edited CR window that brings us to an edited Task window. The CR hasn't been saved so the Task doesn't know that it can be relatedly linked.  
***solution:*** Have to create a related task item that can be saved before linking to CR.  

## SWITCHING STATES <a name="switching_states"></a>
**ALM_Started -> ALM_Implemented:** *Change Package* needs to be closed. Can be created when state is Started. If necessary, owner of *Task* can go back to *Started* state.  
**ALM_Implemented -> ALM_Prepared:** *Notes* section in *Overview* tab needs to be written. Cannot go back to *Implemented* state. Only way to go back to *Started* state is if related *Review* item fails or CCB moves it back.  

## RELATIONSHIPS <a name="relationships"></a>
| field                           | possible item type                                                                                                                           |
| ------------------------------- | -------------------------------------------------------------------------------------------------------------------------------------------- |
| **Work Item For (1..n)**        | Build, CR, Work Package                                                                                                                      |
| **Changes Authorized (0..n)**   | Checklist Content, Design, Design Document, Model, Model Document, Requirement, Requirement Document, Test Case, Test Specification Document |
| **Checklists (0..n)**           | Checklist, Checklist Document                                                                                                                |
| **Review (Only 1)**             | Review                                                                                                                                       |
| **Spawns (0..n)**               | Action, Build, CR, Delivery, Feature, OP, Review, Risk, Task, Work Package                                                                   |
| **Spawned By (0..n)**           | Action, Build, CR, Delivery, Feature, OP, Review, T, Work Package                                                                            |
| **Requires (0..n)**             | Action, Build, CR, Delivery, OP, Review, T, Work Package                                                                                     |
| **Required For (0..n)**         | Action, Build, CR, Delivery, OP, Review, T, Work Package                                                                                     |
