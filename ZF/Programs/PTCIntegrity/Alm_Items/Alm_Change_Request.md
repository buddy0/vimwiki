# ALM CHANGE REQUEST

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [IMPORTANT](#important)
2. [STATES](#states)
3. [TEMPLATE](#template)
4. [RELATIONSHIPS](#relationships)
</details>

## IMPORTANT <a name="important"></a>
* CRs need to be linked to release Builds for new Integrity Process.

## STATES <a name="states"></a>
**Approved -> Planned:** CR state. Related Task needs to be created at Defined State w/ Task fields Owner, [Reviewer], Planned Completion Date, & Remaining Effort filled out.

## Template <a name="template"></a>
```
[Matthew Redoute, date]
-------------------------------------------------------------------------------------
Analysis:

Risk (low/medium/high): 
Proposal of next steps (include effort and verification): Task

-------------------------------------------------------------------------------------
TASK1
-------------------------------------------------------------------------------------
Summary:
Owner: 
Reviewer: 
```
## RELATIONSHIPS <a name="relationships"></a>
| field                         | possible item type                                                                                                                   |
| ----------------------------- | ------------------------------------------------------------------------------------------------------------------------------------ |
| **Change Request For (0..n)** | OP                                                                                                                                   |
| **Work Item For (0..n)**      | Build, CR, Feature                                                                                                                   |
| **Work Items (1..n)**         | CR, T, Work Package                                                                                                                  |
| **Actions (0..n)**            | Action                                                                                                                               |
| **Affects (0..n)**            | Design, Requirement, Test Case                                                                                                       |
| **Tested By (0..1)**          | Test Session                                                                                                                         |
| **Spawns (0..n)**             | Action, Build, CR, Delivery, Feature, OP, Review, Risk, Task, Test Session, Work Package                                             |
| **Spawned By (0..n)**         | Action, Build, CR, Delivery, Design, Design Document, Feature, Input, OP, Requirement, Requirement Document, Review, T, Work Package |
| **Requires (0..n)**           | Action, Build, CR, Delivery, OP, Review, T, Work Package                                                                             |
| **Required For (0..n)**       | Action, Build, CR, Delivery, OP, Review, T, Work Package                                                                             |
