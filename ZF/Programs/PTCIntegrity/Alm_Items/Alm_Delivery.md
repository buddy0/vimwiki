# ALM DELIVERY

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [IMPORTANT](#important)
2. [RELATIONSHIPS](#relationships)
&emsp;a. [RELATIONSHIP BUILD EXAMPLE](#relationship_build_example)
</details>

## IMPORTANT <a name="important"></a>
* Delivery is for 1 project. Contains Builds.  
Builds are for Delivery. Its work items are CR(s)/T(s).

## RELATIONSHIPS <a name = "relationships"></a>
<details>
<summary><b>TABLE</b></summary>

| field                             | possible item type                                                                                               |
| --------------------------------- | ---------------------------------------------------------------------------------------------------------------  |
| **Delivery For (Only 1)**         | Project                                                                                                          |
| **Builds(0..n)**                  | Build                                                                                                            |
| **Action (0..n)**                 | Action                                                                                                           |
| **Review (0..1)**                 | Review                                                                                                           |
| **Planned For Open Point (0..n)** | Open Point                                                                                                       |
| Tested By (0..n)                  | Test Objective                                                                                                   |
| Delivers (0..n)                   | Feature                                                                                                          |
| Spawns (0..n)                     | Action, Build, CR, Delivery, Feature, OP, Review, Risk, Task, Work Package                                       |
| Spawned By (0..n)                 | Action, Build, CR, Delivery, Feature, OP, Review, T, Work Package                                                |
| Requires (0..n)                   | Action, Build, CR, Delivery, OP, Review, T, Work Package                                                         |
| Required For (0..n)               | Action, Build, CR, Delivery, OP, Review, T, Work Package                                                         |
</details>

### RELATIONSHIP BUILD EXAMPLE <a name="relationship_build_example"></a>
* ***CHR MY23 HD460 - V33.0 - Integ 1:*** Linked to CRs/Ts that affect release  
* ***CHR HD460 V33.0:***  
    * Linked to Build [program release] Basis SW -> Linked to component build checkpoints  
    * LInked to Build [program release] Performance SW
