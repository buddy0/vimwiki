# ALM OPEN POINT

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [IMPORTANT](#important)  
&emsp;a. [WHY HAVE OPS](#why_have_ops)
2. [SPAWN CHILD OPS](#spawn_child_ops)  
3. [RELATIONSHIPS](#relationships)
</details>

## IMPORTANT - [video](https://web.microsoftstream.com/video/e3414994-5e4d-4eba-a005-b60057550767) <a name = "important"></a>
![OP Workflow](Images/OP_Workflow.png)
* ***all SW changes need an associated OP for:*** analysis of SW components and teams & deliveries and projects.  
Results in one organized backlog.  
* ***right now, 2 different OPs:*** Requirements and Incident/Problem/Bug.
* ***who can create an OP:*** anyone, use the custom buttons for this. I don't see them tho on my developer viewset.
* ***who owns OPs:*** SWPM is primary then hands off to Technical Owner.
    * ***SWPM:*** manages all OPs, adds technical owner as owner & assignes them the impact analysis/root cause action item, & assigns deliveries to OP: can be in any state & can assign "ERRATA" meaning leaving open w/o delivery (future projects would copy ERRATA OPs).
    * ***Technical Owner:*** does overall impact analysis, leads root cause analysis for Issue OPs, breaks down work of OP into CRs and links them & as a result moves OP state to "Checked", & read across on the OP.
* ***copy items:*** must Copy OPs, CRs, and Tasks to the new project!   
Why? So, that above items can be closed even if it's not done on children OP projects, continuous integration, consistency checker

### WHY HAVE OPS <a name = "why_have_ops"></a>
1. Cross team/Cross component analysis.
2. Cross project impact.
3. Organized & simplified list of changes.  
&emsp;a. Organize by high level change or impact  
&emsp;b. More clearly communicates delivery content to customers, management, & others.

## SPAWN CHILD OPS <a name = "spawn_child_ops"></a>
* ***batch file:*** `SCS\Local\Livonia\Tools\Integrity\User Tools\SpawnOps.bat`  
* ***training slides:*** `SCS\Local\Livonia\Tools\Integrity\User Tools\SpawnOPTraining.ptx` 
    1. Batch file must be modified before user configurations; Can save the file as "SpawnOPs[Paraent OP ID].bat" to make it writable.  
    2. Only section needed to change is section "USER INPUT REQUIRED".
    3. Change all the projects that need child OP to 1. Do not select Parent OP's project.
    4. `set OPSrc="Child OP WS<x>"` Standard is "Child OP" & "WS<x>" indicates where problem found.
    5. `set OPDesc=" "` Copy parent title summary. **Note:** Project specific information will automatically be added to the summary for the script.  
    6. `set ParentOP=` Integrity ID of parent OP.
    7. ***run batch file:*** Double click in file explorer or run from command line.

    ***notes:***  
    Description of Child OP (automatically done by script): Please see linked Parent OP for details.  
    Script will open OPs at the end & user must manually move them to the defined w/ necesary fields completed.   

## RELATIONSHIPS <a name = "relationships"></a>
| field                           | possible item type                                                                                               |
| ------------------------------- | ---------------------------------------------------------------------------------------------------------------  |
| **Change Requests(1..n)**       | CR                                                                                                               |
| **Planned For Delivery (0..1)** | Delivery                                                                                                         |
| **Review (0..n)**               | Review Package                                                                                                   |
| **Actions (0..n)**              | Action                                                                                                           |
| **Inputs (0..n)**               | Inputs, Input Document                                                                                           |
| **Found By (0..n)**             | Test Session                                                                                                     |
| **Tested By (0..1)**            | Test Session                                                                                                     |
| **Spawns (0..n)**               | Action, Build, CR, Delivery, Feature, OP, Review, Risk, Task, Test Session, Work Package                         |
| **Spawned By (0..n)**           | Action, Build, CR, Delivery, Feature, Input Document Design, OP, Review, Risk, T, Work Package, Partner Exchange |
| **Requires (0..n)**             | Action, Build, CR, Delivery, OP, Review, T, Work Package, Partner Exchange                                       |
| **Required For (0..n)**         | Action, Build, CR, Delivery, OP, Review, T, Work Package                                                         |
