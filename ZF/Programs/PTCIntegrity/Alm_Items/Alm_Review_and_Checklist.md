# ALM_REVIEW + ALM_Checklist

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [NOTES](#notes)
2. [STEPS](#steps)
3. [ERROR](#error)
4. [RELATIONSHIPS](#relationships)
</details>

## NOTES <a name="notes"></a>
***Review can only be started when:*** Related Task is in *Prepared* state. Once this happens Review auto. transitions from *Started* to *Realized* state.  
***When was Review item created:*** Once Task is in Defined state, a Review was auto. created in the Defined state as well.  
**Review in Comleted state means:** Related Task auto. transitions from *Prepared* state to *Completed*. As a result, related Change Request auto. transitions from *Started* to *Realized* state.  
**checklist template id:** `4153993`  

## STEPS <a name="steps"></a>
1. **VIEW DIFFERENCES**  
&emsp;**a.** On Review item, click *Relationships* tab -> double click on *Task*. *Task* item opens up.  
&emsp;**b.** Click *Change Packages* -> double click on desired change package -> *Change Package* window opens up.  
&emsp;**c.** Right click on desired file -> *View Differences*.  

2. **CHECKLIST**  
&emsp;**a.** On Review item, click on *edit the item* button(or CTRL E). *Edit ALM_Review* window opens up.  
&emsp;**b.** On main menu click on *Item* -> *Create Related* -> Select *ALM_Checklist* -> click *Ok*. *ALM_Checklist* window opens up.  
&emsp;**c.** In *General* section, for *Template* insert id **4153993** in search bar -> click *Ok* -> click *Apply*. Checklist fills w/ template & state is now *Initiated*.  
&emsp;**d.** Fill out *Checklist*; #'s 6-10 put *not applicable* & comment *na*.  
&emsp;**e.** Change state to *Closed* -> Click *Ok*.  

3. **REVIEW**  
&emsp;**a.** Write out Conclusion -> change *Object State* to *ok* -> change state to *Completed* -> Click *Apply*.  

<details>
<summary><b>OTHER OPTIONS</b></summary>

1. Click *edit the item* button on Menu toolbar. *Edit ALM_Review* window opens up. 
2. Goto *Checklists* section and click *Create a related item and add it to the field* buttton. *Create Related Item* window opens up.  
3. Click *OK*. Copy ALM_Checklist window opens up.  
4. In *General* section, for *Template* insert id **4153993** in search bar. Click *ok*. 
5. In ALM_checklist item, click *Apply*. Checklist fills with the template & *ALM_Checklist* now in state *ALM_Initiated*.
6. Fill out *Checklist*; #'s 6-10 put *not applicable* & comment *na*. 
7. Goto *Relationships* tab, in *Checklist For (Only 1)* field, click on *Add a related item*, & insert into the ALM_Review item id.  
8. In *General* section, for *State* change to *ALM_CLosed*. Click *Apply*. Under *Workflow* tab, visually can see ALM_Checkist is closed.  
9. Click *OK* to exit ALM_Checklist. 
 
***OR*** *to skip step 7, add these steps after step 4*

1. **step 1 above**
2. **step 2 above**
3. **step 3 above**
4. **step 4 above**
5. In *ALM_Checklist* item, click *OK*. ALM_Checklist closes. Back to *Edit ALM_Review* item window. Click ***OK*** to save the newly created checklist.  
Now the *ALM_Checklist field* is now filled out automatically w/ related *ALM_Review*.  
Kind of like the chicken (*ALM_Review*) came before the egg (*ALM_Checklist*).  
6. In *ALM_Review* Right click on newly created checklist and select *Edit item*. *Edit ALM_Checklist* item window opens up. Relationship tab already has the related *ALM_Review* item. 
7. **step 6 above**   
8. **step 7 above**    
9. **step 8 above**  
10. **step 9 above**   
</details>

## ERROR <a name="name"></a>
***MKS124147:*** The following fields which are mandatory in the state ALM_Closed have not been filled in: Checklist For  
***solution:*** I created multiple chechlists, however one of the states was *ALM_Initiated*. Removing them does *not* delete them. Once the *Apply* or *Ok* buton was pressed, the Checklist was saved and given a permanent ID. Thus, it can't be deleted. Completing this checklist solved this issue. 

## RELATIONSHIPS <a name="relationships"></a>
| field                    | possible item type                                                         |
| ------------------------ | -------------------------------------------------------------------------- |
| **Review For (0..1)**    | Build, Delivery, OP, T                                                     |
| **Work Item For (0..n)** | Build                                                                      |
| **Checklists (0..n)**    | Checklist, Checklist Document                                              |
| **Spawns (0..n)**        | Action, Build, CR, Delivery, Feature, OP, Review, Risk, Task, Work Package |
| **Spawned By (0..n)**    | Action, Build, CR, Delivery, Feature, OP, Review, T, Work Package          |
| **Requires (0..n)**      | Action, Build, CR, Delivery, OP, Review, T, Work Package                   |
| **Required For (0..n)**  | Action, Build, CR, Delivery, OP, Review, T, Work Package                   |
