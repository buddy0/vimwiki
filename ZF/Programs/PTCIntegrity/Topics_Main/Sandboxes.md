# INTEGRITY SANDBOXES

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [SANDBOX](#sandbox)
2. [CREATE SANDBOX](#create_sandbox)
3. [DROP SANDBOX](#drop_sandbox)
4. [FIND DEVELOPMENT PATH FOR CREATING VARIANT SANDBOX](#dp_for_creating_variant_sandbox)
5. [RETARGET SANDBOX](#retarget_sandbox)
6. [RANDOM ERROR WHEN CREATING VAIRANT SANDBOX](#error_when_creating_sandbox)
</details>

## SANDBOX <a name="sandbox"></a>
**sandbox:** Private workspace mirroring the public **Project**. Access to projects and their members. A user's computer holds sandboxes and working files.  
***avoid unwanted local data loss:*** Create separate directory for each sandbox and no nesting of sandboxes.  
**resynchronize:** Update out of sync files to most current member revisions. ***Note:*** All changes will be lost.  
***dropping a sandbox:*** Page 55 on `PTC_Source_Integrity_Training--Braking_V2.pdf`.  
**member:** File to the proejct.  

## CREATE SANDBOX <a name="create_sandbox"></a>
1. On *Projects* view, right click on desired project, select *Create Sandbox*. *Create Sandbox* window opens up. ***OR***  
On *Projects* view, click on desired project, goto main menu tab & click on *Sandbox*, select *Create...*. *Create Sandbox* window opens up.
2. Write *Sandbox Location*. Click *Next >*. 
3. Write *Project Name* (Normally, leave as default). Click *Next >*. 
4. Select either: *Current configuration*, *Normal (mainline)*, *Variant* (select *Development Path Name*), or *Build* (select *Revision* & write *Label*). Click *Finish*. 

## DROP SANDBOX <a name="drop_sandbox"></a>
1. In *My Sandboxes* section, right click on project, select *Drop sandbox*. *Confirm Sandbox Drop* window opens up.
2. Select what happens to sandbox: *Nothing*, *Sandbox members only*, or *Entire sandbox directory*. 
3. Click *Yes*.  

## FIND DEVELOPMENT PATH (AKA BRANCH) FOR CREATING VARIANT SANDBOX <a name="dp_for_creating_variant_sandbox"></a>
***read, no copy DP:***  
1. On any view, right click on desired project, select ***View Project Information***. *View Project Information* window opens up. ***OR***     
Select project on any view. On main menu tab, click on *Project*, select *Views*, select ***View Information***. *View Project Information* window opens up.   

***don't know DP, but want to copy DP:***  
1. On any views, right click on project, select ***View Project History***. *Project History* window opens up. ***OR***  
Select project on any view. On main menu tab, click on *Project*, select *Views*, select ***View History***. *Project History* window opens up.  
2. Click on desired checkpoint. Right side of screen, should see *Development Path:*. The text can now be copied.  
This DP used for creating a Variant Sandbox.

***know DP, and want to copy DP:***  
1. On any view, right click on project, select ***View Project Information.*** *Project Information* windown opens up. 
2. Click on tab *Development Paths*, select desired development path, CTRL C. Text is copied now into buffer.

## RETARGET SANDBOX <a name="retarget_sandbox"></a>
**retarget sandbox:** Changes to a different type of sandbox. **Ex.** *Regular* to *Variant*.  
1. On *My Sandboxes* view, right click on desired sandbox, & select *Retarget Sandbox*. *Retarget Sandbox* window opens up. ***OR***  
On main menu tab click on *Sandbox*, *Retarget*. *Retarget Sandbox* window opens up.
2. Can select *Normal(mainline)*, *Variant*, or *Build*.  
3. *Resyncrhonize* newly created sandbox. Because working files are from previous variant & want to have new variants' members from public server to be on local machine.  

## RANDOM ERROR WHEN CREATING VAIRANT SANDBOX (MANOJ HELPED) <a name="error_when_creating_sandbox"></a>
On `/SCS/SCS_Core_Based/08_Software/Application/IO/IO_Gen1/`:  
1. click *Project* tab on main menu -> *Subproject* -> *Configure*. *Configure Subproject* window opens up. 
2. In *Options* section under *Configure this subproject as:* Normal.  
3. In *Change Package* section under *Change Package:** select change package.  
4. Click *OK*.  
