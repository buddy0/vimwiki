# SUBPROJECTS

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [IMPORTANT](#important)
2. [CONFIGURE SUBPROJECTS](#configure_subprojects)
</details>

## IMPORTANT <a name="important"></a>
* A subproject that is in build has a specific checkpoint out of all the subprojects in any of the project level development paths.  
* Each Build subproject has a CPID.
* If 2 project level DP have same Build subproject DP, then they both have same member revisions.  
If 2 project level DP have different subproject DP, then at least one member revision is different.
* Configure correct subproject from Build to Variant.
* If I configure a subproject to Variant from Build and a member has an updated member revision icon:  
1. ***Resynchronize*** will automatically update to latest member revision on public server. Does *not* show in related Task Change Package.
2. ***Update Member Revision*** to latest member, shows in related Task Change Package. 

A build subproject has a development path & checkpoint. This checkpoint is unqiue out of all the subprojects in any of the project level development paths.
This means that a subproject checkpoint in one development path can be applied to another development path.

## CONFIGURE SUBPROJECTS <a name="configure_subprojects"></a>
Needed to change subproject from *Build* to *Variant*.  
To find possible subproject development path, check project history on subproject.  
***note:*** Configuring subproject to build will update all files to member revisions pertaining to that build.  

1. Right click on subproject -> *Configure Subproject*. *Configure Subproject* window opens up.
2. *Options* section: select *Variant* -> choose DP name.   
   *Change Package* section: select desired *Change Package*. *Click Ok*.  
3. Now member can be checked out/in or update member revision.
4. Checkpoint subproject.
5. Right click on subproject -> *Configure Subproject*. *Configure Subproject* window opens up.  
6. *Options* section: select *Build* -> *Revision* -> choose newly created checkpoint number.  
   *Change Package* section: select desired *Change Package*. *Click Ok*. 
