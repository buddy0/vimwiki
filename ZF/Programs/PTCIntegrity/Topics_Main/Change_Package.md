# CHANGE PACKAGE
Involves the item *ALM_Task*. 

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [CREATE CHANGE PACKAGE](#create_change_package)
2. [CLOSE CHANGE PACKAGE](#close_change_package)
3. [NOTES](#notes)
</details>

## CREATE CHANGE PACKAGE <a name="create_change_package"></a>
*ALM_Task* state has to be in *ALM_Started*.  
No Edit mode required to create *Change Package* since creating one leaves no trace in *History* tab. 
1. On *Change Packages* tab, select *Change Package* from top menu, & select *Create...* . 

## CLOSE CHANGE PACKAGE <a name="close_change_package></a>
Needed when transitioning *ALM_Task* state from *ALM_Started* to *ALM_Implemented*.
1. On *Change Packages* tab, select *Change Package* from top menu, & select *Close*. ***OR***  
 On *Change Packages* tab, right click on desired *Change Package*, & select *Close Change Package*.  

 ## NOTES <a name="notes"></a>
 * Can't open up a change package after its been closed unless have permissions.  
 * Can't create another change package in state *ALM_Implemented*. Have to change state from *ALM_Implemented* to *ALM_Started*. 
