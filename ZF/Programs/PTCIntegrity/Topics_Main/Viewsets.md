# INTEGRITY VIEWSETS

<details>
<summary><b>TABLE OF CONTENTS</b></summary>


1. [NEW ZF VIEWSETS](#new_zf_viewsets)
2. [CREATING NEW VIEWSET](#creating_new_viewset)
3. [PROJECT FILTER](#project_filter)
</details>

**viewsets:** Collction of views configured to optimize integrity client interface for specific roels and users.  
They are configured with specific options, menus, and default views including dashboards and queries

## NEW ZF VIEWSETS <a name="new_zf_viewsets"></a>
***Mandatory***
* ALM_ZF Devloper
* ALM_ZF Project Manager
* ALM_ZF Requirements Management 

## CREATING NEW VIEWSET <a name="creating_new_viewset"></a>
**Why:** A newly created viewset allows user to add custimaztions to its viewset such as the menu options.  
1. In main menu, click on *ViewSet* & select *New*. New ViewSet window opens up.   
2. Add a name.  
3. Check *Copy Existing ViewSet* checkbox. Select ok. 
4. Switch to new viewset. In main menu, click on *ViewSet*, select *Switch To*, and click one newly created viewset.  

## PROJECT FILTER <a name="project_filter"></a>
**Why:** If a query has alot of item, can use this filter to specify a particular project.
1. In main menu, goto *View*, *Options...*, & *Options* window opens up.
2. In *view layout* seciton, check *Show project filter*.
3. Click *Ok*.

