# DEVELOPMENT PATH
* Main projects & subprojects have different developement paths.  
* Useful for creating *Variant sandboxes* & *Configure subrojects*.  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [FIND DP FOR MAIN PROJECT](#find_dp_for_main_project)
2. [FIND DP FOR SUBPROJECT](#find_dp_for_subproject)
</details>

## FIND DP FOR MAIN PROJECT <a name="find_dp_for_main_project"></a>
***read, no copy DP:***  
1. On any view, right click on desired project, select ***View Project Information***. *View Project Information* window opens up. ***OR***     
Select project on any view. On main menu tab, click on *Project* -> *Views* -> ***View Information***. *View Project Information* window opens up.   

***don't know DP, but want to copy DP:***  
1. On any views, right click on project -> ***View Project History***. *Project History* window opens up. ***OR***  
Select project on any view. On main menu tab, click on *Project* -> *Views* -> ***View History***. *Project History* window opens up.  
2. Click on desired checkpoint. Right side of screen, should see *Development Path:*. The text can now be copied.  
This DP used for creating a Variant Sandbox.

***know DP, and want to copy DP:***  
1. On any view, right click on project -> ***View Project Information.*** *Project Information* windown opens up. 
2. Click on tab *Development Paths* -> select desired development path -> CTRL C. Text is copied now into buffer.
</details>

## FIND DP FOR SUBPROJECT <a name="find_dp_for_subproject"></a>
1. Right click on subproject -> *Project History*. *Project History* window opens up.  
2. On menu tab: select *View* -> *Change Filter*, select *In Development Path*.  
3. Click *Ok*.
  
