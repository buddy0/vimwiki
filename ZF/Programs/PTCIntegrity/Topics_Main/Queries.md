# INTEGRITY QUERIES

* **query:** Find specific items of the same type. 1 or more filters can be defined.    
* **refresh:** Double click or select in the Integrity Management (IM) section & Press <F5> to refresh the query.
* ***create a query:*** **1.** Click on tab *Query*. **2.** Click on *Create*. **3.** Click on tab *Definition*. **4.** Write query.  
* ***copy & edit query:*** Copy and edit an existing query. After selecting query (not IM item) in tab *Managed Queries*, goto step 2 above and click on *Copy*.  
* ***project filter on query:*** Main menu goto *View* -> *Options* -> *View layout* section -> check *Show project filter*.
* ***Managed Queries:*** View that searches for queries shared with me and ones I created.  
    * Right click to *Add Query to Favorites*, *View Query Definition*, *Copy Query*, *Create Query*, etc.  

## CREATING OWN QUERIES
***note***: For faster search indexing, manage queries top down with top being very picky such as owner instead of state.  

<details>
<summary><b>Orphaned Checklists</b></summary>

1. Goto main menu on *Viewet* -> *Query* -> *Create ...*. *Create Query* window pops up.
2. Click *Add Filter, And*. Type of filter to create = *Fields*. Field to query on = *Type*. Click *Next >*. Type is one of = *ALM_Checklist*. Click *Add*, *Ok*, and *Finsih*.
3. Click *Add Filter, And*. Type of filter to create = *Fields*. Field to query on = *Created By*. Click *Next >*. Created by is one of = *- Me -*. Click *Add*, *Ok*, and *Finsih*. 
4. Click *Add Filter, And*. Type of filter to create = *Fields*. Field to query on = "Template*. Click *Next*. Template is one of = *4153993*. Click *Add*, *Ok*, and *Finsih*.
5. Click *Add Filter, And*. Type of filter to create = *Relationships*. Relationship field to query on = *Checklists For*. Field in related item to query on = *- Has Relationship -*. Click *Next >*. Both checkboxes checked. Click *Finish*. 
</details>

<details>
<summary><b>My_Change_Requests</b></summary>

1. Goto main menu on *Viewset* -> *Query* -> *Creat ...*. *Create Query* window opens up.
2. Click *Add Filter, And*. Type of filter to create = *Fields*. Field to query on = *Type*. Click *Next >*. Type is one of = *ALM_Change Request*. Click *Add*, *Ok*, and *Finsih*.
3. Click *Add Filter, And*. Type of filter to create = *Fields*. Field to query on = *Created By* or *Owners*. Click *Next >*. Created by/Owners is one of = *- Me -*. Click *Add*, *Ok*, and *Finsih*.
4. Click *Add Filter, And*. Type of filter to create = *Fields*. Field to query on = *State*. Click *Next >*. State is one of = *ALM_Closed*. Click *Add*, *Ok*, and *Finsih*. Click on *and State = "ALM_Completed* & click on *Invert* button.
</details>
