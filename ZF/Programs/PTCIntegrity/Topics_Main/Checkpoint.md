# CHECKPOINT
**checkpoint:** Entire package of the modified files. Creates a new project revision.  
***NOTE:*** When Checkpointing make sure the entire sandbox is *Resynchronize*. 

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [IMPORTANT](#important)
2. [CREATING A CHECKPOINT](#creating_a_checkpoint)
3. [EXAMPLE: STEPS FOR CREATING CHECKPOINT](#example_steps_for_creating_checkpoint)
</details>

## IMPORTANT <a name="important"></a>
* Want to find description of previous checkpoint: View Project History -> View -> List -> Click on Date twice. 
* Before checkpointing always view project differences to check if any other changes occured.

## CREATING A CHECKPOINT <a name="creating_a_checkpoint"></a>
1. In *Sandbox* view, right click on topmost `project.pj`, select *Checkpoint Project...* . *Checkpoint* window opens up. ***OR***  
In *Sandbox* view, select the topmost `project.pj`. In the main menu, select *Project*, select *Checkpoint...* . *Checkpoint* window opens up.
2. Write a *Checkpoint Description* & a *Label*. Select *OK*.

## EXAMPLE: STEPS FOR CREATING CHECKPOINT <a name="example_steps_for_creating_checkpoint"></a>
1. [CREATE VARIANT SANDBOX](sandboxes.md)   

2. [CREATE CHANGE PACKAGE](change_package.md)

3. *Optional* - [CONFIGURE SUBPROJECT](subprojects.md)  

3. [MEMBER ACTIONS: CHECK OUT/CHECK IN or UPDATE MEMBER REVISION](members.md)

4. *Optional* - [CREATE SUBPROJECT CHECKPOINT](checkpoint.md)  

5. *Optional* - [CONFIGURE SUBPROJECT](subprojects.md)

6. [CREATE MAIN PROJECT CHECKPOINT](checkpoint.md)

7. [LINK CHECKPOINT TO *ALM_Build* ITEM](alm_build.md)   

8. [CLOSE CHANGE PACKAGE](change_package.md) 
