# INTEGRITY ITEMS [link](https://plm-training.emea.zf-world.com/videos/basic-item-handling/ILMVT001-BasicItemHandling_player.html)  

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INTRODUCTION TO INTEGRITY ITEMS](#introduction_to_integrity_items)
2. [ITEM TYPES USED AT ZF](#item_types_used_at_zf)
3. [CREATING RELATED ITEMS ](#creating_related_items)
</details>

ALM artifacts and processes are kept, managed, and tracked by **items**. At ZF, these are associated w/ the discplines of *Change Management & Problem Resolution Managment*, *Requirement Management for SW*, and *Test Management for all discplines*. Different item types used to represent each artifact type. 

## INTRODUCTION TO INTEGRITY ITEMS <a name="introduction_to_integrity_items"></a>
* **integrity items:** Basic unit of info. in Integrity. Different item typea are available that represent each of the ALM artifacts and processes. Ex. Open point, change request, task, requirement, design specification, and test case.
* Every item type shares basic characteristics: each has a form which configured with fields that capture data relevant to the type, each has a workflow specific to the type.
* Item has multiple tabs: fields, relationships, attachments, properties, workflow, history, ...

## ITEM TYPES USED AT ZF <a name="item_types_used_at_zf"></a>
Diagram below lists item types that support change management processes. Create item: **1.** Left click tab *Item*. **2.** Left click *Create*.  
![Item Types Used at ZF](Images/ItemTypes.png)  

## CREATING RELATED ITEMS <a name="creating_related_items"></a>
1. Goto main menu: Item -> Create Related. Or right click on item -> Create Related Item. ***RECOMMENDED***  
2. Item relationships tab: Created Related item.  
* Item fields:  
    * ***summary:*** BSW_Subsystem: Comment Issue.  
    * ***team:*** `BrkSw OS`, `BrkSw FM`, or `BrkSw MCAL`  




