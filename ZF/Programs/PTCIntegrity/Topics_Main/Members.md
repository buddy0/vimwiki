# MEMBERS
<details>
<summary><b>TABLE OF CONTENS</b></summary>

1. [IMPORTANT](#important)  
2. [CHECK OUT/CHECK IN](#checkout_checkin)  
&emsp;a. [MORE ON CHECK OUT A MEMBER](#more_checkout_member)
3. [ADD/DROP A MEMBER](#add_drop_member)  
&emsp;a. [ADD MEMBER FROM ARCHIVE](#add_member_from_archive)  
4. [UPDATE MEMBER REVISION](#update_member_revision)
5. [EXAMPLES](#examples)
&emsp;a. [MEMBER HISTORY EXAMPLE](#member_history_example)
&emsp;b.[MUTIPLE PROJECTS MEMBER CHECK IN/UPDATE MEMBER REVISION EXAMPLE](#multiple_projects)
6. [QUESTIONS](#questions)
</details>

## IMPORTANT <a name="important"></a>
* Members do ***not*** have a development path and are ***not*** specific to one either. Their revisions are open for any development path(s).
* Each member revision has a CPID, so don't have to go to in depth with the description.
* If a new project level variant and/or subproject variant is created, then a check out/in command will auto. branch the member.  
If member revision is below current member revision, then check in command will auto. branch the member. Guessing same is for Build subproject level when checkpointing.
development path:         MonitorStatus.c at 1.34  
another development path: MonitorStatus.c at 1.34.

Question: If on one development path MonitorStatus.c goes from 1.34 -> 1.35, then will Rynchronize on another development path cause MonitorStatus.c member revision to be 1.35?
Question: I did notice that even after rychronize some members still have the blue member revision icon available because a current member revision is 1.19, but there is an update member revision available 1.20. Why is this?

## CHECK OUT/CHECK IN <a name="checkout_checkin"></a>
*Change Package* needs to be created in *ALM_Task*: [link](change_package).  
1. In *Sandbox* view, right click on member, select *Check Out...*. *Check Out* window opens up. 
2. Select desired *Change Package*. Select *OK*. 
3. Right click on the same member, select *Check In...*. *Check In* window opens up. 
4. Write a description. Select *OK*.
</details> 

### MORE ON CHECK OUT A MEMBER <a name="more_checkout_member"></a>
* When a member is checked out an assoicated *Working CPID* is assigned.  
Every single time a member is checked out, the *entry* count increases on the *Change Package* (Visible on *ALM_Task* item, *Change Package* tab).  
Reverting a member decreases this entry count (make sure to refresh the page).  
* ***If*** I *Check Out* a member first before making changes, ***then*** the *Read Only* privileage is turned off.  
***If*** I don't to above step, ***then*** I have to:  
**1.** Open up sandbox directory in file explorer, right click on member/directory, select  *Properties*. *<filename> Properties* window opens up.  
**2.** On *General* tab, under *Atrributes* section, uncheck *Read-only*. Click *OK*. 

## ADD/DROP A MEMBER <a name="add_drop_member"></a>
Requires a *Change Package*, have an option to create one as well.

### ADD MEMBER FROM ARCHIVE <a name="add_member_from_archive"></a>
Need to *Add a member* first. Once member is added it is now in the archive.  
**Why:** So there is only 1 location for this member among different projects.  
1. In *Sandbox* view right click on desired subproject. Select *Add Members Form Archive*. *Add Members From Archive* window opens up. ***OR***  
In *Sandbox* view click on desired subproject. In main menu, click on *Member*, select *Add From Archive*. *Add Members From Archive* window opens up.  
2. Click on *Select Archives* button. *Locate the Member Archive(s) you would like to use on this <server-name>.* Select desired member to be added. Click *OK*.  
3. In *Change Package* seciton, select desired change package or create a new one. Click *Finish*. *Add Member From Archive* confirmation window opens up.  
4. Select *OK*. 
5. *Resyncrhonize* member after archive ***(why?)***.  
</details>

## UPDATE MEMBER REVISION <a name="update_member_revision"></a>
Change the member revision for the project on public server. Previous member revision will be in Integrity forever.  
**Why:** Done if a member is the same for each project/development path/variant.  
If we were to check out & in a member, a confirm to branch window might pop up if there is no branch for this member.  
Selecting *Yes* creates a new branch w/ a new member revision: 1.8 -> 1.8.1.  
Selecting *No* means that no new branch is created & member revision is upated: 1.8 -> 1.9.  
&emsp;Now, on a different DP update member revision 1.9 can be used instead of check out/check in command which would make member revision 1.8.1.  
 
1. On *Sandbox* view click on desired member.  F
2. On main menu: *Member* -> *Update Member Revision*. *Update Member Revision* window opens up.  
3. Revision to set: Click on *Revsision* & select desired member revision. Click *Ok*.  
4. *Resynchronize* the member b/c now working file differs from member revision. 

## EXAMPLE <a name="example"></a> 

### MEMBER HISTORY EXAMPLE <a name="member_history_example"></a>
On DP MY22, I checked out a member revision currently at 1.1. When I Checked in a *Confirm Create Branch* window popped.   
This happened since the member has not yet been branched on this variant DP.  
This is important because if other variants are going to change this member specifically for their project, then branching is needed.      
I didn't branch but I soon realized that MY22 member is doing a new future implemtation that is not needed for MY21.  
Thus, a bug fix for MY21 was needed so a branch was auto. applied from 1.1 to 1.1.1.1.  

WHAT I DID  
<details>
<summary><b>Member History on MY22 DP</b> (showing MY21 DP WR for reference)</summary>

```
STEP 1         STEP 2                            STEP 3     
                 WR22                      WR22           WR21
               +-----+                   +-----+       +---------+        
               | 1.2 | MY22              | 1.2 | MY22  | 1.1.1.1 | MY21
               +-----+                   +-----+       +---------+  
                  ^                        ^               ^
WR22,WR21     WR21|                        | ---------------       
+-----+        +-----+                   +-----+                         
| 1.1 | MY21   | 1.1 | MY21              | 1.1 | MY21            
+-----+        +-----+                   +-----+
```
</details>

WHAT I SHOULD HAVE DONE  
<details>
<summary><b>Member History on MY22 DP</b> (showing MY21 DP WR for reference)</summary>

```
STEP 1         STEP 2                            STEP 3  
                          WR22            WR21              WR22
                       +---------+       +-----+       +---------+
                       | 1.1.1.1 | MY22  | 1.2 | MY21  | 1.1.1.1 | MY22
                       +---------+       +-----+       +---------+
                            ^               ^              ^
WR22,WR21    WR21|-----------               |---------------
+-----+       +-----+                    +-----+
| 1.1 | MY21  | 1.1 | MY21               | 1.1 | MY21
+-----+       +-----+                    +-----+
```
</details>

### MUTIPLE PROJECTS MEMBER CHECK IN/UPDATE MEMBER REVISION EXAMPLE <a name="multiple_projects"></a>
Each project each had their own DP, but they used the same component subproject DP for DevData.
<details>
<summary><b>Each project each had their own DP, but they used the same component subproject DP for DevData.</b></summary>

```
DT
Configure Subproject: DevData as variant FM_DevData_GM_CHR_JL_PHEV_15.1.4.1.5.1(as build 1.15.1.4.1.5.1.9)
Update: Fmm.c 1.3.1.1.1.2.1.3
New checkpoint needed.
Configure Subproject: DevData as build(1.15.1.4.1.5.1.10)(as variant FM_DevData_GM_CHR_JL_PHEV_15.1.4.1.5.1)

WL/WS PBM
Configure Subproject: DevData as build 1.15.1.4.1.5.1.10(as build 1.15.1.4.1.5.1.9)

JL
Configure Subproject: DevData as build 1.15.1.4.1.5.1.10(as build 1.15.1.4.1.5.1.9)
```
</details>

<details>
<summary><b>Another approach that would have done the same results would have been.</b></summary>

```
DT
Configure Subproject: DevData as variant FM_DevData_GM_CHR_JL_PHEV_15.1.4.1.5.1(as build 1.15.1.4.1.5.1.9)
Update: Fmm.c 1.3.1.1.1.2.1.3
new checkpoint needed
Configure Subproject: DevData as build(1.15.1.4.1.5.1.10)(as variant FM_DevData_GM_CHR_JL_PHEV_15.1.4.1.5.1)

WL/WS PBM
Configure Subproject: DevData as variant FM_DevData_GM_CHR_JL_PHEV_15.1.4.1.5.1(as build 1.15.1.4.1.5.1.9)
Update Revision: Fmm.c 1.3.1.1.1.2.1.3
Configure Subproject: DevData as build 1.15.1.4.1.5.1.10(as variant FM_DevData_GM_CHR_JL_PHEV_15.1.4.1.5.1)

JL
Configure Subproject: DevData as variant FM_DevData_GM_CHR_JL_PHEV_15.1.4.1.5.1(as build 1.15.1.4.1.5.1.9)
Update Revision: Fmm.c 1.3.1.1.1.2.1.3
Configure Subproject: DevData as build 1.15.1.4.1.5.1.10(as variant FM_DevData_GM_CHR_JL_PHEV_15.1.4.1.5.1)
```
</details>

## QUESTIONS <a name="questions"></a>
<details>
<summary><b>Q:</b> Creating a new member revision on one development path. Will resyncrhonize another development path that has the previous member be updated?</summary>

**Example:**  
development path:         MonitorStatus.c at 1.34  
another development path: MonitorStatus.c at 1.34.

If on 'development path' MonitorStatus.c goes from 1.34 -> 1.35, then will Resynchronize on 'another development path' cause MonitorStatus.c member revision to be 1.35?
**A:** No, 'another development path' needs to be updated manually.
</details>

<details>
<summary><b>Q:</b> <b>Not Solved</b> How come some members still have blue member revision icon available even after Resynchronize?</summary>

**Example:***  
A current member revision is 1.19, but there is an update member revision available 1.20  
</details>

<details>
<summary><b>Q:</b> <b>Not Solved</b> Variant sandbox w/ variant subprojects vs Variant sandbox w/ build subprojects. <br>
What will happen if I check in the same changes on a member that I did on a same previous member but on a different DP?</summary>

***Variant sandbox w/ build subprojects:*** After configure to Build, try Resynchronize to see if member revisions are updated. Could this replace *Update Member Revision* command?  
***Variant sandbox w/ variant subprojects:*** After Retarget, Resynchronize to see if member revisions are updated. Guessing

***note:*** If a member revision is incremented & another DP uses same member, then the another DP member needs to *Update Member Revision* since Resynchronize will not work?
</details>
