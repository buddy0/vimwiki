# PREFERENCES

## CHANGING DEFAULT DIFF TOOL & EDITOR
1. Main Menu: *File* -> *Preferences*. *Preferences Configuration* window opens up.
2. *Configuration Management* -> *Diff and Merge Tools*.  
a. Section *Diff Tool Options:* Select *Custom Command:* & enter `C:\Program Files\Beyond Compare 4\BCompare.exe "{3}" "{4}"`.  
b. Click *ok*.  
3. *Configuration Management* -> *Editor*.  
a. Section *Options:* Select *Editor:* & enter  
`C:\Program Files\Notepad++\notepad++.exe`   
`C:\Program Files\Microsoft Office\root\Office16\EXCEL.EXE`  
b. Click *ok*.  
