# CUSTOMIZE MENU

First have to create a customize viewset: [Create new Viewset](viewsets.md)

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [STEPS](#steps)
2. [SANBOX VIEW ACTIONS](#sandbox_view_actons)
3. [MY SANDBOXES VIEW ACTIONS](#my_sandboxes_view_actions)
4. [CUSTOMIZE BUTTONS](#customize_buttons)
</details>

## STEPS <a name="steps"></a>
1. Right click on project/sandbox, select *Customize This Menu...* . *Customize "[My Sandboxes/Sandbox/PRoject] View"* window opens up. 
2. Click on *Plus* sign. *Add to "[My Sandboxes/Sandbox/Project] View"* window opens up.
3. Under *Configuration Management/[Project/Sandbox/Member]*. CTRL select desired *Actions*. Select *OK*.
4. Use the *up* & *down* arrows to move *Actions* in a specific place on the menu. Select *OK*.

## SANBOX VIEW ACTIONS <a name="sandbox_view_actons"></a>
**`Configuration Management/Project`:** *View Project History*, *View Project Information*, *Create Checkpoint*  
**`Configuration Management/Project/Subproject`:** *Configure Subproject*  
**`Configuraiton Management/Member`:** *Add Members From Archive*, *View Member Information*, *View Archive Information*  
**`Configuration Management/Sandbox`:** *View Working File Changes*.

## MY SANDBOXES VIEW ACTIONS <a name="my_sandboxes_view_actions"></a>
**`Configuration Management/Sandbox`:** *Retarget Sandbox*.

## CUSTOMIZE BUTTONS <a name="customize_buttons"></a>
When I created a new viewset, the customized buttons appeared on the main toolbar scene, but they were not shown in the *View Project History* window.  
1. On mainmenu toolbar, *Viewset* -> *Switch To* -> *ALM_ZF Developer*. New viewset opens up.  
2. *View Project History* on any project. *Project History* window opens up.  
3. On main toolbar, *View* -> *Customize*. *Customize Project History View* window opens up.
4. Copy this on any desired viewset :).
