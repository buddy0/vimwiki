# PROJECT PATHS

* ***sw releases:*** `SCS or IBC/<customer_name>/05_Software/20_Main_Microcontroller/03_integration/...`
* ***training documents:*** `SCS/Local/Livonia/Training/_TRW_Livonia_Software_Training_Checklist/project.pj`
* ***GM Autosar programs:*** `/IBC/Customer/GM/Global_B/05_Software/20_Main_Microcontroller/02_Sources/GM_GlobalB_T1XX_Sources/APPLICATION/AUTOSAR_Fm/Fm/project.pj`
* ***SCS programs:*** `/SCS/SCS_Core_Based/08_Softeare/Application/FM/FM_GEN1/project.pj`
