# REVISION NUMBERS

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [IMPORTANT](#important)
2. [DEVELOPMENT PATH & CHECKPOINT](#development_path_&_checkpoint)
3. [MEMBER](#member)
</details>

## IMPORTANT <a name="important"></a>
***variant folder syntax:*** `folder_name\project.pj (development_path_name)`  
***build folder syntax:*** `folder_name\project.pj (checkpoint_name)`  

## DEVELOPMENT PATH & CHECKPOINT <a name="development_path_&_checkpoint"></a>
Lets say I have the current DP `foo_1.2` at checkpoint `foo_1.2.4`. And I want to create 2 DPs.  
***note:*** There is no label for first CP of the development. Labels will follow the next CPs.
1. **DP:** `foo_goo_1.2.4.1`  
   **CP:** `foo_goo_1.2.4.1.1`  
   **CP:** `foo_goo_1.2.4.1.2`  
   **LB:** `foo_goo_1.2.4.1.1_01`
2. **DP:** `foo_moo_1.2.4.2`  
   **CP:** `foo_moo_1.2.4.2.1`  
   **CP:** `foo_moo_1.2.4.2.2`  
   **LB:** `foo_moo_1.2.4.2.1_01`  

***note:*** Labels cannot be member revisions. 

## MEMBER <a name="member"></a>
Lets say I have the current member revision at `1.5`.   
***revision after check out/check in:*** `1.6`  
***revision after branch:*** `1.5.1.1`  


