# KEYPRESSES

| name               | keypress |
| ------------------ | -------- |
| edit item          | CTRL E   |
| view relationships | CTRL R   |
| refresh            | F5       |
| check out          | F2       |
| check in           | F3       |
| Resychronize       | F6       |
| Revert             | ALT + F6 |

