# CLARITY - [Training Documents](https://trw1.sharepoint.com/sites/site5885/SitePages/Clarity%20Support.aspx) 

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INSTALL](#intall)
2. [LOGIN](#login)
3. [OTHER](#other)
</details>

## INSTALL <a name="intall"></a>
1. Goto following ***link:*** [Support Page](https://clarity.ssc.trw.com/niku/TRW/clarity_support.htm).  
2. Under section *Business Unit and Functional Local Support Resources*, *Division A-Active Safety*, & *NA User Support* get *Tina Meingasner* email.  
3. Email her the following information located in this ***text document:*** [Info Setting Up Clarity](Email_Info_Setting_Up_Clarity.txt).    

## LOGIN <a name="login"></a>
1. Goto following ***link:*** [Login Page](https://clarity.ssc.trw.com/niku/nu).  
2. *username:* zf #  
*password:* laptop zf login password  

## OTHER <a name="other"></a>
* Adjust time zone and location in personal settings.  

