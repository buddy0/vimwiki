# TRESOS

## OVERVIEW
**Tresos tool framework:** 
**plugins:** 

## FILE LOCATION PATH
***launch Tresos:*** `C:\sandbox\Tresos\<Tresos_version>\tresos\bin\tresos_gui.exe`  

***SW release for generated code:*** `C:\sw\<project_name>\<VIEW_software_release>\MMC_APPL\Layers\MCAL\Mcal\McalCfg\Tresos\output\generated`  
***change read only properties to configure plugins:*** `C:\sw\<project_name>\<VIEW_software_release>\MMC_APPL\Layers\MCAL\Mcal\McalCfg\Tresos`  
Right click->*Properties*. *General*, *Attributes*, uncheck *Read-only*.  

## EXAMPLE MCALSOFTWARE
***plugins:*** `C:\sandbox\Tc38x\MCALSoftware_1_30_0\MC-ISAR_AS42x_TC3xx_BASIC_1.30.0\MCIsar\1\PluginTresos\eclise\plugins`  
***?***: `..?..\Tresos_MCAL\TresosAurixExtension\eclipse\plugins`  

## INITIAL TRESOS LAUNCH
1. Create Workspace
2. New Project  
&nbsp;&nbsp;&nbsp;&nbsp;a. New Project (for new MCAL or Tresos tool version)  
&nbsp;&nbsp;&nbsp;&nbsp;b. Import Project/General/Mcal folder from release build  
3. Remove read only

## MODULE CONFIGURATIONS
* *prefs.xdm* will be modified
* Can only add 1 adc for generation

##  & GENERATE BUTTONS
* ***verify select project:*** Shows any errors, verifies configuration
* ***generate code:*** `.?.\McalCfg\Tresos\output\generated`. Tresos will automatically put the code into future sw release project. 

## OTHER
McalStatic: 3 branches with 2 branches each (AUTOSAR & nonAUTOSAR) 
McalCfg: separate branch for every program
