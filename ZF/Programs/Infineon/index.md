# INFINEON

## SETTING UP MYFINFINEON  
1. Goto [https://www.infineon.com/](https://www.infineon.com/)  
2. In topright menu, hover over *myInfineon* and select *Login to myInfineon*.  
3. Register Now. Use ZF email. Activate account with email sent (make sure to delete cache cookies regarding to Infineon if *not* working).  

## SETTING UP MYINFINEON COLLABORATION PLATFORM
1. Goto myInfineon website page.  
1. In right section, click on *Enter Collaboration Platform*.  
2. Authenticating with Google Authenticator (Android), can use text message.  
3. "Unable to Authenticate, You are not authorized to access this service" means that *Abir* (ZF Infineon techinal support rep) has to put me in the system.  

## MCAL UM TC3XX
* [Glossary](Glossary.md)

