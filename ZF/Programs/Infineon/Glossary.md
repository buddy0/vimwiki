# GLOSSARY

<details>
<summary><b>CPU</b></summary>

**scratchpad:** Small, fast memory for the temporary storage of data.  
&emsp;**Program Scratch Pad RAM (PSPR) & Data Scratch Pad Ram (DSPR):** coupled program & data SRAM memories.  
***program side memories:*** PSPR, PCACHE, Program TAG RAM (PTAG), & local PFlash bank (LPB).  
***data side memories:*** DSPR, DCACHE, data TAG RAM (DTAG), & Distributed LMU (DLMU).  
**cache:** component that stores data so that future requests for that data can be served faster.  
**program counter (PC):** Dolds address of instruction that is currently fetched & forwarded to the PCU pipelines. CPU handles updates to PC automatically.
</details>

<details>
<summary><b>INSTRUCTION / OPERATIONS</b></summary>

**atomicity:** Operation considered atomic if it is guaranteed to be isolated from other operations that may be happening at the same time.  
&emsp;**Ex:** If file exists or not (2 states), then atomic. If outside user can see file contents (at an intermediate state) then *not* atomic.   
**MFCR (Move From Core Register):** Used for reading the PC through software executed by the CPU. Read will return PC.  
**MTCR (Move To Core Register):** Used for writing to the PC. Should only be done when CPU is halted.  
**DSYNC:** May be used to flush store buffers.  
</details>

<details>
<summary><b>NON VOLATILE MEMORY (NVM)</b></summary>

**volatile memory:** Computer storage that only maintains its data while the device is powered.  
**flash memory:** An electronic non-volatile computer memory storage medium that can be electrically erased and reprogrammed. Used for storing & transferring data between devices.  Developed from EEPROM. Can erased or write data in entire blocks in contrast to single bytes as in EEPROM.  
</details>

<details>
<summary><b>SYSTEM CONTROL UNIT (SCU)</b></summary>

**watchdog timer (WDT):** Recoves & detects any HW or SW failures. Watches over the MCU.  
**End of initialization (Endinit):** Additional protection for registers. Write-protection scheme allowing only writes during certain times & makes accidental modifications of registers nearly impossible (pg. 674).  
</details>

<details>
<summary><b>SAFETY MANAGEMENT UNIT (SMU)</b></summary>
</details>

