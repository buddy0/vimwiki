# DOORS

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INSTALL](#install)
2. [PERMISSIONS](#permissions)
</details>

## INSTALL <a name="install"></a>
1. Goto *IT Self Servie Portal* -> *Install Software*, select software **IBM Rational DOORS 9.6 – 64 Bit – Win7/Win10**.

## PERMISSIONS <a name="permissions"></a>
1. Goto [ZF Identity Management Portal](https://zim.zf-world.com/IdentityManager-ZFWorld/page.axd?RuntimeFormID=fb10d25d-df53-430c-a782-c3cfa160a614&aeweb_handler=p&aeweb_rp=&wproj=0&MenuID=Portal_IAM_MyIdentity&ContextID=VI_Start) -> *Start a new request* -> *Services*.
2. In *Service items in the category* select *-- Active Directory*.
3. In *Product setion*, add to the cart: **D_DOORS-all-users**, **D_TRW**, & **D_TRW-r**.
4. Goto shopping cart. Insert a *Reason* for all of the groups in the cart. Must SAVE before submitting.
