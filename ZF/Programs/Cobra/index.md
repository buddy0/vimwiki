# COBRA

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INSTALL](#install)
2. [SW STEPS](#sw_steps)
3. [TERMS](#terms)
4. [TIPS](#tips)
5. [ACTIONS](#actions)
6. [VIEWS](#views) 
7. [KEYS](#keys)
</details>

## INSTALL - [link](http://skobde-web.ad.trw.com/wiki/SCS/index.php?title=COBRA_IDE_FAQ#Where_do_I_get_the_latest_version_of_the_IDE.3F.E2.80.8B)  <a name="install"></a> 
1. Put Cobra Files in `C:\app\tools\cobra`  (need admin privelages).  
2. Run the reg file.   

## SW STEPS <a name="sw_steps"></a> 
1. Create a folder in `C:\sw` and put WinZip file inside this folder.  
2. Right Click, hover over *WinZip*, & select *Unzip to folder `C:\sw\created_folder_name\name_of_WinZip`*.  
3. Go inside `new folder\MMC_APPL` and double left click on *CallWinmake.vbs*. WINMAKE opens up.  
4. Check *Devel* checkbox if compiling or debugging.  
5. Click on Cobra button. Cobra IDE Launcher, Select a directory as workspace opens up.  
6. Browse workspace to `C:\sw\created_folder_name\folder_created_by_WinZip\MMC_APPL`. Launch.

## TERMS <a name="terms"></a> 
**workspace:** Location on machine where all work through eclipse will be stored as files. Registers imported projects (knows them until removed). Stores must user preferences.  
**indexing:** Links all the files together so goto definition and search works properly throughout entire project.  
**element:** Can be an object, directive, etc.  
**resource:** Project, folders, files.  

## TIPS <a name="tips"></a>
* Saving in BC4 doesn't show in Cobra *History* window.
* How to get back old files? Compare current files with zip files. Or try to look at Cobra *History* window.
* ***Cobra error - cannot Import project:***     
Solution: Workspace probably is at wrong location. Needs to find APP & CAL. Beginning folers is better for workspace.
* ***Cobra error - conflicting workspaces:***  
Solution: Inital zip already had `.metadata`. Delete it & try again.

## ACTIONS <a name="actions"></a>
***compare files:*** Select 2 files -> right click -> *Compare With* -> *Each Other*.  
***local history:*** Each time file is saved, a copy is stored. *History* view. Select 2 revisions & compare w/ each other.  
***refactor rename:*** Can be used instead of manually *Search/Replace*. Right click on object -> *Refactor* -> *Rename* -> type new word -> press ENTER.  
***macro expansion:*** Hover over the macro.  

## VIEWS <a name="views"></a>
***Include Browser:*** *Include Broswer* window -> *Show View* -> *Include Browser* -> drag file to window to see all include directives.    

## KEYS <a name="keys"></a> 
| action                    | keypress          | 
| ------------------------- | ----------------- |
| key assist                | CTRL + SHFT + L   |
| show history              | ALT + SHFT + Q, Z |
| open resource             | CTRL + SHFT + R   |
| open element              | CTRL + SHFT + T   |
| show tab files            | CTRL  + E         |
| quick outline             | CTRL + O          |
| go to matching bracket    | CTRL + SHFT + P   |
| go to line                | CTRL + L          |
| max avtive view or editor | CTRL M            |
| backward history          | ALT + <- OR MB4   |
| forward history           | ALT + -> OR MB5   |
