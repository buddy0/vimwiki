# QAC & POLYSPACE

<details>
<summary><b>TABLE OF CONTENETS</b></summary>

1. [QAC](#qac)  
    a. [IMPORTANT](#important)  
    b. [COMMENT SUPPRESSION](#comment_suppression)  
2. [POLYSPACE](#polyspace)  
    a. [COMMENT SUPPRESSION](comment_suppression)  
</details>

## QAC <a name="qac"></a>

### IMORTANT <a name="important"></a>
[Install](install.md)  
***HTML reports automatically be generatted at this path:*** Example for WS PBM  
`C:\sw\CHR\WS_PBM\MY23\MMC_APPL\MMC_PRODUCT\Services\Fm\01_Ini\BUILD\TriCore_MM_APP\QAC_931\FM\HtmlOutput\_SOURCE_ROOT`  

### COMMENT SUPPRESSION <a name="comment_suppresssion"></a>
See Coding guidelines. There is a section on this.  
***specific message at a line:*** `/* PRQA S <id_1>, <id_2>, ..., <id_n> */`  
***all messages at a line:*** `/* PQRA S ALL */`  
***specific message at current line till end of file:*** `/* PRQA S EOF <id_1>, <id_2>, ..., <id_n> */`  
***all messages at current line till end of file:*** `/* PRQA S ALL EOF */`  

## POLYSPACE

### COMMENT SUPPRESSION
See Coding guidelines. There is a sections on this.


