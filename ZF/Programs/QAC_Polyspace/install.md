# INSTALL
* Really don't need to create sandboxes, all you need is the document mentioned below and QAC_931 executables, but its nice if there's ever an update & you can easily see what  you had before.  
1. **TRAINING PROJECT**  
&emsp;**a.** Create sandbox at Integrity project location `/SCS/Local/Livonia/Training/project.pj`.  
&emsp;**b.** Resynchronize only at *_TRW_Livonia_Software_Training_Checklist\project.pj*.  
&emsp;**c.** Open up *TRW Livonia Software Training List.xlsx*.  
&emsp;**d.** Goto section *Installing Programs* & click on *QAC - 9.3.1 and above*. Open up document.  
&emsp;**e.** Goto section *Installation Instructions* & click on Integrity link which opes up QAC project (path `BrkSw/Tools/Test/Qac/Installation/project.pj`).  

2. **QAC PROJECT**  
&emsp;**a.** Create sandbox at toplevel project. Only Resychronize *QAC_931\project.pj* & *Installation_Instructions.doc* (same document as *QAC - 9.3.1 and above*).  
&emsp;**b.** Continuing on step 3, section *QA C 9.3.1 including MISRA-C:2012:*, run the executables in correct order.  
&emsp;**c.** Goto step 4, section *License Setup*, & follow instructions (step 2 not needed here).  

3. **CHECK**  
&emsp;**a.** Open up Winmake from any SW release. From Tool pulldown section, select QAC_931.  
&emsp;**b.** [Optional] Right underneath that, select TRICORE_MM_APP.INI.  
&emsp;**c.** [Optional] Select specific subsytem to compile from pulldown section.  
&emsp;**d.** Click MSA (Make all files from [Subsystem] button. I also think you can click on the QA checkmark button as well. QAC should open up.  



