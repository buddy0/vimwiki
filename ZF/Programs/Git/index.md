# GIT

* Software for tracking changes in files. 
* ***Distrubuted Version Control System (VCS):*** Single orgin server that holds the repository, but everyone who made the clone of the repo may have full history of the repo, depending on how deep you clone. Anthing that you do on yoru amchine stays local until you push. 

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [OPERATIONS / PARAMETERs](#operations_parameters)
2. [GIT vs PTC INTEGRITY](#git_vs_ptc_integrity)
3. [BITBUCKET](#bitbucket)
</details>

## OPERATIONS / PARAMETERs <a name="operations_parameters"></a>
* ***commit*** Stores the difference of files. Similar to a change package in PTC Integrity. 
* **branch:** Pointer to a commit.
* **tag:** Pointer to a specific commit.  
* **git history:** Linked Lists of commits. 
* ***depth:*** Paramter to git clone allowing to clone a particular part of the origin repo. 
* **submodule:** Another repositry nested w/in a parent repository. Similar to the build share in PTC Integrity.  
* ***detached head:*** Creating a commit that is not on any branch. Commit becomes detached head.  
* **garbage collection:** Goes through repo & sees if any commits are not being referenced (meaning it's not on a branch or deson't havea  tag) & removes the commit. Thus, the change may get lost.  
Thus make sure to either tag a commit or make a branch for it before switching to another branch so garbage collection doesn't remove the commit.  
* **fetch (SmartGit feature):** Get updates from the orinin repo. Similar to Resynchronize in PTC Interity.  
* **prune:** Deletes any origin branch that doesn't have an equivalent on the remote.  

## GIT vs PTC INTEGRITY <a name="git_vs_ptc_integrity"></a>
* ***PTC Integrity:*** Centralized meaning one main server that holds everything. Only check out stuff that your working on or stuff you need to change.
* ***Git:*** Decentralized meaning you have a copy of the entire server when you make a clone. Can do anything on the server locally & stay local on your machine.  
So everybody can be working on stuff simultaneously, have clones of the remote/origin server. Once your happy, can push it to your remote. Now people, can pull down my work & use it.  
* Git branches are abosultely different in PTC Integrity. 
* Git is a folder-based system, not a file-based system. 

## BITBUCKET <a name="bitbucket"></a>
**Bitbucket:** Git-based source code repository hosting service owned by Atlassion.  
**Bitbucket Server:** Combo of Git Server & web interface product. Allows users to do basic Git operations while controlling read & write access to the code.  
