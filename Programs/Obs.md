# OBS

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [INSTALL](#install)
2. [SETTINGS](#settings)
3. [SETUP](#setup)
4. [IMPROVE MIC SOUND](#improve_mic_sound)
</details>

## INSTALL <a name="install"></a>
1. Goto `https://obsproject.com/`. Click *Windows* button to downland installer. 
2. Goto Downloads folder & run installer.  

## SETTINGS <a name="settings"></a>
Main menu toolbar, goto *File* -> *Settings*.  
**OUTPUT:**
* **Output Mode:** Advanced  
* **Recording Path:** `C:\Users\z0118277\Videos\Obs`  
* **Encoder:** NVIDIA NVENC H.264 (new)  
* **Bitrate:** Currently default at 2500 Kbps, might want to change

**AUDIO**  
* **section Global Auido Devices:** Disable all.

**Note:** *Profile* & *Scene collection* can be created exporting settings.
## SETUP - [wiki](https://obsproject.com/wiki/) <a name="setup"></a>
**scenes:** Essentaily folders that contain sources.  
**sources:** The content that makes a scene. For recording I did:
* *Display Capture* 
* *Audio Input Capture* (External mic)

## IMPROVE MIC SOUND [settings](https://www.youtube.com/watch?v=gNUwAwrNiV0&t=325s) <a name="improve_mic_sound"></a>
**target volume db:** -3dB. To achieve this follow settings below.  
**AUDIO MIXER SETTINGS**  
In tab *Audio Mixer*, click on 3 dots (prev cog) -> *Filters*. Add:  
* **Noise Suppression/Noise Gate:** Elminates background noise.
* **Compressor:** Voice becomes even & smooth.
* **Gain:** Increase digitally instead of turning gain up all the way on HW causing line noise. Add +Xdb to reach -3db.
* **Limiter:** Prevents from peeking & cripple noise from being to loud. At -3db.  


