# LAYOUTS - [Understanding Ball Layouts](https://www.youtube.com/watch?v=v8soXaercu8&t=1s)
![Picture of Bowling Ball Measurements](Images/Bowling_Ball_Measurements.png)  
**layout:** Position of weight block relative to your release.  
**flares:** As ball rolls it continues to migrate to a different axis w/ each roll until it reaches its preferred spin axis. [TB](http://tamerbowling.com/what-is-ball-flare/)
**bowling ball track:** Where ball first touches the lane & tracks down the lane. IS the oil lane.  

### ACRONYMS
**PAP:** Positive Access Point. 90 degrees away from track. Acts like an axel on a wheel. Is the alignment. Is a bowler's signature, thumbprint.
**VAL:** Vertical Access Line

## MEASUREMENTS
1. ***PIN to PAP:*** Controls amount of flare.  
**Ex:** `4"` pretty aggressive, produces alot of flare & hook.
2. ***PSA to PAP:*** Controls the migration path.  
**"** Whether or not the flare path is going to go up toward fingers, straight across toward grip center, or downt toward thumb **"**.  
3. ***PIN to VAL:*** Controls transition at breakpoint. Is the pin buffer.  
**"** Closer yout put PIN to VAL, the shorter or faster the transition going to be. Increasing the distance, making it wider (making pin below your fingers) will obtain a slower transition, being a more gradually arcing shape **"**.  

* ***popular league layout:*** 4 x 4 x 2 
***popular ppa tour layour:*** 5 x 5 1/2 x  3

## EXAMPLES - [Effective Modern Layouts](https://www.youtube.com/watch?v=r2v78FPhUSE&t=608s)
Speed dominant players going to enjoy asymmetrical balls which have stronger midlane break point.  
Match or rev dominant players going to enjoy symmetrical balls.  

### SYMMETRICAL BALLS
2 measurements really needed: PIN to PAP distance & VAL angle. Location of the CG doesn't matter b/c after its drilled the PSA is going to be in the thumb hole. 

#### PIN to PAP DISTANCE 
***higher track players:*** Between 3.5 - 4.25". PIN closer to access point down the lane & get more ***side rol***.  
***lower track player:*** Between 4.5 - 5". PIN rolling over top of the ball as it goes through midlane & induce ***forward roll***.  

#### VLA ANGLES 
Whether PIN close to the VLA or not. Will determine sharpness of the breakpoint.  
***later sharper breakpoint:*** Between 20 - 30 degrees. PIN closer to VAL, thus smaller VLA angle.   
***more midlane, roll more forward, & continuous:*** Between 30 - 45 degrees

#### EXAMPLES
* ![Picture of Higher Track Plater](Images/Higher_Track_Player.png)  
Give later sharper breakpoint.  
* ![Picture of Lower Track Player](Images/Lower_Track_Player.png)  
Read midlane & be more continous

## PIN TO PAP - [The A-B-C Zones of Pin to PAP Layouts](https://www.youtube.com/watch?v=EUdjpLhGL5Y)  
***zone A: sharper breakpoint:*** Between 3.5 - 4.25".  
***zone B: medium strong breakpoint:*** Between 4.25 - 5".  
***zone C: more forward rolling breakpoint:*** Between 5 - 5.75".  
***straighter, less flare zone:*** Between 0.75 - 2.5".  
***no mans land:*** Between 2.5 - 3.5".  

-------------------------------------------------------  
## [PART 1: PIN-TO-PAP DISTANCE](https://www.youtube.com/watch?v=ntPS0nrcOWU)  
***pin to pap distance:*** Controls the amount of flare. Will position the core at different angles at moment of release. Control the angle of the weight block.  
**1 1/2":** Lays core on its side & rotation is stable. Result is less overall flare & smooth motion down the lane.  
**3 1/2":** Core is sitting at nearly 45 degree angle. & rotation is unstable. Result in more overall flare & much more motion both front and back & left to right.  
**6":** Core is nearly completely up. Not going to have to migrate so far to achieve a stable position. Result in less overall flare & straighter path.  
![Picture of PIN to PAP](Images/Pin_to_Pap.png)  
![Picture of PIN to PAP 1 1/2"](Images/Pin_to_Pap_one_and_half.png)
![Picture of PIN to PAP 3 1/2"](Images/Pin_to_Pap_three_and_half.png)
![Picture of PIN to PAP 6"](Images/Pin_to_Pap_six.png)

## [PART 2: PSA-TO-PAP DISTANCE](https://www.youtube.com/watch?v=2eCTVEQnIz4) 
***psa to pap distance:** Control the strength of the intermediate differential. Will position the PSA at different angles at the moment of release. Isn't changing the orientation of the weight block significantly
**2":** Positions the PAP at its side & rotation is stable. Stable b/c its completely almost line up on release, PSA will not need to migrate in order to achieve a stable position. Result in less overal flare & smooth motion down the lane.   
**4":** Unstable rotation b/c PSA sitting at nearly 45 degree angle on release. Result in more overall flare and more motion both front to back & right to left.  
**6":** Positions PSA in nearly stable orientation at moment of release. Result in less overal flare & much straighter path throughout the entire lane.
![Picture of PSA to PAP 2"](Images/Psa_to_Pap_two.png)
![Picture of PSA to PAP 4"](Images/Psa_to_Pap_four.png)
![Picture of PSA to PAP 6"](Images/Psa_to_Pap_six.png)  
![Picture of PSA to PAP Different](Images/Psa_to_Pap_different.png)

## [PART 3: PIN BUFFER DISTNACE](https://www.youtube.com/watch?v=mg349vVRAzs)
***pin buffer distance:*** Control how quickly the ball transitions from skid to hook to roll as it travels down the lane. Will affect whether the finger & thumb holes are drilled into the top of white block or side of white block.  
**1":** Holes position into side of weight block. Remove more mass from side of the weight block. More mass removed from the side, the more the differential increases b/c width of weight block is getting slightly smaller while height is unaffected. Since height is greater than width, smaller width makes differenct between the two ever greater than it was before. More differential creates more flair & more shape front to back.  
**4":** Holes position more into top of weight block. Remove more mass from top of weight block. More mass removed from the top, the lower the differential becomes b/c height of weight block is getting slightly smaller while width is unaffected. Since height is greater than width, a small height makes the difference between two less than it originally was. Less differential creates less flare & less overal hook throughout the lane.  
![Picture of PIN Buffer 1"](Images/Pin_Buffer_one.png)
![Picture of PIN Buffer 4"](Images/Pin_Buffer_four.png)




