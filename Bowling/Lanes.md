# LANES

***targeting dots:*** Help get the ball to read sooner.  
6-8ft from foul line.  
***targeting arrows:*** Most widely used targeting method.  
Each arrow are in 5 board increments.  
Right to Left: 5, 10, 15, 20, etc.  
3rd arrow from left & right are exactly 15ft from foul line.  
***range finders:*** Break Point control Reveals where the ball is.
1st set: start at 34ft, end at 37ft.  
2nd set: start at 40ft, end at 43ft.  
***distance to head pin:*** 60ft.
***width of lane:*** 41 inches
