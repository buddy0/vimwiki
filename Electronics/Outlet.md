# OUTLET - [YT](https://www.youtube.com/watch?v=UwGoU3XVpnI)

## WIRES 
| name    | color | side  | screw  | comment                                      |
| ------- | ----- | ----- | ------ | -------------------------------------------- |
| hot     | black | right | brass  |                                              |
| neutral | white | left  | silver |                                              |
| ground  | brown | left  | green  |                                              |
|         | red   | right | brass  | only for switch outlet, remove tab in middle |
 
![Picture of Outlet](Images/Outlet.png)  

## NOTES
* Always wire to screw terminal, not the push pins at the back side. 
* Strip gage built onto the backside of the outlet. Determines how much to strip the wire.

## NEW CIRCUIT INTO ELECTRICAL PANEL
1. Turn off main breaker.
2. Unscrew screws & take cover off.
3. Make sure no power from any of the breakers. Use a voltage tester & test all the hot wires (black). 
4. Run a new wire into panel to connect new circuit.
5. Connect:  
ground wire to ground bus, located at top  
neutral wire to netureal bus, located at side  
hot wire to circuit breaker  
6. Insert circuit breaker onto circuit bus. 
7. Make room for new breaker in the panel
8. Replace panel & turn main breaker on. 

### NOTES
* Do not touch main lugs & main line, these will still be live even though main breaker switch is turned off. 
* Individual circuit breakers are attached to the circuit bus. Next to circuit bus is the neutral bus.

## EQUIPMENT/TOOLS
* voltage tester  
* 14/2 wire for 15 amp circuit (solid core or stranded core)
* wacco 221
* 15 amp breaker
* 15 Amp Commercial Grade Duplex Outlet, cover plate
* ethernet spool, crimp tool, ethernet wall plate (x3), jack (x3)
* drywall
