# MONITORS

## LIQUID CRYSTAL DISPLAY (LCD)
**liquid crystal display (LCD):** The monitors below vary in how they arrange their lcd molecules.  
* **Twisted Nematic (TN):** Molecules arranged in paralled, but not on a level plane that twist or untwist themselves when a voltage is applied. Consist of nematic liquid crystals suspended between 2 plates of polarized glass. Oldest.  
* **Vertical Alignment (VA):** Crystals are vertical aligned & move into a horizontal position to let light shine through. Molecules arranged vertically, perpendicular to the glass panel that tilt when voltage is applied Incredible color contrast & image depth. 
*   **In-Plane Switching (IP):** Perfectly aligned liquid crystals that form a parallel pattern to produce bold colors & on screen color contrast. Molecules are parrellel to glass panel that rotate when votate when voltage is applied. Most widespread of LCD display. Depicts excellent picture quality from all viewing angles w/ superior color contranst.  

**Organic Light Emitting Diodes (OLED):** Self illuminating pixels. Commonly used in smartphones, handheld gaming devices, & tv screens.
**Quantum Light Emitting Diodes (QLED):** Regular LED tv execpt it uses tiny nanoparticles called quantum dots to super-charge its brightness & color

| type | pro(s) | con(s) |
| ---- | ------ | ------ |
| IPS  | execllent color reproduction<br>wide viewing angles<br>better sunlight visibility<br>longer lifespans | IPS glow<br>lower response times/refresh rates<br>expensive<br>limited contrast ratio<br>inefficient power consumption |
| VA   | Best contrast ratio<br>viewing blacks :)<br>good comprosise between TN & IPS | high response time resulting in motion blur |
| TN   | faster response time<br>cheaper<br> | can only be viewd head on<br>limited color depth<br>poor visibility in direct sunlight<br>age not good as IPS |
| OLED | perfrect blacks<br>infinite contrast ration<br>widest viewing angles<br>energy efficient, no backlight<br>thinner | burn in, image retention<br>average brightness levels<br>|
| QLED | 



