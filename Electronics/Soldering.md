# Soldering

<details>
<summary><b>Basic Soldering Lession 1 - "Solder & Flux"</b></summary>

## SOLDER
* **solder:** Metal allow made by combining tin and lead in different proportions. 
* ***proportions:*** tin/lead, ex. 60/40 is 60% tin and 40% lead. 

| tin/lead ration | melt begin temp   | fully melted temp | 
| --------------- | ----------------- |------------------ |
| pure lead       |                   | 620 F, 327 C      | 
| pure tin        |                   | 450 F, 232 C      | 
| 50/50           |  361 F, 183 C     | 420 F, 216 C      |
| 60/40           |  361 F, 183 C     | 375 F, 191 C      |
| 63/37           |  361 F, 183 C     | 361 F, 183 C      |   

* ***between begin melted & fully melted temp:*** Solder exists as a plastic or semi-liquid state (some but not all of it has melted). Plastic range is different for other combinations.
* **solvent:** Substance that dissolves a solute (dissolved component in a solution), resulting in a solution. 
* ***starting phase:*** Hot solder comes into contact w/ copper surface, a metal solvent action takes place. Solder dissolves & penetrates copper surface. Molecules of solder & copper blend to form new metal alloy.
* ***wetting action:*** Aka solvent action. Can only occur if copper surface is free from dirt & any oxide film that forms when metal is exposed to air. 

## FLUX
* **flux:** Rosin in a modified form & activator to accelerate their action.  
When melted it removes the thin film of oxide. Available in paste & liquid form. When cooled it is almost non corrosive & non conductive.  
Do not use acid base flux (containing zinc or choloride) for electronic repair (remain corrosive at all temperatures). Acid core solders should also not be used in electronic work.  Rosin fluxes should still be removed w/ a solvent after soldering. This prevents their sticky surface from collecting dirt & moisture.  

## SOLDERING IRON
* Different temp rise even tho solder tip temp is the same.  
    * ***fast temp rise:*** big solder tip, small work mass
    * ***slow temp rise:*** small solder tip, big work mass

## WAYS TO HELP SOLDERING
* **cleaning tip:** Wipe it on a wire brush before each use & shocking off remaining oxides on a wet sponge.  
* **work clean:** Work is cleaned with a solvent such as trichloroethane or isopropyl alcohol to remove any grease or oil.
* **thermal linkage:** Pointy and flat tip have a small linkage & big thermal linkage, respectively. If using pointy, can put some solder next to the joint to increase thermal linkage. 
* ***time:*** Apply heat no more than 2 seconds.
* As materials are brought up to solder melt temperature, 80% of bond strength between pad and board may be lost. Thus, use a light touch, no more than using a pencil. 

## WODRK PIECE INDICATOR (WPI)
Your Action -> Work Piece Reaction -> Observing Change -> New Action.  
***Observing Change:*** Where WPI come in: sight, sound, or touch. Ex. Noticing that a big tip on a small work mass is heating up too quickly. Thus, our WPI is ***heat rate recognition***.  
</details>

Errors when soldering:  
* Don't use sand paper to clean off tip. Use solder bit cleaning sponge. Add little water to sponge. Now tin the iron.
* Don't carry solder on the tip for soldering iron. Solder tip on one side & solder wire on the other side.
* Use steel wool to unrust old copper wire.  
* Don't use thin solder on big wire. Twist solder decrease soldering time.
* Use soldering flux on the copper wire, then solder.
* Don't use big tip for small components

Soldering tips:  
* Start w/ a clean tip by removing excess solder from a coil brush or wet sponge. 
* Pre tin wires and pads. Melted solder bonds to solder instantly.
* Use high heat but for only short heat times.
* Tin soldering tip at end of soldering to prevent oxidation.
* Sponge: Use dionzed water not tap water.

How to tin a soldering iron new session:  
1. With soldering *not* on, coat tip w/ tinning flux.
2. Wrap tip w/ solder. 
3. Set solder iron horizontal & turn on iron. 
4. Tip should now be bright, shiny for use.

How to tin a soldering iron during soldering: 
1. With soldering *on*, tip iron into flux.
2. Retin the tip. 
3. Wipe it off: either wet sponge or coil brush.

## REDDIT
* "For circuits that are mission-critical, sponges are required. Although, I've seen people use both - the brass mesh to quickly get the coarse material off, then a wet sponge to get the fine particulates afterwards" 

## CLEANING FLUX FROM PCB/
***note:*** Make sure there are no pointy leads, could cause damage to fingers!
1. Use Isopropyl Alcohol to clean flux residue and oils from a cicruit board.  
2. Use a paper towel which gets into little but carring the unwanted residue.

## FUTURE TOOLS
* Delcast R1K Solder Dispenser Reel
* KOTTO Solder Smoke Absorber Remover Fume Extractor Smoke Prevention
* Maginifier glass
* PanaVise model 201 & model 239 OR Weller ESF-120 OR NorthridgeFix PCB
* KNIPEX Tools - Electronics Super Knips, Multi-Component (7861125), 5-Inch 



