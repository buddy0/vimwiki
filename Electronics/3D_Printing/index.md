# 3D PRINTING

## HOTEND
***hotends supported by VORON ecosystem:*** slice mosquito, v6, & triangle labs dragons.  
***high flow variant:***  W/ larger melt zone results in issues w/ more stringing & oozing

### SLICE MOSQUITO
***cost:*** $150  
***melt zone:*** Best. Top of copper to bottom of nozzle  
***heat break:*** Stop heat transfer as much as possibly as quickly as possible
***heat sink:*** great, find w/ 30mm fan
***structure:*** heat blocks attached to heat sink. Black portion not required for printing, simply black aluminum mounts. Makes heat break & heat sink non structural items. As a result much thinner, lighter, 
& bleed off heat more quickly.  
No not have to heat tighten and can one-hand nozzle change.  
***mounting:*** top, side screws

### V6
***cost:*** E3D version $70, Triagle Labs version $15  
***meltzone:*** Wort.  
***heatbreak:*** 
***heat sink:*** worst  
***structure:*** Heat break is structural.  
Have to heat tightern and no one-handed nozzle changes.  
***mouting:*** groove mount, outdated, boo  

### TRIANGLE LABS DRAGONS
***cost:*** $60  
***melt zone:*** top of copper to bottom of nozzle  
***heat break:*** 
***heat sinkK:*** great, fine w/ 30mm fan  
***structure:***  
***mount:*** top screw
