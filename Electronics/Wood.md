# WOOD

1. Softwoods
Wood/lumber milled from conifer trees, have needles & produce cones.  
***Exs:*** pine, cedar, fir, spruce, & redwood  
Spruce, pine, & fir used for framing new contruction. Resist rot & insects. Which makes cedar & redwood ideal for decking & outdoor furniture.
***note:*** Softwoods are not named b/c they are soft.

2. Hardwoods
***deciduous trees:*** Trees that do not produce needles or cone, produce leaves & seeds.  
***Ex:*** oak, maple, cherry, mahogany, & walnut.  
***monocotyledon trees:*** Similar characteristics of hardwoods.  
***Ex:*** bamboo & palm (can also fall under engineered wood)  
***note:*** Not necessarily stronger than softwoods & well known for wood grain patterns.  

3. Engineered Wood: Manufactured Wood
Dot no occur naturally but instead are manufactured.  
Made from waste wood of sawmills.  
Purpose is to meet certain sizes.  
***Exs:*** plywood, oriented strand board, medium denisty fiber board, & composite board.
