# KiCad

<details>
<summary><b>TABLE OF CONTENTS</b></summary>

1. [NAVIGATION THROUGH EDITORS](#navigation_through_editors)
2. [GLOSSARY](#glossary)
3. [TUTORIAL PART 1: PROJECT](#tutorial_part_1)
4. [TUTORIAL PART 2: SCHEMATIC](#tutorial_part_2)  
&emsp;a. [SYMBOL LIBRARY TABLE SETUP](#symbol_library_table_setup)  
&emsp;b. [SCHEMATIC EDITOR BASICS & SCHEMATIC SHEET SET UP](#schematic_editor_basics_and_sheet_set_up)  
&emsp;c. [SELECTING & MOVING OBJECTS](#selecting_and_moving_objects)  
&emsp;d. [WIRING SCHEMATIC](#wiring_schematic)  
&emsp;e. [ANNOTATION, SYMBOL PROPERTIES, & FOOTPRINTS](#annotation_symbol_properties_and_footprints)  
&emsp;f. [ELECTRICAL RULES CHECK (ERC)](#erc)  
&emsp;g. [BILL OF MATERIALS](#bom)  
5. [TUTORIAL PART 3: CIRCUIT BOARD](#tutorial_part_3)  
&emsp;a. [PCB EDITOR BASICS](#pcb_editor_basics)  
&emsp;b. [BOARD SETUP & STACKUP](#board_setup_and_stackup)  
&emsp;c. [IMPORTING CHANGES FROM SCHEMATIC](#importing_changes_from_schematic)  
&emsp;d. [DRAWING A BOARD OUTLINE](#drawing_board_outline)  
&emsp;e. [PLACING FOOTPRINTS](#placing_footprints)  
&emsp;f. [ROUTING TRACKS](#routing_tracks)  
&emsp;&emsp; i. [WAYS TO CONNECT TRACKS FRONT AND BACK](#ways_to_connect_tracks_front_and_back)  
&emsp;g. [PLACING COPPER ZONES](#placing_copper_zones)  
&emsp;h. [DESIGN RULE CHECKING](#design_rule_checking)  
&emsp;i. [3D VIEWER](#3d_viewer)  
&emsp;j. [FABRICATION OUTPUTS](#fabrication_outputs)  
6. [TUTORIAL PART 4: CUSTOM SYMBOLS & FOOTPRINTS](#tutorial_part_4)  
&emsp;a. [LIBRARY & LIBRARY TABLE BASICS](#library_and_library_table_basics)  
&emsp;b. [CREATING NEW GLOBAL OR PROJECT LIBRARIES](#creating_new_global_or_project_libraries)  
&emsp;c. [CREATING NEW SYMBOLS](#creating_new_symbols)  
&emsp;&emsp;i. [SYMBOL PINS](#symbol_pins)  
&emsp;&emsp;ii. [SYMBOL PROPERTIES](#symbol_properties)  
&emsp;d. [CREATING NEW FOOTPRINTS](#creating_new_footprints)  
&emsp;&emsp;i. [FOOTPRINT PADS](#footprint_pads)  
&emsp;&emsp;ii. [FOOTPRINT GRAPHICS](#footprint_graphics)  
&emsp;&emsp;iii. [KICAD LIBRARY CONVENTIONS](#kicad_library_conventions)  
&emsp;&emsp;iv. [ADD SWITCH TO SCHEMATIC](#add_switch_to_schematic)  
&emsp;&emsp;v. [ADD SWITCH TO LAYOUT](#add_switch_to_layout)  
&emsp;d. [LINKING SYMBOLS, FOOTPRINTS, & 3D MODELS](#linking_symbols_footprints_and_3d_models)  
&emsp;&emsp;i. [SYMBOLS & FOOTPRINTS](#symbols_and_footprints)  
&emsp;&emsp;ii. [FOOTPRINTS & 3D MODELS](#footprints_and_3d_models)  
</details>

## NAVIGATION THROUGH EDITORS <a name="navigation_through_editors"></a>
***pan:*** dragging with MMB or RMB  
***zoom:*** scrollwheel or F1/F2

## GLOSSARY <a name="glossary"></a>
**archive:** Maintains all data for that project in the system, but you can no longer schedule or track additional time for that project.  
**deleting:** Removes all data for that project permanently.  

## TUTORIAL PART 1: PROJECT <a name="tutorial_part_1"></a>
Create a new project.  
1. In Project window, *File* -> *New Project*. Make sure *Create a new folder for the project* checkbox is ticked.  
2. In Project Files pane, `.kicad_pro`, `.kicad_sch`, & `.kicad_pcb` is shown.  

## TUTORIAL PART 2: SCHEMATIC <a name="tutorial_part_2"></a>

### SYMBOL LIBRARY TABLE SETUP <a name="symbol_library_table_setup"></a>
1st time schematic editor opened, dialog appears asking how to configure global symbol library table.  
**symbol library table:** Tells KiCad which symbol libraries to use & where they are located. 
***Configure Global Symbol Library Table:***  Select default option if default libraries have been installed.  
***location of default library table:*** `C:\Program Files\KiCad\6.0\share\kicad\symbols\`   

### SCHEMATIC EDITOR BASICS & SCHEMATIC SHEET SET UP <a name="schematic_editor_basics_and_sheet_set_up"></a>
***pan around schematic:*** click & drag w/ MMB or RMB.  
***zoom:*** mousewheel or F1 & F2.  
***left toolbar:*** Basic display settings.  
***right toolbar:*** Editing schematic tools.  
***view all hotkeys:*** *Help* -> *List Hotkeys* or CTRL F1  
***schematic sheet setup:*** *File* -> *Page Settings*   
***adding symbols to schematic:*** *Add a Symbol* button or press A. Action triggers *Footprint Library Table Setup* dialog.  
***Choose Symbol dialog:*** Lists available symbol libraries & component symbols.  

### SELECTING & MOVING OBJECTS <a name="selecting_and_moving_objects"></a>
***select objects:*** 
1. Left click on them. Additional objects can be selected by shift + left click or removed by ctrl + shift + left click.  
2. Drag selection. 
right-to-left: selects objects that are entirely closed  
left-to-right: selects objects that are partially enclosed.

***note:*** Can selet an entire symbol (by clicking on the shape) or one text field in the symbol (by clicking the text).  
***move selected object:*** Press M.  
***rotate selected object:*** Press R.  
***select & move drag hotkey:*** Press G.  
***moving unconnected symbols:*** G and M behave identically.  
***move symbol & keep wires attached:*** G  
***move symbol * leave wires behind:*** M  

### WIRING SCHEMATIC <a name="wiring_schematic"></a>
***Add a Wire:*** Button, press W, hover over an unconnected pin.  
***finish wiring:*** Click on symbol pin or double click anywhere.  
***Add Power:*** Power symbol library, *Add a Power Port* button, or P hotkey.  
***Add a label:*** In right toolbar, *Net Label* button or *L* -> type label name -> place label into schematic so that square attachment point overlaps w/ wire.  
***note:*** Labels and power ports with same name are conntected together. Visually wires aren't connecting together, but they are shorted.   

### ANNOTATION, SYMBOL PROPERTIES, & FOOTPRINTS <a name="annotation_symbol_properties_and_footprints"></a>
**Annotation:** Each symbol needs a unique reference designator. Can be done by *Fill in schematic reference designators* button.
**Symbol Properties:** 
1. Select symbol -> right-click -> Properies ***OR***  
Hover over symbol -> Press E ***OR***  
Double click on symbol  

* **Footprint Assignment:** Defines how each component will attach to the PCB. Click on footprint button. Panes:  
    * ***Libraries (left):*** Lists availble footprint libraries.
    * ***Symbols (middle):*** Shows symbols in schematic  
    * ***Footprints (right):*** Shows footprints that can be chosen for the symbol selected in the middle pane. Footprints can be viewed by right click -> View selected footprint.   Double click to assign footprint.  
* ***footprint filter button:***  
    * ***Symbol (left):*** Filters that can be defined in each symbol.   
    * ***Pin count (middle):*** Filters by pin count.   
    * ***Library (right):*** Filters by selected library. Libraries are selected in the left pane. 

***note:*** Best to use library or symbol filter but not both.  

### ELECTRICAL RULES CHECK (ERC) <a name="erc"></a>
Checks for common connection issues, unannotated symbols, typos in net labels, etc.  
1. *Inspect* -> *Electrical Rules Checker*.  

***see full list of rules:*** *Schematic Setup* -> *Electrical Rules* -> *Violation Severity*.  
***prevent error - Input Power pin not driven by any Output Power pins:*** Add power port symbols to avoid that net is undriven.  

### BILL OF MATERIALS <a name="bom"></a>
***generate BOM:*** Tools -> Generate BOM  

## TUTORIAL PART 3: CIRCUIT BOARD <a name="tutorial_part_3"></a>

### PCB EDITOR BASICS <a name="pcb_editor_basics"></a>
***main part of the editor:*** canvas  
***toolbar on left side:*** display options  
***toolbar on right side:*** designing the PCB  
***toolbar on far right side:*** appearance panel for changing visibility, colors, & opacity  
***toolbar on far bottom right side:*** selection filter, enables/disables PCB objects.   

### BOARD SETUP & STACKUP <a name="board_setup_and_stackup"></a>
* ***page setup:*** File -> Page Settings.  
* ***board setup:*** File -> Board Setup. Defines how PCB will be manufactured. 
    * ***stackup:*** what copper & dielectric layers PCB will have (& their thickness).
    * ***design rules:*** Sizes & spaceing for tracks and vias.
        * ***constraints:*** Specify overriding rules for everything in the board design.
        * ***net classes:*** Set of design rules associated w/ specific group of nets.  
        "Track width & spacing can be managed manually, but net classes are recommended b/c they provide an automatic way to manage & check design rules.

### IMPORTING CHANGES FROM SCHEMATIC <a name="importing_changes_from_schematic"></a>
***import design data from schematic into layout:*** Tools -> Update PCB from Schematic ***OR***    
F8 ***OR***  
button  
***note:*** Each time schematic is edited, designer must use *Update PCB from Schematic* button to keep schematic and layout in sync. 

### DRAWING A BOARD OUTLINE <a name="drawing_board_outline"></a>
Defined by drawing board outline on the layer Edge.Cuts layer.  
Useful to draw board outline w/ coarse grid (1mm) to get round numbers.  
1. On Appearance pane click on later *Edge.Cuts*.
2. On right toolbar, choose shape & draw surrounding footprints. Requirement is that outline is single closed shape that doesn't intersect itself.  

### PLACING FOOTPRINTS <a name="placing_footprints"></a>
* Some footprints have exact requirements for their locations: connectors, indicators, or buttons & switches.  
* Some components may need to be placed according to electrical considerations: bypass capacitors should be close to the power IC power pins and sensitive analog components should be far from digital interference.
* Almost all components have a courtyard which should not intersect.
* Otherwise, components should be positioned for ease of routing.

**ratsnet:** Thin lines indicating unrouted connections between pads.
1. Select component.
2. Move: Press *M*.  
Rotate: Press *R*.  
Flip to other side: Press *F*. Appears mirrored and pads changed color from red to blue.  

***note:*** All PCB layers viewed from front side of the board.  
***note:*** Each layer has unique color, shown by swatches of the Appearance panel.
Default: F.Cu (Front Copper) are red, B.Cu (Back Copper) are blue.

### ROUTING TRACKS <a name="routing_tracks"></a>
Now that components are placed, connect pads w/ copper traces.
1. Right side toolbar, select button *Route Tracks* ***or***  
Press *x*.  
2. Click on pad to begin trace and another pad to finish the trace.  

#### WAYS TO CONNECT TRACKS FRONT AND BACK <a name="ways_to_connect_tracks_front_and_back"></a>
1. through hole pads
2. via. Insert: Press X to start routing on layer B.Cu -> Press V and click halfway, layer is now on F.Cu -> Click the finishing copper pad.

### PLACING COPPER ZONES <a name="placing_copper_zones"></a>
***copper zones:*** Often used for ground & power connections b/c they provide a lower impedance connection than traces.  
To add GND zone to bottom layer:    
1. ***outline zone:*** switch to B.Cu layer -> right toolbar click *Add a filled zone* button -> Click on PCB to place 1st corner of the zone.  
2. *Copper Zone Properties* window opens up -> select GND & B.Cu -> Click Ok to place other zone corners -> complete double click on last zone.  
3. ***fill zone w/ copper:*** Edit -> *Fill All Zones* or Press B. 

***note:*** Copper added to the zones, but doesn't connect to VCC, pads, traces, & is clipped by board's edge.  
***note:*** Zones are not filled automatically when first drawn.  
***note:*** Zones can make it hard to see other other parts. They can be hidden except for their boundaries by selecting *Show only zone boundaries* button on left toolbar.  
Does overlap w/ ground trace & connects through GND pads through thin traces called **thermal reliefs**.  
**thermal reliefs:** Make pads easier to solder. Thermal reliefs & other zone settings can be modified in zone properties dialog.  

### DESIGN RULE CHECKING <a name="design_rule_checking"></a>
1. *Inspect* -> *Design Rules Checker* ***OR***  
Top toolbar, select *Design Rules Checker* button
***see design rules:*** File -> *Board Setup* -> *Design Rules* -> *Violation Severity*  

### 3D VIEWER <a name="3d_viewer"></a>
*View* -> *3D Viewer*  
***raytracing mode:*** Slower but offers more accurate rendering. *Preferences* -> *Raytracing*.  

### FABRICATION OUTPUTS <a name="fabrication_outputs"></a>
1. ***open plot dialog:*** *File* -> *Plot*. Plot design in several formats, but Gerber is right for ordering from a PCB fabricator.  
2. Necessary layers are checked: copper layers (*.Cu), board outline (Edge.Cuts), soldermask (*.Mask), & silkscreen (*.Silkscreen).  
3. Click *Plot* to generate Gerber files.  
4. Click *Generate Drill Files* -> click *Generate Drill Files* to create files specifying locatiion of all holes that will be drilled in the board.
5. Close Plot dialog.

## TUTORIAL PART 4: CUSTOM SYMBOLS & FOOTPRINTS <a name="tutorial_part_4"></a>

### LIBRARY & LIBRARY TABLE BASICS <a name="library_and_library_table_basics"></a>
Symbols & footprints are organized into libraries. Can hold symbols or footprints, but not both.  
KiCad keeps track of user's symbol & footprint libraries in the symbol & footprint library table. Each table is a list of library names & location on disk.  
There are global & project library tables.  
***viewing symbol library table:*** In Project Manager, Schematic or Symbol Editor windows, *Preferences* -> *Managed Symbol Libraries*.  
***viewing footprint library table:*** In Project Manager, Board or Footprint Editor windows, *Preferences* -> *Managed Footprint Libraries*.  

Paths to libraries are defined w/ path substitution variables enabling user to move all of their libraries to new location w/o modifing library tables.  
Only thing needed to change is to redefine the variable to point to the new location.  
***KiCad's path subsitution variables are edited:*** In Project Manager or any Editor window, *Preferences* -> *Configure Paths*.  
***on first run:*** KiCad prompts user to set up symbol & footprint library table. To go through this again, delete or rename table files.   
Location of table files depends on OS. Windows: `%APPDATA%\kicad\6.0\sym-lib-table` & `%APPDATA%\kicad\6.0\fp-lib-table`  

### CREATING NEW GLOBAL OR PROJECT LIBRARIES <a name="creating_new_global_or_project_libraries"></a>
1. Choose library.  
2. Open Symbol Editor from Project Manager -> *File* -> *New Library* -> *Project* or *Global* -> choose name for new library & save it in project directory. 

### CREATING NEW SYMBOLS <a name="creating_new_symbols"></a>
1. Select library -> *File* -> *New Symbol*. *New Symbol* window opens up.  
2. Fill at least name & reference designator. Click *Ok*. Symbol now appears under the library selected.  

***note:*** In canvas, cross indicates center of the footprint.  

#### SYMBOL PINS <a name="symbol_pins"></a>
1. Draw symbol by adding a pin, right toolbar *Add a pin* button -> click on canvas -> Pin Properties dialog appears.
2. Add 2nd pin, press *Insert*. New pin numbered 3 id added to symbol.
3. Can add graphical features. Switch to finer grid, right click on grid -> Grid -> select smaller grid. Add grahpical shapes.

***note:*** Small grids useful for adding graphical features, but symbol pins **must always be placed on a 50 mil (1.27mm) grid. Pins not aligned like this will not be able to connect wires in schematic.

#### SYMBOL PROPERTIES <a name="symbol_properties"></a>
1. *File* -> *Symbol Properites* ***or***  
Double clicking canvas.  

### CREATING NEW FOOTPRINTS <a name="creating_new_footprints"></a>
1. Open Footprint Editor. Create new footprint library. *File* -> *New Library* -> *Project* or *Global*.
2. Select new library in libraries pane. Create footprint, *File* -> *New Footprint*.  

#### FOOTPRINT PADS <a name="footprint_pads"></a>
1. Change grid to appropriate size. *View* -> *Grid Properties* -> change *User Defined Grid sizes*. Switch to the new grid size.
2. Click *Add a pad* button -> place pad -> press ESC. Double click on pad to edit properties. Here, can edit pad & hole shape.  

***note:*** By convention, through-hole footprints have pin 1 located at (0,0) & oriented w/ pin 1 in top left.  
***note:*** By default, KiCad's default coordinate system has positive Y-axis oriented downwards.
***note:*** Shortcut, right click on pad, click *Push Pad Properites to Other Pads* -> click *Change Pads on Current Footprint*.  

#### FOOTPRINT GRAPHICS <a name="footprint_graphics"></a>
Good footprint will have exact part outline drawn on fabrication layer (F.Fab), slightly larger outline on the silkcreen layer (F.Silkscreen), and a courtyard (F.Courtyard) surrounding entire footprint to prevent overlaps w/ other footprints.  
1. Switch to *F.Fab*. Fabrication outline should precisely match phyiscal dimensions of the part. Use *Grid Properties*. Draw shape. 
2. Switch to *F.Silkscreen*. Ex: Outline be outside of the part outline by 0.11mm.  
3. Switch to *F.Courtyard*. Ex: Ouline should be 0.25mm cleareance. 

***note:*** Outline can either set the User Grid or draw rectangle & pick its coordinates.  

#### KICAD LIBRARY CONVENTIONS <a name="kicad_library_conventions"></a>
[Library Conventions | KiCad EDA](https://klc.kicad.org/)  

#### ADD SWITCH TO SCHEMATIC <a name="add_switch_to_schematic"></a>
1. Go back to symbol editor & open symbol -> Edit *Symbol Properties*. 
2. In *General* tab, Click in *Footprint field -> click library icon that appears -> Browse to project library library & double click on switch footprint -> Save the symbol.  
Switch footprint now assigned to this symbol by default. Footprint does not need to be manually selected each time symbol is added to a schematic.  
3. Open schematic -> add new symbol -> select & place new symbol.

#### ADD SWITCH TO LAYOUT <a name="add_switch_to_layout"></a>
1. Save schematic -> Open Board Editor -> Add symbol to layout. 
2. Update PCB w/ schematic changes using *Tools* -> *Update PCB from Schematic*. Place switch footprint onto board.
3. Delete unwanted traces. Select trace -> press U several times to expand selection to include all segments -> press Delete.
4. Re route new traces.
5. Press B to fill the zones.
6. Run DRC.

### LINKING SYMBOLS, FOOTPRINTS, & 3D MODELS <a name="linking_symbols_footprints_and_3d_models"></a>

#### SYMBOLS & FOOTPRINTS <a name="symbols_and_footprints"></a>
Each symbol in schematic needs a footprint assigned to it during schematic entry process.  
Name of footprint assigned to each symbol stored in *Footprint* field of the symbol's properties.

#### FOOTPRINTS & 3D MODELS <a name="footprints_and_3d_models"></a>
3D component models are stored in separate files.  
Filenames for the component's 3D model(s) are saved in the footprint. 
3D model filenames, along w/ model scale, rotation, offset, & opacity are set in the *3D Models* tab of the Footprint Properties.  
Both STEP(.step) & VRML(.wrl) 3D model formats are supported. 
***STEP files:*** Useful where dimensional accuraacy is needed.  
***VRML files:*** Useful for visually attractive renders.

[FreeCAD](https://www.freecadweb.org/) & [StepUp Workbench](https://github.com/easyw/kicadStepUpMod/) useful for creating component 3D models.  
