# NEATENING SCHEMATIC
*[KiCad Tutorial - How to neaten up your schematic!](https://www.youtube.com/watch?v=S24RvYZWYUQ)*    

* Do not cross wires. Especially power & ground pins. Connect them by name.  
Ex. Do not connect all ground pins to one ground symbol. Each ground pin have own ground symbol.  

* Decoupling caps. Put them on the side of the power. Make sure they're close as possible to the IC that your tring to decouple.

* Layout. Place all the components with the same unit. Put some text and borders.

* Make use of *Hierarchy Sheets*.

*[KiCad Tutorial - How to re-use schematics in new projects](https://www.youtube.com/watch?v=vKr8Q_xCBUU)*  

* ***hierarchy sheets:*** Building blocks, schematic sheet where you can take existing schematics you already had in different projects & link them to current project.  
***pros:*** Don't have to redraw the circuit. Don't have to put it all in one sheet.

* ***leave the sheet:*** *View* -> *Leave Sheet*.

* ***hierarchy labels:*** Global labels that go to a different sheet. In contrast to regular labels or local labels. 

* ***place hierarchical pin:*** Place my pin to the outside world. 

1. Click *Hierarchical Sheet*. *Hierarchical Sheet Properties* window opens up. *File name* is sheet wanted to be linked.
2. Double click goes to schematic.

