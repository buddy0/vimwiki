# SAVING MONEY - [link](https://www.youtube.com/watch?v=NskNfAlD3wU)

1. RETIREMENT FUND
    * 401K: 5% - 15%.

2. CHECKING ACCOUNT
    * Paycheck goes into checking account. 

3. NECESSITIES
    * Needs vs wants: Food, shelterm transportation, healthcare, utilities.

4. EMERGENCY FUND
    * Liquid, easily Accessible: $2,500+ (maybe in a separate checking account)

5. DEBT PAYOFF
    * Avalanche method: tackle the highest interest rate first (better logically).
    * Snowball method: tackle the lowest balance of debt first.

6. 4-8 MONTHS PAYROLL
    * Online savings -> 2.4% APR to keep up with inflation.

7. INVEST
    * Real estate, stock market, bonds, crypto, etc. 
