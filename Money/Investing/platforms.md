# INVESTING PLATFORMS

## BEST NEW INVESTING APPS FOR 2020 - [link](https://www.youtube.com/watch?v=iJUPa7ssN2k&t=0s)  
* ***popular platforms:*** Vanguard, M1 Vinance, E-Trade, Robinhood, Wealthsimple, Fidelity, Wealthfront, Public, Charles Schwab, Stash, Fundrise, Acorns, Stockpile, Betterment, Webull. 

* ***new kids on the block:*** M1 Finance, 

### M1 Finance 
* Great for investors w/ a longer term outlook. 
* Not for short term investing.  
* Buy & hold.  
* Beginner, Intermediate, Advanced
* Free investing platform.  
* Investment minimums: $100 taxable account, $500 retirement, $10 min. per trade. 
* Fractional Share Investing: Buy a fraction of any share on the platform.  
* Allows you to reinvest your dividends.  
* downside: Not the platform for purchasing full individula stocks & 1 trading window per day, 10am EST (after 10am then they won't process your trade until next business day).  
* Invest w/ pie investing. Pick as many stocks, ETFs, & other equities and place them into a pie. Once in a pie, able to assign a certain % to each one. Then, every time money is invested, dollars will be spread among each individual equity. 

