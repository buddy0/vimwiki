# INDEX FUNDS

## [Index Fund Investing Millionaire (For Beginners) - Vanguard Index Funds](https://www.youtube.com/watch?v=lZthyEmk6xE&t=0s)  

* **index fund:** Way for you to invest in hundreds or thousands of companies by purchasing one share of that index fund. 
** different types of index funds: real estate, international, total world, bond (BND), Vanguard S&P 500 (VXUS) (stocks of the 500 largest companies in the US), Vanguard Toal Stock Market (VTI, holds over 3,500 different stocks raning from small, mid, to large companies). 
* Most index funds are fully invested in stocks (unless it's a bond index). 

### HOW TO INVEST IN AN INDEX FUND
1. Choose investing platform.  
2. Type of invetsment account: IRA, 401k, 403b, taxable investment account. 

### ADVANTAGES
* Passive management. Don't have to  research on each individual company publicly trafed on the stock market. 
* % of company held w/in an index fund is already predetermined for you. All you do is deposit your money into your brokerage account, purchase how many shares you want, & busy work of buying each individual company is all handled for you. 
* Effective. 
* Low cost/fees.  
* Easy & difficult to screw up


**actively managed mutual fund:** Fund that holds bunch of different stocks, kind of like an index fund. Difference is that mutual fund has fund manager that is actively picking which stocks to buy, sell, how often, & how much they're spending on each stock. Index fund also has a fund manager, but his role is more hands off. Decisions are already determined based on the index that index funds attracts.  