# HSA 

**Health Savings Account (HSA):** [definition](https://www.healthcare.gov/glossary/health-savings-account-hsa/)  
**Qualified Medical Expenses (QME):** [link](https://healthequity.com/qme) 

## [HSA Explained (THE ULTIMATE TAX LOOPHOLE!)](https://www.youtube.com/watch?v=Tt9uKo8ycjI)
* Investment account goes hand in hand w/ health insurance. Not a subsitute for health insurance, but goes w/ your health insurance.  
* Anyone with a HSA-qualified plan can open a HSA. HSA-qualified plans are generally ones w/ *high deductible* (HD HPs). 
* Since health insurance can be exepensive, many people HD health plans b/c they have affordable monthly premiums, but higher out of pocket expensives. Thus, HSAs are meant to help w/ those out of pocket expensives.  
* All contributions, investment growth, & withdrawals are *tax-free*. More tax benefits than an IRA b/c you have either have to pay taxes on money you contribute or on withdrawal. 
* Use HSA like a checking account to pay for health related expenses. 
* Some employees match money going into HSA. 

## [The TRUTH About an HSA For Financial Independence - Health Savings Account Investing](https://www.youtube.com/watch?v=IHOjkJdBVdA)
* Look at an HSA as a *Health Investment Account* to be treated like an additional retirement account. Think about an HSA as a way to start preparing now for medical expenses when you're 60. 
* 3 things you can do w/ money in an HSA account. 
1. Let money sit in the account & slowly lsoe value due to inflation. 
2. Spend the money on medical expenses today or near future. 
3. Invest money w/in HSA account & let it sit there. Try to pay medical expenses out of pocket while the money in HSA account can still be invested for as long as possible. 
* Triple tax savings:
1. Money you contribute to an HSA is pre-tax money from your paycheck.  
2. Gains from your investement growth, dividend payouts, & interest accumulation is *not* taxed at all.  
3. Withdrawing money for medical expenses, no payment on any taxes. 
* ROTH IRA on steroids. 
* Jarrad charges all of his medical expenses to a good rewards credit card & paying it off at end of every single month. Since he'll be spending money on these medical expenses out of pocket anyway, might as well charge it instead of paying cash so he can take advantage of his 2% cash back rewards. 
* Ways to open an HSA: but first need to be at least 18 years old & covered by a HDHP
1. Employer Sponsored HSA. If employer offers HDHP, then they may or may not offer you an HSA account as well. If they do, then they've already chosen the third-party company for you, so they should provid a link to open up an account. If empployer does *not* provide an HSA company for you or you get health insurance on your own, then you need to open an HSA on your own. This means you have to deposit money into the account on your own. 
* 3 common HSA providers where you can open an HSA account: 
1. HealthEquity. Monthl 0.03% adminstration fee. 
2. Fidelity. 0 fees but their user interface if bad. 
3. Lively. 0 fees and user interface is good.  
But, no matter who HSA provider is that your employer chooses, you have full control to roll that account into a different HSA company.   
* Jarrad personaly invests his HSA money into low-cost index funds. 
* Not tied to yoir employer, if you leave you still have access to that account through that third-party HSA compnay that you use. 
* You can withdraw money for nonmedical expenses, but it does come w/ a price; than those funds will be taxed as ordinary income & IRS will impose a 20% penalty as well.
* After you reach age of 65 or become disabled, withdrawal from HSA funds results in no penalty, but the amount will still be taxed as ordinary income. 
* **Ex.** Amount of tax-free money I can accumulate over time, if I maxed out my HSA at $3,600 per year as a single person over 30 years then at a 7% return, my account would grow to $340,000. 
