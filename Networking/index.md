# Networking

## What a Network Engineer does - [link](https://www.youtube.com/watch?v=y_CubB8lPJ0&t=353s)
![network map](map.png)  

* Upper part: Computers are connected to switches. Help desk technicians work here helping end users fix issues.

* Middle part: In charge of the network, make sure the network is running all the time. Devices that are worked on are:
    * switch: Connects devices within a network (often a local area network or LAN) and forwards data packets between devices. Only sends data to a single device. [link](https://www.cloudflare.com/learning/network-layer/what-is-a-network-switch/)  
      * topics: VLANs, Trucking, Security, different models  
![switch picture](switch.png)  
    * router: Forwards data packets between computer networks (LANS, wide area networks or WANs, or autonomous systems). Perform the traffic directing functions on the Internet.  
        * protocols: OSPF, EIGRP, RIP
        * models: 1800s, 2800s, 3800s
Therefore, switch is used for interconnecting devices while a router is necessary for Internet connection. Both switches and routers have interconnected operating system or IOS.
* Outer part: BGP and MPLS. DNS/DHCP, Infoblocks, firewalls  
* Lower part: Layer 3 switches, multi-layer switch, acts like a switch and router combined.  
## OSI Model - [link](https://www.cloudflare.com/learning/ddos/glossary/open-systems-interconnection-model-osi/)  
The open systems interconnection (OSI) model enables diverse communication systems to communicate using standard protocols. OSI provides a standard for different computer systems to communicate with each other. Useful for narrowing down a problem to a specific layer.   
![OSI Model diagram](osimodel.png)  
**Example** 
Layer 2 switch operates at the data link layer which forward data based on the destination MAC address.  
Layer 3 switch operates at the network layer which forward data based on the destination IP address.  

## Addresses
IP address: Act as a mailing address enabling internet communications directed at that address to reach that device. All IP packets include their source and destination IP addresses in their headers. These addresses often change due to limited IPv4 addresses.  

MAC address: Permanent identifier for each piece of HW, like a serial number. Not part of Internet traffic. Only used inside a given network. Thus, network switches refer to MAC addresses to send Internet traffic to the right devices.  

How do network switches know the MAC addresses of the devices in their network?
* Layer 2 network switches maintain a table called Content Addressable Memory (CAM) that matches MAC addresses to the switch's Ethernet ports.  
